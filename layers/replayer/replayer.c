/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 * \file   replayer.c
 * \author Diogo
 * \date   Sat Oct 15 18:26:07 CEST 2011
 * \brief  Logging and replay funcionalities.
 *
 *
 * @addtogroup core
 * @section replayer_sec Replayer
 * Replayer is a layer between an algorithm and the core.
 * It runs in two modes:
 * <ul>
 *  <li>LOG mode where it forwards all requests between algorithm and core and stores
 *  those values in a binary file.</li>
 *  <li>REPLAY mode which simulates a running core by returning the logged events from the binary file. This way a deterministic algorithm can be verified.</li>
 * </ul>
 *
 * The modes of the replayer is controlled by environment variables:
 * <ul>
 *   <li>when REPFILE is set to point to a file, the replayer starts in replay mode.</li>
 *   <li>when LOGFILE is set to point to a file, the replayer starts in log mode.</li>
 * </ul>
 */

#define _ZTAS_CORE
#include <ztas/proc/cfg.h>
#include <ztas/types.h>
#include <ztas/ztime.h>
#include <ztas/layer/layer.h>

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
//#include "cpu.h"

/* algorithm interface */
extern void* up_init(int32_t pid);
extern void up_fini(void* state);
extern void up_recv(void* state, const void* data, ztas_size_t size);
extern void up_trig(void* state, int32_t aid);

/* framework internal interface */
void* do_init(int32_t pid);
void do_fini(void* state);
void do_recv(void* state, const void* data, ztas_size_t size);
void do_trig(void* state, int32_t aid);
extern int do_ucast(int32_t dst, const void* data, ztas_size_t size, int flags);
extern int do_bcast(const void *data, ztas_size_t size, int flags);
extern int do_alarm(int32_t aid, uint32_t ms);
extern int do_sched(int32_t aid, const ztime_t* at);
extern void do_term(void);
extern void do_clock(ztime_t* ts);
extern const char* do_cread(const char* key);

/* this component implements

 for the framework calls:
 void* __ztas_init(int32_t pid);
 void  __ztas_fini(void* state);
 void  __ztas_recv(void* state, const void* data, size_t size);
 void  __ztas_trig(void* state, int32_t aid);

 for the algorithm calls:
 int  ztas_ucast(int32_t dst, const void* data, ztas_size_t size);
 int  ztas_bcast(const void *data, ztas_size_t size);
 int  ztas_alarm(int32_t aid, uint32_t ms);
 int  ztas_sched(int32_t aid, const hctime_t* at);
 void ztas_term(void);
 void ztas_clock(ztime_t* ts);
 */

#ifdef REPLAYER
static int relax_size = 1;
#endif

// *** replayer type definitions ***

#define CPU_ALARM  1234
#define CPU_PERIOD 1000

typedef struct {
    int logmode;
    FILE* tracefp;
    void* state;
    cfg_t *cfg;
} replayer_t;

typedef enum {
    INIT, FINI, RECV, TRIG, TERM, UCAST, BCAST, ALARM, SCHED, CLOCK, RETCODE
} event_t;

typedef struct {
    event_t type;
    ztas_size_t size;
} event_log_t;

typedef struct {
    int flags;
    int32_t dst;
    ztas_size_t size;
    char data[0];
} event_ucast_t;

typedef struct {
    int flags;
    ztas_size_t size;
    char data[0];
} event_bcast_t;

typedef struct {
    int32_t aid;
    uint32_t ms;
} event_alarm_t;

typedef struct {
    int32_t aid;
    ztime_t at;
} event_sched_t;

// global state of the replayer (in case it runs)
static __thread replayer_t* replayer = NULL;

/** \brief helper method to get the string type of a log event */
const char* get_type(event_t type) {
    switch (type) {
    case INIT:
        return "INIT";
    case FINI:
        return "FINI";
    case RECV:
        return "RECV";
    case TRIG:
        return "TRIG";
    case TERM:
        return "TERM";
    case CLOCK:
        return "CLOCK";
    case ALARM:
        return "ALARM";
    case SCHED:
        return "SCHED";
    case UCAST:
        return "UCAST";
    case BCAST:
        return "BCAST";
    default:
        fprintf(stderr, "no event type: %d\n", type);
        assert(0);
        return 0;
    }
}

/**
 * \brief Log an event in file
 */
static void
logit(replayer_t* replayer, event_t event, const void* data, ztas_size_t size) {
    event_log_t e;
    e.type = event;
    e.size = size;
    ztas_size_t ret = fwrite(&e, 1, sizeof(event_log_t), replayer->tracefp);
    assert(sizeof(event_log_t) == ret && "REPLAYER: error writing to log");

    ret = fwrite(data, 1, size, replayer->tracefp);
    assert(size == ret && "REPLAYER: error writing to log");
    fflush(replayer->tracefp);

}

#ifndef REPLAYER

static int
log_return(replayer_t* replayer, int returnValue) {
    logit(replayer, RETCODE, &returnValue, sizeof(int));
    return returnValue;
}

#endif

#ifdef REPLAYER
static void*
replay_next(replayer_t* replayer, event_log_t* e) {
    ztas_size_t ret = fread(e, 1, sizeof(event_log_t), replayer->tracefp);

    if (e->size > 0) {
        void* data = malloc(e->size);
        ret = fread(data, 1, e->size, replayer->tracefp);
        if (ret != e->size) {
            fprintf(stderr, "Error while reading replay file: %u/%u\n", ret, e->size);
            return NULL;
        }
        return data;
    }
    return NULL;
}

/**
 * reads a return code event from the log and returns a copy of the value.
 */
static int
read_next_return(replayer_t *replayer) {
    int result;
    event_log_t e;
    int* data = replay_next(replayer, &e);
    assert(e.type == RETCODE && "REPLAYER: expected return code log");
    assert(e.size == sizeof(int) && "REPLAYER: wrong size for return code log");

    // copy and free so we don't have shared pointers.
    result = *data;
    free(data);
    return result;
}
#endif

/* this method won't be called when in replay mode. */
#ifndef REPLAYER
// *** call from framework to algorithm ***
void*
do_init(int32_t pid) {

    char buf[128];
    sprintf(buf, "p%d:trace", pid);
    const char* tracefile = __ztas_cread(buf);

    replayer_t* localReplayer = (replayer_t*) malloc(sizeof(replayer_t));
    replayer = localReplayer;
    assert(replayer && "REPLAYER: replayer allocation problem");
    replayer->logmode = 0;
    if (tracefile == NULL) {
        replayer->state = up_init(pid);
    } else {
        printf("Replayer logfile: %s\n", tracefile);
        replayer->logmode = 1;
        replayer->tracefp = fopen(tracefile, "w+");
        assert(replayer->tracefp && "REPLAYER: could not open log file");
        logit(replayer, INIT, (const void*) &pid, sizeof(int32_t));

        replayer->state = up_init(pid);
    }

    // set global to 0 again, return the local copy
    replayer = NULL;
    return localReplayer;
}
#endif

void
do_fini(void* state) {
    assert(state);
    replayer = (replayer_t*) state;

    if (replayer->logmode) {
        logit(replayer, FINI, NULL, 0);
        up_fini(replayer->state);
        // if the mode is not 0, we assume a file is being used.
        fclose(replayer->tracefp);
        replayer->tracefp = 0;
    } else {
        up_fini(replayer->state);
    }

    replayer = NULL;
}

void
do_recv(void* state, const void* data, ztas_size_t size) {
    assert(state);
    replayer = (replayer_t*) state;

    ztas_size_t zsize = (ztas_size_t) size;
    assert((size_t) zsize == size && "REPLAYER: size cannot be represented in ztas_size_t");

    if (replayer->logmode) {
        logit(replayer, RECV, data, size);
    }
    up_recv(replayer->state, data, size);

    replayer = NULL;
}

void
do_trig(void* state, int32_t aid) {
    assert(state);
    replayer = (replayer_t*) state;

    if (replayer->logmode) {
        logit(replayer, TRIG, (const void*) &aid, sizeof(int32_t));
    }
    up_trig(replayer->state, aid);

    replayer = NULL;
}

/* dispatch calls from algorithm to framework */
void
up_term(void) {
    assert(replayer && "missing global replayer pointer");

#ifdef REPLAYER
    event_log_t e;
    event_ucast_t* le = replay_next(replayer, &e);
    assert(le == NULL && "REPLAYER: no data on replay_next() call");
    assert(e.size == 0 && "REPLAYER: term event has size");
#else
    if (replayer->logmode) {
        logit(replayer, TERM, NULL, 0);
    }
    do_term();
#endif
}

int
up_ucast(int32_t dst, const void* data, ztas_size_t size, int flags) {
    assert(replayer && "missing global replayer pointer");

#ifdef REPLAYER
    event_log_t e;
    event_ucast_t* le = replay_next(replayer, &e);
    assert(le && "REPLAYER: no data on replay_next() call");
    assert(e.size > sizeof(event_ucast_t) && "REPLAYER: wrong event size");

    size_t ssize = size < le->size ? size : le->size;
    assert(memcmp(le->data, data, ssize) == 0 && "REPLAYER: (ERROR) wrong ucast content");

    if (relax_size) {
        assert(size >= le->size && "REPLAYER: (ERROR) wrong ucast size");
    } else {
        assert(size == le->size && "REPLAYER: (ERROR) wrong ucast size");
    }

    assert(dst == le->dst && "REPLAYER: (ERROR) wrong destination id");

    assert(flags == le->flags && "REPLAYER: (ERROR) wrong ucast flags");

    free(le);
    return read_next_return(replayer);
#else
    if (replayer->logmode) {
        event_ucast_t* e = (event_ucast_t*) malloc(sizeof(event_ucast_t) + size);
        e->flags = flags;
        e->dst = dst;
        e->size = size;
        memcpy(e->data, data, size);
        logit(replayer, UCAST, e, sizeof(event_ucast_t) + size);
        free(e);
        return log_return(replayer, do_ucast(dst, data, size, flags));
    } else {
        return do_ucast(dst, data, size, flags);
    }
#endif
}

int
up_bcast(const void* data, ztas_size_t size, int flags) {
    assert(replayer && "missing global replayer pointer");

#ifdef REPLAYER
    event_log_t e;
    event_bcast_t* le = replay_next(replayer, &e);
    assert(le && "REPLAYER: no data on replay_next() call");
    assert(e.size > sizeof(event_bcast_t) && "REPLAYER: wrong event size");

    size_t ssize = size < le->size ? size : le->size;
    assert(memcmp(le->data, data, ssize) == 0 && "REPLAYER: (ERROR) wrong bcast content");

    if (relax_size) {
        assert(size >= le->size && "REPLAYER: (ERROR) wrong bcast size");
    } else {
        assert(size == le->size && "REPLAYER: (ERROR) wrong bcast size");
    }
    assert(flags == le->flags && "REPLAYER: (ERROR) wrong bcast flags");

    free(le);
    return read_next_return(replayer);
#else
    if (replayer->logmode) {
        event_bcast_t* e = (event_bcast_t*) malloc(sizeof(event_bcast_t) + size);
        e->flags = flags;
        e->size = size;
        memcpy(e->data, data, size);
        logit(replayer, BCAST, e, sizeof(event_bcast_t) + size);
        free(e);
        return log_return(replayer, do_bcast(data, size, flags));
    } else {
        return do_bcast(data, size, flags);
    }
#endif
}

int
up_alarm(int32_t aid, uint32_t ms) {
    assert(replayer && "missing global replayer pointer");

#ifdef REPLAYER
    event_log_t e;
    event_alarm_t* le = replay_next(replayer, &e);
    assert(le && "REPLAYER: no data on replay_next() call");
    assert(e.size == sizeof(event_alarm_t) && "REPLAYER: wrong event size");
    assert(aid == le->aid && "REPLAYER: (ERROR) wrong alarm id");
    assert(ms == le->ms && "REPLAYER: (ERROR) wrong alarm time");
    free(le);
    return read_next_return(replayer);
#else
    if (replayer->logmode) {

        event_alarm_t e;
        e.aid = aid;
        e.ms = ms;
        logit(replayer, ALARM, (const void*) &e, sizeof(event_alarm_t));
        return log_return(replayer, do_alarm(aid, ms));
    } else {
        return do_alarm(aid, ms);
    }
#endif

}

int
up_sched(int32_t aid, const ztime_t* at) {
    assert(replayer && "missing global replayer pointer");
#ifdef REPLAYER
    event_log_t e;
    event_sched_t* le = replay_next(replayer, &e);
    assert(le && "REPLAYER: no data on replay_next() call");
    assert(e.size == sizeof(event_sched_t) && "REPLAYER: wrong event size");
    assert(aid == le->aid && "REPLAYER: (ERROR) wrong sched id");
    assert(ZTIME_EQ(*at, le->at) && "REPLAYER: (ERROR) wrong sched time");
    free(le);
    return read_next_return(replayer);
#else
    if (replayer->logmode) {
        event_sched_t e;
        e.aid = aid;
        ZTIME_SET(e.at, *at);
        logit(replayer, SCHED, (const void*) &e, sizeof(event_sched_t));
        return log_return(replayer, do_sched(aid, at));
    } else {
        return do_sched(aid, at);
    }
#endif
}

void
up_clock(ztime_t* ts) {
    assert(replayer && "missing global replayer pointer");

#ifdef REPLAYER
    event_log_t e;
    ztime_t* le = replay_next(replayer, &e);
    assert(le && "REPLAYER: no data on replay_next() call");
    assert(e.size == sizeof(ztime_t) && "REPLAYER: wrong event size");
    ZTIME_SET(*ts, *le);
    free(le);
#else
    do_clock(ts);
    if (replayer->logmode) {
        logit(replayer, CLOCK, (const void*) ts, sizeof(ztime_t));
    }
#endif
}

const char*
up_cread(const char* key) {
    assert(replayer && "missing global replayer pointer");
#ifdef REPLAYER
    return cfg_get(replayer->cfg, key);
#endif
    return do_cread(key);
}

#ifdef REPLAYER

static void
replayit(replayer_t* replayer) {
    while (1) {
        event_log_t e;
        void* data = replay_next(replayer, &e);
        if (!data)
        break;

        switch (e.type) {
            case INIT:
            assert(0 && "REPLAYER: INIT twice");
            break;
            case FINI:
            do_fini(replayer);
            break;
            case RECV:
            do_recv(replayer, data, e.size);
            break;
            case TRIG:
            do_trig(replayer, *(int32_t*) data);
            break;
            default:
            assert(0 && "REPLAYER: handling event type not implemented");
        }
        free(data);
    }
}

int
main(int argc, char**argv) {
    cfg_t *cfg = cfg_parse(argv[2]);
    int pid = atoi(argv[1]);

    char buf[128];
    sprintf(buf, "p%d:trace", pid);
    const char* repfile = cfg_get(cfg, buf);

    assert(repfile && "need a trace file for replay.");

    replayer = (replayer_t*) malloc(sizeof(replayer_t));
    assert(replayer && "REPLAYER: replayer allocation problem");

    replayer->cfg = cfg;

    printf("Replayer started\n");
    printf("--> pid       : %d\n", pid);
    printf("--> trace file: %s\n", repfile);
    replayer->logmode = 0;
    replayer->tracefp = fopen(repfile, "r");
    assert(replayer->tracefp && "REPLAYER: could not open replay file");

    event_log_t e;
    void* data = replay_next(replayer, &e);
    assert(data && "REPLAYER: Error reading trace file, cannot init process");
    assert(e.type == INIT && "REPLAYER: expected type INIT");
    int32_t newPid = *(int32_t*) data;
    if(newPid != pid) {
        printf("--> WARNING: PID (%d) of trace file differs from the one given as argument (%d).\n", newPid, pid);
    }

    // note we're calling the UP-init here,
    // this won't call the replayer's do_init!
    replayer->state = up_init(pid);

    replayit(replayer);
    return 0;
}

#endif
