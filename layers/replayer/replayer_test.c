/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 * logger/replaeyr test.
 * compile with -DREPLAYER to test the replayer,
 * without, the logger is tested.
 * The logger test has to be executed first to have a trace.
 *
 */

#include <ztas.h>
#include <stdio.h>
#include <stdlib.h>

#define LAYERU
#define LAYERD __

#include <assert.h>

#include "replayer.c"

/// TEST values ///
#define ALARM_ID 332
#define ALARM_TIME 100
#define PID 4
#define SCHED_ID 444
#define SCHED_TIME 101

static int algorithmState = 12345;
static int recvData = 0; // data not important, we'll compare the address
static int32_t ucastDest = 22;
static int ucastData = 0; // data not important, we'll compare the address
static int bcastData = 0; // data not important, we'll compare the address
ztime_t schedTime;

struct CallCounter {
    int alarm;
    int ucast;
    int bcast;
    int sched;
    int trig;
    int fini;
    int recv;
    int clock;
};

struct CallCounter counter = { 0, 0, 0, 0, 0, 0, 0, 0 };
int alarmCalled = 0;
int upCallCounter = 0;

ztime_t clocktime;

///// ALGORITHM MOCK /////
void*
ztas_init(int32_t pid) {

    ZTIME_ZERO(schedTime);
    ZTIME_ZERO(clocktime);

    assert(pid == PID && "invalid pid");
    int alarmRet = ztas_alarm(ALARM_ID, ALARM_TIME);
    int ucastRet = ztas_ucast(ucastDest, &ucastData, sizeof(ucastData), 0);
    int bcastRet = ztas_bcast(&bcastData, sizeof(bcastData), 0);
    int schedRet = ztas_sched(SCHED_ID, &schedTime);
    ztas_clock(&clocktime);

    // assert the return values are correct (and replayed)
    assert(alarmRet == 2 && "invalid alarm return value");
    assert(ucastRet == 0 && "invalid ucast return value");
    assert(bcastRet == 1 && "invalid bcast return value");
    assert(schedRet == 4 && "invalid sched return value");

    return &algorithmState;
}

void
ztas_fini(void* state) {
    assert(state == &algorithmState && "invalid state");
    ++counter.fini;
}

void
ztas_recv(void* state, const void* data, uint32_t size) {
    assert(state == &algorithmState && "invalid state");

    // can't compare the pointers since the replayer will copy the value.
    assert(*(int*)data == recvData && "invalid data");
    assert(size == sizeof(recvData) && "invalid data size");
    ++counter.recv;
}

void
ztas_trig(void* state, int32_t aid) {
    assert(state == &algorithmState && "invalid state");
    assert(aid == ALARM_ID && "invalid aid");
    ++counter.trig;
}

//// FRAMEWORK MOCK //////

int
__ztas_ucast(int32_t dst, const void* data, size_t size, int flags) {
    assert(dst == ucastDest && "invalid ucast dest");
    assert(data == &ucastData && "invalid ucast data");
    assert(size == sizeof(ucastData) && "invalid ucast data size");
    ++counter.ucast;
    return 0;
}

int
__ztas_bcast(const void *data, size_t size, int flags) {
    assert(data == &bcastData && "invalid bcast data");
    assert(size == sizeof(bcastData) && "invalid bcast data");
    ++counter.bcast;
    return 1;
}

int
__ztas_alarm(int32_t aid, uint32_t ms) {
    assert(aid == ALARM_ID && "invalid aid");
    assert(ms == ALARM_TIME && "invalid alarm time");
    ++counter.alarm;
    return 2;
}

int
__ztas_sched(int32_t aid, const ztime_t* at) {
    assert(aid == SCHED_ID && "invalid sched id");
    assert(at == &schedTime && "invalid time");
    ++counter.sched;
    return 4;
}

void
__ztas_clock(ztime_t* ts) {
    assert(ts == &clocktime && "invalid clock pointer");
    ++counter.clock;
}

void
__ztas_term(void) {
}

const char*
__ztas_cread(const char* key) {
    return "build/layers/replayer/tracefile.log";
}

// enable this main in logger-mode only.
// if in replayer mode, the replayer comes with its own main.
#ifndef REPLAYER

int
main(const int argc, const char* argv[])
{
    void* state = __ztas_init(PID);
    __ztas_trig(state, ALARM_ID);
    __ztas_recv(state, &recvData, sizeof(recvData));
    __ztas_fini(state);

    assert(counter.alarm==1 && "alarm counter");
    assert(counter.bcast==1 && "bcast counter");
    assert(counter.fini==1 && "fini counter");
    assert(counter.recv==1 && "recv counter");
    assert(counter.sched==1 && "sched counter");
    assert(counter.trig==1 && "trig counter");
    assert(counter.ucast==1 && "ucast counter");
    assert(counter.clock==1 && "clock counter");

    return 0;
}

#endif
