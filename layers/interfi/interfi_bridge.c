/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#define _ZTAS_CORE

#include <stdio.h>
#include <stdlib.h>
#include <ztas/types.h>
#include <ztas/ztime.h>
#include <ztas/layer/layer.h>
#include "interfi/embeddableInterfi.h"

/* framework internal interface */
void* do_init(int32_t pid);
void do_fini(void* state);
void do_recv(void* state, const void* data, ztas_size_t size);
void do_trig(void* state, int32_t aid);
extern int do_ucast(int32_t dst, const void* data, ztas_size_t size, int flags);
extern int do_bcast(const void *data, ztas_size_t size, int flags);
extern int do_alarm(int32_t aid, uint32_t ms);
extern int do_sched(int32_t aid, const ztime_t* at);
extern void do_term();
extern void do_clock(ztime_t* ts);
extern const char* do_cread(char* key);

typedef struct {
    void* interpreter;
    void* state;
} StateWrapper;

/**
 * registers ztas_*-methods to the interpreter so the interpreted
 * algorithm can call those methods.
 */
static void __register_ztas_interface();
static const char* __cread_binary_name(uint32_t pid);
static const char* __cread_interpreter_arguments(uint32_t pid);
/**
 * __ztas_init --> ztas_init
 */
void* do_init(int32_t pid) {
    __register_ztas_interface();

    void *interpreter = interfi_initialize(__cread_binary_name(pid), __cread_interpreter_arguments(pid));
    if (!interpreter) {
        printf("Error initializing interfi. Aborting.\n");
        abort();
    }

    CArgument args[] = { { .type = 0, .value.int32Val = pid } };
    void* state = (void*) interfi_run_function(interpreter, "ztas_init", 1, args);

    StateWrapper *bridgeState = (StateWrapper *) malloc(sizeof(StateWrapper));
    bridgeState->interpreter = interpreter;
    bridgeState->state = state;

    return bridgeState;

}

/**
 * __ztas_fini --> ztas_fini
 */
void do_fini(void* state) {
    StateWrapper* bridgeState = (StateWrapper*) state;
    CArgument args[] = { { .type = 1, .value.pointer = bridgeState->state } };
    interfi_run_function(bridgeState->interpreter, "ztas_fini", 1, args);
    interfi_destroy(bridgeState->interpreter);

    free(state);
}

/**
 * __ztas_recv --> ztas_recv
 */
void do_recv(void* state, const void* data, ztas_size_t size) {
    StateWrapper* bridgeState = (StateWrapper*) state;
    CArgument args[] = { { .type = 1, .value.pointer = bridgeState->state },
            { .type = 1, .value.pointer = (void*) data }, { .type = 0, .value.int32Val = size } };

    interfi_run_function(bridgeState->interpreter, "ztas_recv", 3, args);
}

/**
 * __ztas_trig --> ztas_trig
 */
void do_trig(void* state, int32_t aid) {
    StateWrapper* bridgeState = (StateWrapper*) state;
    CArgument args[] = { { .type = 1, .value.pointer = bridgeState->state }, { .type = 0, .value.int32Val = aid } };
    interfi_run_function(bridgeState->interpreter, "ztas_trig", 2, args);
}

/**
 * ztas_term --> __ztas_term
 */
void up_term(void) {
    do_term();
}

/**
 * ztas_ucast --> __ztas_ucast
 */
int up_ucast(int32_t dst, const void* data, size_t size, int flags) {
    return do_ucast(dst, data, size, flags);
}

/**
 * ztas_bcast --> __ztas_bcast
 */
int up_bcast(const void *data, size_t size, int flags) {
    return do_bcast(data, size, flags);
}

/**
 * ztas_alarm --> __ztas_alarm
 */
int up_alarm(int32_t aid, uint32_t ms) {
    return do_alarm(aid, ms);
}

/**
 * ztas_sched --> __ztas_sched
 */
int up_sched(int32_t aid, const ztime_t* at) {
    return do_sched(aid, at);
}

/**
 * ztas_clock --> __ztas_clock
 */
void up_clock(ztime_t* ts) {
    do_clock(ts);
}

/**
 * ztas_cread --> __ztas_cread
 */
const char* up_cread(char* key) {
    return do_cread(key);
}

static void __register_ztas_interface() {
    static int registered = 0;

    if (registered) {
        return;
    }

    // we could equally register the do_* methods,
    // but maybe we need the indirection later.
    interfi_register_function("ztas_alarm", up_alarm);
    interfi_register_function("ztas_sched", up_sched);
    interfi_register_function("ztas_clock", up_clock);
    interfi_register_function("ztas_bcast", up_bcast);
    interfi_register_function("ztas_ucast", up_ucast);
    interfi_register_function("ztas_term", up_term);
    interfi_register_function("ztas_cread", up_cread);

    registered = 1;
}

static const char* __cread_binary_name(uint32_t pid) {
    char key[20];
    sprintf(key, "p%d:interfi_binary", pid);
    const char* binary = do_cread(key);
    if (!binary) {
        printf("No binary file specified in configuration. (use 'interfi_binary' in [Pid%d]-Section)\n", pid);
        abort();
    }

    return binary;
}

static const char* __cread_interpreter_arguments(uint32_t pid) {
    char key[512];
    sprintf(key, "p%d:interfi_args", pid);
    const char* args = do_cread(key);
    if (!args) {
        return "";
    } else {
        return args;
    }
}
