/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#define _ZTAS_CORE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <ztas/log.h>
#include <ztas/types.h>
#include <ztas/ztime.h>
#include <ztas/layer/layer.h>

/**
 * This bridge does not make sense without
 * asco, we'll just define it here!
 */
#define USE_ASCO

#include <asco_marker.h>
#include <asco_interface.h>

#include <string.h>



/* framework internal interface */
void* do_init(int32_t pid);
void do_fini(void* state);
void do_recv(void* state, const void* data, ztas_size_t size);
void do_trig(void* state, int32_t aid);
extern int do_ucast(int32_t dst, const void* data, ztas_size_t size, int flags);
extern int do_bcast(const void *data, ztas_size_t size, int flags);
extern int do_alarm(int32_t aid, uint32_t ms);
extern int do_sched(int32_t aid, const ztime_t* at);
extern void do_term();
extern void do_clock(ztime_t* ts);
extern const char* do_cread(char* key);

extern void* up_init(int32_t pid);
extern void up_fini(void* state);
extern void up_recv(void* state, const void* data, ztas_size_t size);
extern void up_trig(void* state, int32_t aid);

void *algorithmState;
static void* realMessage;
static ztas_size_t realSize;



//#ifdef USE_ASCO
typedef struct {
    int32_t size;
    char data[];
} message_t;

typedef struct {
    int32_t dst;
    void* data;
    size_t size;
    int64_t flags;
} ubcast_msg_t;

static message_t *incomingMessage = 0;

static void verify_message(void *data) {
    if (incomingMessage) {
        asco_fail_ifn(memcmp(incomingMessage->data,
                incomingMessage->data + incomingMessage->size,
                incomingMessage->size) == 0,
                "invalid input. should be dropped");
    }
}

/**
 * verifies and copies the first message into the buffer that's being used
 * by ukv
 */
static void decode_first_message(void *data) {
    if (incomingMessage) {
        realMessage = incomingMessage->data;
        realSize = incomingMessage->size;
    }
}

/**
 * verifies and copies second message into the buffer for ukv
 */
static void decode_second_message(void *data) {
    if (incomingMessage) {
        realMessage = incomingMessage->data + incomingMessage->size;
        realSize = incomingMessage->size;
    }
}

//#endif

/**
 * __ztas_init --> ztas_init
 */
void* do_init(int32_t pid) {

    DLOG("INIT\n");

    // Those are used for input inside handler.
    // make asco ignore them.
    ASCO_NOSTATE(realMessage);
    ASCO_NOSTATE(realSize);
    ASCO_NOSTATE(incomingMessage);
    // and the pid
    ASCO_NOSTATE(pid);

    ASCO_NOSTATE(stdout);

    // register all checker methods as asco-hooks
    asco_add_traversal_hook(decode_first_message, (void*)0, BeforeFirstTraversal);
    asco_add_traversal_hook(decode_second_message, (void*)0, BeforeSecondTraversal);
    asco_add_traversal_hook(verify_message, (void*)0, AfterHandler);
    asco_add_traversal_hook(verify_message, (void*)0, BeforeHandler);

    ASCO_HANDLER_BEGIN

    algorithmState = up_init(pid);

    ASCO_HANDLER_END

    return 0;
}

/**
 * __ztas_fini --> ztas_fini
 */
void do_fini(void* state) {
    DLOG("FINI\n");

    ASCO_HANDLER_BEGIN

    up_fini(algorithmState);

ASCO_HANDLER_END}

/**
 * __ztas_recv --> ztas_recv
 */
void do_recv(void* state, const void* data, ztas_size_t size) {
    DLOG("RECV\n");
    incomingMessage = (message_t*) data;

    assert(size == (2*incomingMessage->size)+sizeof(message_t) && "invalid message size");

    ASCO_HANDLER_BEGIN
    up_recv(algorithmState, realMessage, realSize);
    ASCO_HANDLER_END

    incomingMessage = 0;

}

/**
 * __ztas_trig --> ztas_trig
 */
void do_trig(void* state, int32_t aid) {
    DLOG("TRIG with aid %d\n", aid);
    ASCO_NOSTATE(aid);

    ASCO_HANDLER_BEGIN
    up_trig(algorithmState, aid);
    ASCO_HANDLER_END

}

/**
 * ztas_term --> __ztas_term
 */
void up_term(void) {
    DLOG("TERM\n");
    do_term();
}

void __asco_instrumented___ztas_term(void) {
    // kill it at the second term
    if (!asco_is_first_traversal()) {
        do_term();
    }
}

/**
 * ztas_ucast --> __ztas_ucast
 */
int up_ucast(int32_t dst, const void* data, ztas_size_t size, int flags) {
    DLOG("UCAST to %d with size %d\n", dst, size);
    return do_ucast(dst, data, size, flags);
}



void send_ucast(void*dataS, void*dataR) {
    ubcast_msg_t* msgS = (ubcast_msg_t*) dataS;
    ubcast_msg_t* msgR = (ubcast_msg_t*) dataR;

    asco_fail_ifn(msgS->dst == msgR->dst, "destination mismatch");
    asco_fail_ifn(msgS->size == msgR->size, "size mismatch");
    asco_fail_ifn(msgS->flags == msgR->flags, "flags mismatch");
    asco_fail_ifn(memcmp(msgS->data, msgR->data, msgS->size) == 0, "content mismatch");

    ztas_size_t msg_size = sizeof(message_t) + (2 * msgS->size);
    message_t *msg = (message_t*) malloc(msg_size);
    msg->size = msgS->size;
    memcpy(msg->data, msgS->data, msgS->size);
    memcpy(msg->data + msgS->size, msgR->data, msgS->size);

    do_ucast(msgS->dst, msg, msg_size, msgS->flags);

    free(msgS->data);
    free(msgR->data);
    free(msgS);
    free(msgR);
    free(msg);
}

int __asco_output_message___ztas_ucast(int32_t dst, const void* data, ztas_size_t size, int flags) {
    ubcast_msg_t *msg = (ubcast_msg_t*) malloc(sizeof(ubcast_msg_t));
    msg->dst = dst;
    msg->data = asco_fetch_data(data, size);
    msg->size = size;
    msg->flags = flags;

    asco_output_message(send_ucast, msg);
    return 0;
}

/**
 * ztas_bcast --> __ztas_bcast
 */
int up_bcast(const void *data, ztas_size_t size, int flags) {
    DLOG("BCAST with size %d\n", size);
    return do_bcast(data, size, flags);
}

void send_bcast(void*dataS, void*dataR) {
    ubcast_msg_t* msgS = (ubcast_msg_t*) dataS;
    ubcast_msg_t* msgR = (ubcast_msg_t*) dataR;

    asco_fail_ifn(msgS->dst == 0 && msgR->dst == 0, "destination mismatch");
    asco_fail_ifn(msgS->size == msgR->size, "size mismatch");
    asco_fail_ifn(msgS->flags == msgR->flags, "flags mismatch");
    asco_fail_ifn(memcmp(msgS->data, msgR->data, msgS->size) == 0, "content mismatch");

    ztas_size_t msg_size = sizeof(message_t) + 2 * msgS->size;
    message_t *msg = (message_t*) malloc(msg_size);
    msg->size = msgS->size;
    memcpy(msg->data, msgS->data, msgS->size);
    memcpy(msg->data + msgS->size, msgR->data, msgS->size);

    do_bcast(msg, msg_size, msgS->flags);

    free(msgS->data);
    free(msgR->data);
    free(msgS);
    free(msgR);
    free(msg);

}

int __asco_output_message___ztas_bcast(const void* data, ztas_size_t size, int flags) {
    ubcast_msg_t *msg = (ubcast_msg_t*) malloc(sizeof(ubcast_msg_t));
    msg->dst = 0;
    msg->data = asco_fetch_data(data, size);
    msg->size = size;
    msg->flags = flags;

    asco_output_message(send_bcast, msg);
    return 0;
}

/**
 * ztas_alarm --> __ztas_alarm
 */
int up_alarm(int32_t aid, uint32_t ms) {
    DLOG("ALARM %d in %dms\n", aid, ms);
    return do_alarm(aid, ms);
}

int __asco_instrumented___ztas_alarm(int32_t aid, uint32_t ms) {
    if (asco_is_first_traversal()) {
        asco_message_enqueue_clone(&aid, sizeof(uint32_t));
        asco_message_enqueue_clone(&ms, sizeof(uint32_t));
        int result = do_alarm(aid, ms);
        asco_message_enqueue_clone(&result, sizeof(int));
        return result;

    } else {
        asco_message_verify_delete(&aid, sizeof(uint32_t));
        asco_message_verify_delete(&ms, sizeof(uint32_t));
        int *result = asco_message_dequeue(sizeof(int));
        int newResult = *result;
        free(result);
        return newResult;
    }
}

/**
 * ztas_sched --> __ztas_sched
 */
int up_sched(int32_t aid, const ztime_t* at) {
    DLOG("SCHED\n");
    return do_sched(aid, at);
}

int __asco_instrumented___ztas_sched(int32_t aid, const ztime_t* at) {
    if (asco_is_first_traversal()) {
        asco_message_enqueue_clone(&aid, sizeof(aid));
        asco_message_enqueue_clone(&at, sizeof(uint32_t));
        int result = do_sched(aid, at);
        asco_message_enqueue_clone(&result, sizeof(int));
        return result;

    } else {
        asco_message_verify_delete(&aid, sizeof(aid));
        asco_message_verify_delete(&at, sizeof(uint32_t));
        int *result = asco_message_dequeue(sizeof(int));
        int newResult = *result;
        free(result);
        return newResult;
    }
}

/**
 * ztas_clock --> __ztas_clock
 */
void up_clock(ztime_t* ts) {
    DLOG("CLOCK\n");
    do_clock(ts);
}

void __asco_instrumented___ztas_clock(ztime_t* ts) {
    if (asco_is_first_traversal()) {
        do_clock(ts);
        asco_message_enqueue_clone(ts, sizeof(ztime_t));
    } else {
        ztime_t *result = asco_message_dequeue(sizeof(ztime_t));
        memcpy(ts, result, sizeof(ztime_t));
        free(result);
    }
}

/**
 * ztas_cread --> __ztas_cread
 */
const char* up_cread(char* key) {
    DLOG("asco bridge: cread\n");
    return do_cread(key);
}

const char* __asco_instrumented___ztas_cread(char* key) {
    if (asco_is_first_traversal()) {
        asco_message_enqueue_clone(key, sizeof(char) * (strlen(key) + 1));
        const char* result = do_cread(key);
        asco_message_enqueue((void*) result, sizeof(char) * (strlen(result) + 1));
        return result;
    } else {
        asco_message_verify_delete(key, sizeof(char) * (strlen(key) + 1));
        return (char*) asco_message_dequeue(0);
    }
}
