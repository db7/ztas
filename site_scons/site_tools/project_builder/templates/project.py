# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
import sys

header = """
# ####################################
# DO NOT MODIFY THIS FILE!
# This file is generated, all modifications will be lost!
# 
# This file contains generated helper classes that
# can be used in experiments for project
# {projectname}

import os, sys
#sys.path.append('{path_to_ztax}/')
#from configuration import AbstractProcessConfiguration
#from project import AbstractProject, AbstractProcessConfiguration

"""

skeleton="""
class Project{project_name}(object):
    def __init__(self):

        self.projectDir = os.path.abspath(os.path.dirname(__file__))
        
        self.deployDir = "/tmp/%s/" % '{project_name}'.lower()
        
        ### Project files ###
        self.filePaths = {{
            {projectFiles}
        }}
        
        
        self._fileNatures = [
            {projectFileNatures}
        ]
        
    def __makeAbsolutePath(self, relativeTargetPath):
        \"\"\"
        Applies the relativeTargetPath to the current project dir making absolute
        paths out of them. This way the experiments using the path's can be anywhere.
        \"\"\"
        return os.path.abspath(self.projectDir+os.sep+relativeTargetPath)
        
    def getFile(self, name):
        assert name in self.filePaths, ("File %s not available." % name)
        return self.filePaths[name]
        
    def getFileNameByNature(self, **nature):
        if len(nature) == 0:
            raise AttributeError('Specify a nature to find the file')
            
        for (fileName, fileNature) in self._fileNatures.items():
            if nature == fileNature:
                return fileName
        else:
            raise KeyError('file with nature %s could not be found' %(repr(nature)))
            
    def getFilePathByNature(self, **nature):
        return self.filePaths[self.getFileNameByNature(nature)]
    
                
        
# create instance that can be used from experiments
{project_name}=Project{project_name}()
"""

projectFiles = """
            '{file_name}':self.__makeAbsolutePath('{file_path}'),"""

projectFileNatures = """
            ('{file_name}', {file_nature}),""" 
            
processConfigCreators = """
    def create{process_name}Config(self):
        return {process_name}Config()
"""

procConfig = """
# #######
# Configuration for process {process_name} in project {project_name}
#
class {process_name}Config(AbstractProcessConfiguration):
    ## public fields of the config
    {projectValues}
    
    def __init__(self):
        AbstractProcessConfiguration.__init__(self)
        
        self.configFileName = '{process_name}.cfg'
        
        formats = [{algorithmFormats}
                ]
        
        self._setFormat(''.join(formats))
        
        # adds all config fields as members that is used in convenience methods to set
        # config values.
        {memberDeclarations}
         
"""
    
projectValues = """
    {field_name} = {default_value}"""

algorithmFormats = """
            "{format}", # algorithm {name}"""

memberDeclarations = """
        self._addMember('{field_name}')"""

