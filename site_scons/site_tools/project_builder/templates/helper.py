# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
# 
# some helper methods to use templates for code generation
#

def printTemplate(fp, template, **kwargs):
    fp.write(template.format(**kwargs))
        
def printNestedTemplate(fp, module, template, **values):
    fp.write(formatNestedTemplate(module, template, **values))
    
def formatNestedTemplate(module, template, **values):
    replaceValues = {}
    for name in values:
        # assume its a sub template
        if isinstance(values[name], list):
            nested = ''
            for row in values[name]:
                assert isinstance(row, dict)
                nested += formatNestedTemplate(module, getattr(module, name), **row)
            replaceValues[name] = nested
        else:
            replaceValues[name] = values[name]
    return template.format(**replaceValues)
            

