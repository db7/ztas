# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
"""


"""
import os
import sys

from SCons.Script import AddOption

import project

def generate(env):
	
	AddOption('--deploy-dir', dest='deploy-dir', nargs=1)
	AddOption('--deploy-host', dest='deploy-host', nargs=1)
	AddOption('--build-dir', dest='build-dir', nargs=1)
	
	env.Project = project.Project

def exists(env=None):
	return True
