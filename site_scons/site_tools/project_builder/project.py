# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
from SCons.Script import *
from SCons.Node import NodeList

from templates import project, helper
import os


class Project(object):
    """
    Generates a scaffolded python class that can be used in an experiment's Dudefile.
    """
    def __init__(self, name):
        super(Project, self).__init__()
        
        self.name = name
        self.files = {}
        self.experiments = {}
                                
    def addFiles(self, files):
        try:
            iterator = iter(files)
        except TypeError:
            files = [files]
            
        for file in files:
            self.addFile(file)
            
    def addFile(self, file, name=None, nature=None):
        """
        Adds a file to the project.
        Each file can only be used once, otherwise we'll get naming issues.
        
        @param file SCons Node object
        @param optional name (default is node name)
        @param nature allows specifying a unique set of key-value pairs that can
        be used to find the file if the name is unknown. 
        """
        
        if isinstance(file, NodeList):
            assert len(file) == 1, "Can only add one file. NodeList was given."
            file = file[0]
            
            
        # make the file a SCons-File.
        # The File-Call is idempotent (ignored if called multiple times))
        file = File(file)
        
        if not name:
            name = file.name
            
        if not nature:
            nature = {}
        
        if nature:
            assert isinstance(nature, dict), "Nature must be a dict."
        
        assert name not in self.files, "Each process can only be added once to a project"
        
        self.files[name] = {'file':file, 'nature':nature}
        
    def _iterFiles(self):
        for file in self.files.values():
            yield file['file']
        
    def addExperiment(self, name):
        """
        Adds an experiment.
        If the experiment folder is not present, it'll be created.
        """
        
        assert name not in self.experiments, "Experiment %s already added to the project." % name
        
        experimentPath = self.__getCurrentSourceDir() + os.sep + name 
        if not os.path.exists(experimentPath):
            os.makedirs(experimentPath)
            
        self.experiments[name] = experimentPath
        
    def _scaffoldForDeployment(self, deploymentFolder):
        """
        Creates the scaffolding-file for a project for deployment.
        All project files are copied to the same place so the relative file paths
        of the usual scaffold doesn't work.
        """
        
        targetFile = self._prepareScaffoldTargetFile(deploymentFolder)
        
        fileNames = []
        for file in self.files.values():
            fileName = file['file'].name
            fileNames.append({
                              'file_name': fileName,
                              'file_path': fileName
                            })
            
        with open(targetFile, 'w') as f:
            self._printHeader(f)
            self._printProjectSkeleton(f, project, fileNames, self._collectProjectFileNatures())
        
        self.addFile(File(targetFile))
        
    def scaffold(self, target=''):
        """
        Generates a python class with the project's values to simplify project execution 
        in an experiment.
        """
        
        targetFile = self._prepareScaffoldTargetFile(os.path.join(self.__getCurrentSourceDir(), target))
        with open(targetFile, 'w') as f:
            self._printHeader(f)
            self._printProjectSkeleton(f, project, self._collectProjectFilePaths(target), self._collectProjectFileNatures())
            
        
            

    def deploy(self, deployTempdir, host, dir, buildDir = None):
        
        
        print "=========================================="
        print " Deploying Project " + self.name
        print "=========================================="
        
        
        try:
            import muddi
        except ImportError as e:
            print "  ----  Need muddi to deploy project. Aborting.  ----  "
            return
                
        
        # do extra scaffolding for the deployment
        self._scaffoldForDeployment(deployTempdir)
        
               
        if not dir.endswith(self.name):
            dir = dir + os.sep + self.name
            
        upload = []
        uploadFiles = []
        for filePath in self._absluteFilePaths(buildDir):
            uploadFiles.append(filePath)
             
        upload.append((host, uploadFiles, dir + os.sep))
        
        for (experiment, experimentPath) in self.experiments.items():
            experimentUploads = []
            for (dirpath, dirnames, filenames) in os.walk(experimentPath, followlinks=False):
                for filename in filenames:
                    experimentUploads.append(os.path.join(dirpath, filename))
                    
            upload.append((host, experimentUploads, os.path.join(dir, experiment)))

                
        config = muddi.conf([], upload, [], logdir='log')
        
        error = muddi.check(config, quiet=False)
        error += muddi.upload(config, quiet=False)

        # builder returns True/False for success instead of 0/1        
        return error==0
        
        
    def _absluteFilePaths(self, buildDir):
        if not buildDir:
            return [file.abspath for file in self._iterFiles()]
        else:
            # we assume the build is in #/build/
            r = []
            for f in self._iterFiles():
                if str(f).find('build/') == -1 or str(f).find('deployment/') != -1:
                    r.append(f.abspath)
                else:
                    f1 = File(buildDir + '/' + str(f).split('build/')[1])
                    r.append(f1.abspath)
            return r
            
        
    def __getCurrentSourceDir(self):
        return Dir('.').srcnode().abspath
                            
    def _prepareScaffoldTargetFile(self, absTargetDir):
        if not absTargetDir.endswith(os.sep):
            absTargetDir += os.sep
        try:
            if os.path.exists(absTargetDir):
                if os.path.isfile(absTargetDir):
                    raise Exception("Target directory for the scaffolded file exists but is a file. Choose another one.")
            else:
                os.makedirs(absTargetDir)
        except Exception as e:
            raise Exception("Error preparing the directory for the scaffold target file")
        
        print "deployment file will be created in ", absTargetDir
        
        return absTargetDir + self._getProjectFileName()
           
    def _printHeader(self, f):
        helper.printTemplate(f, project.header, projectname=self.name, path_to_ztax=os.path.dirname(os.path.abspath(__file__)))
        
        
    def _printProjectSkeleton(self, f, project, projectFilePaths, projectFileNatures):
        
        helper.printNestedTemplate(f,
                                   project,
                                   project.skeleton,
                                   project_name=self.name,
                                   projectFiles=projectFilePaths,
                                   projectFileNatures=projectFileNatures)
        
    def _getProjectFileName(self):
        return "scaffold_" + self.name.lower() + ".py"
        
    
    def _collectProjectFilePaths(self, targetDir=''):
        projectFiles = []
        
        fileNames = self.files.keys()
        fileNames.sort()
                            
        for fileName in fileNames:
            fileEntry = {}
            fileEntry['file_name'] = fileName
            fileEntry['file_path'] = os.path.relpath(self.files[fileName]['file'].abspath, self.__getCurrentSourceDir() + os.sep + targetDir)
            projectFiles.append(fileEntry)
            
        return projectFiles

    def _collectProjectFileNatures(self):
        projectFileNatures = []
        
        fileNames = self.files.keys()
        fileNames.sort()
        
        for fileName in fileNames:
            if len(self.files[fileName]['nature']) == 0:
                continue
            
            fileEntry = {}
            fileEntry['file_name'] = fileName
            fileEntry['file_nature'] = repr(self.files[fileName]['nature'])
            projectFileNatures.append(fileEntry)
            
        return projectFileNatures
