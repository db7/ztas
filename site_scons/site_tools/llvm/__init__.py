# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
import os, subprocess
import SCons
from SCons.Script import *

def BC2ObjectLLVMGCC(env, source):
    #bld = Builder(action = 'llc $SOURCE  -march=c -f $TYPE -o $TARGET',
    #              suffix = '.c',
    #              src_suffix = '.bc')
    bld = Builder(action = 'llc $SOURCE -f -o $TARGET',
                  suffix = '.s',
                  src_suffix = '.bc')
    env.Append(BUILDERS = {'LLC' : bld})

    assert len(source) == 1
    source = source[0]
    target1 = str(source).split('.bc')[0] + '.s'
    target2 = str(source).split('.bc')[0] + '.o'

    asm = env.LLC(target1, source)
    obj = env.Object(target2, asm)
    return obj


def llvm_config(option):
    return subprocess.Popen(['llvm-config', option], stdout=subprocess.PIPE).communicate()[0]

def generate(env):

    if not exists(env):
        return

    version = llvm_config('--version').replace('\n', '')
    bindir  = llvm_config('--bindir')

    env.Prepend(PATH = bindir[:-1])
    env.PrependENVPath('PATH', bindir[:-1])

    print "Using LLVM", version
    if "3." in version:
        
        clang = Builder(action = 'clang -c -emit-llvm $CFLAGS $_CPPINCFLAGS -o $TARGET $SOURCE', 
                        suffix = '.bc',
                        src_suffix = '.c',
                        source_scanner=SCons.Defaults.CScan)
        clango = Builder(action = 'clang -c $CFLAGS -o $TARGET $SOURCE', 
                         suffix = '.o',
                         src_suffix = '.bc')
        llvm_link = Builder(action = 'llvm-link $SOURCES -o $TARGET',
                            suffix = '.bc',
                            src_suffix = '.bc')
        env.Append(BUILDERS = {'Bitcode'   : clang})
        env.Append(BUILDERS = {'Link'      : llvm_link})
        env.Append(BUILDERS = {'BC2Object' : clango})
    else:
        clang = Builder(action = 'llvm-gcc -c -emit-llvm $CFLAGS $_CPPINCFLAGS -o $TARGET $SOURCE', 
                        suffix = '.bc',
                        src_suffix = '.c')
        llvm_link = Builder(action = 'llvm-link $SOURCES -o $TARGET',
                            suffix = '.bc',
                            src_suffix = '.bc')
        env.Append(BUILDERS = {'Bitcode'   : clang})
        env.Append(BUILDERS = {'Link'      : llvm_link})
        env.AddMethod(BC2ObjectLLVMGCC, 'BC2Object')


    # from Sergei's SConstruct
    #env.ParseConfig('llvm-config --libs Core AsmParser BitReader ' + 
    #'BitWriter Support JIT X86CodeGen ipo Linker Interpreter MCJIT')
    #env.ParseConfig('llvm-config --ldflags')

def exists(env=None):
    try:
        llvm_config('--version') # exception if it does not exist
        return True
    except Exception:
    # we have to catch everything here, since subprocess will
    # throw an OSError in case the llvm-config is not even there.
        return False
