# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------

from SCons.Script import *
import unittest
from registry import Registry


class RegistryTest (unittest.TestCase):
    
    def setUp(self):
        Registry.clear()
    
    def testInit(self):
        
        # not empty names
        self.assertRaises(AssertionError, Registry, None)
        self.assertRaises(AssertionError, Registry, '')
        
        # no duplicates
        registry = Registry('artifact')
        self.assertRaises(AssertionError, Registry, 'artifact')
        
    def testSetPrograms(self):
        registry = Registry('artifact')
        
        registry.addProgram('thisismyprogram', 'myprogram')
        self.assertEquals('thisismyprogram', registry.getProgram('myprogram'))
        
    def testAddSource(self):
        registry = Registry('algorithm')
        
        registry.addSources('src1')
        
        self.assertEqual('src1', registry.getSources()[0].name)
        
        registry.addSources([['src1', 'src2'], 'src3'])
        
        self.assertEquals(['src1', 'src1', 'src2', 'src3'], self.__collectNodeNames(registry.getSources()))
            
    
    def testGet(self):
        registry = Registry('artifact')
        self.assertEquals(registry, Registry.get('artifact'))
        
        
    def testCollectObjects(self):
        reg1 = Registry('reg1')
        reg1.addObjects('obj1')
        
        reg2 = Registry('reg2')
        reg2.addObjects('obj2')
        
        self.assertEquals(['obj1', 'obj2'], self.__collectNodeNames(Registry.collectObjects(['reg1', 'reg2'])))
        
    def testCollectSources(self):
        reg1 = Registry('reg1')
        reg1.addSources('src1')
        
        reg2 = Registry('reg2')
        reg2.addSources('src2')
        
        self.assertEquals(['src1', 'src2'], self.__collectNodeNames(Registry.collectSources(['reg1', 'reg2'])))
        
    def __collectNodeNames(self, nodes):
        if not isinstance(nodes, list):
            nodes = [nodes]
        
        nodeNames = []
        
        for node in nodes:
            if hasattr(node, 'name'):
                nodeNames.append(node.name)
            else:
                nodeNames.append(node)
                
        return nodeNames
    
if __name__ == '__main__':
    unittest.main()
