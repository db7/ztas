# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
from registry import RegistryStore



def generate(env):
    
    env.Registry = RegistryStore()

def exists(env):
    return True
