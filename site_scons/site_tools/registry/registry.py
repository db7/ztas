# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
from SCons.Script import File
import SCons


class RegistryStore(object):
    
    
    def __init__(self):
        self._registries = {}
    
    def getOrCreate(self, name):
        if name in self._registries:
            return self._registries[name]
        
        registry = Registry(name)
        self._registries[name] = registry
        return registry
    
    def __call__(self, name):
        return self.getOrCreate(name)
    
    def __checkRegistriesExist(self, names):
        for name in names:
            if name not in self._registries:
                raise SCons.Errors.InternalError('Registry %s not found. The build order is messed up.' % name) 
    
    def collectObjects(self, *names):
        names = SCons.Util.flatten(names)
        self.__checkRegistriesExist(names)

        objects = [self._registries[name].getObjects() for name in names]
        return reduce(lambda a, b: a + b, objects, [])
    
    def collectSharedObjects(self, *names):
        names = SCons.Util.flatten(names)
        self.__checkRegistriesExist(names)

        objects = [self._registries[name].getSharedObjects() for name in names]
        return reduce(lambda a, b: a + b, objects, [])

    def collectSources(self, *names):
        names = SCons.Util.flatten(names)
        self.__checkRegistriesExist(names)
        
        sources = [self._registries[name].getSources() for name in names]
        return reduce(lambda a, b: a + b, sources, [])
    
    def collectPrograms(self, *names):
        names = SCons.Util.flatten(names)
        self.__checkRegistriesExist(names)
        
        programs = [self._registries[name].getPrograms() for name in names]
        return reduce(lambda a, b: a + b, programs, [])

    def collectFiles(self, *names, **kwargs):
        """
        Collect any files registered in the project. 
        Keyword argument 'tag' can be set to filter by a specific tag. Default tag is ''.
        """
        names = SCons.Util.flatten(names)
        self.__checkRegistriesExist(names)
        
        if 'tag' not in kwargs:
            kwargs['tag'] = ''
        
        files = [self._registries[name].getFiles(kwargs['tag']) for name in names]
        return reduce(lambda a, b: a + b, files, [])

class Registry:
    """
    Registry keeps allows centralized registration of components of ztas.
    """

    
    __objKey = '__objects__'
    __shobjKey = '__sharedobjects__'
    __srcKey = '__source__'
    __libKey = '__libraries__'
    __progKey = '__programs__'
    
    def __init__(self, name):
        assert name is not None and len(name) > 0, "Name must not be empty"
        
        self._files = {
                       self.__objKey:[],
                       self.__shobjKey:[],
                       self.__srcKey:[],
                       self.__libKey:[],
                       self.__progKey:{}
                       }

    
    def getObjects(self):
        return list(self._files[self.__objKey])

    def getFiles(self, tag=""):
        return list(self._files[tag])

    def getSharedObjects(self):
        return list(self._files[self.__shobjKey])

    def getSources(self):
        return list(self._files[self.__srcKey])

    def getLibraries(self):
        return list(self._files[self.__libKey])
    
    
    def __iterNestedList(self, items):
        flatItems = SCons.Util.flatten(items)
        
        for item in flatItems:
            yield item
        
    
    def addSources(self, *sources):
        for source in self.__iterNestedList(sources):
            self._files[self.__srcKey].append(File(source))

    def addLibraries(self, *libraries):
        for library in self.__iterNestedList(libraries):
            self._files[self.__libKey].append(library)

    def addObjects(self, *objects):
        for obj in self.__iterNestedList(objects):
            self._files[self.__objKey].append(obj)

    def addSharedObjects(self, *shObjects):
        for shObj in self.__iterNestedList(shObjects):
            self._files[self.__shobjKey].append(shObj)

    def getProgram(self, name=None):
        assert name in self._files[self.__progKey], "%s not registered as program. Did you forget to call reg.registerProgram(..)?" % (name,)
        return self._files[self.__progKey][name]
    
    def getPrograms(self):
        return self._files[self.__progKey].values()

    def addProgram(self, program, name=None):
        assert program, "program cannot be None!"
        
        if not name:
            name = program.name
        assert name not in self._files[self.__progKey], "%s already registered as program" % (name,)
        self._files[self.__progKey][name] = program

    def addFiles(self, files, tag=""):
        if tag not in self._files:
            self._files[tag] = []

        for fle in self.__iterNestedList(files):
            self._files[tag].append(fle)

