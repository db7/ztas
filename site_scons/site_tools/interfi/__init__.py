# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
import os

from SCons.Script import *

_interfiPath = 'deps/interfi'

_libPath = ['#deps/interfi/target/interfi', '#deps/interfi/target/ExtensibleInterpreter']
_libs = ['EmbeddableInterpreter.so', 'ztas_proc_noif']
_rpath = [Dir('#deps/interfi/target/interfi').abspath]

__bridgeObject = None
def buildBridge(env):
    global __bridgeObject
    
    if not __bridgeObject:
        envt = env.Clone()
        envt.Append(CFLAGS='-DLAYERU= -DLAYERD=_ ')
        __bridgeObject = envt.Object('interfi_bridge_01', '#/layers/interfi/interfi_bridge.c', CPPPATH=['#deps/interfi/'] + env['CPPPATH'])
    return __bridgeObject

def buildZtasLoggerInterpreter(env, name='ztas_loggerinterpreter'):
    
    ztasInterpreter = env.Program(name,
                                   buildBridge(env),
                                   LIBS=['logger_12'] + _libs + env['LIBS'],
                                   LIBPATH=_libPath + env['LIBPATH'],
                                   RPATH=_rpath)
    
    return ztasInterpreter[0]

def buildZtasPlayerInterpreter(env, name='ztas_playerinterpreter'):
    
    ztasInterpreter = env.Program(name,
                                   buildBridge(env),
                                   LIBS=['player_12', 'EmbeddableInterpreter.so', 'zproc'] + env['LIBS'],
                                   LIBPATH=_libPath + env['LIBPATH'],
                                   RPATH=_rpath)
    
    return ztasInterpreter[0]
    

def generate(env):
    if not exists():
        return
    
    env.AddMethod(buildZtasLoggerInterpreter)
    env.AddMethod(buildZtasPlayerInterpreter)
    

def exists(env=None):
    if not os.path.exists(_interfiPath):
        return False
    
    if not os.path.exists(os.path.join(_interfiPath, 'target/interfi')):
        print "Dependency interfi exists but is not built. Go to deps/interfi and run scons."
        return False

    return True
