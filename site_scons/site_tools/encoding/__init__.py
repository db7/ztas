# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
from SCons.Script import *

import os, subprocess
import SCons

AddOption('--encoding', dest='encoding', nargs=0)

from llvm import llvm_config

if os.environ.has_key('SEPPATH'):
    seppath = os.environ['SEPPATH']
    path = seppath + '/external/LLVM/llvm/bin'
    cpppath = seppath + '/build/include'
else:
    seppath = None

def ObjectBC_enc(env, source, **kvargs):
    env = env.Clone()
    path = seppath + '/external/LLVM/llvm/bin'
    cpppath = seppath + '/build/include'
    env.Append(CPPPATH = cpppath)
    env.AppendENVPath('PATH', path)
    env.Replace(CC='llvm-gcc')
    env.Replace(CFLAGS='-std=gnu89 -emit-llvm -Wall -O0 -DENCODED ')

    x = SCons.Util.splitext(source)[0] + '.bc'
    x = x.split('/')[-1]
    return env.Object(x, source)



def LinkBC_enc(env, *kvargs):
    env = env.Clone()
    path = seppath + '/external/LLVM/llvm/bin'
    cpppath = seppath + '/build/include'
    env.Append(CPPPATH = cpppath)
    env.AppendENVPath('PATH', path)
    env.Replace(CC='llvm-gcc')
    env.Replace(CFLAGS='-std=gnu89 -emit-llvm -Wall -O0 ')
    
    bld = Builder(action = '$LLVMLINK $SOURCES -f -o $TARGET',
                  suffix = '.bc',
                  src_suffix = '.bc')
    
    env.Append(BUILDERS = {'LinkBC1' : bld}, LLVMLINK = 'llvm-link')
    o = env.LinkBC1(*kvargs)
    env.Ignore(o,o)
    return o



def __create_environment(env):
    env_enc = env.Clone()

##### Dependencies for encoding
    path = seppath + '/external/LLVM/llvm/bin'
    cpppath = seppath + '/build/include'

    conf = Configure(env_enc)
    conf.env.Append(CPPPATH = cpppath)
    conf.env.AppendENVPath('PATH', path)
    env_enc = conf.Finish()

    env_enc.Replace(CC='llvm-gcc')
    old = env_enc.Dictionary()['CFLAGS']
    old = str(old)
    vals = [x for x in old.split(' ') if x.find('-D') != -1]
    vals = ' '.join(vals) + ' '
    env_enc.Replace(CFLAGS='-std=gnu89 -emit-llvm -Wall -O0 -DENCODED ' + vals)
    env_enc.Replace(CCFLAGS='')
    env_enc.Replace(LD='llvm-ld')

    bld = Builder(action = '$LLVMLINK $SOURCES $BCLIBS -f -o $TARGET',
                  suffix = '.bc',
                  src_suffix = '.bc')
    
    env_enc.Append(BUILDERS = {'LinkBC' : bld})

    # [env_enc.Object(f.split('.c')[0]+'.bc', f) for f in files]
    # env_enc.Library('../ztas_wrapper-' + suffix, files)

    import os
    BCDIR  = os.environ['SEPPATH'] + '/build/lib/bitcode'
    LIBDIR = os.environ['SEPPATH'] + '/build/lib'
    REPLACEMENT_LIB = BCDIR + '/replacementOps.bc'
    SCRUTINIZERLIB  = LIBDIR  + '/libparexc_scrutinizer_pass.so'
    # FUNCTIONCLONEPASSLIB=$(LIBDIR)/FunctionClone.so
    # DEBUGPASSLIB=$(LIBDIR)/libparexc-parexcer-passes.so
    ECOPSLIB = LIBDIR + '/libparexc_scrutinizer-encodedInstructions-lessSafe_runtime.so'

    # '$BCDIR/scrutinizerCommunication_anbdmem_dummy.o ' +\
    # '$BCDIR/scrutinizer_pageBased_mvs_anbdmem.bc ' +\
    # '$BCDIR/scrutinizer_mvs_anbdmem.bc ' +\
    # '$BCDIR/scrutinizer_MemoryUpdater.bc ' +\
    BCLIBSANBDmem = \
    '$BCDIR/decodingWrappers_anbdmem.bc ' +\
    '$BCDIR/initGlobalMvs_anbdmem.bc ' +\
    '$BCDIR/encodedOperations-sse.bc ' +\
    '$BCDIR/encodingDecoding-sse.bc ' +\
    '$BCDIR/encodedOperationsAN-sse.bc ' +\
    '$BCDIR/encodedOperationsANB_lessSafe-sse.bc ' +\
    '$BCDIR/encodedOperationsANBD-sse.bc ' +\
    '$BCDIR/bigint-sse.bc ' +\
    '$BCDIR/stacklifter-func_ptr_support.bc ' +\
    '$BCDIR/alloca.bc '
    #'-lztas_wrapper-anbdmem'

    # '$BCDIR/scrutinizerCommunication_anb_dummy.o ' +\
    # '$BCDIR/scrutinizer_pageBased_mvs_anb.bc ' +\
    # '$BCDIR/scrutinizer_mvs_anb.bc ' +\
    BCLIBSANB = \
    '$BCDIR/decodingWrappers_anb.bc ' +\
    '$BCDIR/initGlobalMvs_anb.bc ' +\
    '$BCDIR/encodedOperations-sse.bc ' +\
    '$BCDIR/encodingDecoding-sse.bc ' +\
    '$BCDIR/encodedOperationsAN-sse.bc ' +\
    '$BCDIR/encodedOperationsANB_lessSafe-sse.bc ' +\
    '$BCDIR/encodedOperationsANBD-sse.bc ' +\
    '$BCDIR/bigint-sse.bc ' +\
    '$BCDIR/stacklifter-func_ptr_support.bc ' +\
    '$BCDIR/alloca.bc ' 
    #'-lztas_wrapper-anb'

    # '$BCDIR/scrutinizer_pageBased_mvs_an.bc ' +\
    # '$BCDIR/scrutinizer_mvs_an.bc ' +\
    BCLIBSAN = \
    '$BCDIR/decodingWrappers_an.bc ' +\
    '$BCDIR/initGlobalMvs_an.bc ' +\
    '$BCDIR/encodedOperations-sse.bc ' +\
    '$BCDIR/encodingDecoding-sse.bc ' +\
    '$BCDIR/encodedOperationsAN-sse.bc ' +\
    '$BCDIR/encodedOperationsANB_lessSafe-sse.bc ' +\
    '$BCDIR/encodedOperationsANBD-sse.bc ' +\
    '$BCDIR/bigint-sse.bc ' +\
    '$BCDIR/stacklifter-func_ptr_support.bc ' +\
    '$BCDIR/alloca.bc '
    #'-lztas_wrapper-an'


    # OTHERLIBS = '-lhashtable -lm -lparexc_scrutinizer_externals'
    # EXTRALIBS = '-L.... -L$LIBDIR ' +\
    #     '-lboost_filesystem-mt -lboost_program_options-mt -lboost_regex-mt ' +\
    #     '-lboost_serialization-mt -lboost_system-mt -lboost_thread-mt ' +\
    #     ' -lzmq -L build/pods/core -L build/core -L build/encoding -lztas_proc ' #+\

    OTHERLIBS = ''
    EXTRALIBS = ''

    env_enc.Append(
        BCLIBSANBDmem     = BCLIBSANBDmem
        , BCLIBSANB       = BCLIBSANB
        , BCLIBSAN        = BCLIBSAN
        , EXTRALIBS       = EXTRALIBS
        , OTHERLIBS       = OTHERLIBS
        , BCDIR           = BCDIR
        , REPLACEMENT_LIB = REPLACEMENT_LIB
        , ENCOPSLIB       = ECOPSLIB
        , SCRUTINIZERLIB  = SCRUTINIZERLIB
        , ALLOCAPASSLIB   = LIBDIR + '/libparexc-parexcer-passes.so'
        )


    env_enc.Append(
        LIBDIR = LIBDIR
        , LLVMLD = 'llvm-ld'
        , LLVMLINK='llvm-link'
        , OPT='opt'
        , NOUNALIGNED = 1
        , GLOBALA = 65521
        , SAFE_FUNCTION_SUFFIX = '_scrutinizerSafeCloned'
        , OPERATION='less-safe-sse2'
        , USE_AX=0
        # only ANB/ANBDMEM
        , ANBDEBUG=0
        , WATCHDOG='dummy'
        , CHECKFREQUENCY=10
        # change this value also for the called watchdog (-qLength=X)
        , QUEUELENGTH=10000
        , CHOOSENSIGNATURES='deps/encoding/sig.conf'
        , SIGNATURES = 'deps/encoding/Signatures'
        )

## Link in replacement ops
    bld = Builder(action = '$LLVMLINK $SOURCES $REPLACEMENT_LIB -f -o $TARGET',
                  suffix = '_repLinked.bc',
                  src_suffix = '.bc')
    env_enc.Append(BUILDERS = {'RepLinked' : bld})


## Remove 64-bit specific code
    bld = Builder(action = '$OPT -load=$SCRUTINIZERLIB -disable-opt ' + \
                      '-parexc-scrutinizer-convI64ToI32 $SOURCE -f -o $TARGET',
                  suffix = '_64to32.bc',
                  src_suffix = '_repLinked.bc')

    env_enc.Append(BUILDERS = {'From64to32' : bld})

## Clone all functions and adapt calls
    bld = Builder(action = '$OPT -load=$SCRUTINIZERLIB -disable-opt ' + \
                      '-parexc_scrutinizer_clone-functions $SOURCE ' +\
                      '-cloneSuffix=$SAFE_FUNCTION_SUFFIX -setupFptr=false -f -o $TARGET',
                  suffix = '_cloned.bc',
                  src_suffix = '_64to32.bc')

    env_enc.Append(BUILDERS = {'Cloned' : bld})

## Replace allocas
    bld = Builder(action = '$OPT  -load=$ALLOCAPASSLIB -disable-opt -parexc-alloca '+\
                      '$SOURCE -f -o $TARGET',
                  suffix = '_AllocaRemoved.bc',
                  src_suffix = '_cloned.bc')

    env_enc.Append(BUILDERS = {'AllocaRemoved' : bld})

## Prepare all functions
### AN specific
    bld = Builder(action = '$OPT -load=$SCRUTINIZERLIB -disable-opt -lowerselect ' +\
                      '-insert-preparations -noUnalignedLoad=$NOUNALIGNED '+\
                      '-functionSuffix=$SAFE_FUNCTION_SUFFIX -for_an ' +\
                      '$SOURCE -f -o $TARGET',
                  suffix = '_prepared.bc',
                  src_suffix = '_AllocaRemoved.bc')

    env_enc.Append(BUILDERS = {'PreparedAN' : bld})


### ANB specific
    bld = Builder(action = '$OPT -load=$SCRUTINIZERLIB -disable-opt -lowerselect ' +\
                      '-insert-preparations -noUnalignedLoad=$NOUNALIGNED '+\
                      '-functionSuffix=$SAFE_FUNCTION_SUFFIX -for_anb ' +\
                      '$SOURCE -f -o $TARGET',
                  suffix = '_prepared.bc',
                  src_suffix = '_AllocaRemoved.bc')

    env_enc.Append(BUILDERS = {'PreparedANB' : bld})


### ANB specific
    bld = Builder(action = '$OPT -load=$SCRUTINIZERLIB -disable-opt -lowerselect ' +\
                      '-insert-preparations -noUnalignedLoad=$NOUNALIGNED '+\
                      '-functionSuffix=$SAFE_FUNCTION_SUFFIX -for_anbdmem ' +\
                      '$SOURCE -f -o $TARGET',
                  suffix = '_prepared.bc',
                  src_suffix = '_AllocaRemoved.bc')

    env_enc.Append(BUILDERS = {'PreparedANBDmem' : bld})

## Remove ConstantExpr
    bld = Builder(action = '$OPT -load=$SCRUTINIZERLIB ' +\
                      '-disable-opt -removeConstExpr ' +\
                      '$SOURCE -f -o $TARGET',
                  suffix = '_constantsRemoved.bc',
                  src_suffix = '_prepared.bc')

    env_enc.Append(BUILDERS = {'ConstantsRemoved' : bld})

## Remove Gepis
    #bld = Builder(action = '$OPT -load=$SCRUTINIZERLIB -disable-opt -disable-inlining -debug'
    bld = Builder(action = '$OPT -load=$SCRUTINIZERLIB -disable-opt -disable-inlining  ' +\
                      '-insert-replaceGepis ' +\
                      '$SOURCE -f -o $TARGET',
                  suffix = '_gepisRemoved.bc',
                  src_suffix = '_constantsRemoved.bc')

    env_enc.Append(BUILDERS = {'GepisRemoved' : bld})

## Check code to encode
    bld = Builder(action = 'if $OPT -load=$SCRUTINIZERLIB -f -o /dev/null -verify ' +\
                      '-disable-opt -check-prepared-code -suffix=$SAFE_FUNCTION_SUFFIX -scrutinizerAN ' +\
                      '$SOURCE; then echo "SCRUTINIZING TEST OK"; else echo "SCRUTINIZING TEST FAILED"; exit 1; fi',
                  suffix = '_beforeScrutinizing.bc',
                  src_suffix = '_gepisRemoved.bc.bc')

    env_enc.Append(BUILDERS = {'TestBeforeScrutinizing' : bld})

## Encode Constants
    bld = Builder(action = '$OPT -f -load=$SCRUTINIZERLIB -disable-opt -disable-inlining ' + \
                      '-generateGVLoweringProg -loweringA=$GLOBALA ' + \
                      '$SOURCE -o $TARGET',
                  suffix = '_gvLoweringGenerator.bc',
                  src_suffix = '_gepisRemoved.bc')

    env_enc.Append(BUILDERS = {'LoweringGeneratorStart' : bld})

### AN specific
    bld = Builder(action='$LLVMLD -native -L$LIBDIR -load=$SCRUTINIZERLIB ' +\
                      '-lparexc_scrutinizer_lowerGvs_an -lparexc_scrutinizer-encodedInstructions_runtime ' +\
                      '$SOURCE -o $TARGET; chmod u+x $TARGET',
                  suffix = '_gvLoweringGenerator',
                  src_suffix = '_gvLoweringGenerator.bc')

    env_enc.Append(BUILDERS = {'LoweringGeneratorAN' : bld})

### ANB specific
    bld = Builder(action='$LLVMLD -native -L$LIBDIR -load=$SCRUTINIZERLIB ' +\
                      '-lparexc_scrutinizer_lowerGvs_anb -lparexc_scrutinizer-encodedInstructions_runtime ' +\
                      '$SOURCE -o $TARGET; chmod u+x $TARGET',
                  suffix = '_gvLoweringGenerator',
                  src_suffix = '_gvLoweringGenerator.bc')

    env_enc.Append(BUILDERS = {'LoweringGeneratorANB' : bld})

### LoweringEND
    bld = Builder(action='LD_LIBRARY_PATH=$LIBDIR $SOURCE > $TARGET',
                  suffix = '_gvLowering.c',
                  src_suffix = '_gvLoweringGenerator')

    env_enc.Append(BUILDERS = {'LoweringGeneratorEnd' : bld})

## Encode unsage functions and unlinked version

### AN
    bld = Builder(action='$OPT -load=$SCRUTINIZERLIB -insert-safeblock ' +\
                      '-load=$ENCOPSLIB -A=$GLOBALA -safeSuffix=$SAFE_FUNCTION_SUFFIX ' + \
                      '-wrapperSuffix=scrutinizerSafe -an -funcPtrSetupFunc=___fptr_map_setup_fn_scrutinizerSafe ' +\
                      '$SOURCE -f -o $TARGET',
                  suffix = '_unlinked-tmp.bc',
                  src_suffix = '_gepisRemoved.bc')

    env_enc.Append(BUILDERS = {'SafeblockAN' : bld})

### ANB
    bld = Builder(action='$OPT -load=$SCRUTINIZERLIB -insert-safeblock ' +\
                      '-load=$ENCOPSLIB -A=$GLOBALA -safeSuffix=$SAFE_FUNCTION_SUFFIX ' + \
                      '-wrapperSuffix=scrutinizerSafe -anb -funcPtrSetupFunc=___fptr_map_setup_fn_scrutinizerSafe ' +\
                      '$SOURCE -f -o $TARGET',
                  suffix = '_unlinked-tmp.bc',
                  src_suffix = '_gepisRemoved.bc')

    env_enc.Append(BUILDERS = {'SafeblockANB' : bld})

### ANBDmem
    bld = Builder(action='$OPT -load=$SCRUTINIZERLIB -insert-safeblock ' +\
                      '-load=$ENCOPSLIB -A=$GLOBALA -safeSuffix=$SAFE_FUNCTION_SUFFIX ' + \
                      '-wrapperSuffix=scrutinizerSafe -anbdmem -funcPtrSetupFunc=___fptr_map_setup_fn_scrutinizerSafe ' +\
                      '$SOURCE -f -o $TARGET',
                  suffix = '_unlinked-tmp.bc',
                  src_suffix = '_gepisRemoved.bc')

    env_enc.Append(BUILDERS = {'SafeblockANBDmem' : bld})

### second step
    bld = Builder(action = '$OPT -inline -adce -instcombine -dse -die -f -o $TARGET $SOURCE',
                  suffix = '_unlinked-tmp1.bc',
                  src_suffix = '_unlinked-tmp.bc')

    env_enc.Append(BUILDERS = {'SafeblockCombine' : bld})

### AN
    bld = Builder(action='$OPT -load=$SCRUTINIZERLIB -cleanup-ANB -encoding=an ' +\
                      '-cleanWrapperSuffix=scrutinizerSafe -f -o $TARGET $SOURCE',
                  suffix = '_safeblock.bc',
                  src_suffix = '_unlinked-tmp1.bc')

    env_enc.Append(BUILDERS = {'SafeblockEndAN' : bld})

    bld = Builder(action='$LLVMLINK -f -o $TARGET $SOURCES',
                  suffix = '_unlinked.bc',
                  src_suffix = ['_unlinked-tmp1.bc', '_encConstant.bc'])

    env_enc.Append(BUILDERS = {'UnlinkedAN' : bld})


### ANB
    bld = Builder(action='$OPT -load=$SCRUTINIZERLIB -cleanup-ANB -encoding=anb ' +\
                      '-cleanWrapperSuffix=scrutinizerSafe -f -o $TARGET $SOURCE',
                  suffix = '_safeblock.bc',
                  src_suffix = '_unlinked-tmp1.bc')

    env_enc.Append(BUILDERS = {'SafeblockEndANB' : bld})



    bld = Builder(action='$OPT -load=$SCRUTINIZERLIB -insert-ANB -load=$ENCOPSLIB -anbA=$GLOBALA ' +\
                      '-anbsafeSuffix=$SAFE_FUNCTION_SUFFIX -signatureFile=$SIGNATURES ' +\
                      '-onlyCheckEvery=$CHECKFREQUENCY -queueLength=$QUEUELENGTH ' +\
                      '-anbWrapperSuffix=scrutinizerSafe -anbFuncPtrSetupFunc=___fptr_map_setup_fn_scrutinizerSafe ' +\
                      '-encodingKind=anb -anbDebug=$ANBDEBUG -encOpsLib=$ENCOPSLIB -randomSeed=12345 ' +\
                      '-use_AX=$USE_AX $SOURCE -f -o $TARGET -choosenSignatures=$CHOOSENSIGNATURES',
                  suffix = '_unlinked_tmp.bc',
                  src_suffix = '_gepisRemoved.bc')

    env_enc.Append(BUILDERS = {'UnlinkedANB' : bld})


## ANBDmem
    bld = Builder(action='$OPT -load=$SCRUTINIZERLIB -cleanup-ANB -encoding=anbdmem ' +\
                      '-cleanWrapperSuffix=scrutinizerSafe -f -o $TARGET $SOURCE',
                  suffix = '_safeblock.bc',
                  src_suffix = '_unlinked-tmp1.bc')

    env_enc.Append(BUILDERS = {'SafeblockEndANBDmem' : bld})


    bld = Builder(action='$OPT -load=$SCRUTINIZERLIB -prepareVersion -insert-ANB -load=$ENCOPSLIB -anbA=$GLOBALA ' +\
                      '-anbsafeSuffix=$SAFE_FUNCTION_SUFFIX -signatureFile=$SIGNATURES ' +\
                      '-onlyCheckEvery=$CHECKFREQUENCY -queueLength=$QUEUELENGTH ' +\
                      '-anbWrapperSuffix=scrutinizerSafe -anbFuncPtrSetupFunc=___fptr_map_setup_fn_scrutinizerSafe ' +\
                      '-encodingKind=anbdmem -anbDebug=$ANBDEBUG -encOpsLib=$ENCOPSLIB -randomSeed=12345 ' +\
                      '-use_AX=$USE_AX $SOURCE -f -o $TARGET -choosenSignatures=$CHOOSENSIGNATURES',
                  suffix = '_unlinked_tmp.bc',
                  src_suffix = '_gepisRemoved.bc')

    env_enc.Append(BUILDERS = {'UnlinkedANBDmem' : bld})


## ANB and ANBDmem

    bld = Builder(action = '$LLVMLINK -f -o $TARGET $SOURCES',
                  suffix = '_unlinked.bc',
                  src_suffix = ['_unlinked_tmp.bc', '_encConstants.bc', '_unlinked_tmp1.bc'])
    
    env_enc.Append(BUILDERS = {'UnlinkedEnd' : bld})




## Linking
    bld = Builder(action = 'llc $SOURCE',
                  suffix = '.s',
                  src_suffix = '.bc')
    env_enc.Append(BUILDERS = {'LLC' : bld})


    bld = Builder(action = '$LLVMLD -link-as-library -disable-internalize $SOURCE ' +\
                      '-o ${TARGET}.bc && llc -f ${TARGET}.bc && $CC -c ${TARGET}.s -o $TARGET',
                  suffix = '_an.o',
                  src_suffix = ['_unlinked.bc', '.bc'])
    env_enc.Append(BUILDERS = {'' : bld})


    bld = Builder(action = '$LLVMLD -link-as-library -disable-internalize $SOURCES ' +\
                      '-o ${TARGET}.bc && llc -f ${TARGET}.bc && $CC -c ${TARGET}.s -o $TARGET',
                  suffix = '_an.o',
                  src_suffix = ['_unlinked.bc', '.bc'])

    env_enc.Append(BUILDERS = {'LinkAN' : bld})

    bld = Builder(action = '$LLVMLD -link-as-library -disable-internalize -native  $SOURCES ' +\
                      '-o $TARGET',
                  suffix = '_anb.o',
                  src_suffix = ['_unlinked.bc', '.bc'])

    env_enc.Append(BUILDERS = {'LinkANB' : bld})

    bld = Builder(action = '$LLVMLD -link-as-library -disable-internalize -native  $SOURCES ' +\
                      '-o $TARGET',
                  suffix = '_anbdmem.o',
                  src_suffix = ['_unlinked.bc', '.bc'])

    env_enc.Append(BUILDERS = {'LinkANBDmem' : bld})

## Link to binary
    bld = Builder(action = '$LLVMLD -disable-internalize -native  $SOURCES ' +\
                      '$EXTRALIBS ' +\
                      '$BCLIBSAN ' +\
                      '$LIBDIRS $OTHERLIBS -o $TARGET',
                  suffix = '.an',
                  src_suffix = ['_unlinked.bc', '.bc'])

    env_enc.Append(BUILDERS = {'LinkItAN' : bld})

    bld = Builder(action = '$LLVMLD -disable-internalize -native  $SOURCES ' +\
                      '$EXTRALIBS ' +\
                      '$BCLIBSANB ' +\
                      '$LIBDIRS $OTHERLIBS -o $TARGET',
                  suffix = '.anb',
                  src_suffix = ['_unlinked.bc', '.bc'])

    env_enc.Append(BUILDERS = {'LinkItANB' : bld})

    bld = Builder(action = '$LLVMLD -disable-internalize -native  $SOURCES ' +\
                      '$EXTRALIBS ' +\
                      '$BCLIBSANBDmem ' +\
                      '$LIBDIRS $OTHERLIBS -o $TARGET',
                  suffix = '.anbdmem',
                  src_suffix = ['_unlinked.bc', '.bc'])

    env_enc.Append(BUILDERS = {'LinkItANBDmem' : bld})


###
    bld = Builder(action = 'anBChecker -file=$SOURCE -number=1000')
    env_enc.Append(BUILDERS = {'Signatures' : bld})


    ### Done return environment
    return env_enc



def __encode_an(env, target, files):
    env_enc = __create_environment(env)
    
    files = SCons.Util.flatten(files)   
    files = [File(file) for file in files]
    
    bcFiles = []
    srcFiles = []
    for file in files:
        if SCons.Util.splitext(file.name)[1] == '.bc':
            bcFiles.append(file)
        else:
            srcFiles.append(file)
            
    objs = bcFiles
    for srcFile in srcFiles:
        objs.append(env_enc.Object(SCons.Util.splitext(srcFile.name)[0]+'.bc', srcFile))
    objs = SCons.Util.flatten(objs)
 
    objs += ['#/build/deps/encoding/libztas_elib-an.bc']
    
    o = env_enc.RepLinked(target, objs)
    env_enc.Depends(o, '#/build/deps/encoding/libztas_elib-an.bc')
    o = env_enc.From64to32(target, o)
    o = env_enc.Cloned(target, o)
    o = env_enc.AllocaRemoved(target, o)
    o = env_enc.PreparedAN(target, o)
    o = env_enc.ConstantsRemoved(target, o)
    o = env_enc.GepisRemoved(target, o)
    gepis = o
    #env_enc.TestBeforeScrutinizing(target,o)
    o = env_enc.LoweringGeneratorStart(target, o)
    o = env_enc.LoweringGeneratorAN(target, o)
    o = env_enc.LoweringGeneratorEnd(target, o)
    o = env_enc.Object(target + '_encConstant.bc', o)
    enccon = o
    o = env_enc.SafeblockAN(target, gepis)
    o = env_enc.SafeblockCombine(target, o)
    o = env_enc.SafeblockEndAN(target, o)
    safeblock = o
    o = env_enc.UnlinkedAN(target, [enccon, safeblock])
#    o = env_enc.LinkItAN('run_' + target, [o])
    o = env_enc.LinkBC('lib' + target , [o,'#/build/deps/encoding/libztas_scrut-an.bc'], 
                       BCLIBS='$EXTRALIBS $BCLIBSAN $LIBDIRS $OTHERLIBS')

    env_enc.Depends(o, ['#/build/core/libzrunner-enc.a', '#/build/deps/encoding/libztas_wrapper-an.a'])
    bc = o
    o = env_enc.LLC(o)
    o = env_enc.Object(o)

    return [o, bc]



def __encode_anb(env, target, files):
    env_enc = __create_environment(env)
    env_enc.Append(CFLAGS='-DANB ')

    files = SCons.Util.flatten(files)   
    files = [File(file) for file in files]
    
    bcFiles = []
    srcFiles = []
    for file in files:
        if SCons.Util.splitext(file.name)[1] == '.bc':
            bcFiles.append(file)
        else:
            srcFiles.append(file)
            
    objs = bcFiles
    for srcFile in srcFiles:
        objs.append(env_enc.Object(SCons.Util.splitext(srcFile.name)[0]+'.bc', srcFile))    
    objs = SCons.Util.flatten(objs)
    
    objs += ['#/build/deps/encoding/libztas_elib-anb.bc']

    o = env_enc.RepLinked(target, objs)
    env_enc.Depends(o, '#/build/deps/encoding/libztas_elib-anb.bc')
    o = env_enc.From64to32(target, o)
    o = env_enc.Cloned(target, o)
    o = env_enc.AllocaRemoved(target, o)
    o = env_enc.PreparedANB(target, o)
    o = env_enc.ConstantsRemoved(target, o)
    o = env_enc.GepisRemoved(target, o)
    gepis = o
    # env_enc.TestBeforeScrutinizing(target,o)
    o = env_enc.LoweringGeneratorStart(target, o)
    o = env_enc.LoweringGeneratorANB(target, o)
    o = env_enc.LoweringGeneratorEnd(target, o)
    o = env_enc.Object(target + '_encConstant.bc', o)
    enccon = o

    o = env_enc.SafeblockANB(target, gepis)
    o = env_enc.SafeblockCombine(target, o)
    o = env_enc.SafeblockEndANB(target, o)
    safeblock = o
    
    # sig = File('#/encoding/Signatures')
    # env.Command(sig.abspath, sig.srcnode(), Copy("$TARGET", "$SOURCE"))
    # #env_enc.Signatures('Signatures', 'Signatures')

    o = env_enc.UnlinkedANB(target, safeblock)
    env_enc.Depends(o, File('#/deps/encoding/sig.conf'))
    
    o = env_enc.UnlinkedEnd(target, [o, enccon])

    # no scrut
    # o = env_enc.LinkBC('lib' + target, [o],
    #                    BCLIBS='$EXTRALIBS $BCLIBSANB $LIBDIRS $OTHERLIBS')
    # env_enc.Depends(o, ['#/build/encoding/libztas_scrut-anb.bc', '#/build/pods/core/libzproc-enc.a', '#/build/encoding/libztas_wrapper-anb.a'])

    o = env_enc.LinkBC('lib' + target, [o, '#/build/deps/encoding/libztas_scrut-anb.bc'],
    BCLIBS='$EXTRALIBS $BCLIBSANB $LIBDIRS $OTHERLIBS')
    
    env_enc.Depends(o, ['#/build/core/libzrunner-enc.a', '#/build/deps/encoding/libztas_wrapper-anb.a'])
    bc = o
    o = env_enc.LLC(o)
    o = env_enc.Object(o)

    return [o, bc]



def __encode_anbdmem(env, target, files):
    env_enc = __create_environment(env)
    env_enc.Append(CFLAGS='-DANBDmem ')

    files = SCons.Util.flatten(files)   
    files = [File(file) for file in files]
    
    bcFiles = []
    srcFiles = []
    for file in files:
        if SCons.Util.splitext(file.name)[1] == '.bc':
            bcFiles.append(file)
        else:
            srcFiles.append(file)
            
    objs = bcFiles
    for srcFile in srcFiles:
        objs.append(env_enc.Object(SCons.Util.splitext(srcFile.name)[0]+'.bc', srcFile))
        
    objs = SCons.Util.flatten(objs)
        
    objs += ['#/build/deps/encoding/libztas_elib-anbdmem.bc']
             
    o = env_enc.RepLinked(target, objs)
    env_enc.Depends(o, '#/build/deps/encoding/libztas_elib-anbdmem.bc')
    o = env_enc.From64to32(target, o)
    o = env_enc.Cloned(target, o)
    o = env_enc.AllocaRemoved(target, o)
    o = env_enc.PreparedANBDmem(target, o)
    o = env_enc.ConstantsRemoved(target, o)
    o = env_enc.GepisRemoved(target, o)
    gepis = o
    #env_enc.TestBeforeScrutinizing(target,o)
    o = env_enc.LoweringGeneratorStart(target, o)
    o = env_enc.LoweringGeneratorANB(target, o)
    o = env_enc.LoweringGeneratorEnd(target, o)
    o = env_enc.Object(target + '_encConstant.bc', o)
    enccon = o
    o = env_enc.SafeblockANBDmem(target, gepis)
    o = env_enc.SafeblockCombine(target, o)
    o = env_enc.SafeblockEndANBDmem(target, o)
    safeblock = o
    
    o = env_enc.UnlinkedANBDmem(target, safeblock)
    env_enc.Depends(o, File('#/deps/encoding/sig.conf'))

    o = env_enc.UnlinkedEnd(target, [o, enccon])
    # o = env_enc.LinkItANBDmem('run_' + target, [o])
    o = env_enc.LinkBC('lib' + target , [o, '#/build/deps/encoding/libztas_scrut-anbdmem.bc'], 
                       BCLIBS='$EXTRALIBS $BCLIBSANBDmem $LIBDIRS $OTHERLIBS')
    #o = env_enc.LinkANBDmem('../' + target, [o])

    env_enc.Depends(o, ['#/build/core/libzrunner-enc.a', '#/build/deps/encoding/libztas_wrapper-anbdmem.a'])
    bc = o
    o = env_enc.LLC(o)
    o = env_enc.Object(o)

    return [o, bc]



def Encode(env, target, files, encoding):
    if encoding == 'an':
        return __encode_an(env, target, files)
    elif encoding == 'anb':
        return __encode_anb(env, target, files)
    elif encoding == 'anbdmem':
        return __encode_anbdmem(env, target, files)
    else:
        assert False



### for extensions
def EncLibrary(env, target, files):
    enc = env.Clone()
    path = seppath + '/external/LLVM/llvm/bin'
    cpppath = seppath + '/build/include'
    enc.Append(CPPPATH = cpppath)
    enc.AppendENVPath('PATH', path)
    enc.Replace(CC='llvm-gcc')
    enc.Replace(CFLAGS='-std=gnu89 -emit-llvm -Wall -O0 -DENCODED ')
    enc.Replace(CCFLAGS='')
 #   enc.Append(CPPPATH = ['#/pods/core'])

    files = SCons.Util.flatten(files)   
    files = [File(file) for file in files]

    objs = []
    for file in files:
        objs.append(enc.Object(SCons.Util.splitext(file.name)[0]+'.bc', file))
    objs = SCons.Util.flatten(objs)

    #objs = [enc.Object(f.split('.c')[0] + '.bc', f) for f in files]

    
    bld = Builder(action = '$LLVMLINK $SOURCES -f -o $TARGET',
                  prefix = 'lib',
                  suffix = '.bc',
                  src_suffix = '.bc')

    enc.Append(BUILDERS = {'LinkBC' : bld}, LLVMLINK = 'llvm-link')
    return enc.LinkBC(target, objs)



def generate(env):
    if not exists(env):
        return
    
    env.AddMethod(ObjectBC_enc)
    env.AddMethod(LinkBC_enc)
    env.AddMethod(Encode)
    env.AddMethod(EncLibrary)
    
    env.Append(LIBPATH = '#/build/deps/encoding')
    
    if os.environ.has_key('SEPPATH'):
        seppath = os.environ['SEPPATH']
        env.Append(LIBPATH = seppath + '/build/lib')
    else:
        assert False
    
    SConscript('#/deps/encoding/SConscript', duplicate=False, variant_dir='#/build/deps/encoding/', exports='env')
    

def exists(env):
    if GetOption('encoding') == None:
        return False
    
    try:
        llvm_config('--version')
    except Exception:
        return False

    if not seppath:
        return False
    
    encPath = os.path.abspath('./deps/encoding/')
    if os.path.exists(encPath):
        return True
    
    return False
