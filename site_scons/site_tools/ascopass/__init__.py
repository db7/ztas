# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
import os

from SCons.Script import *
import SCons

# _null helper stolen from SCons to 
# identify not-passed param values (that are not None)
_null = SCons.Environment._null

_ascoPath = 'deps/asco'

_libPath = ['#/' + _ascoPath + '/target/pass', '#/' + _ascoPath + '/target/support']


def createCustomComstrAction(action, comstrVariable):
    """
    This method creates an action based on the action string
    and an environment variable which is used for output when 
    ASCO_DEBUG is False.
    When the action is called with ASCO_DEBUG=True, the action's
    string will be printed, otherwise the environment variable's string.
    """
    assert isinstance(action, str), "expecting string action"
    assert isinstance(comstrVariable, str), "expecting string COMSTR"
    
    def printComstr(target, source, env):
        if env['LLVM_DEBUG']:
            return env.subst(action, target=target, source=source)
        else:
            return env.subst(comstrVariable, target=target, source=source)    
    
    
    return SCons.Action.Action(action, printComstr)   

def __getFromKwargs(kwargs, key, default):
    if key in kwargs:
        value = kwargs[key]
        del kwargs[key]
        return value
    else:
        return default

def _prepareAndHarden(env, target, sources, **kwargs):
    """
    Builder that links all links the bcs sources into a file and hardens everything
    with AscoPass.
    Recognized additional keyword arguments:
        optargs: specifies compiler optimization (any opt option can be passed), default `-inline`.
                Be careful with -O3, as this might remove ztas_init etc, since ztas_proc is not linked in yet.
        runtimelib: specifies the path to the run time library after hardening, default is $ASCO_SUPPORTLIB_PATH/asco.
        bridge: the layer used to bridge ztas to asco. Default is the one in layers/asco_bridge
        instrumentopt: activates the optimization when hardening. This is turned OFF by default for stability reasons.
        hardenargs: additional arguments when hardening the file. See AscoPass readme for possible options.
    """
    
    
    targetName = str(target)
    
    if len(targetName) == 0:
        raise SCons.Errors.UserError('Need target name')
    
    # extract all source line endings
    srcEndings = tuple(set([os.path.splitext(str(f))[1] for f in SCons.Util.flatten(sources)]))
    if len(srcEndings) == 0:
        raise SCons.Errors.UserError("No source files specified to harden.")
    
    if len(srcEndings) > 1:
        print tuple(srcEndings)
        raise SCons.Errors.UserError(("The source files have a different types %s .\n" + 
                    "Need either C-suffixes %s or bc-suffixes %s") % (str(srcEndings), env['CPPSUFFIXES'], env['LLVM_BC_SUFFIX']))
        
    assert len(srcEndings) == 1
    
    
            
    compiledSources = sources
    if srcEndings[0] in env['CPPSUFFIXES']:
        # let's use an env copy
        env = env.Clone()
        
        # append some flags.
        # ztas_bridge must not use ztas_size_t, otherwise the signatures 
        # of malloc and __asco_instrumented_malloc will not match.
        env.Append(CCFLAGS=['-DUSE_ASCO', '-D_ZTAS_NO_SIZE_T'], CPPPATH=[env['ASCO_CPPPATH']])
        compiledSources = env.LlvmBc(sources, **kwargs)
        
    
        
        
    optargs = __getFromKwargs(kwargs, 'optargs', '-inline')
    instrumentopt = __getFromKwargs(kwargs, 'instrumentopt', False)
    ascoBridge = __getFromKwargs(kwargs, 'bridge', env.Registry.collectFiles('asco_bridge'))
        
    linklibs = [env['ASCO_SUPPORTLIB_PATH'] + 'asco_interface']
    linklibs.append(__getFromKwargs(kwargs, 'runtimelib', env['ASCO_SUPPORTLIB_PATH'] + 'asco'))
    
                
    bcLinked = env.LlvmLink(targetName + '_linked', compiledSources + [env['ASCO_SUPPORTLIB_PATH'] + 'asco_mates', ascoBridge])
    
    args = ['-analysisfile=' + str(File(targetName + '.log')),
            '-no-inst-opt' if not instrumentopt else '']
    
    # extend the array with what we got from the client
    args += SCons.Util.flatten(__getFromKwargs(kwargs, 'hardenargs', []))
    
    bcAsco = env.LlvmOpt(targetName + '_asco',
                             bcLinked,
                             library=env['ASCO_PASS_LIB'],
                             passes='asco',
                             args=' '.join(args))
    
    bcAscoLinked = env.LlvmLink(targetName + '_ascolinked', bcAsco + linklibs)
    if len(optargs) == 0:
        env.Depends(bcAscoLinked, compiledSources)
        env.Depends(bcAscoLinked, ascoBridge)
        return bcAscoLinked
    else:
        bcAscoLinkedOpt = env.LlvmOpt(targetName, bcAscoLinked, args=optargs)
        env.Depends(bcAscoLinkedOpt, compiledSources)
        env.Depends(bcAscoLinkedOpt, ascoBridge)
        return bcAscoLinkedOpt
    

# Builder to instrument every function in a module
_asco_instrument_module = SCons.Builder.Builder(
        action=createCustomComstrAction('$LLVM_OPT -load=$ASCO_PASS_LIB -asco -instrumentmodule < $SOURCE > $TARGET', '$LLVM_OBJ_COMSTR'),
        src_suffix='$LLVM_BC_SUFFIX',
        suffix='$LLVM_BC_SUFFIX',
        source_scanner=SCons.Defaults.ProgScan)

def generate(env):
    if not exists():
        return
    
    env.SetDefault(ASCO_CPPPATH='#/' + _ascoPath + '/include',
                   ASCO_SUPPORTLIB_PATH='#/' + _ascoPath + '/target/support/',
                   ASCO_DEBUG=False, # specify how the comstr looks like
                   ASCO_PASS_LIB=File('#/' + _ascoPath + '/target/pass/libAscoPass.so'),
                   ASCO_MODINST_COMSTR='Instrumenting module $SOURCE > $TARGET')
    
    env.AddMethod(_prepareAndHarden, 'AscoHarden')
    env.Append(BUILDERS={'AscoInstrument':_asco_instrument_module
                         }
               )
    
    
def exists(env=None):
    if not os.path.exists(_ascoPath):
        print "AscoPass-Tool does not exist"
        return False
    
    if not os.path.exists(os.path.join(_ascoPath, 'target/pass')):
        print "Dependency asco exists but is not built. Go to deps/asco and run scons."
        return False

    return True
