# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
from SCons.Script import *
import sys, os

def install(env):
    """
    Installs the build tools on the current environment.
    """
        
    AddMethod(env, buildLocal)
    
    AddMethod(env, existTools)
    
def __localTarget(target):
    """
    Helper function returning the filename
    of a target node.
    """
    
    basename = os.path.basename(str(target))
    (filename, _) = os.path.splitext(basename)
    if len(filename) == 0:
        raise AttributeError("Filename is empty")
    
    return filename

def buildLocal(env, builder, sources, *args, **kwargs):
    """
    Builds the sources using the passed builder in the directory
    of the executing build file.
    This is sometimes necessary if one wants to build a bunch of files
    in the source directory without specifying a target name for each
    source.
    """
    
    results = []
    if not isinstance(sources, list):
        sources = [sources]
        
    assert 'target' not in kwargs and 'source' not in kwargs, "the keywords source and target must not be used for the local builder."
        
    for source in sources:
        result = builder(target=__localTarget(source), source=source, *args, **kwargs)
        if isinstance(result, list):
            results.extend(result)
        else:
            results.append(result)
            
    return results

def existTools(env, *tools):
    """
    Checks if the required tools exist, returns False if at least one
    tool is missing. True otherwise.
    """
    
    missing = []
    for tool in tools:
        if tool not in env['TOOLS']:
            missing.append(tool)
            return False
    else:
        return True
