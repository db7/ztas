# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
"""
Helper functions to produce bc files
"""
import buildtools

def generate(env):
    buildtools.install(env)
    

def exists(env=None):
    return True
