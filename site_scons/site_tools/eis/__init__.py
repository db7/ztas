# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
"""
Helper functions to produce bc files
"""
from SCons.Script import *
import SCons
import sys, os

from llvm import llvm_config

_version = None

def _generate_version2(env):
    """ LLVM 2.6 and old evaluator """
    bld = Builder(action = '$INSTRUMENT --mode $FAULT_TYPE --triggerFrequency 1 $SOURCE $TARGET',
                  suffix = '.${FAULT_TYPE}.bc',
                  src_suffix = '.bc')

    env.Append(BUILDERS = {'InjectFaults' : bld},
               INSTRUMENT = File('#/deps/silistra/evaluator/bin/fault_injector'))

def _generate_version3(env):
    """ LLVM 3.2 """

    bld = Builder(action = '$INSTRUMENT -$FAULT_TYPE ' +
                  '-triggerAlways ' +
                  '$SOURCE $TARGET',
                  suffix = '.${FAULT_TYPE}.bc',
                  src_suffix = '.bc')

    env.Append(BUILDERS = {'InjectFaults' : bld},
               INSTRUMENT = File('#/deps/silistra/evaluator3/build/tools/injector/injector'))

    if not env.has_key('LIBS') or 'EISRuntime' not in env['LIBS']:
        env.Append(LIBPATH = Dir('#/deps/silistra/evaluator3/build/runtime'))
        env.Append(LIBS = ['EISRuntime'])

def EisClone(env):
    env = env.Clone()
    if '3.' in _version:
        #env.AddMethod(_generate_version3, 'EisClone')
        _generate_version3(env)
    else:
        #env.AddMethod(_generate_version2, 'EisClone')
        _generate_version2(env)
    return env

def generate(env):
    global _version

    if not exists(env):
        return
    
    _version = llvm_config('--version') ## no throw (since exists check it)
    env.AddMethod(EisClone, 'EisClone')

def exists(env):
    try:
        llvm_config('--version')
    except Exception:
        return False

    ## To check for tools, one option seems
    ##
    ## import SCons.Tool
    ## tool = SCons.Tool.FindTool(['eis'], env)
    ## tool == None if exists returned 0
    ev3 = File('#/deps/silistra/evaluator3/build/tools/injector/injector')
    if os.path.exists(str(ev3)):
        return True

    ev  = File('#/deps/silistra/evaluator/bin/fault_injector')
    if os.path.exists(str(ev)):
        return True
    
    return False
