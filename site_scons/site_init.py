# -*- python -*-

# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------

"""
 Scripts in this path will automatically
 added to the environment prior to calling any build scripts.
 http://www.scons.org/doc/2.1.0/HTML/scons-user/x3713.html
""" 

from SCons.Script import *

import subprocess

_targetName='update-tools'

def ExternalTools(tools):
    """
    Checks if tools exist or attempt to update/checkout them if the --update-dependencies option
    is specified.
    
    @param tools: all tools to be used as a list of tuples (name, url, revision)
             If revision is None or 'HEAD', the head will be used.
             The revision needs to be specified using the changeset-ID!
             
    @return: a list of tool names that can be used in the environment constructor.
    """
    
    # from tool list, extract the names only
    toolNames = [tool[0] for tool in tools]
    
    if _targetName in COMMAND_LINE_TARGETS:
        print "=============================\nUpdating Tool Dependencies\n============================="
        siteToolsDir = Dir('#/site_scons/site_tools/').abspath
        print "Tools will be installed in ", siteToolsDir
        for (tool, url, revision) in tools:
            toolDir = os.path.join(siteToolsDir, tool)
            if not revision:
                revision = 'HEAD'
                
            revisionArgs = ['--rev', revision] if revision != 'HEAD' else []
            if os.path.exists(toolDir):
                print " =>Tool", tool, "exists, attempt to update:"
                subprocess.call(['hg', 'pull', '-R', toolDir])
            else:
                print " => Tool", tool, "does not exist, will check it out from ", url
                subprocess.call(['hg', 'clone', url, toolDir])
            
            print " => Updating tool", tool, "to revision", revision
            subprocess.call(['hg', 'update'] + revisionArgs + ['-R', toolDir])
            print " => hg summary says:"
            subprocess.call(['hg', 'summary', '-R', toolDir])
                
        print "=============================\nDone updating dependencies\n============================="
        
        print "Updating project finished."
        exit(0)
    else:
               
        try:
            for toolName in toolNames:
                Tool(toolName)
        except:
            print ('WARNING: SCons tools missing. Run `scons %s` to update the external tools.' % _targetName)
            exit(1)
    
    return toolNames
