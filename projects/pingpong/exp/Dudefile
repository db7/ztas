# -*- python -*-

# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------

                     
dude_version = 3
from dude.defaults import *
import muddi
import os
import glob
import sys
import struct

############################
##  Configuration          #
############################

## experiment variables

optspace = {
    'processes' : [1, 2, 3]
}

## experiment dir
deploy_dir  = '/tmp/exp'
logdir      = 'logs'
build_dir   = os.path.abspath(root + "/../../../build")

## algorithm (program directory, and binary)
algorithm_dir  = build_dir + '/algorithms/ping-pong'
algorithm      = 'run_ping-pong'

## hosts
hosts = ['localhost']

############################
##  Execution              #
############################

def prepare_global():
    """Upload binaries. Run before any experiment is started."""

    # files to upload
    files = [algorithm_dir + '/' + algorithm]

    # get unique set of hosts
    uhosts = set(hosts)

    # prepare list of hosts and files
    up = [(host, files, deploy_dir) for host in uhosts]

    # configure muddi to upload files (logs in prepare directory)
    cfg = muddi.conf([], up, [], logdir = "prepare")

    # check and create if necessary deploy_dir on the remote end
    muddi.check(cfg)

    # upload files
    r = muddi.upload(cfg)

    # if files were not uploaded, abort
    assert r == 0

def fork_exp(optpt):
    """Start the processes. Run in a python child process."""

    # create list of host:port
    host_str = [hosts[i%len(hosts)] + ':' + str(2000 + i) for i in range(0, optpt['processes'])]
    host_str = ' '.join(host_str)

    # create commands to be executed of the form: "deploy_dir/algorithm pid port host_str"
    # commands a 4-tuple (host where command is executed, command, term_command, kill_command)
    # NOTE: ping-pong processes should have a timeout, which calls
    # ztas_term() otherwise processes do not finish.

    procs = [
        (hosts[i%len(hosts)], deploy_dir + '/' + algorithm +\
             (' %d %d ' % (i, 2000 + i)) + host_str,
         None, 'killall -9 ' + algorithm)
        for i in range(0, optpt['processes'])
        ]

    # configure muddi
    cfg = muddi.conf(procs, [], [], logdir = logdir)

    # execute remote commands and assert
    assert muddi.execute(cfg) == 0


############################
##  Summaries              #
############################

import dude.summaries
def f(line):
    """Helper function to generate output files based on the terminal
    output of the programs. Convert timestamps to readable format and
    output 4 columns: src, dst, rtt, time."""

    ## a RTT line looks like
    # PROCESS(2): RTT to 2 rtt.hgh = 0 rtt.low = 491000 time.hgh = 307508997 time.low = 4164930888

    # take src
    src = line.split('(')[1].split(')')[0]

    # take dst
    dst = line.split('RTT to ')[1].split(' ')[0]

    # take high and low of rtt
    high, low = line.split('rtt.')[1:]
    
    # clean high and low and convert to int
    high = high.split('=')[1]
    high = int(high)
    low = low.split('=')[1].split('\t')[0]
    low = int(low)
    
    # convert high,low to 64bit integer (nanoseconds)
    rtt = ( high << 32 | low )
    
    # transform in microseconds (us)
    rtt = rtt/1000

    # take high and low of time
    high, low = line.split('time.')[1:]
    # clean high and low and convert to int
    high = high.split('=')[1]
    high = int(high)
    low = low.split('=')[1]
    low = int(low)
    
    # convert high,low to 64bit integer (nanoseconds)
    t = ( high << 32 | low )
    
    # transform in microseconds (us)
    t = t/1000

    
    # return string src dst rtt time
    return "%s %s %d %d" %(src, dst, rtt, t)

s = dude.summaries.FilesLineSelect (
    name   = 'rtt',
    files  = 'logs/execution-*',
    regex  = '.*RTT.*',
    split  = f,
    header = "src dst rtt time",
    fname_split = lambda fname: fname.split('.')[0].split('execution-')[1]
    )

## list of summaries
summaries = [s]
