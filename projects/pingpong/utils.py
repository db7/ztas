# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
"""
Utils to be shared among experiments.
"""

import os

def printExperimentErrors(subdir=None):
    """
    Search the given subdirectory (or the current directory) for the
    experiment's .errput-Files and prints them.
    """
    
    curDir = os.getcwd()
    print "Experiment Errors"
    print "--------------------------"
    if subdir:
        curDir += os.sep + subdir
        
    for fileName in os.listdir(curDir):
        if os.path.splitext(fileName)[1] in ['.errput']:
            print "Content of ", fileName
            with open(curDir + os.sep + fileName, 'r') as f:
                print f.read()
