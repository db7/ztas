#include <stdio.h>
#include <stdlib.h>
#include <asco_interface.h>

#define ATOI_MAXLEN 64

/** Some instrumented stdlib definitions we're going to use */
char * __asco_instrumented_strncpy(char *dst, const char *src, size_t n);

/**
 * atoi wrapper fetching the string form asco land and then
 * calling the original atoi.
 */
int __asco_local_message_atoi(const char *str) {
    char buffer[ATOI_MAXLEN + 1];
    __asco_instrumented_strncpy(buffer, str, ATOI_MAXLEN);
    return atoi(buffer);
}

/**
 * fflush just proxying
 */
int __asco_local_message_fflush(FILE *f) {
    return fflush(f);
}
