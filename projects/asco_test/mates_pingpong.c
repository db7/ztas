#include <stdio.h>
#include <stdlib.h>
#include <asco_interface.h>

#define ATOI_MAXLEN 64

/**
 * fflush just proxying
 */
int __asco_local_message_fflush(FILE *f) {
    return fflush(f);
}
