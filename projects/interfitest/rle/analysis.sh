#!/bin/bash

showLogs() {
find . -type f -name 'execution*' -print | while read filename; do
    echo "$filename"
    cat "$filename"
done
}

# $1 = target
# $2 = trigger
debugExp() {
    echo "running experiment with target=$1, trigger=$2"
    gdb --args ../ztas_playerinterpreter 0 raw/exp__target$1__trigger$2/config.ini
}