# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
import argparse
import ConfigParser
import os
import subprocess
import sys

sys.path.append('..')
from scaffold_interfitest import interfitest

parser = argparse.ArgumentParser()
parser.add_argument('--gdb', help='run the test within gdb', action="store_true")
args = parser.parse_args()


_analysisIni = 'analysis.ini'
processes = 4
halt_after = 5


def writeAnalysisConfig():
    
    parser = ConfigParser.ConfigParser()
    parser.add_section('main')
    procList = ','.join([str(i) for i in range(processes)])
    parser.set('main', 'processes', procList)
    parser.set('main', 'halt_after', halt_after)
    
    parser.add_section('rle')
    parser.set('rle', 'processes', procList)
    
    for process in range(processes):
        interfiArgs = [
        '-analysis=triggers,targets,target_dump,profile',
        '-rand-generation=10',
        '-analyse-trigger-location=ztas_trig',
        '-injection-log=injection%d.log' % process,
        '-analysis-file=Analysis%d.py' % process
        ]
        
        section = 'p%d' % process
        parser.add_section(section)
        parser.set(section, 'host', 'localhost')
        parser.set(section, 'port', '300%d' % process)
        parser.set(section, 'interfi_binary', interfitest.getFile('rle_check_leader.bc'))
        parser.set(section, 'interfi_args', ' '.join(interfiArgs))
        parser.set(section, 'trace', 'trace%d.log' % process)
    
    parser.write(open(_analysisIni, 'w'))

writeAnalysisConfig()

call = [interfitest.getFile('ztas_loggerinterpreter'), '0', '1', '2', '3', '-f', _analysisIni]
if args.gdb:
    call = ['gdb', '--args'] + call
print "calling", ' '.join(call)
returnVal = subprocess.call(call)
if returnVal:
    print "Error executing the testrun!"

exit(0)
print "and replay number 0 again!"
call = [interfitest.getFile('ztas_playerinterpreter'), '4', _analysisIni]
subprocess.call(call)

