# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
import argparse
import ConfigParser
import os
import subprocess
import sys

sys.path.append('..')
from scaffold_interfitest import interfitest

ini = 'pingpong.ini'

def writeAnalysisConfig():
    processes = 6
    procList = ','.join([str(i) for i in range(processes)])
    options = {'main': 
                    {'processes': procList,
                     'halt_after': 5
                     },
               'rle':
                    {
                     'processes': procList
                     }
               }
    
    for process in range(processes):
        interfiArgs = [
        '-analysis=triggers,targets,target_dump',
        '-rand-generation=10000',
        '-injection-log=injection%d.log' % process,
        '-analysis-file=Analysis%d.log' % process
        ]
        options['p%d' % process] = {
                     'host':'localhost',
                     'port':'300' + str(process),
                     'interfi_binary':interfitest.getFile('ping-pong.bc'),
                     'interfi_args':' '.join(interfiArgs),
                     'trace':'trace%d.log' % process
                     }
    
    
    parser = ConfigParser.ConfigParser()
    for section in options:
        parser.add_section(section)
        for (key, value) in options[section].items():
            parser.set(section, key, value)
    
    with open(ini, 'w') as f:
        parser.write(f)
    
writeAnalysisConfig()


parser = argparse.ArgumentParser()
parser.add_argument('--gdb', help='run the test within gdb', action="store_true")
args = parser.parse_args()

call = [interfitest.getFile('ztas_loggerinterpreter'), '0', '1', '2', '3', '4', '5', '-f', ini]
if args.gdb:
    call = ['gdb', '--args'] + call
print "calling", call
subprocess.call(call)

