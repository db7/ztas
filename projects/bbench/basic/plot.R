## ---------------------------------------------------------------------
## Copyright (c) 2013 Technische Universitaet Dresden
## Distributed under the MIT license. See accompanying file LICENSE.
## ---------------------------------------------------------------------
library(ggplot2)
library(plyr)

read.table('/tmp/bbench/basic/output/results', header = T) -> df
df <- ddply(df, c('processes', 'req_period', 'req_size', 'res_size', 'waitfor'),
            function(x) {
              y <- x[x$i > 100,]
              return (y)
})

ggplot(df, aes(x=i)) +
  geom_line(aes(y = first), colour = "red") +
  geom_line(aes(y = last) , colour = "blue") +
  geom_line(aes(y = wf) , colour = "grey")
