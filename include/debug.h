/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _DEBUG_H_
#define _DEBUG_H_

#ifdef DEBUG
#include <stdio.h>

#define DLOG(...) do { printf("[%s:%d] DEBUG: ", __FILE__, __LINE__); printf(__VA_ARGS__); } while(0)
#define WLOG(...) do { printf("[%s:%d] WARNING: ", __FILE__, __LINE__); printf(__VA_ARGS__); } while(0)
#define ELOG(...) do { printf("[%s:%d] ERROR: ", __FILE__, __LINE__); printf(__VA_ARGS__); } while(0)
#else
#define DLOG(...)
#define WLOG(...)
#define ELOG(...) do { printf("[%s:%d] ERROR: ", __FILE__, __LINE__); printf(__VA_ARGS__); } while(0)
#endif

#endif
