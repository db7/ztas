/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

/**
 * \file  ztas.h
 * \brief ztas basic interface

 * @mainpage the main page
 * Welcome to ztas!
 * Ztas is a framework for developing distributed algorithms
 * @section core
 * Some modules of the core (to be continued sometime :) )
 * <ul>
 *  <li>\ref fi_sec </li>
 *  <li>\ref interface_sec </li>
 *  <li>\ref replayer_sec </li>
 * </ul>
 * @section sec_alg Algorithms
 * Some algorithms:
 * <ul>
 *  <li>\ref alg_pingpong</li>
 *  <li>\ref alg_lead_election</li>
 *  <li>\ref alg_relch</li>
 *  <li>\ref alg_clocksync</li>
 *  <li>\ref alg_fd</li>
 * </ul>
 * @section  sec_ext Extensions
 * The extensions section provides modules that are extending the core,
 * i.e. they sit mostly between the core and the algorithms.
 * <ul>
 *  <li>\ref ext_ct</li>
 *  <li>\ref ext_ztax</li>
 * </ul>
 *
 *
 */


#ifndef _ZTAS_H_
#define _ZTAS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <ztas/types.h>
#include <ztas/ztime.h>
#include <ztas/ztdur.h>
#include <ztas/flags.h>

/**
 * \brief send a message consisting of `data` array with `size` bytes to `dst`.
 * Return ZTAS_UCAST_OK, ZTAS_UCAST_FULL, ZTAS_UCAST_DSTERR, (ZTAS_UCAST_ERROR)
 * flags == 0 or ZTAS_CAST_DEFAULT uses TCP
 * flags == ZTAS_CAST_UMSG uses unreliable unordered datagram service (UDP)
 */
int  ztas_ucast(int32_t dst, const void* data, size_t size, int flags);

/**
 * \brief send a message consisting of `data` arry with `size` bytes
 * to all known processes including itself.
 * flags == 0 or ZTAS_CAST_DEFAULT uses TCP
 * flags == ZTAS_CAST_UMSG uses unreliable unordered datagram service (UDP)
 */
int  ztas_bcast(const void *data, size_t size, int flags);

/**
 * \brief set an alarm identified by `aid` to be triggered in `ms` milliseconds.
 */
int  ztas_alarm(int32_t aid, uint32_t ms);

/**
 * \brief schedule an alarm identified by `aid` to be triggered at `at` time.
 */
int  ztas_sched(int32_t aid, const ztime_t* at);

/**
 * \brief read the current clock value in `ts` variable.
 */
void ztas_clock(ztime_t* ts);

/**
 * \brief terminate the process.
 */
void ztas_term (void);


/**
 * \brief read the value of a key from the configuration object.
 * Read the value of a key from the configuration object.  Key is of
 * the form "section:field". If key does not exist, NULL string is
 * returned.
 */
const char* ztas_cread(const char* key);

/** \brief Called once the process is initialized.
 * The user is expected to implement this method. */
void* ztas_init(int32_t pid);

/** \brief Called once the process is finished.
 * The user is expected to implement this method.
 */
void  ztas_fini(void* state);

/** \brief Called once a message is received.
 * The user is expected to implement this method.
 */
void  ztas_recv(void* state, const void* data, size_t size);

/** \brief Called once a previously scheduled alarm is triggered.
 *
 * The user is expected to implement this method.
 */
void  ztas_trig(void* state, int32_t aid);

#ifdef __cplusplus
}
#endif

#endif /* _ZTAS_H_ */
