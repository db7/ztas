/* ----------------------------------------------------------------------
 * Copyright (c) 2012,2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _ZTDUR_H_
#define _ZTDUR_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <ztas/ztime.h>

#define ZTDUR_SET(a, b) {(a).low = (b).low; (a).high = (b).high;}


#define ZTDUR_GT(a, b) (ZTDUR_TS(a) > ZTDUR_TS(b))



/**
 * duration of time period between a and b
 * @param a start of period, can be ztime or ztdur
 * @param b end of period, can be ztime or ztdur
 * @param r duration, can be ztime or ztdur.
 *
 * If ztime is provided as result, make sure that the duration
 * cannot be negative, as ztime doesn't contain negative values.
 *
 * @todo test this function, may not be working with signed values!
 */
#define ZTDUR_DUR(a, b, r){													\
			int64_t tsA = ZTDUR_TS(a);											\
			int64_t tsB = ZTDUR_TS(b);											\
			ZTDUR_SET64(r, (tsB-tsA));											\
    }


/**
 * refactor to 32-bit calculations
 */
#define ZTDUR_ADD(a, b, r){													\
			int64_t tsA = ZTDUR_TS(a);											\
			int64_t tsB = ZTDUR_TS(b);											\
			ZTDUR_SET64(r, (tsB+tsA));											\
    }

/**
 * refactor to 32-bit calculations
 */
#define ZTDUR_MINUS(a, b, r){													\
			int64_t tsA = ZTDUR_TS(a);											\
			int64_t tsB = ZTDUR_TS(b);											\
			ZTDUR_SET64(r, (tsA - tsB));											\
    }

#define ZTDUR_SETMS(a, ms){														\
			int64_t ns = ((int64_t)(ms)*MS);									\
			ZTDUR_SET64(a, ns);													\
	}

#define ZTDUR_GTZ(a) ( ZTDUR_TS(a) > (int64_t)0 )

#define ZTDUR_ZERO(a) {(a).low = 0; (a).high = 0;}

#define ZTDUR_DIV2(a) {int64_t tmp = ZTDUR_TS(a); 								\
				ZTDUR_SET64(a, tmp/(int64_t)2) 									\
				}


#define ZTDUR_IS_ZERO(a) ((a).low == 0 && (a).high == 0)

#define ZTDUR_LOG(a) LOG1("%"PRId64, ZTDUR_TS(a))

#define ZTDUR_ABS(a, b) {int64_t ts = ZTDUR_TS(a);						\
                        ZTDUR_SET64(b, labs(ts));						\
                        }

#ifdef __cplusplus
}
#endif



#endif /* _ZTDUR_H_ */
