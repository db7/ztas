/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _ZTAS_STRING_H_
#define _ZTAS_STRING_H_
#include <ztas/types.h>

#ifndef ENCODED
# include <string.h>
# include <strings.h>
#else

#include <ztas/types.h>
/* string.h should not be included */
#  define _STRING_H 1 

void* dmemcpy(void* dst, const void* src, uint32_t size);
int   dstrlen(const char *s);
void  dbzero (void *s, size_t n);
//// int dstrcmp (const char * s1, const char * s2);

#define memcpy dmemcpy
#define strlen dstrlen
#define bzero  dbzero

#endif

#endif /* _ZTAS_STRING_H_ */
