/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _VECTOR_H_
#define _VECTOR_H_
#include <stdint.h>

typedef struct {
    int32_t max_size;
    int32_t size;
    int32_t array[];
} vector_t;

vector_t* vector_init(int32_t max_size);
vector_t* vector_init_copy(vector_t* vector);
void vector_fini(vector_t* vector);
void vector_clear(vector_t* vector);
int vector_push(vector_t* vector, int32_t value);
int vector_has(vector_t* vector, int32_t value);
void vector_sort(vector_t* vector);
const int32_t* vector_array(vector_t* vector);
int32_t vector_size(vector_t* vector);

#define VECTOR_OK 1
#define VECTOR_NOK 0
#define VECTOR_FULL -1
#endif
