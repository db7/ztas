/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 * \file   tree.h
 * \author diogo
 * \date   
 * \brief  A tree
 *
 * Detailed description of file.
 */
#ifndef _TREE_H_
#define _TREE_H_

#include <stdint.h>

/**
 * @name    Example API Actions
 * @brief   Example actions available.
 * @ingroup example
 *
 * This API provides certain actions as an example.
 *
 * @param [in] repeat  Number of times to do nothing.
 *
 * @retval TRUE   Successfully did nothing.
 * @retval FALSE  Oops, did something.
 *
 * Example Usage:
 * @code
 *    example_nada(3); // Do nothing 3 times.
 * @endcode
 */


/** \brief tree_t should be always used as a pointer */
typedef struct tree tree_t;

tree_t* tree_init(void);
void    tree_fini (tree_t* t);

/**
 * Returns NULL if put was ok, otherwise returns the current value
 * with the same index(key).
 */
void*   tree_put  (tree_t* t, int32_t key, void* value);
void*   tree_get  (tree_t* t, int32_t key);
void*   tree_pop  (tree_t* t, int32_t key);

/**
 * del removes the node from the tree and calls free() on the value
 */
void    tree_del  (tree_t* t, int32_t key);
void*   tree_popdf(tree_t* t);
void*   tree_pop_top(tree_t* t);

/* iterator */
typedef struct item tree_it_item_t;
typedef struct {
    tree_it_item_t* item;
    tree_t* tree;
    int32_t key;
} tree_it_t;



void    tree_it_init(tree_t* set, tree_it_t* it);

/**
 * \code
 *   tree_it_t it;
 *   tree_it_init(tree, &it);
 *   int32_t val;
 *   while ((val = tree_it_next(&it))) print(val);
 * \endcode
 */
void* tree_it_next(tree_it_t* it);

int32_t tree_it_key(tree_it_t* it);

#endif /* _TREE_H_ */
