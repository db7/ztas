/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 * A lock-free queue for 1 producer and 1 consumer.
 *
 * First appeared in L. Lamport. Specifying concurrent program
 * modules. ACM Transactions on Programming Languages and Systems,
 * 5(2):190–222, 1983.
 *
 * I added a top and a size method. - diogo
 */

#ifndef _QUEUE_H_
#define _QUEUE_H_
#include <stdint.h>

typedef struct {
    void** buffer;
    volatile int32_t size;
    volatile int32_t head;
    volatile int32_t tail;
} queue_t;


queue_t* queue_create(int32_t size);
int32_t      queue_enq(queue_t* q, void* i);
void*    queue_deq(queue_t* q);
void*    queue_top(queue_t* q);
void     queue_pop(queue_t* q);
int32_t      queue_empty(queue_t* q);
void     queue_free(queue_t* q);
int32_t      queue_size(queue_t* q);
int32_t      queue_fsize(queue_t* q);

#define QUEUE_OK     0
#define QUEUE_FULL  -1
#define QUEUE_EMPTY  1
#define QUEUE_NEMPTY 0

#endif
