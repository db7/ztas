/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _IDSET_H_
#define _IDSET_H_

#include <stdint.h>

typedef struct idset idset_t;

idset_t* idset_init(int32_t min, int32_t N);
idset_t* idset_init_copy(const idset_t* set);
idset_t* idset_init_str(const char* list);
void     idset_copy(idset_t* dst, const idset_t* set);
void     idset_fini(idset_t* set);
void     idset_clear(idset_t* set);

int32_t  idset_size(const idset_t* set);
void     idset_add (idset_t* set, int32_t id);
void     idset_del (idset_t* set, int32_t id);

/* iterator */
typedef struct {
    int32_t index;
    idset_t* set;
} idset_it_t;


void    idset_it_init(idset_t* set, idset_it_t* it);

/**
 * \code
 *   idset_it_t it;
 *   idset_it_init(set, &it);
 *   int32_t val;
 *   while ((val = idset_it_next(&it)) >= 0) print(val);
 * \endcode
 */
int32_t idset_it_next(idset_it_t* it);

#endif /* _IDSET_H_ */
