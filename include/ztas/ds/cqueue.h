/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _CQUEUE_H_
#define _CQUEUE_H_
#include <ztas/ds/queue.h>
#include <pthread.h>

typedef struct {
    void** buffer;
    volatile int size;
    volatile int head;
    volatile int tail;
    pthread_spinlock_t lock;
} cqueue_t;


cqueue_t* cqueue_create(int size);
int       cqueue_enq(cqueue_t* q, void* i);
void*     cqueue_deq(cqueue_t* q);
void*     cqueue_top(cqueue_t* q);
void      cqueue_pop(cqueue_t* q);
int       cqueue_empty(cqueue_t* q);
void      cqueue_free(cqueue_t* q);

#define CQUEUE_OK     QUEUE_OK
#define CQUEUE_FULL   QUEUE_FULL
#define CQUEUE_EMPTY  QUEUE_EMPTY
#define CQUEUE_NEMPTY QUEUE_NEMPTY

#endif
