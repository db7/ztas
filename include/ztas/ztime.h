/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _ZTIME_H_
#define _ZTIME_H_

#ifdef __cplusplus
extern "C" {
#endif


#include <ztas/types.h>
#include <ztas/ztimex.h>



#define NS 1
#define US (1000*NS)
#define MS (1000*US)
#define S  (1000*MS)

#define ZTIME_GT(a, b) (                                        \
        ((a).high > (b).high)                                   \
        || (((a).high == (b).high) && ((a).low > (b).low))      \
        )

/**
 * Compares a and b, returns true if a is greater than b
 *
 * @param a: ztime_t timestamp
 * @param b: some value (NOT ztime_t)
 */
#define ZTIME_GT32(a, b) (                     \
        ((a).high > 0 ) || ((a).low > (b) )    \
        )

#define ZTIME_GE(a, b) (                                           \
        ZTIME_GT(a,b)                                              \
        || (((a).high == (b).high) && ((a).low == (b).low))        \
        )

#define ZTIME_LT(a, b) ( ZTIME_GE(b,a) )

#define ZTIME_LE(a, b) ( ZTIME_GT(b,a) )

/**
 * duration of time period between a and b
 * @param a start of period
 * @param b end of period
 * @param r duration
 *
 * Note that b MUST be greater than a as the data types
 * don't support negative numbers
 */
#define ZTIME_DUR(a, b, r) {                                    \
        assert (ZTIME_GE(b,a));                                 \
        (r).low  = (b).low - (a).low;                           \
        if ((a).low > (b).low) {                                \
            (r).high = (b).high - (a).high - 1;                 \
        } else {                                                \
            (r).high = (b).high - (a).high;                     \
        }                                                       \
    }

#define ZTIME_PLUS(a, b, r) {                                           \
        (r).high = (b).high + (a).high;                                 \
        (r).low  = (b).low + (a).low;                                   \
        if ((uint32_t)(r).low < (uint32_t)(a).low ||                    \
            (uint32_t)(r).low < (uint32_t)(b).low) {                    \
            ++(r).high;                                                 \
        }                                                               \
    }

#define ZTIME_MINUS(a, b, r){                                           \
        (r).high = (a).high - (b).high;                                 \
        (r).low = (a).low - (b).low;                                    \
        if((uint32_t)(r).low > (uint32_t)(a).low ||                     \
           (uint32_t)(r).low > (uint32_t)(b).low){                      \
            --(r).high;                                                 \
        }                                                               \
    }

#define MAX_LOW_US ((uint32_t)(-1))/1000

#define ZTIME_ADD_US(a, dur_us, r) {                            \
        (r).low  = (a).low + 1000*dur_us;                       \
        (r).high = (a).high;                                    \
        if ((r).low < (a).low) {                                \
            ++(r).high;                                         \
        }                                                       \
        if (dur_us > MAX_LOW_US) {                              \
            ++(r).high;                                         \
        }                                                       \
    }

#define MAX_LOW_MS ((uint32_t)(-1))/1000000

/**
* This macro adds dur_ms milliseconds to a resulting
* in r.
* Note that this might fail if dur_ms is too big to fit in 32bit
* (enforced by assertion!)
* @param a starting timestamp
* @param dur_ms milliseconds to add
* @param r result
*/
#define ZTIME_ADD_MS(a, dur_ms, r) {                            \
        assert((MS*(dur_ms))/(MS) == dur_ms);                   \
        assert((MS*(dur_ms))/(dur_ms) == MS);                   \
        (r).low  = (a).low + (MS*(dur_ms));                     \
        (r).high = (a).high;                                    \
        if ((r).low < (a).low) {                                \
            ++(r).high;                                         \
        }                                                       \
        if ((dur_ms) > MAX_LOW_MS) {                            \
            ++(r).high;                                         \
        }                                                       \
    }

#define ZTIME_ADD_S(a, dur_ms, r) {                             \
        (r).low  = (a).low + S*dur_ms;                          \
        (r).high = (a).high;                                    \
        if ((r).low < (a).low) {                                \
            ++(r).high;                                         \
        }                                                       \
        if (dur_ms > MAX_LOW_MS) {                              \
            ++(r).high;                                         \
        }                                                       \
    }

/**
 * Substracts dur_ms nanosecs from a time
 * @param a 		time
 * @param dur 		nanosecs to be substracted
 * @param r			result time
 */
#define ZTIME_SUB(a, dur, r) {                                  \
        (r).low  = (a).low - dur;                               \
        (r).high = (a).high;                                    \
        if ((r).low > (a).low) {                                \
            --(r).high;                                         \
        }                                                       \
    }

/* I guess this is not necessary when we work with ns
        if (dur > MAX_LOW) {                                    
            --(r).high;                                         
        }                                                       
*/

/**
 * Substracts dur_ms millisecs from a time
 * @param a 		time
 * @param dur 		milisecs to be substracted
 * @param r		result time
 */
#define ZTIME_SUB_MS(a, dur, r) {                               \
        (r).low  = (a).low - MS*(dur);                          \
        (r).high = (a).high;                                    \
        if ((r).low > (a).low) {                                \
            --(r).high;                                         \
        }                                                       \
        if (dur > MAX_LOW_MS) {                                 \
            --(r).high;                                         \
        }                                                       \
    }

/**
 * Adds a duration to a time, i.e. it can result
 * in a substraction if the duration is negative.
 *
 * It is expected that the result is not negative as
 * this can be a ztime_t again.
 */
#define ZTIME_ADD_DUR(a, b, r){                                         \
        uint64_t tsA = ZTIME_TS(a);                                     \
        int64_t tsB = ZTDUR_TS(b);                                      \
        ZTIME_SET64((r), (uint64_t)(tsA+tsB));                          \
    }

/**
 * Divides a ztime_t by 2
 *
 * Steps taken:
 * 1. shift low by 1 --> divide by 2
 * 2. merge the lowse bit of high part to the highest bit of the low part
 * 3. shift high by 1 --> divide by 2
 */
#define ZTIME_DIV2(a) {                                                 \
        (a).low = (a).low >> 1;                                         \
        (a).low |= ((a).high & 0x0001) <<                               \
            ((sizeof((a).high)*8)-1);                                   \
        (a).high = (a).high >> 1;                                       \
    }

/**
 * This macro transforms a ztime_t in a int32_t representing microseconds.
 *
 * Note that this might fail if dur_ms is too big to fit in 32bit
 * (enforced by assertion!)
 *
 * @param a starting timestamp
 * @param dur_ms milliseconds to add
 * @param r result
 */
#define ZTIME_2US_32(a, i) {                              \
        uint64_t x = ZTIME_TS(a);                         \
        x /= 1000;                                        \
        assert (x <= (uint64_t) ((uint32_t) -1));         \
        i = (uint32_t) x;                                 \
    }

#define ZTIME_EQ_NS(a, ns) ( ((a).high == (uint32_t)((uint64_t)(ns)>>32)) && \
                             ((a).low == ((ns)& 0x00000000FFFFFFFFL)))

#define ZTIME_SET_LOW(a, val) { (a).low = val; (a).high = 0; }

#define ZTIME_EQ(a, b) (((a).low == (b).low) && ((a).high == (b).high))

#define ZTIME_SET(a, b) {(a).low = (b).low; (a).high = (b).high;}

#define ZTIME_IS_ZERO(a) ((a).low == 0 && (a).high == 0)

#define ZTIME_ZERO(a) {(a).low = 0; (a).high = 0;}
#define ZTIME_LOG(a) LOG1("%"PRIu64, ZTIME_TS(a))

#define ZTIME_DEFINE_ZERO {0, 0}

#ifdef __cplusplus
}
#endif

#endif /* _ZTIME_H_ */
