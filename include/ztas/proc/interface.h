/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _ZTAS_INTERFACE_H_
#define _ZTAS_INTERFACE_H_

#ifndef _ZTAS_CORE
#   define _ZTAS_CORE
#endif

#include <ztas.h>

#endif /* _ZTAS_INTERFACE_H_ */
