/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _ZTAS_PROTO_H_
#define _ZTAS_PROTO_H_

#include <stdint.h>

void* ztas_init(int32_t pid);
void  ztas_fini(void* state);
void  ztas_recv(void* state, const void* data, size_t size);
void  ztas_trig(void* state, int32_t aid);

#endif /* _ZTAS_PROTO_H_ */
