/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
int  L(ztas_ucast)(int32_t dst, const void* data, size_t size, int flags);
int  L(ztas_bcast)(const void *data, size_t size, int flags);
int  L(ztas_alarm)(int32_t aid, uint32_t ms);
int  L(ztas_sched)(int32_t aid, const ztime_t* at);
void L(ztas_clock)(ztime_t* ts);
void L(ztas_term) (void);

