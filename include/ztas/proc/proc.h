/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _ZTAS_PROC_H_
#define _ZTAS_PROC_H_

#include <ev.h>
#include <ztas/proc/cfg.h>
#include <ztas/proc/mod.h>
#define _ZTAS_NO_SIZE_T
#include <ztas/types.h>


typedef struct proc proc_t;

proc_t* proc_init_cfg(struct ev_loop* loop, int pid, const cfg_t* cfg);
proc_t* proc_init(struct ev_loop* loop, int pid, const char* fname);
void    proc_fini(proc_t*);
void    proc_stop(proc_t*);
int     proc_lsend(proc_t* proc, void* data, size_t size);
int     proc_bcast(proc_t* proc, const void* data, size_t size, int flags);
int     proc_sendv(proc_t* proc, int32_t dst, void** data, uint64_t* sizes,
                   int len);
int     proc_ucast(proc_t* proc, int32_t dst, const void* data, size_t size,
                   int flags);
int     proc_sched(proc_t* proc, int32_t aid, const ztime_t* at);
int     proc_alarm(proc_t* proc, int32_t aid, uint32_t ms);


proc_t* proc_init_cfg_uif(struct ev_loop* loop, int pid, const cfg_t* cfg,
    ztas_uif_t* uif);

#endif /* _ZTAS_PROC_H_ */
