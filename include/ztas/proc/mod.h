/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _MOD_H_
#define _MOD_H_
#include <stdint.h>

typedef struct {
    uint32_t low;
    uint32_t high;
} _ztime_t;

// upcall interface
typedef void* (ztas_init_f)(int32_t pid);
typedef void  (ztas_fini_f)(void* state);
typedef void  (ztas_recv_f)(void* state, const void* data, size_t size);
typedef void  (ztas_trig_f)(void* state, int32_t aid);

typedef struct {
    ztas_init_f* init;
    ztas_fini_f* fini;
    ztas_recv_f* recv;
    ztas_trig_f* trig;
} ztas_uif_t;

// downcall interface
typedef void        (ztas_term_f)  (void);
typedef int         (ztas_alarm_f) (int32_t aid, uint32_t ms);
typedef int         (ztas_sched_f) (int32_t aid, _ztime_t* ts);
//typedef uint64_t    (ztas_clock_f) (void);
typedef void        (ztas_clock_f) (_ztime_t*);
typedef int         (ztas_ucast_f) (int32_t pid, const void* data, int32_t size,
                                    int flags);
typedef int         (ztas_bcast_f) (const void* data, int32_t size, int flags);
typedef const char* (ztas_cread_f) (const char* key);
typedef int         (ztas_sendv_f)(int32_t dst, void** data, uint64_t* sizes,
                                   int len);

typedef struct {
    ztas_term_f*  term;
    ztas_alarm_f* alarm;
    ztas_sched_f* sched;
    ztas_clock_f* clock;
    ztas_ucast_f* ucast;
    ztas_bcast_f* bcast;
    ztas_cread_f* cread;
    ztas_sendv_f* sendv;
} ztas_dif_t;

// module interface
typedef void (ztas_mod_init_f) (const ztas_dif_t*);

typedef struct {
    ztas_uif_t uif;
    void* handle;
} ztas_mod_t;

#endif
