/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _CFG_H_
#define _CFG_H_

#include <ztas/ds/idset.h>

typedef struct _dictionary_ cfg_t;

cfg_t*      cfg_parse(const char* fname);
void        cfg_close(cfg_t* cfg);

int         cfg_set(cfg_t* cfg, const char* entry, const char* val);
int         cfg_get_port (const cfg_t* cfg, int pid);
const char* cfg_get_host (const cfg_t* cfg, int pid);
const char* cfg_get_mod  (const cfg_t* cfg, int pid);
idset_t*    cfg_get_idset(const cfg_t* cfg);
const char* cfg_get(const cfg_t* cfg, const char* key);

#endif /* _CFG_H_ */
