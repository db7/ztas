/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _CONNECTION_H_
#define _CONNECTION_H_
#include <stdlib.h>
#include <stdint.h>
#include <ev.h>

typedef struct connection connection_t;

typedef uint64_t (connection_recv_cb) (connection_t* connection, void** data, ssize_t* read, void* args);
typedef void     (connection_done_cb) (connection_t* connection, void* data, void* args);
typedef void     (connection_close_cb)(connection_t* connection, void* data, void* args);

typedef struct {
    connection_recv_cb*  recv;
    connection_done_cb*  done;
    connection_close_cb* close;
} connection_cb_t;


connection_t* connection_init  (struct ev_loop* loop, const char* server_host,
                                int server_port, const connection_cb_t* server,
                                void* args);

/**
 * stop all watchers, close connection and set socket value to -1
 */
void          connection_stop  (connection_t* connection);
void          connection_fini  (connection_t* connection);


/**
 * assign a socket to connection and start watchers */
void connection_set   (connection_t* connection, int sock);

/** open new socket, try to connect to hostname:port and start a
 * connect watcher */
int connection_config(connection_t* connection, const char* hostname, int port);

/** if connection open, send message.  If connection closed enqueue
 * message and if this connection is the initiator (connection_config
 * was called), then try to connect to hostname:port again. */
int connection_send (connection_t* connection, void* data, uint64_t size);


/** send multiple messages. dataptr and sizeptr are arrays of length len. */
int connection_sendv(connection_t* connection, void** dataptr,
                     uint64_t* sizeptr, int len);

/** checks whether connection is ok before trying */
int connection_sendv_check(connection_t* connection, int len);

/** getter and setter for a void* data in connection */
void* connection_get_data(connection_t* connection);
void  connection_set_data(connection_t* connection, void* data);

/** save stats */
const char* connection_stats(connection_t* connection);


/** gets the address to which the socket is connected. Only makes
 * sense if hostname was given to init or config. */
const struct sockaddr_in* connection_addr(connection_t* connection);

#define CONNECTION_SEND_OK    0
#define CONNECTION_SEND_FULL  1
#define CONNECTION_SEND_ERROR 1

#endif /* _CONNECTION_H_ */
