/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _LISTENER_H_
#define _LISTENER_H_
#include <stdint.h>
#include <ev.h>

#include "connection.h"

typedef struct listener listener_t;

listener_t* listener_init(struct ev_loop* loop, int listen_port, 
                          const connection_cb_t* cbs, void* args);
void        listener_stop(listener_t* listener);
void        listener_fini(listener_t* listener);

#endif /* _LISTENER_H_ */
