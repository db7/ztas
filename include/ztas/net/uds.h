/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 * \file uds.h
 * \brief Unreliable Datagram Service
 *
 * This module implements an unreliable datagram service using UDP.
 */
#ifndef _UDS_H_
#define _UDS_H_
#include <sys/socket.h>
#include <ev.h>
#include <netinet/in.h>

typedef struct uds uds_t;


/**
 * \brief  Callback on receipt of datagrams.
 * \param  uds  The uds object which received the datagram.
 * \param  data Received datagram.
 * \param  data Size of received datagram.
 * \param  addr Datagram sender address.
 * \param  args Arbitrary data structure given to uds on initialization.
 */
typedef void (uds_recv_cb)(uds_t* uds, const void* data, size_t size,
                           struct sockaddr_in* addr, void* args);

/**
 * \brief  Allocate and initialize a uds object.
 * \param  loop Event loop.
 * \param  port Port for receiving UDP messages.
 * \param  cb   Receive callback.
 * \param  args Arbitrary data structure to be passed to the callback.
 * \return A new uds object.
 *
 * uds_init adds one watcher to event loop.
 */
uds_t* uds_init(struct ev_loop* loop, int port, uds_recv_cb* cb, void* args);


/**
 * \brief  Remove watcher from event loop.
 * \param  uds  An uds object.
 */
void uds_stop(uds_t* uds);

/**
 * \brief  Free uds data structures.
 * \param  uds  An uds object.
 *
 * `uds` cannot be further used after this call.
 */
void uds_fini(uds_t* uds);


/**
 * \brief  Send data object to destination address.
 * \param  uds  An uds object.
 * \param  data Data to be sent.
 * \param  size Size of `data`.
 * \param  addr Address of destination.
 *
 */
void uds_send(uds_t* uds, const void* data, size_t size,
              const struct sockaddr_in* addr);

/**
 * \brief  Send an array of data objects to destination address.
 * \param  uds  An uds object.
 * \param  data Array of data objects to be sent.
 * \param  size Array of sizes of each objest of `data`.
 * \param  size Length of `data` and `size` arrrays.
 * \param  addr Address of destination.
 *
 */
void uds_sendv(uds_t* uds, const void** data, size_t* size, int len,
               const struct sockaddr_in* addr);


/**
 * \brief  Set a sockaddr_in with host name and port.
 * \param  addr Address object to be initilized.
 * \param  host Hostname.
 * \param  port Port number.
 * \return int  0 if failed to resolve hostname, otherwise 1.
 */
int uds_addr_set(struct sockaddr_in* addr, const char* host, int port);

#endif
