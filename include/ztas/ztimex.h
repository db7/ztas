/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _ZTIMEX_H_
#define _ZTIMEX_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

uint64_t __ztas_now64();

#define ZTIME_TS(a) (((uint64_t) (a).high) << 32 | ((uint64_t)(a).low & 0x00000000FFFFFFFFL))
#define ZTDUR_TS(a) ((int64_t)((int64_t) (a).high) << 32 | ((int64_t)(a).low & 0x00000000FFFFFFFFL))

#define ZTIME_SET32(a, b) {                               \
        (a).high = ((uint64_t)(b) >> 32);                 \
        (a).low  = ((uint64_t)(b) & 0x00000000FFFFFFFFL); \
    }

#define ZTIME_SET64(a,b) (ZTIME_SET32((a), (b)))


#define ZTDUR_SET64(a, b) {                               \
        (a).high = (int32_t)((int64_t)(b) >> 32);                 \
        (a).low  = (int32_t)((int64_t)(b) & 0x00000000FFFFFFFFL); \
    }

#define ZTDUR_SET32(a, b) {                               \
		ZTDUR_SET64((a), (int64_t)(b));						\
	}


#ifdef __cplusplus
}
#endif

#endif /* _ZTIMEX_H_ */
