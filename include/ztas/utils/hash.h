/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _HASH_H_
#define _HASH_H_

#include <stdint.h>

uint32_t hash(const char* str, uint32_t size);

#endif /* _HASH_H_ */
