/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _CRC_H_
#define _CRC_H_

#include <stdint.h>
uint32_t crc(const void* key, uint32_t len);

#endif /* _CRC_H_ */
