/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 * layer.h to be called by any ztas layer.
 */
// trick from macro_helper.h again also for X

#define PASTER(x,y) x ## y
#define EVALUATOR(x,y)  PASTER(x,y)
#define EVAL(X) X

#define LU(F) EVALUATOR(EVAL(LAYERU), F)
#define LD(F) EVALUATOR(EVAL(LAYERD), F)

#define up_init  LU(ztas_init)
#define up_fini  LU(ztas_fini)
#define up_recv  LU(ztas_recv)
#define up_trig  LU(ztas_trig)
#define up_ucast LU(ztas_ucast)
#define up_bcast LU(ztas_bcast)
#define up_alarm LU(ztas_alarm)
#define up_sched LU(ztas_sched)
#define up_clock LU(ztas_clock)
#define up_term  LU(ztas_term)
#define up_cread LU(ztas_cread)

#define do_init  LD(ztas_init)
#define do_fini  LD(ztas_fini)
#define do_recv  LD(ztas_recv)
#define do_trig  LD(ztas_trig)
#define do_ucast LD(ztas_ucast)
#define do_bcast LD(ztas_bcast)
#define do_alarm LD(ztas_alarm)
#define do_sched LD(ztas_sched)
#define do_clock LD(ztas_clock)
#define do_term  LD(ztas_term)
#define do_cread LD(ztas_cread)


