/* ----------------------------------------------------------------------
 * Copyright (c) 2012,2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
// This file must not have an include guard.

/**
 * @addtogroup core
 * @section flex_sec The Flexible Interface
 * The flex layer provides a way to create interfaces using preprocessor
 * definitions.
 *
 * Suppose a module should be able to work both with ztas and zte.
 * If using ztas, it's expected to provide upcall functions like
 * <ul>
 *  <li>ztas_init</li>
 *  <li>ztas_trig</li>
 * </ul>
 * etc. On the other hand it is expected to make downcalls to e.g.
 * <ul>
 *  <li>ztas_ucast</li>
 *  <li>ztas_alarm</li>
 * </ul>
 * When working with zte the methods are basically the same but with
 * a different name.
 *
 * To be able to use both, one has to specify function names and function calls
 * using macros that replace the actual names when compiling.
 *
 * The definition
 * @code
 * void* L(trig)(uint32_t aid){...}
 * @endcode
 * can then be both
 * @code
 * void* ztas_trig(uint32_t aid){...}
 * @endcode
 * and
 * @code
 * void* zte_trig(uint32_t aid){...}
 * @endcode
 *
 * Default name is `ztas`. The default can be changed by passing -DLAYER=name to
 * the compiler call.
 *
 * <h3>Interface Configuration</h3>
 * As stated above the interface names can be changed by setting the preprocessor
 * directive LAYER with the layer's name.<br />
 * Beyond that the following settings can be specified
 * <ul>
 *  <li>USE_CONFIG: if specified, the algorithm is treated to use the configuration provided
 * configuration mechanims. Which means its init method's signature will be
 * @code
 * void LAYER_init(int32_t pid, void* config, size_t size);
 * @endcode
 * instead of the unconfigured init signature:
 * @code
 * void LAYER_init(int32_t pid);
 * @endcode
    </li>
   </ul>
 *
 */

#include <ztas/types.h>
#include <ztas/ztime.h>


/* If no layer name is provided, use zte */
#ifndef LAYER
 #define L_NAME zte
#else /* otherwise use the defined layer */
 #define L_NAME LAYER
#endif

#ifndef L_NAME
 #error no layer name specified
#endif

#define MACRO_NAME L_NAME
#include <ztas/macro_helper.h>

///////////////////////////////
// Upcall definitions
///////////////////////////////
void* L(init)(int32_t pid, const void* config, size_t size);
void L(fini)(void* state);
void L(recv)(void* state, const void* data, uint32_t size);
void L(trig)(void* state, int32_t aid);


///////////////////////////////
// Downcall definitions
///////////////////////////////
int  L(ucast)(int32_t dst, const void* data, size_t size);
int  L(bcast)(const void *data, size_t size);
int  L(alarm)(int32_t aid, uint32_t ms);
int  L(sched)(int32_t aid, const ztime_t* at);
void L(term)();
void L(clock)(ztime_t* ts);




