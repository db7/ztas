/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _ZTAS_MEM_H_
#define _ZTAS_MEM_H_
#include <ztas/types.h>

#ifdef ENCODED
extern void* malloc32(size_t);
#define malloc malloc32
void  free(void*);

int32_t atoi(const char*);

#else  /* !ENCODED */ 
#  include <stdlib.h>
#endif /* ENCODED */

#endif /* _ZTAS_MEM_H_ */
