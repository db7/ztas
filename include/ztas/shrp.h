/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 * A shared pointer interface
 */
#ifndef _SHRP_H_
#define _SHRP_H_


void* shrp_malloc(unsigned size);
void* shrp_realloc(void* ptr, unsigned size);
void  shrp_free(void* ptr);
void  shrp_hold(void* ptr);
void  shrp_holdcpy(void** ptr, unsigned size);


#endif /* _SHRP_H_ */
