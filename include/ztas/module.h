/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _MODULE_
#define _MODULE_

#include <ztas/proc/mod.h>
#include <ztas/string.h>

// If MODULE_PREFIX is not defined, assume an interface will be
// compiled together, so don't create uif.
#ifndef MODULE_PREFIX
#define MODULE_PREFIX __
#else
#define CREATE_UIF
#endif

#define PASTER(x,y) x ## y
#define EVALUATOR(x,y)  PASTER(x,y)
#define EVAL(X) X

#define MP(X) EVALUATOR(EVAL(MODULE_PREFIX),X)

static ztas_dif_t _dif;

void __attribute__ ((constructor))
module_init(void) {}

void
ztas_mod_init(const ztas_dif_t* dif)
{
    memcpy(&_dif, dif, sizeof(ztas_dif_t));
}

void
MP(ztas_clock)(ztime_t* ts)
{
    _dif.clock((_ztime_t*) ts);
}

int
MP(ztas_alarm)(int32_t aid, uint32_t ms)
{
    return _dif.alarm(aid, ms);
}
int
MP(ztas_sched)(int32_t aid, const ztime_t* at)
{
    return _dif.sched(aid, (_ztime_t*) at);
}

int
MP(ztas_ucast)(int32_t dst, const void* data, size_t size, int flags)
{
    return _dif.ucast(dst, data, size, flags);
}

int
MP(ztas_bcast)(const void* data, size_t size, int flags)
{
    return _dif.bcast(data, size, flags);
}

void
MP(ztas_term)(void)
{
    _dif.term();
}

const char*
MP(ztas_cread)(const char* key)
{
    return _dif.cread(key);
}

int
MP(ztas_sendv)(int32_t dst, void** data, uint64_t* sizes, int len)
{
    return _dif.sendv(dst, data, sizes, len);
}

#ifdef CREATE_UIF
void* ztas_init(int32_t pid);
void  ztas_fini(void* state);
void  ztas_recv(void* state, const void* data, size_t size);
void  ztas_trig(void* state, int32_t aid);

void* __ztas_init(int32_t pid) { return ztas_init(pid); }
void  __ztas_fini(void* state) { ztas_fini(state); }
void  __ztas_recv(void* state, const void* data, size_t size)
{ ztas_recv(state, data, size); }
void  __ztas_trig(void* state, int32_t aid) { ztas_trig(state, aid); }
#endif

#endif /* _MODULE_ */
