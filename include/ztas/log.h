/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _ZTAS_LOG_H_
#define _ZTAS_LOG_H_
#include <ztas/types.h>

#ifdef ZTAS_DEBUG
#define PDEBUG printf("[%s:%d] ", __FILE__, __LINE__)
// logging like printf, only available if ZTAS_DEBUG defined
#define DLOG(fmt, ...) do{ PDEBUG; printf(fmt, ##__VA_ARGS__); } while(0)
#else
#define PDEBUG
#define DLOG(fmt, ...)
#endif

#ifdef ENCODED
#include <scrutinizer_stdio.h>
# define LOG(A)             LOG0(A)
# define LOG0(A)            do { PDEBUG; testPrintf(A); } while(0)
# define LOG1(A,B)          do { PDEBUG; testPrintf_1_int(A,B); } while(0)
# define LOG2(A,B,C)        do { PDEBUG; testPrintf_2_int(A,B,C); } while(0)
# define LOG3(A,B,C,D)      do { PDEBUG; testPrintf_3_int(A,B,C,D); } while(0)
# define LOG4(A,B,C,D,E)    do { PDEBUG; testPrintf_4_int(A,B,C,D,E); } while(0)

#else /* !ENCODED */
# include <stdio.h>
# define LOG(A)          LOG0(A)
# define LOG0(A)         do { PDEBUG; printf(A); fflush(stdout); } while(0)
# define LOG1(A,B)       do { PDEBUG; printf(A,B); fflush(stdout); } while(0)
# define LOG2(A,B,C)     do { PDEBUG; printf(A,B,C); fflush(stdout); } while(0)
# define LOG3(A,B,C,D)   do { PDEBUG; printf(A,B,C,D); fflush(stdout); } while(0)
# define LOG4(A,B,C,D,E) do { PDEBUG; printf(A,B,C,D,E); fflush(stdout); } while(0)

#endif /* ENCODED */


#endif /* _ZTAS_LOG_H_ */
