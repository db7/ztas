/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 * Flags returned by different methods of ztas
 */
#ifndef _ZTAS_FLAGS_H_
#define _ZTAS_FLAGS_H_


// flags for ztas_ucast and ztas_
#define ZTAS_CAST_DEFAULT 0 //<< default flag of ztas_ucast and ztas_bcast
#define ZTAS_CAST_UMSG    1 //<< unreliable unordered message

// return values of ztas_ucast
#define ZTAS_UCAST_OK     0 //<< send successful
#define ZTAS_UCAST_FULL   1 //<< send failed (buffer is full)
#define ZTAS_UCAST_DSTERR 2 //<< send failed (dst is unreachable)
#define ZTAS_UCAST_ERROR  3 //<< FULL | UNREACH
#define ZTAS_UCAST_ESIZE  4 //<< when sending message with UMSG
#define ZTAS_UCAST_EFLAG  6 //<< invalid flag

// return values of ztas_bcast
#define ZTAS_BCAST_OK     0 //<< send successful
#define ZTAS_BCAST_NOK    1 //<< error when sending to some destination
#define ZTAS_BCAST_ESIZE  4 //<< when sending message with UMSG
#define ZTAS_BCAST_EFLAG  6 //<< invalid flag

#endif
