/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _ZTAS_MSG_H_
#define _ZTAS_MSG_H_
#include <stdint.h>

typedef struct {int32_t ph[6];} ztas_msg_t;

void    ztas_msg_init(void* msg, int32_t src, int32_t type);
int32_t ztas_msg_type(const void* msg);
int32_t ztas_msg_src (const void* msg);

#define ZTAS_MSG ztas_msg_t _ph;

#endif /* _ZTAS_MSG_H_ */
