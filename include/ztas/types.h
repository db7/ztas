/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _ZTAS_TYPES_H_
#define _ZTAS_TYPES_H_

#ifdef __cplusplus
extern "C" {
#endif

/** when using g++ this macro is needed to
 * have the PRIxxx constants available
 */
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <stdint.h>
typedef uint32_t ztas_size_t;

#ifndef _ZTAS_NO_SIZE_T
/* Unless _ZTAS_CORE is defined, size_t will be a 32-bit unsigned
 * integer. That avoids confusion when encoding any algorithm.  */
#ifndef _ZTAS_CORE
#define _SIZE_T
typedef ztas_size_t size_t;
/* By defining __need_null, stddef will create a definition NULL */
#define __need_null
#endif
#endif // ! _ZTAS_NO_SIZE_T
#include <stddef.h>


/* Boolean mock */
typedef int32_t bool_t;
#define True 1
#define False 0

/* absolute time */
typedef struct {
    uint32_t low;
    uint32_t high;
} ztime_t;

typedef struct {
    int32_t low;
    int32_t high;
} ztdur_t;

#ifdef __cplusplus
}
#endif

#endif /* _ZTAS_TYPES_H_ */
