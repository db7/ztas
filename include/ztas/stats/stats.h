/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _ZTAS_STATS_H_
#define _ZTAS_STATS_H_
#include <ev.h>
#include <ztas/proc/cfg.h>

typedef struct stats stats_t;

stats_t* stats_init(struct ev_loop* loop, int32_t pid, const cfg_t* cfg);
void stats_stop(stats_t* stats);
void stats_fini(stats_t* stats);
void stats_report(stats_t* stats, const char* topic, const char* information);

#endif
