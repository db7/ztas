/* ----------------------------------------------------------------------
 * Copyright (c) 2012,2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 * \file   cpu.h
 * \author diogo
 * \date   Mon Feb  6 14:08:10 CET 2012
 * \brief  CPU measurements for OS process.
 *
 * This module measures the CPU utilization of OS process.
 *
 */

#ifndef _CPU_STATS_H_
#define _CPU_STATS_H_

#include <stdint.h>

typedef struct cpu_stats cpu_stats_t;

/** \brief Initilize the cpu stats with a give `pid`.
 *
 * Measurements are stored in a file named "cpu-pid.dat".
 */
cpu_stats_t* cpu_stats_init(int32_t pid, const char* dir);

/** \brief Finilize cpu stats closing file pointers.
 */
void cpu_stats_fini(cpu_stats_t* stats);

/** \brief Perform CPU measurement writing to and flushing file.
 *
 *  If called repeatedly, multiple lines of measurement are stored in
 *  file.
 */
void cpu_stats_measure(cpu_stats_t* stats);

#endif /* _CPU_STATS_H_ */
