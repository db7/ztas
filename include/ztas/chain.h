/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _ZTAS_CHAIN_H_
#define _ZTAS_CHAIN_H_
#include <assert.h>
#define CHAIN_DEBUG

/* Implement a method which returns either DONE or NEXT.
 * DONE if the operator was used, NEXT if it was not
 */

#define ZTAS_CHAIN_DONE 1
#define ZTAS_CHAIN_NEXT 0

/*
 * usage:
 *  CHAIN (
 *      || CHAIN_HANDLER(first_option(...))
 *      || CHAIN_HANDLER(second_option(...))
 *  )
 */
#ifndef CHAIN_DEBUG
#  define CHAIN_HANDLER(X)
#  define CHAIN(COND) if (!(0 COND)) {assert(0 && "chain error");}
#else
#  define CHAIN_HANDLER(X) (__count += X)
#  define CHAIN(COND) do {                                              \
        int __count = 0;                                                \
        if (0 COND) {}                                                  \
        assert (__count != 0 && "No chain handler returned OK");        \
        assert (__count >= 1 && "Multiple chain handler returned OK");  \
    } while(0)
#endif

#endif
