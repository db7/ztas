/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _ZTAS_TEST_H_
#define _ZTAS_TEST_H_
#include <ztas/log.h>

static char test_case_msg[] = "\
-----------------------------------------------------\n\
%s\n\
-----------------------------------------------------\n";

#define TEST_CASE(f,...) do {                \
        printf(test_case_msg, #f);           \
        f(__VA_ARGS__);                      \
    } while(0);

#endif
