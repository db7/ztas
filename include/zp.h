/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

#ifndef _ZP_H_
#define _ZP_H_

#ifdef __cplusplus
extern "C" {
#endif

#define _ZTAS_NO_SIZE_T
#include <ztas/ztime.h>
#include <ztas/ztdur.h>
#include <ztas/flags.h>

    typedef struct zp   zp_t;
    typedef struct zmsg {
        const void* data;
        size_t size;
    } zmsg_t;

  /**
   * \brief send a message consisting of `data` array with `size` bytes to `dst`.
   * Return ZP_UCAST_OK, ZP_UCAST_FULL, ZP_UCAST_DSTERR, (ZP_UCAST_ERROR)
   * flags == 0 or ZP_CAST_DEFAULT uses TCP
   * flags == ZP_CAST_UMSG uses unreliable unordered datagram service (UDP)
   */
  int  zp_ucast(zp_t* zp, int32_t dst, zmsg_t* msg, int flags);

  /**
   * \brief send a message consisting of `data` arry with `size` bytes
   * to all known processes including itself.
   * flags == 0 or ZTAS_CAST_DEFAULT uses TCP
   * flags == ZTAS_CAST_UMSG uses unreliable unordered datagram service (UDP)
   */
  int  zp_bcast(zp_t* zp, zmsg_t* msg, int flags);

  /**
   * \brief set an alarm identified by `aid` to be triggered in `ms` milliseconds.
   */
  int  zp_alarm(zp_t* zp, int32_t aid, uint32_t ms);

  /**
   * \brief schedule an alarm identified by `aid` to be triggered at `at` time.
   */
  int  zp_sched(zp_t* zp, int32_t aid, const ztime_t* at);

  /**
   * \brief read the current clock value in `ts` variable.
   */
  void zp_clock(zp_t* zp, ztime_t* ts);

  /**
   * \brief terminate the process.
   */
  void zp_term (zp_t* zp);


  /**
   * \brief read the value of a key from the configuration object.
   * Read the value of a key from the configuration object.  Key is of
   * the form "section:field". If key does not exist, NULL string is
   * returned.
   */
  const char* zp_cread(const char* key);

  /** \brief Called once the process is initialized.
   * The user is expected to implement this method. */
  zp_t* zp_init(int32_t pid, const char* cfg);

  /** \brief Called once the process is finished.
   * The user is expected to implement this method.
   */
  void  zp_fini(zp_t* zp);

  /** \brief Called once a message is received.
   * The user is expected to implement this method.
   */
  int  zp_recv(zp_t* zp, zmsg_t* msg);


#ifdef __cplusplus
}
#endif

#endif /* _ZP_H_ */
