# -*- python -*-

# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------

import os

###############################
## Environment configuration ##
###############################

libpath = ['.',
           '/usr/lib',
           '/usr/lib/x86_64-linux-gnu',  # Debian installs zmq here
           # shouldnt be necessary '#/build/encoding',
           '#/build/core',
           '#/build/layers',
           '#/build/extensions',
           '#/build/algorithms',
           ]
path = ['/usr/bin']
cpppath = ['.',
           '#/include',
           '#/algorithms/include/'
           ]

# dependency management
ExternalTools([('testing', 'https://bitbucket.org/frairon/scons_testing', 'HEAD'),
              #('ex_llvm', 'https://bitbucket.org/db7/scons_llvm', 'HEAD')
              ])

env = Environment(PATH=path,
                  LIBPATH=libpath,
                  CPPPATH=cpppath,
                  tools=['default',
                         'testing',
                         'registry',
                         'project_builder',
                         'build_tools'])


####################
## Optional Tools ##
####################

#tools = ['llvm', 'eis', 'encoding', 'interfi', 'ex_llvm', 'ascopass']
tools = []
for tool in tools:
    sconsTool = Tool(tool)
    if sconsTool.exists(env):
        sconsTool(env)

###################
## Tool Settings ##
###################
env.SetTestAlias('test')

####################
## Compiler flags ##
####################

#env.Append(CXXFLAGS='-O0 -Wall -g ')  # -fno-strict-aliasing ')
env.Append(CFLAGS='-Wall -O0 -g ')  # -fno-strict-aliasing ')

#env.Append(CXXFLAGS = '-Wall -O2')
#env.Append(CFLAGS = '-Wall -O3 -fno-strict-aliasing ')
#env.Append(CFLAGS = '-Wall -O3 -flto -fno-strict-aliasing ')
#env.Append(LINKFLAGS ='-flto ')

#######################
## Address sanitizer ##
#######################

# #env.Append(CFLAGS = '-Wall -O0 -g -fno-strict-aliasing -fsanitize=address -fno-omit-frame-pointer ')
# env.Append(CFLAGS = '-emit-llvm -fsanitize=address -fno-omit-frame-pointer ')
# env.Replace(OBJSUFFIX = '.bc')
# #env.Append(LINKFLAGS ='-g -fsanitize=address ')
# env.Replace(CC = 'clang')

##################
## Dependencies ##
##################

env.Append(LIBS=['m', 'dl', 'pthread'])

###################
## gprof support ##
###################

# env.Append(CCFLAGS=['-g','-pg'], LINKFLAGS=['-pg'])


##################
## gcov support ##
##################

# env.Append(CCFLAGS=['-fprofile-arcs', '-ftest-coverage'])
# boost_libs.append('gcov')


####################
## Subdirectories ##
####################
subdirs = ['deps', 'core', 'layers', 'algorithms', 'projects']

for subdir in subdirs:
    SConscript(subdir + '/SConscript', variant_dir='build/' + subdir,
               duplicate=False, exports='env')

##############
## Projects ##
##############

# this has to be the last one because it depends on all the algorithm
# definitions!
# SConscript(dirs='projects', variant_dir='build/projects',
#            duplicate = False, exports='env boost_libs')
