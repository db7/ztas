.. 
    # ------------------------------------------------------------------
    # Copyright (c) 2013 Technische Universitaet Dresden
    # Distributed under the MIT license. See accompanying file LICENSE.
    # ------------------------------------------------------------------

Code Style
==========


C Code Style
------------

In ZTAS we use a variantion of the `BSD KNF style <http://en.wikipedia.org/wiki/Indent_style#BSD_KNF_style>`_, also known as Kernel Normal Form, with the following differences:

- 4 spaces indentation
- pointer types have the * close to the type instead of the variable
- wrapped lines are aligned with the inner-most parenthesis

Some examples follow:

While and For Loops
~~~~~~~~~~~~~~~~~~~

.. code-block:: c

    while (!queue_empty(connection->requests)) {
        message_t* m = queue_deq(connection->requests);
        connection->cbs.done(connection, m->data, connection->args);
        free(m);
    }

    for (i = 0; i < MAX_INFLIGHT; ++i) {
        message_t* r = (message_t*) malloc(sizeof(message_t));
        r->data = NULL;
        r->size = 0;
        queue_enq(connection->free_req, (void*) r);
    }


If-else Conditionals
~~~~~~~~~~~~~~~~~~~~

The opening bracket of a function on the same line; space between binary operators and assignment operators; no space between variables and parenthesis; one space between if and condition; else on the same line as the closing bracket of if.

.. code-block:: c

    int flag = 1;
    if (-1 == setsockopt(connection->sock, IPPROTO_TCP, TCP_NODELAY,
                         (char*)&flag, sizeof(flag))) {
        printf("Couldn't setsockopt(TCP_NODELAY)\n");
        exit(EXIT_FAILURE);
    }

    if (sent != m->size && sent < 0) {
        ev_io_stop(connection->loop, &connection->send_w);
        return;
    } else {
        connection->backoff /= 2;
        if (connection->backoff < MIN_BACKOFF)
            connection->backoff = MIN_BACKOFF;
    }


Functions
~~~~~~~~~

The return type of the function on a single line; The opening bracket of a function on a single
line as well.

.. code-block:: c

    void
    connection_stop(connection_t* connection)
    {
        connection_teardown(connection);
        // stop async watchers
        ev_async_stop(connection->loop, &connection->async_w);
        ev_timer_stop(connection->loop, &connection->wait_w);
    }



Function Prototypes
~~~~~~~~~~~~~~~~~~~

The return type of the function on the same line. If function is
static, use `static` modifier only on the prototype.

.. code-block:: c

    static void connection_async_cb(struct ev_loop* loop, struct ev_async* watcher,
                                    int revents);

..   // -- What ------------------------------------


Type Definitions
~~~~~~~~~~~~~~~~

.. code-block:: c

    typedef struct {
        void*    data;
        uint64_t size;
    } message_t;


.. Separating sections inside a source code
.. ----------------------------------------

.. It can be useful to separate parts of the code. These are the following standard separators:

.. .. code:: python

..    ## -- What ------------------------------------


.. .. code:: c

..    // -- What ------------------------------------

.. Here is the emacs extension to add such a line:

.. .. code:: lisp

..    some emacs code


Python Code Style
-----------------

- Mandatory 4 spaces for indentation
- Mandatory adding the following line to files that do not end with .py::

    # -*- python -*-

