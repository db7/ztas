Project Builder
===============

Project builder helps you to deploy your experiments.

1. Create a folder in projects
2. Add a SConscript to the directory with the following initial lines:

   .. code-block:: python

      # -*- python -*-
      Import('env', 'project')

3. All necessary files for your experiment have to be given to the
   project object, eg,

   .. code-block:: python

      project.addFiles(env.Registry.collectFiles('rsm_test'))


4. Create a subdirectory for an experiment you want to perform. Add
   each subdirectory to your project, eg,

   .. code-block:: python

      project.addExperiment('simple_perf')

5. In each experiment subdirectory add scripts to initialize your
   experiment. You can use scaffold_project to access the files you
   added, eg, we have in `rsm/simple_perf/Dudefile`:

   .. code-block:: python

      sys.path.append("..")
      from scaffold_rsm import rsm
      ...
      rsm.filePaths['librsm_replica.so']

  

6. Deploy your experiment some local directory.

   .. code-block:: console

      scons project/rsm --deploy-dir=/tmp
  

   You can also deploy your experiment remotely with `--deploy-host`.

