.. 
    # ------------------------------------------------------------------
    # Copyright (c) 2012,2013 Technische Universitaet Dresden
    # Distributed under the MIT license. See accompanying file LICENSE.
    # ------------------------------------------------------------------

.. ztas documentation master file, created by
   sphinx-quickstart on Mon Feb  6 14:34:12 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ztas's documentation!
================================

Contents:

.. toctree::
   :maxdepth: 2

   interface
   style
   workflow
   algorithms
   core
   config
   projbuilder

TODO List
=========

.. todolist::

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
