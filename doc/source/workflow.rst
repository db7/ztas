.. 
    # ------------------------------------------------------------------
    # Copyright (c) 2013 Technische Universitaet Dresden
    # Distributed under the MIT license. See accompanying file LICENSE.
    # ------------------------------------------------------------------

Workflow Examples
=================

Here the 'hale' algorithm is used with three processes.

Create a Trace
----------------
The trace gets saved for a normal run.

1. start first process with:

   .. code-block:: console

      LOGFILE=algorithms/hale/hale.trace ./build/algorithms/hale/run_hale 0 -f exampleConfig.ini


2. start second process with::

   ./build/algorithms/hale/run_hale 1 -f exampleConfig.ini


3. start third process with::

   ./build/extensions/ztas_controller 1001 -f controller.ini

Replay Trace
------------
The saved trace gets replayed with a faulty version of hale. 

- in case the version is not encoded use:

.. code-block:: console

   TRIGGER_NO_TO_INJECT=10010 SEED_FOR_INJECT=4444314 \
   REPFILE=algorithms/hale/hale.trace \
   INJECTION_LOG=algorithms/hale/injection.log \
   ./build/algorithms/hale/run_hale.modifyOperands 0 -f exampleConfig.ini

- in case the version is encoded use:

.. code-block:: console

   SignatureFile=./deps/encoding/Signatures TRIGGER_NO_TO_INJECT=4941 \
   SEED_FOR_INJECT=4444314 REPFILE=algorithms/hale/hale.trace \
   INJECTION_LOG=algorithms/hale/injection.log \
   ./build/algorithms/hale/run_hale-modifyOperands.an-trace 0 -f exampleConfig.ini
