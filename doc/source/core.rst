.. 
    # ------------------------------------------------------------------
    # Copyright (c) 2013 Technische Universitaet Dresden
    # Distributed under the MIT license. See accompanying file LICENSE.
    # ------------------------------------------------------------------

Core architecture
=================

.. graphviz::

   digraph foo {
      "proc" -> "server";
      "proc" -> "timers";
      "proc" -> "endpoints";
   }

