.. 
    # ------------------------------------------------------------------
    # Copyright (c) 2013 Technische Universitaet Dresden
    # Distributed under the MIT license. See accompanying file LICENSE.
    # ------------------------------------------------------------------

Configuration File
==================


The configuration file given to ztas is a INI file like this::

   [main]
   processes = 0,1,3;

Where the processes contains a comma-separated list of processes
running in the system.

.. todo:: add link to INI file standard definition

You can save the OS-pid of the process which runs a ztas process by
adding `savepid` your `main` section ::

  [main]
  ...
  savepid = /tmp;

To run processes remotely you have to add one section per process in
the ini file.::

  [p0]
  host = somehost;
  port = someport;

  [p1]
  host = anotherhost;
  potr = anotherport;

  [p3]
  host = somehost;
  port = anotherport;
