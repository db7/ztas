.. -*- rst -*-

ZTAS
====

ZTAS is a framework to build distributed applications 

.. .. _Python: http://www.python.org/
 
Installation
------------

For those who are getting it for the first time::

    % hg clone https://bitbucket.org/db7/ztas
    % cd ztas
    % scons update-tools
    % scons download-deps
    % scons build-deps
    % scons
    % scons test

Requirements
------------

The requirements to compile ztas are minimal. You'll need 

* scons
* gcc

And that's it.

Documentation
-------------

More details can be found in the
`documentation <http://ztas.readthedocs.org>`_.

Contribute
----------

Contributions or feedback in any form are welcome. Issues, bugs and
feature requests can be reported at:
https://bitbucket.org/db7/ztas/issues
