# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
"""
deps.py - helper functions to download and compile dependencies
"""
import os
import sys
import urllib2

def download_file(url, fname):
    """ Download file with progress bar.
    Based on http://stackoverflow.com/questions/22676/how-do-i-download-a-file-over-http-using-python
    """

    u = urllib2.urlopen(url)
    f = open(fname, 'wb')
    meta = u.info()
    file_size = int(meta.getheaders("Content-Length")[0])
    print "Downloading: %s Bytes: %s" % (fname, file_size)

    file_size_dl = 0
    block_sz = 8192
    while True:
        buffer = u.read(block_sz)
        if not buffer:
            break

        file_size_dl += len(buffer)
        f.write(buffer)
        status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
        status = status + chr(8)*(len(status)+1)
        print status,
    f.close()

def DownloadFile(target, source, env):
    # is the tar there?
    if not os.path.exists(target):
        try:
            download_file(source, target)
        except:
            print "Error downloading %s (%s)", (target, source)
            sys.exit(1)


## from http://www.scons.org/wiki/RunningConfigureAndMake
# An SConscript file to untar a package, run configure and then make
# using a build directory.
#
# Replacing this with SConscript files written to compile just the
# parts of the package that we want would be quicker, but would take some time
# to do.
import time
import SCons.Script

def run(cmd):
    """Run a command and decipher the return code. Exit by default."""
    res = os.system(cmd)
    # Assumes that if a process doesn't call exit, it was successful
    if (os.WIFEXITED(res)):
        code = os.WEXITSTATUS(res)
        if code != 0:
            print "Error: return code: " + str(code)
            #if SCons.Script.keep_going_on_error == 0:
            sys.exit(code)

def ConfigureMake(target, source, env):
    """Run configure as necessary and then run make"""
    startdir = os.getcwd()   # restore cwd at the end
    pkg_name = str(target[0]).split('/')[-1]

    print SCons.Script.Dir('#/deps').abspath, pkg_name
    sourceDir = SCons.Script.Dir('#/deps').abspath + '/' + pkg_name

    # Untar the source tgz file
    if not os.path.exists(sourceDir):
        cmd = 'tar xfz %s -C %s' % (source[0].abspath, SCons.Script.Dir('#/deps').abspath)
        print '... untarring source files'
        run(cmd)

    buildDir = SCons.Script.Dir('#/build').abspath + '/deps/' + pkg_name

    if not os.path.exists(buildDir):
        os.makedirs(buildDir)
    os.chdir(buildDir)

    # See if configure needs to be run. Doesn't check for failed configures
    if os.path.exists('config.status'):
        print '... configure is up-to-date'
    else:
        # Customize the stock package here with other files if necessary
        # If configure needs interactive responses, write them to a file
        # config_responses and pass it as input to configure with 
        # >config_responses

        # Now run configure
        print '... running configure: ' + time.asctime(time.localtime(time.time()))
        print os.getcwd()
        cmd = sourceDir + '/configure CFLAGS="$CFLAGS -fno-strict-aliasing" --disable-shared '
        run(cmd)
        print '... configure done: ' + time.asctime(time.localtime(time.time()))

    # Always run make
    print '... running make: ' + time.asctime(time.localtime(time.time()))
    run('make -s')
    print '... make finished: ' + time.asctime(time.localtime(time.time()))
    os.chdir(startdir)    # restore the original cwd

    return 0

def log_output_fn(target, source, env):
    """The message seen in build logs when this action is called"""
    return "Building '%s'\n from '%s'\n at: %s" % (target[0], source[0],
      time.asctime(time.localtime(time.time())))
