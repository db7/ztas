/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#define __USE_GNU   // enable RUSAGE_THREAD
#include <sys/resource.h>
#include <sys/time.h>
#include <inttypes.h>
#include <ztas/stats/cpu.h>

struct cpu_stats {
    double t_thread;
    double t_user;
    double t_self;
    uint64_t t_last;
    FILE* fp;
};

static
uint64_t get_time()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return ((uint64_t) tv.tv_sec)*1000000000 + (uint64_t)(tv.tv_usec)*1000;
}

/* static struct timespec st, endt; */
/* static double t1= 0.0, time1= 0.0; */
cpu_stats_t*
cpu_stats_init(int32_t pid, const char* dir) {
    int ret = 0;
    double t;
    struct rusage  usage;
    struct timeval time;

    cpu_stats_t* stats = (cpu_stats_t*) malloc(sizeof(cpu_stats_t));
    assert (stats);

    char fname[100];
    if (!dir) dir = ".";
    sprintf(fname, "%s/cpu-%d.dat", dir, pid);

    printf("cpu stats file: %s\n", fname);
    stats->fp = fopen(fname, "w+");

    assert (stats->fp && "failed opening file");

    // measure time now
    stats->t_last = get_time();

    // measure thread CPU time
    ret = getrusage(RUSAGE_THREAD, &usage);

    if (ret == -1) {
        fprintf(stderr, "\n\n Error in getrusage command");
        exit(-1);
    }

    time = usage.ru_stime; // system time
    t = (double)time.tv_sec + (double) time.tv_usec / 1000000.0;
    time = usage.ru_utime; // user time
    stats->t_thread = t + (double)time.tv_sec + (double) time.tv_usec / 1000000.0;


    // measure self CPU time
    ret = getrusage(RUSAGE_SELF, &usage);

    if (ret == -1) {
        fprintf(stderr, "\n\n Error in getrusage command");
        exit(-1);
    }

    time = usage.ru_stime; // system time
    t = (double)time.tv_sec + (double) time.tv_usec / 1000000.0;
    time = usage.ru_utime; // user time
    stats->t_self = t + (double)time.tv_sec + (double) time.tv_usec / 1000000.0;

    return stats;
}

void
cpu_stats_fini(cpu_stats_t* stats)
{
    assert (stats);
    if (stats->fp)
        fclose(stats->fp);
    stats->fp = NULL;
    free(stats);
}

static double
cpu_measure_thread(cpu_stats_t* stats) {
    int kResult = 0;
    struct rusage  usage;
    struct timeval time;
    double t = 0;
    double elapsed = 0;

    // measure thread CPU time
    kResult = getrusage(RUSAGE_THREAD, &usage);

    if (kResult == -1)
        fprintf(stderr, "\n\n Error in getrusage command");

/*
    time = usage.ru_stime; // system time
    t = (double)time.tv_sec + (double) time.tv_usec / 1000000.0;
    time = usage.ru_utime; // user time
    t = t + (double)time.tv_sec + (double) time.tv_usec / 1000000.0;
*/
    time = usage.ru_utime; // user time
    t = (double)time.tv_sec + (double) time.tv_usec / 1000000.0;

    elapsed = t - stats->t_thread;
    stats->t_thread = t;

    return elapsed;
}


static double
cpu_measure_self(cpu_stats_t* stats) {
    int kResult = 0;
    struct rusage  usage;
    struct timeval time;
    double t = 0;
    double elapsed = 0;

    // measure self CPU time
    kResult = getrusage(RUSAGE_SELF, &usage);

    if (kResult == -1)
        fprintf(stderr, "\n\n Error in getrusage command");

    time = usage.ru_stime; // system time
    t = (double)time.tv_sec + (double) time.tv_usec / 1000000.0;
    time = usage.ru_utime; // user time
    t = t + (double)time.tv_sec + (double) time.tv_usec / 1000000.0;

    elapsed = t - stats->t_self;
    stats->t_self = t;

    return elapsed;
}

void
cpu_stats_measure(cpu_stats_t* stats) {
    // measure time now
    uint64_t now = get_time();
    double b_thread = cpu_measure_thread(stats);
    double b_self   = cpu_measure_self(stats);
    fprintf(stats->fp, "%"PRId64" %"PRId64" %f %f\n", now, now - stats->t_last, b_thread, b_self);
    fflush(stats->fp);

    stats->t_last = now;
}
