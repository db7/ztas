/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ev.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <ztas/proc/cfg.h>
#include <ztas/net/connection.h>
#include <ztas/stats/stats.h>

struct stats {
    connection_t* server;
    struct ev_loop* loop;
    FILE* fp;
    int32_t pid;
};

static
uint64_t get_time()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return ((uint64_t) tv.tv_sec)*1000000000 + (uint64_t)(tv.tv_usec)*1000;
}

stats_t*
stats_init(struct ev_loop* loop, int32_t pid, const cfg_t* cfg)
{
    const char* dir = cfg_get(cfg, "main:stats");
    if (!dir) return NULL;

    stats_t* stats = (stats_t*) malloc(sizeof(stats_t));
    assert (stats);
    stats->loop   = loop;
    stats->pid    = pid;

    //stats->server = connection_init(loop,

    char fname[100];
    sprintf(fname, "%s/stats-%d.dat", dir, pid);
    printf("stats file: %s\n", fname);

    stats->fp = fopen(fname, "w+");
    assert (stats->fp);

    return stats;
}

void
stats_stop(stats_t* stats)
{
}

void
stats_fini(stats_t* stats)
{
    fclose(stats->fp);
    free (stats);
}

void
stats_report(stats_t* stats, const char* topic,
                 const char* information)
{
    fprintf(stats->fp, "%lu:%d:%s:%s\n", get_time(),
            stats->pid, topic, information);
    //fflush(stats->fp);
    //printf("%d:%s:%s\n", stats->pid, topic, information);
}
