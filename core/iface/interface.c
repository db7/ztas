/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
* @addtogroup core
* @section interface_sec Simple algorithm-core-interface
* This implementation simply passes all requests from an algorithm to the core
* and vice versa.
* Other implementations of the interface layer are the Replayer (replayer.c) and
* the Fault Injector (ztas_fault_injector.cpp)
*/

#include <ztas/ztime.h>

#define LAYERU
#define LAYERD __
#include <ztas/layer/layer.h>

/* from algorithm */
extern       void* up_init(int32_t pid);
extern       void  up_fini(void* state);
extern       void  up_recv(void* state, const void* data, size_t size);
extern       void  up_trig(void* state, int32_t aid);

/* from framework */
extern       int   do_ucast(int32_t dst, const void* data, size_t size, int flags);
extern       int   do_bcast(const void *data, size_t size, int flags);
extern       int   do_alarm(int32_t aid, uint32_t ms);
extern       int   do_sched(int32_t aid, const ztime_t* at);
extern       void  do_clock(ztime_t* ts);
extern       void  do_term(void);
extern const char* do_cread(char* key);

/* dispatch calls to algorithm */
void*
do_init(int32_t pid)
{
    return up_init(pid);
}

void
do_fini(void* state)
{
    up_fini(state);
}

void
do_recv(void* state, const void* data, size_t size)
{
    up_recv(state, data, size);
}



void
do_trig(void* state, int32_t aid)
{
    up_trig(state, aid);
}

/* dispatch calls from algorithm to framework */
void
up_term (void)
{
    do_term();
}


int
up_ucast(int32_t dst, const void* data, size_t size, int flags)
{
    return do_ucast(dst, data, size, flags);
}

int
up_bcast(const void *data, size_t size, int flags)
{
    return do_bcast(data, size, flags);
}

int
up_alarm(int32_t aid, uint32_t ms)
{
    return do_alarm(aid, ms);
}

int
up_sched(int32_t aid, const ztime_t* at)
{
    return do_sched(aid, at);
}

void
up_clock(ztime_t* ts)
{
    do_clock(ts);
}

const char*
up_cread(char* key)
{
    return do_cread(key);
}
