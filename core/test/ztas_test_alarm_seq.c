/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ev.h>

#include <ztas/proc/proc.h>
#include <ztas/proc/cfg.h>
#include <ztas/proc/interface.h>

#include <ztas/log.h>
#include <ztas/mem.h>
proc_t* proc;


#define MAX_ALARM 10

typedef struct {
    int count;
} state_t;

void*
ztas_init(int32_t pid)
{
    state_t* s = (state_t*) malloc(sizeof(state_t));
    s->count = 0;
    ztas_alarm(s->count, 1);

    return (void*) s;
}

void
ztas_trig(void* state, int32_t aid)
{
    state_t* s = (state_t*) state;
    LOG1("alarm triggered: %d\n", aid);
    if (aid == MAX_ALARM) {proc_stop(proc); return;}
    ztas_alarm(++s->count, 1);
}


void
ztas_fini(void* state)
{
    free(state);
}

void ztas_recv(void* state, const void* data, size_t size){}

int
main(int argc, char* argv[])
{
    // prepare configuration file
    cfg_t* cfg = cfg_parse(NULL);
    cfg_set(cfg, "main:processes", "0");

    // initilize loop and proc
    struct ev_loop* loop = ev_default_loop(0);
    proc = proc_init_cfg(loop, 0, cfg);

    // run test
    ev_loop(loop, 0);

    // terminate
    proc_fini(proc);
    ev_loop_destroy(loop);
    cfg_close(cfg);

    return 0;
}
