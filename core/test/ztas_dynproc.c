/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas.h>
#include <ztas/mem.h>
#include <ztas/log.h>
#include <ztas/module.h>
#include <assert.h>

/* message type */
typedef struct {
    int32_t pid;
    enum { REQUEST, RESPONSE } type;
    ztime_t t0;
} message_t;

/* state type */
typedef struct {
    int32_t pid;
    ztime_t t0;
} self_t;

void*
ztas_init(int32_t pid)
{
    // create self state
    self_t* self = (self_t*) malloc(sizeof(self_t));
    assert (self && "failed to initialize self");

    LOG2("[%s] pid: %u\n", DYNPROC, pid);
    // initialize state and copy configuration
    self->pid = pid;
    ztas_clock(&self->t0);
    return self;
}


void
ztas_fini(void* state)
{
    free(state);
}


void
ztas_recv(void* state, const void* data, size_t size)
{
    // read current time
    ztime_t now;
    ztas_clock(&now);

    // load self state
    self_t* self = (self_t*) state;

    // receive message
    message_t* msg = (message_t*) data;
    assert (msg != NULL && "msg: NULL");

    LOG2("[%s] got a message in %u\n", DYNPROC, self->pid);
}

void
ztas_trig(void* state, int32_t aid)
{
    assert (0);
}

