/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/proc/cfg.h>
#include <ztas/proc/proc.h>
#include <ev.h>
#include <stdlib.h>

proc_t* p0 = NULL;
proc_t* p1 = NULL;

void
timeout_cb(struct ev_loop* loop, struct ev_timer* w, int revents)
{
    if (EV_ERROR & revents) {
        exit(EXIT_FAILURE);
    }
    proc_stop(p0);
    proc_stop(p1);
}

int
main(const int argc, const char* argv[])
{
    struct ev_loop* loop = ev_loop_new(0);

    cfg_t* cfg = cfg_parse(NULL);
    cfg_set(cfg, "main:processes", "0,1");
    cfg_set(cfg, "p0:mod", "build/core/test/libztas_dynproc1.so");
    cfg_set(cfg, "p1:mod", "build/core/test/libztas_dynproc2.so");

    p0 = proc_init_cfg(loop, 0, cfg);
    p1 = proc_init_cfg(loop, 1, cfg);


    struct ev_timer timer;
    ev_timer_init(&timer, &timeout_cb, 3., 0.);
    ev_timer_start(loop, &timer);
    ev_loop(loop, 0);

    proc_fini(p0);
    proc_fini(p1);

    cfg_close(cfg);

    ev_loop_destroy(loop);
    return 0;
}
