/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ev.h>

#include <ztas/proc/proc.h>
#include <ztas/proc/cfg.h>
#include <ztas/proc/interface.h>

#include <ztas/log.h>
#include <ztas/mem.h>
#include <ztas/string.h>
proc_t* proc;

void*
ztas_init(int32_t pid)
{
    ztas_alarm(1, 100);
    return NULL;
}

void
ztas_trig(void* state, int32_t aid)
{
    if (aid == 1) {
        const char message[] = "hello world";
        ztas_ucast(0, message, strlen(message) + 1, 0);
    } else {
        // we have to stop proc in the trigger otherwise the message
        // given to ztas_recv (ie, data) is freed twice (once by
        // proc_stop and once when the method returns)!

        proc_stop(proc);
    }
}


void ztas_fini(void* state){}

void ztas_recv(void* state, const void* data, size_t size)
{
    LOG1("message: %s\n", (char*) data);
    ztas_alarm(2, 0);
}

int
main(int argc, char* argv[])
{
    // prepare configuration file
    cfg_t* cfg = cfg_parse(NULL);
    cfg_set(cfg, "main:processes", "0");

    // initilize loop and proc
    struct ev_loop* loop = ev_default_loop(0);
    proc = proc_init_cfg(loop, 0, cfg);

    // run test
    ev_loop(loop, 0);

    // terminate
    proc_fini(proc);
    ev_loop_destroy(loop);
    cfg_close(cfg);

    return 0;
}
