/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/ds/tree.h>
#include <ztas/log.h>
#include <ztas/mem.h>
#include <assert.h>

typedef struct item item_t;
struct item {
    int32_t hash;
    void* value;
    item_t* left;
    item_t* right;
    item_t* parent;
};

struct tree {
    item_t* root;
};



tree_t*
tree_init()
{
    tree_t* t = (tree_t*) malloc(sizeof(tree_t));
    assert(t);
    t->root = NULL;
    return t;
}

static void*
put_recursive(item_t* ti, int32_t hash, void* value)
{
    // decide side
    if (ti->hash == hash)
        return ti->value;

    if (ti->hash > hash) {
        if (ti->left == NULL) {
            item_t* i = (item_t*) malloc(sizeof(item_t));
            i->hash = hash;
            i->value = value;
            i->left = NULL;
            i->right = NULL;
            i->parent = ti;
            ti->left = i;
            return NULL;
        } else {
            return put_recursive(ti->left, hash, value);
        }
    } else {
        if (ti->right == NULL) {
            item_t* i = (item_t*) malloc(sizeof(item_t));
            i->hash = hash;
            i->value = value;
            i->left = NULL;
            i->right = NULL;
            i->parent = ti;
            ti->right = i;
            return NULL;
        } else {
            return put_recursive(ti->right, hash, value);
        }
    }
    assert (0);
}

void*
tree_put(tree_t* t, int32_t hash, void* value)
{
    assert(t);
    if (t->root == NULL) {
        item_t* i = (item_t*) malloc(sizeof(item_t));
        i->hash = hash;
        i->value = value;
        i->left = NULL;
        i->right = NULL;
        i->parent = NULL;
        t->root = i;
        return NULL;
    } else {
        return put_recursive(t->root, hash, value);
    }
}


void*
get_recursive(item_t* ti, int32_t hash)
{
    if (ti == NULL) return NULL;

    if (ti->hash == hash)
        return ti->value;
    if (ti->hash > hash)
        return get_recursive(ti->left, hash);
    if (ti->hash < hash)
        return get_recursive(ti->right, hash);

    assert (0);
}

void*
tree_get(tree_t* t, int32_t hash)
{
    assert(t);
    return get_recursive(t->root, hash);
}


static void*
put_recursive_item(item_t* ti, item_t* tj)
{
    // decide side
    assert (ti->hash != tj->hash);

    if (ti->hash > tj->hash) {
        if (ti->left == NULL) {
            tj->parent = ti;
            ti->left = tj;
            return NULL;
        } else {
            return put_recursive_item(ti->left, tj);
        }
    } else {
        if (ti->right == NULL) {
            tj->parent = ti;
            ti->right = tj;
            return NULL;
        } else {
            return put_recursive_item(ti->right, tj);
        }
    }
    assert (0);
}


static void*
pop_recursive(item_t* ti, int32_t hash)
{
    if (ti == NULL) return NULL;

    if (ti->hash > hash)
        return pop_recursive(ti->left, hash);

    if (ti->hash < hash)
        return pop_recursive(ti->right, hash);

    if (ti->hash == hash) {
        void* value = ti->value;

        item_t* leave = NULL;
        if (ti->left) {
            // get max from left
            leave = ti->left;
            while (leave->right) leave = leave->right;

            if (leave == ti->left) {
                ti->left->parent = ti->parent;
                ti->left->right  = ti->right;
            } else {
                leave->parent->right = NULL;
                leave->parent = ti->parent;
                leave->right  = ti->right;

                put_recursive_item(leave, ti->left);
            }

            if (ti->right)
                ti->right->parent = leave;
        } else if (ti->right) {
            // get min from right
            leave = ti->right;
            while (leave->left) leave = leave->left;

            if (leave == ti->right) {
                ti->right->parent = ti->parent;
                ti->right->left   = ti->left;
            } else {
                leave->parent->left = NULL;
                leave->left   = ti->left;
                leave->parent = ti->parent;

                put_recursive_item(leave, ti->right);
            }

            if (ti->left)
                ti->left->parent = leave;
        }

        if (ti->parent) {
            if (ti->parent->right == ti)
                ti->parent->right = leave;
            if (ti->parent->left == ti)
                ti->parent->left = leave;
        }
        free(ti);

        return value;
    }
    assert (0);
}

void*
tree_pop(tree_t* t, int32_t hash)
{
    assert(t);

    if (t->root && t->root->hash == hash) {
        void* value = t->root->value;

        if (t->root->left && t->root->right) {
            put_recursive_item(t->root->left, t->root->right);
            item_t* old = t->root;
            t->root = t->root->left;
            free(old);
        } else if (t->root->left) {
            item_t* old = t->root;
            t->root = t->root->left;
            free(old);
        } else if (t->root->right) {
            item_t* old = t->root;
            t->root = t->root->right;
            free(old);
        } else {
            free(t->root);
            t->root = NULL;
        }

        return value;
    }
    return pop_recursive(t->root, hash);
}



void
tree_del(tree_t* t, int32_t hash)
{
    assert(t);
    void* x = pop_recursive(t->root, hash);
    if (x) free(x);
}



void
tree_fini(tree_t* t)
{
    assert(t);
    free(t);
}


static item_t*
popdf_recursive(item_t* ti)
{
    if (ti == NULL) return NULL;

    // any item on left
    item_t* i = popdf_recursive(ti->left);
    if (i != NULL) {
        if (ti->left == i) ti->left = NULL;
        return i;
    }

    // any item on right
    i = popdf_recursive(ti->right);
    if (i != NULL) {
        if (ti->right == i) ti->right = NULL;
        return i;
    }

    // neither left or right, then self
    return ti;
}

void*
tree_popdf(tree_t* t)
{
    assert(t);

    item_t* i = popdf_recursive(t->root);
    if (i == NULL) return NULL;

    if (i == t->root) t->root = NULL;

    void* data = i->value;
    free(i);
    return data;
}


// TODO: Check if this is fast (probably, could be done faster?)
/**
 * Pop the top element.
 * This is useful for iteration over the whole tree.
 */
void*
tree_pop_top(tree_t* t)
{
    assert(t);

    if (t->root) {
        void* value = t->root->value;

        if (t->root->left && t->root->right) {
            put_recursive_item(t->root->left, t->root->right);
            item_t* old = t->root;
            t->root = t->root->left;
            free(old);
        } else if (t->root->left) {
            item_t* old = t->root;
            t->root = t->root->left;
            free(old);
        } else if (t->root->right) {
            item_t* old = t->root;
            t->root = t->root->right;
            free(old);
        } else {
            free(t->root);
            t->root = NULL;
        }

        return value;
    }
    return NULL;
}

void
tree_it_init(tree_t* tree, tree_it_t* it)
{
    assert (tree);
    assert (it);
    it->tree = tree;
    it->item = tree->root;
    it->key  = 0;
}

static item_t*
next_sibling(item_t* item)
{
    if (!item) return NULL;
    if (!item->parent) return NULL;
    if (item->parent->left  == item) return item->parent->right;
    if (item->parent->right == item) return NULL;
    assert (0);
}

int32_t
tree_it_key(tree_it_t* it)
{
    return it->key;
}

void*
tree_it_next(tree_it_t* it)
{
    assert (it);
    assert (it->tree);

    item_t* item = it->item;
    if (!item) return NULL;
    it->key = item->hash;

    // has left child
    if (it->item->left) {
        it->item = it->item->left;
        return item->value;
    }

    // has right child
    if (it->item->right) {
        it->item = it->item->right;
        return item->value;
    }

    // go up
    item_t* next  = item;
    item_t* level = it->item->parent;
    while ((next = next_sibling(next)) == NULL && level) {
        next = level;
        level = level->parent;
    }

    it->item = next;
    return item->value;
}
