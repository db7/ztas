/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/mem.h>
#include <assert.h>
#include <ztas/ds/queue.h>

queue_t*
queue_create(int32_t size)
{
    queue_t* q = (queue_t*) malloc(sizeof(queue_t));
    assert(q && "Could not allocate queue");
    q->buffer = malloc(sizeof(void*) * size);
    assert (q->buffer && "Could not allocate queue buffer");
    q->size = size;
    q->head = q->tail = 0;
    return q;
}

int32_t
queue_enq(queue_t* q, void* i)
{
    assert(q && "Invalid queue");
    assert (i && "Invalid queue element");

    if (q->tail - q->head == q->size)
        return QUEUE_FULL;

    assert (q->tail - q->head < q->size && "Inconsistent tail and head");
    q->buffer[q->tail % q->size] = i;
    ++q->tail;
    return QUEUE_OK;
}

void*
queue_deq(queue_t* q)
{
    assert(q && "Invalid queue");
    if (q->tail - q->head == 0)
        return NULL;

    assert (q->head < q->tail && "Inconsistent tail and head");
    void* value = q->buffer[q->head % q->size];
    ++q->head;
    return value;
}

void*
queue_top(queue_t* q)
{
    assert(q && "Invalid queue");
    if (q->head < q->tail)
        return q->buffer[q->head % q->size];
    else
        return NULL;
}

void
queue_pop(queue_t* q)
{
    assert(q && "Invalid queue");
    assert(q->head < q->tail && "Should not be called");
    ++q->head;
}

int32_t
queue_empty(queue_t* q)
{
    return q->tail == q->head ? QUEUE_EMPTY : QUEUE_NEMPTY;
}

void
queue_free(queue_t* q)
{
    free(q->buffer);
    free(q);
}

int32_t
queue_size(queue_t* q)
{
    return q->tail - q->head;
}

int32_t
queue_fsize(queue_t* q)
{
    return q->size - queue_size(q);
}
