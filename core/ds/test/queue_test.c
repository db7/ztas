/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

/* author: Haosheng He
 * Name : queue_test.c
 * Date : 10.04.2013
 * Desc : Unit testing of Data structure queue.
*/

#include <ztas/ds/queue.h>
#include <ztas/log.h>
#include <ztas/mem.h>
#include <assert.h>
#include <stdlib.h>

void test_init();
void test_enq();
void test_deq();
void test_top();
void test_pop();
void test_empty();
void test_free();
void test_size();

int main(const int argc, const char* argv[])
{
    test_init();
    test_free();
    test_enq();
    test_deq();
    test_top();
    test_pop();
    test_empty(); 
    return 0;
}

void test_init()
{
    //log
    printf("Queue initialization test.");

    queue_t* test_queue = NULL;
    //test1
    int queue_size = 64;
    test_queue = queue_create(queue_size);
    assert (test_queue->size == 64);
    assert (test_queue->head == 0);
    assert (test_queue->tail == 0);
    //test2
    queue_size = 1024;
    test_queue = queue_create(queue_size);
    assert (test_queue->size == 1024);
    assert (test_queue->head == 0);
    assert (test_queue->tail == 0);
    //free
    queue_free(test_queue);

    //log
    printf("		Done.\n");
}

void test_enq()
{
    //log
    printf("Queue enqueue test.");
    //init
    queue_t* test_queue = NULL;
    int queue_size = 64;
    int ele = 'a';
    test_queue = queue_create(queue_size);
    //test 1
    //add elements
    queue_enq(test_queue, ele);
    assert(queue_top(test_queue) == 'a');
    //free queue
    queue_free(test_queue);
    //log
    printf("		Done.\n");
}

void test_deq()
{
    //log
    printf("Queue dequeue test.");
    //init
    queue_t* test_queue = NULL;
    int queue_size = 64;
    int ele[3];
    ele[0] = 'a';
    ele[1] = 'b';
    ele[2] = 'c';
    test_queue = queue_create(queue_size);
    //test 1
    //add elements
    queue_enq(test_queue, ele[0]);
    queue_enq(test_queue, ele[1]);
    queue_enq(test_queue, ele[2]);
    assert(queue_top(test_queue) == 'a');
    queue_deq(test_queue);
    assert(queue_top(test_queue) == 'b');
    queue_deq(test_queue);
    assert(queue_top(test_queue) == 'c');
    //free queue
    queue_free(test_queue);
    //log
    printf("		Done.\n");
}

void test_top()
{
    //log
    printf("Queue top test.");
    //init
    queue_t* test_queue = NULL;
    int queue_size = 64;
    int ele = 'a';
    test_queue = queue_create(queue_size);
    //test 1
    //add elements
    queue_enq(test_queue, ele);
    assert(queue_top(test_queue) == 'a');
    //delete elements
    queue_deq(test_queue);
    assert(queue_top(test_queue) == NULL);
    //free queue
    queue_free(test_queue);
    //log
    printf("		Done.\n");
}
void test_pop()
{
    //log
    printf("Queue dequeue test.");
    //init
    queue_t* test_queue = NULL;
    int queue_size = 64;
    int ele[3];
    ele[0] = 'a';
    ele[1] = 'b';
    ele[2] = 'c';
    test_queue = queue_create(queue_size);
    //test 1
    //add elements
    queue_enq(test_queue, ele[0]);
    queue_enq(test_queue, ele[1]);
    queue_enq(test_queue, ele[2]);
    assert(queue_top(test_queue) == 'a');
    queue_pop(test_queue);
    assert(queue_top(test_queue) == 'b');
    queue_pop(test_queue);
    assert(queue_top(test_queue) == 'c');
    //free queue
    queue_free(test_queue);
    //log
    printf("		Done.\n");
}

void test_empty()
{
    //log
    printf("Queue empty test.");
    //init
    queue_t* test_queue = NULL;
    int queue_size = 64;
    int ele = 'a';
    test_queue = queue_create(queue_size);
    //test 1
    //add elements
    queue_enq(test_queue, ele);
    assert(queue_empty(test_queue) != QUEUE_EMPTY);
    //delete elements
    queue_deq(test_queue);
    assert(queue_empty(test_queue) == QUEUE_EMPTY);
    //free queue
    queue_free(test_queue);
    //log
    printf("		Done.\n");
}

void test_free()
{
    //log
    printf("Queue free test.");
    //test
    queue_t* test_queue = NULL;
    int queue_size = 64;
    //init
    test_queue = queue_create(queue_size);
    //add elements
    queue_enq(test_queue, &queue_size);
    assert(test_queue->buffer != NULL);
    //free queue
    queue_free(test_queue);
    assert(test_queue->buffer == NULL);
    //log
    printf("		Done.\n");
}
