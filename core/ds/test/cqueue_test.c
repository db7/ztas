/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

/* author: Haosheng He
 * Name : cqueue_test.c
 * Date : 26.04.2013
 * Desc : Unit testing of Data structure cqueue.
*/
#define _ZTAS_CORE
#include <ztas/ds/cqueue.h>
#include <ztas/log.h>
#include <ztas/mem.h>
#include <assert.h>
#include <stdlib.h>

void test_init();
void test_enq();
void test_deq();
void test_top();
void test_pop();
void test_empty();
void test_free();

int main(const int argc, const char* argv[])
{
    test_init();
    test_free();
    test_enq();
    test_deq();
    test_top();
    test_pop();
    test_empty(); 
    return 0;
}

void test_init()
{
    //log
    printf("Cqueue initialization test.");

    cqueue_t* test_cqueue = NULL;
    //test1
    int cqueue_size = 64;
    test_cqueue = cqueue_create(cqueue_size);
    assert (test_cqueue->size == 64);
    assert (test_cqueue->head == 0);
    assert (test_cqueue->tail == 0);
    //test2
    cqueue_size = 1024;
    test_cqueue = cqueue_create(cqueue_size);
    assert (test_cqueue->size == 1024);
    assert (test_cqueue->head == 0);
    assert (test_cqueue->tail == 0);
    //free
    cqueue_free(test_cqueue);

    //log
    printf("		Done.\n");
}

void test_enq()
{
    //log
    printf("Cqueue encqueue test.");
    //init
    cqueue_t* test_cqueue = NULL;
    int cqueue_size = 64;
    int ele = 'a';
    test_cqueue = cqueue_create(cqueue_size);
    //test 1
    //add elements
    cqueue_enq(test_cqueue, ele);
    assert(cqueue_top(test_cqueue) == 'a');
    //free cqueue
    cqueue_free(test_cqueue);
    //log
    printf("		Done.\n");
}

void test_deq()
{
    //log
    printf("Cqueue decqueue test.");
    //init
    cqueue_t* test_cqueue = NULL;
    int cqueue_size = 64;
    int ele[3];
    ele[0] = 'a';
    ele[1] = 'b';
    ele[2] = 'c';
    test_cqueue = cqueue_create(cqueue_size);
    //test 1
    //add elements
    cqueue_enq(test_cqueue, ele[0]);
    cqueue_enq(test_cqueue, ele[1]);
    cqueue_enq(test_cqueue, ele[2]);
    assert(cqueue_top(test_cqueue) == 'a');
    cqueue_deq(test_cqueue);
    assert(cqueue_top(test_cqueue) == 'b');
    cqueue_deq(test_cqueue);
    assert(cqueue_top(test_cqueue) == 'c');
    //free cqueue
    cqueue_free(test_cqueue);
    //log
    printf("		Done.\n");
}

void test_top()
{
    //log
    printf("Cqueue top test.");
    //init
    cqueue_t* test_cqueue = NULL;
    int cqueue_size = 64;
    int ele = 'a';
    test_cqueue = cqueue_create(cqueue_size);
    //test 1
    //add elements
    cqueue_enq(test_cqueue, ele);
    assert(cqueue_top(test_cqueue) == 'a');
    //delete elements
    cqueue_deq(test_cqueue);
    assert(cqueue_top(test_cqueue) == NULL);
    //free cqueue
    cqueue_free(test_cqueue);
    //log
    printf("		Done.\n");
}
void test_pop()
{
    //log
    printf("Cqueue decqueue test.");
    //init
    cqueue_t* test_cqueue = NULL;
    int cqueue_size = 64;
    int ele[3];
    ele[0] = 'a';
    ele[1] = 'b';
    ele[2] = 'c';
    test_cqueue = cqueue_create(cqueue_size);
    //test 1
    //add elements
    cqueue_enq(test_cqueue, ele[0]);
    cqueue_enq(test_cqueue, ele[1]);
    cqueue_enq(test_cqueue, ele[2]);
    assert(cqueue_top(test_cqueue) == 'a');
    cqueue_pop(test_cqueue);
    assert(cqueue_top(test_cqueue) == 'b');
    cqueue_pop(test_cqueue);
    assert(cqueue_top(test_cqueue) == 'c');
    //free cqueue
    cqueue_free(test_cqueue);
    //log
    printf("		Done.\n");
}

void test_empty()
{
    //log
    printf("Cqueue empty test.");
    //init
    cqueue_t* test_cqueue = NULL;
    int cqueue_size = 64;
    int ele = 'a';
    test_cqueue = cqueue_create(cqueue_size);
    //test 1
    //add elements
    cqueue_enq(test_cqueue, ele);
    assert(cqueue_empty(test_cqueue) != CQUEUE_EMPTY);
    //delete elements
    cqueue_deq(test_cqueue);
    assert(cqueue_empty(test_cqueue) == CQUEUE_EMPTY);
    //free cqueue
    cqueue_free(test_cqueue);
    //log
    printf("		Done.\n");
}

void test_free()
{
    //log
    printf("Cqueue free test.");
    //test
    cqueue_t* test_cqueue = NULL;
    int cqueue_size = 64;
    //init
    test_cqueue = cqueue_create(cqueue_size);
    //add elements
    cqueue_enq(test_cqueue, &cqueue_size);
    assert(test_cqueue->buffer != NULL);
    //free cqueue
    cqueue_free(test_cqueue);
    assert(test_cqueue->buffer == NULL);
    //log
    printf("		Done.\n");
}
