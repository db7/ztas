/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/ds/idset.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

void
test_simple()
{
    idset_t* set = idset_init(0, 10);
    
    idset_add(set, 1);
    assert (idset_size(set) == 1);

    idset_add(set, 3);
    assert (idset_size(set) == 2);

    idset_add(set, 6);
    assert (idset_size(set) == 3);

    idset_it_t it;
    idset_it_init(set, &it);
    assert(idset_it_next(&it) == 1);
    assert(idset_it_next(&it) == 3);
    assert(idset_it_next(&it) == 6);
    assert(idset_it_next(&it) == -1);
    assert(idset_it_next(&it) == -1);

    idset_del(set, 5);
    assert (idset_size(set) == 3);

    idset_del(set, 3);
    assert (idset_size(set) == 2);
    idset_it_init(set, &it);
    assert(idset_it_next(&it) == 1);
    assert(idset_it_next(&it) == 6);
    assert(idset_it_next(&it) == -1);
}

void
test_min()
{
    idset_t* set = idset_init(10, 10);
    
    idset_add(set, 11);
    assert (idset_size(set) == 1);

    idset_add(set, 13);
    assert (idset_size(set) == 2);

    idset_add(set, 16);
    assert (idset_size(set) == 3);

    idset_it_t it;
    idset_it_init(set, &it);
    assert(idset_it_next(&it) == 11);
    assert(idset_it_next(&it) == 13);
    assert(idset_it_next(&it) == 16);
    assert(idset_it_next(&it) == -1);
    assert(idset_it_next(&it) == -1);

    idset_del(set, 15);
    assert (idset_size(set) == 3);

    idset_del(set, 13);
    assert (idset_size(set) == 2);
    idset_it_init(set, &it);
    assert(idset_it_next(&it) == 11);
    assert(idset_it_next(&it) == 16);
    assert(idset_it_next(&it) == -1);
}

int
main(const int argc, const char* argv[])
{
    test_simple();
    test_min();

    return 0;
}
