/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/ds/tree.h>
#include <ztas/log.h>
#include <ztas/mem.h>
#include <assert.h>

void
test1()
{
   tree_t* t = tree_init();

   // insert item
    char* value = "hello world";
    tree_put(t, 1234, value);

    char* x = tree_get(t, 1234);
    assert (x == value);

    x = tree_pop(t, 1234);
    assert (x == value);

    x = tree_get(t, 1234);
    assert (x == NULL);


    tree_put(t, 10, value);
    tree_put(t, 5, value);
    tree_put(t, 8, value);


    tree_put(t, 16, value);
    tree_put(t, 7, value);

    x = tree_pop(t, 5);
    assert (x == value);

    tree_put(t, 9, value);

    x = tree_pop(t, 8);
    assert (x == value);

    tree_put(t, 5, value);

    x = tree_pop(t, 5);
    assert (x == value);

    x = tree_pop(t, 6);
    assert (x == NULL);

    x = tree_pop(t, 7);
    assert (x == value);

    x = tree_pop(t, 7);
    assert (x == NULL);

    tree_fini(t);
}

void
test2()
{
    tree_t* t = tree_init();
    tree_fini(t);
}

void
test3()
{
    tree_t* t = tree_init();
    char* value = "hello world";

    // prepare tree
    tree_put(t, 1234, "hallo 1234");
    tree_put(t, 10, "hallo 10");
    tree_put(t, 5, "hallo 5");
    tree_put(t, 8, "hallo 8");
    tree_put(t, 16, "hallo 16");
    tree_put(t, 7, "hallo 7");
    tree_pop(t, 5);
    tree_put(t, 9, "hallo 9");
    tree_pop(t, 8);
    tree_put(t, 5, "hallo 5_2");
    tree_pop(t, 7);
    tree_put(t, 8, "hallo 8_2");

    tree_it_t it;
    tree_it_init(t, &it);
    char* x;
    while ((x = tree_it_next(&it))) {
        printf("%s\n", x);
    }
    tree_fini(t);
}


int
main(const int argc, const char* argv[])
{
    test1();
    test2();
    test3();
    return 0;
}
