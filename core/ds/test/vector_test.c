/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

/* author: Haosheng He
 * Name : vector_test.c
 * Date : 10.04.2013
 * Desc : Unit testing of Data structure vector.
*/

#include <ztas/ds/vector.h>
#include <ztas/log.h>
#include <ztas/mem.h>
#include <assert.h>
#include <stdlib.h>

/*
typedef struct {
    int32_t max_size;
    int32_t size;
    int32_t array[];
} vector_t;

vector_t* vector_init(int32_t max_size);
vector_t* vector_init_copy(vector_t* vector);
void vector_fini(vector_t* vector);
void vector_clear(vector_t* vector);
int vector_push(vector_t* vector, int32_t value);
int vector_has(vector_t* vector, int32_t value);
void vector_sort(vector_t* vector);
const int32_t* vector_array(vector_t* vector);
int32_t vector_size(vector_t* vector);

#define VECTOR_OK 1
#define VECTOR_NOK 0
#define VECTOR_FULL -1
*/

void test_init();
void test_copy();
void test_clear();
void test_push();
void test_has();
void test_sort();
void test_size();

int main(const int argc, const char* argv[])
{
    test_init();
    test_copy();
    test_clear();
    test_push();
    test_has();
    test_sort();
    test_size();
    return 0;
}

void test_init()
{
    //log
    printf("Vector initialization test.");
    //init
    int32_t size = 64;
    vector_t* vector = vector_init(size);
    assert(vector);
    assert(vector->max_size == 64);
    assert(vector->size == 0);
    //free
    vector_fini(vector);
    //log
    printf("		Done.\n");
}
void test_copy()
{
    //log
    printf("Vector copy test.");
    int32_t size = 64;
    vector_t* vector = vector_init(size);
    vector_t* vector_copy = NULL;
    assert(vector);
    int ret = -1;
    ret = vector_push(vector, 512);
    assert(ret == VECTOR_OK);
    vector_copy = vector_init_copy(vector);
    assert(vector_copy);
    assert(vector_copy->max_size == vector->max_size);
    assert(vector_copy->size == vector->size);
    assert(vector_copy->array[0] == vector->array[0]);
    //free
    vector_fini(vector);
    //log
    printf("		Done.\n");
}
void test_clear()
{
    //log
    printf("Vector clear test.");
    int32_t size = 64;
    vector_t* vector = vector_init(size);
    assert(vector);
    int ret = -1;
    ret = vector_push(vector, 512);
    assert(ret == VECTOR_OK);
    vector_clear(vector);
    assert(vector->size == 0);
    //free
    vector_fini(vector);
    //log
    printf("		Done.\n");
}
void test_push()
{
    //log
    printf("Vector push test.");
    int32_t size = 64;
    vector_t* vector = vector_init(size);
    assert(vector);
    assert(vector->size == 0);
    int ret = -1;
    int32_t val0 = 512,val1 = 1024;
    ret = vector_push(vector, val0);
    assert(ret == VECTOR_OK);
    assert(vector->size == 1);
    assert(vector->array[0] == val0);
    ret = vector_push(vector, val1);
    assert(ret == VECTOR_OK);
    assert(vector->size == 2);
    assert(vector->array[1] ==val1);
    //free
    vector_fini(vector);    
    //log
    printf("		Done.\n");
}
void test_has()
{
    //log
    printf("Vector has test.");
    int32_t size = 64;
    vector_t* vector = vector_init(size);
    assert(vector);
    assert(vector->size == 0);
    int ret = -1;
    int32_t val0 = 512, val1 = 1024, val2 = 2048;
    ret = vector_push(vector, val0);
    assert(ret == VECTOR_OK);
    ret = vector_push(vector, val1);
    assert(ret == VECTOR_OK);
    ret = vector_push(vector, val2);
    assert(ret == VECTOR_OK);
    assert(vector_has(vector, val1) == VECTOR_OK);
    //free
    vector_fini(vector);    
    //log
    printf("		Done.\n");
}
void test_sort()
{
    //log
    printf("Vector sort test.");
    int32_t size = 64;
    vector_t* vector = vector_init(size);
    assert(vector);
    assert(vector->size == 0);
    int ret = -1;
    int32_t val0 = 128, val1 = 256, val2 = 512, val3 = 1024;
    ret = vector_push(vector, val2);
    assert(ret == VECTOR_OK);
    ret = vector_push(vector, val0);
    assert(ret == VECTOR_OK);
    ret = vector_push(vector, val3);
    assert(ret == VECTOR_OK);
    ret = vector_push(vector, val1);
    assert(ret == VECTOR_OK);
    vector_sort(vector);
    assert(vector->array[0] <= vector->array[1]);
    assert(vector->array[1] <= vector->array[2]);
    assert(vector->array[2] <= vector->array[3]);
    //free
    vector_fini(vector);    
    //log
    printf("		Done.\n");
}

void test_size()
{
    //log
    printf("Vector size test.");
    int32_t size = 64;
    vector_t* vector = vector_init(size);
    assert(vector);
    assert(vector->size == 0);
    int ret = -1;
    int32_t val0 = 512, val1 = 1024, val2 = 256;
    ret = vector_push(vector, val0);
    assert(ret == VECTOR_OK);
    assert(vector->size == 1);
    ret = vector_push(vector, val1);
    assert(ret == VECTOR_OK);
    assert(vector->size == 2);
    ret = vector_push(vector, val2);
    assert(ret == VECTOR_OK);
    assert(vector->size == 3);
    //free
    vector_fini(vector);    
    //log
    printf("		Done.\n");
}
