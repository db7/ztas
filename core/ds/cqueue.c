/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <stdlib.h>
#include <assert.h>
#include <ztas/ds/cqueue.h>

cqueue_t*
cqueue_create(int size)
{
    cqueue_t* q = (cqueue_t*) malloc(sizeof(cqueue_t));
    assert(q && "Could not allocate cqueue");
    q->buffer = malloc(sizeof(void*) * size);
    assert (q->buffer && "Could not allocate cqueue buffer");
    q->size = size;
    q->head = q->tail = 0;

    pthread_spin_init(&q->lock, PTHREAD_PROCESS_PRIVATE);
    return q;
}

int
cqueue_enq(cqueue_t* q, void* i)
{
    assert(q && "Invalid cqueue");
    assert(i && "Invalid cqueue element");

    pthread_spin_lock(&q->lock);
    if (q->tail - q->head < q->size) {
        q->buffer[q->tail % q->size] = i;
        ++q->tail;
        pthread_spin_unlock(&q->lock);
        return CQUEUE_OK;
    } else {
        assert (q->tail - q->head == q->size && "Inconsistent tail and head");
        pthread_spin_unlock(&q->lock);
        return CQUEUE_FULL;
    }
}

void*
cqueue_deq(cqueue_t* q)
{
    assert(q && "Invalid cqueue");
    pthread_spin_lock(&q->lock);
    if (q->head < q->tail) {
        void* value = q->buffer[q->head % q->size];
        ++q->head;
        pthread_spin_unlock(&q->lock);
        return value;
    } else {
        pthread_spin_unlock(&q->lock);
        return NULL;
    }
}

void*
cqueue_top(cqueue_t* q)
{
    assert(q && "Invalid cqueue");
    pthread_spin_lock(&q->lock);
    if (q->head < q->tail) {
        pthread_spin_unlock(&q->lock);
        return q->buffer[q->head % q->size];
    } else {
        pthread_spin_unlock(&q->lock);
        return NULL;
    }
}

void
cqueue_pop(cqueue_t* q)
{
    assert(q && "Invalid cqueue");
    assert(q->head < q->tail && "Should not be called");
    pthread_spin_lock(&q->lock);
    ++q->head;
    pthread_spin_unlock(&q->lock);
}

int
cqueue_empty(cqueue_t* q)
{
    pthread_spin_lock(&q->lock);
    int r = q->tail == q->head ? CQUEUE_EMPTY : CQUEUE_NEMPTY;
    pthread_spin_unlock(&q->lock);
    return r;
}

void
cqueue_free(cqueue_t* q)
{
    pthread_spin_destroy(&q->lock);
    free(q->buffer);
    free(q);
}
