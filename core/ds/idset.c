/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/ds/idset.h>
#include <ztas/types.h>
#include <ztas/string.h>
#include <ztas/mem.h>
#include <assert.h>

#define IDSET_MAX 10000

/* this set contains a range from min up to min+N
 */

struct idset {
    int32_t N;
    int32_t min;
    int32_t size;
    char array[0];
};

idset_t*
idset_init(int32_t min, int32_t N)
{
    assert (N > 0 && N < IDSET_MAX);
    idset_t* set = (idset_t*) malloc(sizeof(idset_t) + N);
    assert (set);
    set->N    = N;
    set->size = 0;
    set->min  = min;
    bzero(set->array, N);
    return set;
}

idset_t*
idset_init_copy(const idset_t* src)
{
    assert(src && "set is null");
    idset_t* set = (idset_t*) malloc(sizeof(idset_t) + src->N);
    assert (set);
    memcpy((void*) set, (const void*) src, sizeof(idset_t) + src->N);
    return set;
}

void
idset_copy(idset_t* dst, const idset_t* src)
{
    assert(dst && "destination set is null");
    assert(src && "destination set is null");
    assert(dst->N == src->N && "set sizes don't match");
    memcpy((void*) dst, (void*) src, sizeof(idset_t) + src->N);
}

void
idset_fini(idset_t* set)
{
    assert (set && "set is null");
    free(set);
}

int32_t
idset_size(const idset_t* set)
{
    assert (set && "set is null");
    return set->size;
}

void
idset_add(idset_t* set, int32_t id)
{
    assert (set && "set is null");
    id -= set->min;
    assert (id >= 0 && id < set->N);
    if (set->array[id] == 0) {
        set->array[id] = 1;
        ++set->size;
    }
}

void
idset_del(idset_t* set, int32_t id)
{
    assert (set && "set is null");
    id -= set->min;
    assert (id >= 0 && id < set->N);
    if (set->array[id]) {
        set->array[id] = 0;
        --set->size;
    }
}

void
idset_clear(idset_t* set)
{
    assert (set && "set is null");
    bzero(set->array, set->N);
    set->size = 0;
}

void
idset_it_init(idset_t* set, idset_it_t* it)
{
    assert (set && "set is null");
    assert (it);
    it->set = set;
    it->index = 0;
}

int32_t
idset_it_next(idset_it_t* it)
{
    assert (it && "set iterator is null");
    assert (it->set);
    while (it->index < it->set->N && it->set->array[it->index] == 0) ++it->index;

    if (it->index == it->set->N) return -1;

    return it->set->min + it->index++;
}

// adds id's inside list to an idset
idset_t*
idset_init_str(const char* list)
{
    assert(list && "list string is null");
    idset_t* idset = idset_init(0, 1002);
    
    int32_t b = 0;
    int32_t e = 0;

    for (;; b = ++e) {
        while (list[e] != ',' && list[e] != '\n' && list[e] != '\0' && list[e] != ';') ++e;
        while (list[b] == ' ') ++b;
        if (b == e) break;

        int32_t pid;
        char cpid[64];
#ifdef ENCODED
        memcpy(cpid, list+b, e-b);
#else
        strncpy(cpid, list+b, e-b);     //maybe use memcpy to, then ifdef is obsolete ?
#endif
        cpid[e-b] = '\0';
        pid = atoi(cpid);
        idset_add(idset, pid);

        if (list[e] == '\0' || list[e] == '\n' || list[e] == ';') break;
    }

    return idset;
}
