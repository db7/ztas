/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/mem.h>
#include <ztas/log.h>
#include <ztas/string.h>
#include <assert.h>
#include <ztas/ds/vector.h>

vector_t*
vector_init(int32_t max_size)
{
    vector_t* vector = (vector_t*) malloc(sizeof(vector_t) +
                                          max_size*sizeof(int32_t));
    assert (vector);
    bzero(vector, sizeof(vector_t) + max_size*sizeof(int32_t));
    vector->max_size = max_size;
    vector->size = 0;
    return vector;
}

vector_t*
vector_init_copy(vector_t* vector)
{
    vector_t* copy = (vector_t*) malloc(sizeof(vector_t) +
                                        vector->max_size*sizeof(int32_t));
    assert (vector);
    memcpy(copy, vector, sizeof(vector_t) +
           vector->max_size*sizeof(int32_t));
    return copy;
}

void
vector_fini(vector_t* vector)
{
    free(vector);
}

void
vector_clear(vector_t* vector)
{
    vector->size = 0;
}

int
vector_push(vector_t* vector, int32_t value)
{
    if (vector->size < vector->max_size) {
        vector->array[vector->size++] = value;
        return VECTOR_OK;
    }
    return VECTOR_FULL;
}

int
vector_has(vector_t* vector, int32_t value)
{
    int i;
    for (i = 0; i < vector->size; ++i)
        if (vector->array[i] == value) return VECTOR_OK;
    return VECTOR_NOK;
}


/**
 * a simple bubble sort
 */
static void
bsort(int32_t* array, int32_t size)
{
    int32_t i, j, v;
    for (i = (size - 1); i > 0; --i) {
        for (j = 1; j <= i; ++j) {
            if (array[j-1] > array[j]) {
                v = array[j-1];
                array[j-1] = array[j];
                array[j] = v;
            }
        }
    }
}

void
vector_sort(vector_t* vector)
{
    bsort(vector->array, vector->size);
}

const int32_t*
vector_array(vector_t* vector)
{
    return vector->array;
}


int32_t
vector_size(vector_t* vector)
{
    return vector->size;
}
