#include <zp.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

void
zp_init_test()
{
  zp_t* zp = zp_init(0, NULL);
  zp_fini(zp);
}

void
zp_alarm_test()
{
  zp_t* zp = zp_init(0, NULL);
  zp_alarm(zp, 123, 10);
  zmsg_t msg;
  int r = zp_recv(zp, &msg);
  assert (r == 123);
  zp_fini(zp);
}

void
zp_ucast_test()
{
  zp_t* zp = zp_init(0, NULL);
  zp_alarm(zp, 123, 10);
  const char* m = "hallo welt";
  zmsg_t msg;
  msg.data = m;
  msg.size = strlen(m);
  int r = zp_ucast(zp, 0, &msg, 0);
  assert (r == 0);

  zmsg_t msg2;
  r = zp_recv(zp, &msg2);
  assert (r == 0);
  printf ("%s\n", ((char*)msg2.data));
  zp_fini(zp);
}

void
zp_timed_ucast_test()
{
  zp_t* zp = zp_init(0, NULL);
  zp_alarm(zp, 123, 10);
  const char* m = "hallo welt";
  zmsg_t msg;
  msg.data = m;
  msg.size = strlen(m);

  zp_alarm(zp, 123, 1000);
  int r = zp_ucast(zp, 0, &msg, 0);
  assert (r == 0);

  zmsg_t msg2;
  r = zp_recv(zp, &msg2);
  assert (r == 0);
  printf ("%s\n", ((char*)msg2.data));

  r = zp_recv(zp, &msg2);
  assert (r == 123);
  printf ("timed out\n");

  zp_fini(zp);
}

int
main(const int argc, const char* argv[])
{
  zp_init_test();
  zp_alarm_test();
  zp_ucast_test();
  zp_timed_ucast_test();
  return 0;
}
