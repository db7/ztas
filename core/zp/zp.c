#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <zp.h>
#include "../proc/proc.h"
#include <ztas/ds/queue.h>
#include <ztas/proc/cfg.h>
#include <ztas/shrp.h>
#include <ev.h>
#include <errno.h>
#include <assert.h>
#include <string.h>

struct zp {
    pthread_mutex_t lock;
    pthread_cond_t  cenq;
    pthread_cond_t  cdeq;
    pthread_t       thread;
    struct ev_loop* loop;

    proc_t*         proc;
    queue_t*        queue;   // queue of events
    queue_t*        fqueue;  // free queue of events

    int             wdeq;    // waiting dequeuer count
    int             wenq;    // waiting enqueuer flag

    int32_t         pid;
    // address
    // port
};

/* struct zmsg { */
/*     const void* data; */
/*     size_t size; */
/* }; */

typedef struct zev {
    enum { ALARM, MSG } type;
    int32_t aid;
    zmsg_t msg;
} zev_t;

static void*
zp_run(void* arg)
{
    zp_t* zp = (zp_t*) arg;
    ev_run(zp->loop, 0);
    return NULL;
}

static void* _init(int32_t pid) { return NULL; }
static void  _fini(void* state) {}
static void
_recv(void* state, const void* data, size_t size)
{
    zp_t* zp = (zp_t*) state;
    shrp_hold((void*)data);

    pthread_mutex_lock(&zp->lock);
    zev_t* zev = queue_deq(zp->fqueue);
    assert (zev);
    zev->type = MSG;
    zev->msg.data = data;
    zev->msg.size = size;
    if (queue_enq(zp->queue, zev) == QUEUE_FULL) {
        zp->wenq = 1;
        pthread_cond_wait(&zp->cenq, &zp->lock);
    }

    if (zp->wdeq) { // unblock dequeuers
        pthread_cond_broadcast(&zp->cdeq);
    }

    pthread_mutex_unlock(&zp->lock);
}

static void
_trig(void* state, int32_t aid)
{
    zp_t* zp = (zp_t*) state;

    pthread_mutex_lock(&zp->lock);
    zev_t* zev = queue_deq(zp->fqueue);
    assert (zev);
    zev->type = ALARM;
    zev->aid = aid;
    zev->msg.data = NULL;
    zev->msg.size = 0;
    if (queue_enq(zp->queue, zev) == QUEUE_FULL) {
        zp->wenq = 1;
        pthread_cond_wait(&zp->cenq, &zp->lock);
    }

    if (zp->wdeq) { // unblock dequeuers
        pthread_cond_broadcast(&zp->cdeq);
    }
    pthread_mutex_unlock(&zp->lock);
}

void*  __ztas_init(int32_t pid)
{ assert (0); return NULL; }
void   __ztas_fini(void* state)
{ assert (0);}
void   __ztas_recv(void* state, const void* data, size_t size)
{ assert (0); }
void   __ztas_trig(void* state, int32_t aid)
{ assert (0); }



zp_t*
zp_init(int32_t pid, const char* fname)
{
    zp_t* zp = (zp_t*) malloc(sizeof(zp_t));
    zp->pid = pid;

    // cfg can be either a filename or a server IP:port
    // split other in a ':'

    zp->loop = ev_loop_new(0);

    ztas_uif_t uif = {_init, _fini, _recv, _trig};
    cfg_t* cfg = cfg_parse("exampleConfig.ini");
    zp->proc = proc_init_cfg_uif(zp->loop, pid, cfg, &uif);

    // add state to proc
    zp->proc->state = zp;

    // no waiting threads
    zp->wenq = 0;
    zp->wdeq = 0;

    // create queues
    zp->queue = queue_create(1000);
    zp->fqueue = queue_create(1000);

    // create empty events
    int i = 1000;
    while (i--) {
        zev_t* zev = (zev_t*) malloc(sizeof(zev_t));
        bzero(zev, sizeof(zev_t));
        if (queue_enq(zp->fqueue, zev) == QUEUE_FULL) {
            assert (0 && "should not happen");
        }
    }

    // initialize lock and conditional variables
    pthread_mutex_init(&zp->lock, NULL);
    pthread_cond_init(&zp->cdeq, NULL);
    pthread_cond_init(&zp->cenq, NULL);

    // initialize thread
    if (pthread_create(&zp->thread, NULL, zp_run, zp) == 1) {
        perror("pthread_create");
        exit(EXIT_FAILURE);
    }
    return zp;
}

void
zp_fini(zp_t* zp)
{
    proc_stop(zp->proc);
    pthread_join(zp->thread, NULL);
    proc_fini(zp->proc);
    free(zp);
}

int
zp_ucast(zp_t* zp, int32_t dst, zmsg_t* msg, int flags)
{
    pthread_mutex_lock(&zp->lock);
    int r = proc_ucast(zp->proc, dst, msg->data, msg->size, flags);
    pthread_mutex_unlock(&zp->lock);
    return r;
}

int
zp_bcast(zp_t* zp, zmsg_t* msg, int flags)
{
    pthread_mutex_lock(&zp->lock);
    int r = proc_bcast(zp->proc, msg->data, msg->size, flags);
    pthread_mutex_unlock(&zp->lock);
    return r;
}

int
zp_alarm(zp_t* zp, int32_t aid, uint32_t ms)
{
    pthread_mutex_lock(&zp->lock);
    int r = proc_alarm(zp->proc, aid, ms);
    pthread_mutex_unlock(&zp->lock);
    return r;
}

int
zp_sched(zp_t* zp, int32_t aid, const ztime_t* at)
{
    pthread_mutex_lock(&zp->lock);
    int r = proc_sched(zp->proc, aid, at);
    pthread_mutex_unlock(&zp->lock);
    return r;
}

int
zp_recv(zp_t* zp, zmsg_t* msg)
{
    pthread_mutex_lock(&zp->lock);
    while (queue_empty(zp->queue)) {
        ++zp->wdeq;
        pthread_cond_wait(&zp->cdeq, &zp->lock);
        --zp->wdeq;
    }

    zev_t* zev = queue_deq(zp->queue);
    assert (zev && "cannot happen");

    int ret;
    if (zev->type == ALARM) {
        assert (zev->aid >= 0);
        ret = zev->aid;
    } else {
        msg->data = zev->msg.data;
        msg->size = zev->msg.size;
        ret = 0; // msg ok
    }

    if (queue_enq(zp->fqueue, zev) == QUEUE_FULL) {
        assert (0);
    }

    if (zp->wenq) { // unblock enqueuer
        zp->wenq = 0;
        pthread_cond_signal(&zp->cenq);
    }

    pthread_mutex_unlock(&zp->lock);

    return ret;
}
