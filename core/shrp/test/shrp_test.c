/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

/* author: Haosheng He
 * Name : shrp_test.c
 * Date : 21.04.2013
 * Desc : Unit testing of shared pointer.
*/

#define SHRP_SPINLOCK 0xFFFFAAAA

#include <ztas/shrp.h>
#include <ztas/log.h>
#include <ztas/mem.h>
#include <assert.h>
#include <stdint.h>

#ifdef SHRP_SPINLOCK
#include <pthread.h>
#endif

#define MAGIC 0xFEEDDEAD

typedef struct {
#ifdef SHRP_SPINLOCK
    pthread_spinlock_t lock;
#endif
    uint32_t users;
    uint32_t size;
    uint32_t magic;
    char data[0];
} shrp_t;

void test_malloc();
void test_realloc();
void test_free();
void test_hold();
void test_holdcpy();

int main(int argc, char* argv[])
{
    test_malloc();
    test_realloc();
    test_free();
    test_hold();
    test_holdcpy();
    return 0;
}

void test_malloc()
{
    //log
    printf("shared pointer malloc test.");

    unsigned int size = 32;
    void* p_mem = shrp_malloc(size);
    shrp_t* p_shrp = (shrp_t*)(((char*)p_mem) - sizeof(shrp_t));
    assert(p_shrp->users == 1);
    assert(p_shrp->size == 32);
    assert(p_shrp->magic == MAGIC);
    assert(p_shrp->data == (char*)p_mem);
    shrp_free(p_mem);
    //log
    printf("		Done.\n");
}

void test_realloc()
{
    //log
    printf("shared pointer realloc test.");

    printf (0 && "not implemented");

    //log
    printf("		Done.\n");
}

void test_free()
{
    //log
    printf("shared pointer free test.");

    unsigned int size = 32;
    void* p_mem = shrp_malloc(size);
    shrp_free(p_mem);
    //log
    printf("		Done.\n");
}

void test_hold()
{
    //log
    printf("shared pointer hold test.");

    unsigned int size = 32;
    uint32_t old_users = -1;
    void* p_mem = shrp_malloc(size);
    shrp_t* p_shrp = (shrp_t*)(((char*)p_mem) - sizeof(shrp_t));
    old_users = p_shrp->users;
    shrp_hold(p_mem);
    assert(old_users && p_shrp->users && (old_users -= p_shrp->users));
    shrp_free(p_mem);
    //log
    printf("		Done.\n");
}

void test_holdcpy()
{
    //log
    printf("shared pointer holdcpy test.");
    //test shared pointer
    unsigned int size = 32;
    uint32_t old_users = -1;
    void* p_mem = shrp_malloc(size);
    void** p_mem_ = malloc(sizeof (int));
    *p_mem_ = p_mem;
    shrp_t* p_shrp = (shrp_t*)(((char*)p_mem) - sizeof(shrp_t));
    old_users = p_shrp->users;
    shrp_holdcpy(p_mem_,size);
    assert(old_users && p_shrp->users && (old_users -= p_shrp->users));
    shrp_free(p_mem);
    //test normal pointer
    p_mem = (void*)malloc(size);
    *p_mem_ = p_mem;
    shrp_holdcpy(p_mem_,size);
    assert(*p_mem_ != p_mem);
    free(p_mem);
    free(p_mem_);
    //log
    printf("		Done.\n");
}

