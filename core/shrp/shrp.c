/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

/**
 * Shared pointer implementation
 *
 *
 */

#include <assert.h>
#include <ztas/mem.h>
#include <ztas/string.h>
#include <ztas/shrp.h>
#include <stdint.h> // needed for uint32_t

#ifndef ENCODED

#ifdef SHRP_SPINLOCK
#include <pthread.h>
#endif


#define MAGIC 0xFEEDDEAD

typedef struct {
#ifdef SHRP_SPINLOCK
    pthread_spinlock_t lock;
#endif
    uint32_t users;
    uint32_t size;
    uint32_t magic;
    char data[0];
} shrp_t;

void*
shrp_malloc(unsigned size)
{
    shrp_t* p = (shrp_t*) malloc(sizeof(shrp_t) + size);
    assert (p);
#ifdef SHRP_SPINLOCK
    pthread_spin_init(&p->lock, 0);
#endif
    p->users = 1;
    p->size = size;
    p->magic = MAGIC;
    return (void*) p->data;
}

void*
shrp_realloc(void* ptr, unsigned size)
{
    assert (ptr);
    //shrp_t* p = (shrp_t*) ((char*)ptr) - sizeof(shrp_t);
    assert (0 && "not implemented");
    return NULL;
}

void
shrp_free(void* ptr)
{
    assert (ptr);
    shrp_t* p = (shrp_t*) (((char*)ptr) - sizeof(shrp_t));
    assert (p->magic == MAGIC && "invalid shared pointer");

#ifdef SHRP_SPINLOCK
    pthread_spin_lock(&p->lock);
    --p->users;
    if (p->users == 0) {
        pthread_spin_unlock(&p->lock);
        free(p);
    } else {
        pthread_spin_unlock(&p->lock);
    }
#else
    unsigned users = __sync_fetch_and_sub(&p->users, 1);
    if (users == 1) free(p);
#endif
}

void
shrp_hold(void* ptr)
{
    assert (ptr);
    shrp_t* p = (shrp_t*) (((char*)ptr) - sizeof(shrp_t));
    assert (p->magic == MAGIC && "invalid shared pointer");
    assert (p->users > 0); // unsafe

#ifdef SHRP_SPINLOCK
    pthread_spin_lock(&p->lock);
    ++p->users;
    pthread_spin_unlock(&p->lock);
#else
    __sync_fetch_and_add(&p->users, 1);
#endif

}

void
shrp_holdcpy(void** ptr, unsigned size)
{
    assert (ptr);
    assert (*ptr);
    shrp_t* p = (shrp_t*) (((char*)*ptr) - sizeof(shrp_t));
    if (p->magic == MAGIC) {
        shrp_hold(*ptr);
    } else {
        // p is not a shared pointer
        void* ptr_new = shrp_malloc(size);
        memcpy(ptr_new, *ptr, size);
        *ptr = ptr_new;
    }
}
#else // ENCODED

void*
shrp_malloc(unsigned size)
{
    return malloc(size);
}

void*
shrp_realloc(void* ptr, unsigned size)
{
    assert (ptr);
    //shrp_t* p = (shrp_t*) ((char*)ptr) - sizeof(shrp_t);
    assert (0 && "not implemented");
    return NULL;
}

void
shrp_free(void* ptr)
{
    free(ptr);
}

//void
//shrp_hold(void* ptr)
//{
// assert (0 && "not implemented for encoded");
//}

void
shrp_holdcpy(void** ptr, unsigned size)
{
    assert (ptr);
    assert (*ptr);

    void* ptr_new = malloc(size);
    memcpy(ptr_new, *ptr, size);
    *ptr = ptr_new;
}

#endif
