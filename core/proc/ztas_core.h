/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _ZTAS_CORE_H_
#define _ZTAS_CORE_H_

#define _ZTAS_CORE

#include <stdint.h>
#include <ztas/types.h>

// methods implemented in the core
int  __ztas_sendv(int32_t dst, void** data, uint64_t* sizes, int len);
int  __ztas_ucast(int32_t dst, const void* data, size_t size, int flags);
int  __ztas_bcast(const void *data, size_t size, int flags);
int  __ztas_alarm(int32_t aid, uint32_t ms);
int  __ztas_sched(int32_t aid, const ztime_t* at);
void __ztas_clock(ztime_t* ts);
void __ztas_term (void);
const char* __ztas_cread (char* key);

#endif /* _ZTAS_CORE_H_ */
