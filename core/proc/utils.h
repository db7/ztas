/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _ZTAS_UTILS_H_
#define _ZTAS_UTILS_H_

#include <stdint.h>

void save_pid(int32_t pid, const char* dir);

#endif
