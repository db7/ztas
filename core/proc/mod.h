/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _MOD_LOADER_H_
#define _MOD_LOADER_H_
#include <ztas/proc/mod.h>

void* mod_load(ztas_uif_t* uif, const ztas_dif_t* dif, int32_t pid, const char* fname);
void mod_close(void* handle);

#endif
