/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _TIMERS_H_
#define _TIMERS_H_
#include <ev.h>
#include <ztas/ds/tree.h>
#include <ztas/ds/idset.h>
#include <ztas/proc/proc.h>

//typedef struct proc proc_t;
typedef struct {
    tree_t* timers;
    idset_t* idset;
    struct ev_loop* loop;
} timer_mgr_t;

timer_mgr_t* timers_init(struct ev_loop* loop);
void         timers_stop(timer_mgr_t* mgr);
void         timers_fini(timer_mgr_t* mgr);
void         timers_set (timer_mgr_t* tm, int32_t aid, uint32_t ms, proc_t* proc);
#endif
