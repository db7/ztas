/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h> // exit
#include <assert.h>
#define __USE_GNU // to enable RTLD_DEFAULT
#include <dlfcn.h>
#undef __USE_GNU
#include <string.h> // strdup
#include <libgen.h> // basename
#include <sys/sendfile.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h> //chmod
#include <errno.h>

#include "ztas_core.h"
#include "mod.h"

static void*
dlsym_exit_on_error(void* handle, const char* funcName);

void
copy_mod(const char* src, const char* dst) {
    printf("Copying %s to %s\n", src, dst);
    char buf[4096];
    size_t size;

    FILE* fi = fopen(src, "r");
    FILE* fo = fopen(dst, "w+");

    if (!fi) {
        perror("dst file");
        exit(EXIT_FAILURE);
    }
    if (!fo) {
        perror("dst file");
        exit(EXIT_FAILURE);
    }

    while ((size = fread(buf, 1, 4096, fi))) {
        fwrite(buf, 1, size, fo);
    }

    fclose(fi);
    fclose(fo);

    int r = chmod(dst, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP);
    assert(r == 0);
}

char*
tmp_fname(int32_t pid, const char* fname) {
    char name[1024];
    sprintf(name, "/tmp/ztas-mod%d.so", pid);
    return strdup((const char*) &name);
}

void
uif_handle(void* handle, ztas_uif_t* uif) {
    uif->init = (ztas_init_f*) dlsym_exit_on_error(handle, "__ztas_init");
    uif->fini = (ztas_fini_f*) dlsym_exit_on_error(handle, "__ztas_fini");
    uif->recv = (ztas_recv_f*) dlsym_exit_on_error(handle, "__ztas_recv");
    uif->trig = (ztas_trig_f*) dlsym_exit_on_error(handle, "__ztas_trig");
}

#ifndef ZTAS_MOD
extern void* __ztas_init(int32_t pid);
extern void __ztas_fini(void* state);
extern void __ztas_recv(void* state, const void* data, size_t size);
extern void __ztas_trig(void* state, int32_t aid);

void
default_uif(ztas_uif_t* uif) {
    uif->init = &__ztas_init;
    uif->fini = &__ztas_fini;
    uif->recv = &__ztas_recv;
    uif->trig = &__ztas_trig;

}
#endif

void
default_dif(ztas_dif_t *dif) {
    dif->term = (ztas_term_f*) &__ztas_term;
    dif->alarm = (ztas_alarm_f*) &__ztas_alarm;
    dif->sched = (ztas_sched_f*) &__ztas_sched;
    dif->clock = (ztas_clock_f*) &__ztas_clock;
    dif->ucast = (ztas_ucast_f*) &__ztas_ucast;
    dif->bcast = (ztas_bcast_f*) &__ztas_bcast;
    dif->sendv = (ztas_sendv_f*) &__ztas_sendv;
    dif->cread = (ztas_cread_f*) &__ztas_cread;
}

void*
mod_load(ztas_uif_t* uif, const ztas_dif_t* dif, int32_t pid, const char* fname) {
    assert(uif);
    /* this following code can help one to load a .so module and use it
     * instead of the default lowif */
#ifndef ZTAS_MOD
    assert(!fname && "ZTAS_MOD flag not defined. Check your ini file!");
    // set default handlers
    default_uif(uif);
    return NULL;
#endif
    // the following code only works if the code does not contain any
    // __ztas_uif functions, ie, the code is not linked with
    // iface/interface.c

    assert(fname && "ZTAS_MOD flag defined. Check your ini file!");
    printf("loading %s\n", fname);
    char* name = tmp_fname(pid, fname);
    copy_mod(fname, name);

    void* handle = dlopen(name, RTLD_NOW | RTLD_LOCAL);
    if (!handle) {
        fprintf(stderr, "%s\n", dlerror());
        exit(EXIT_FAILURE);
    }
    free(name);

    ztas_mod_init_f* mod_init = dlsym_exit_on_error(handle, "ztas_mod_init");

    if (!dif) {
        ztas_dif_t ddif;
        default_dif(&ddif);
        (*mod_init)(&ddif);
    } else
        (*mod_init)(dif);

    uif_handle(handle, uif);

    return handle;
}

void
mod_close(void* handle) {
    if (handle == NULL)
        return;
    int r = dlclose(handle);
    char* error = dlerror();
    if (error || r) {
        // is error always set ?
        fprintf(stderr, "%s\n", error);
        perror("Closing ztas_mod");
        exit(EXIT_FAILURE);
    }
}

static void*
dlsym_exit_on_error(void* handle, const char* funcName) {
    char* error;
    void* func = dlsym(handle, funcName);
    if (func == NULL) {
        fprintf(stderr, "%s not found.\n", funcName);
        exit(EXIT_FAILURE);
    }
    if ((error = dlerror()) != NULL) {
        fprintf(stderr, "error loading %s: %s\n", funcName, error);
        exit(EXIT_FAILURE);
    }

    return func;
}

#if 0
int
main() {
    ztas_uif_t p0, p1;
    void* h0 = mod_load(&p0, 0, "./modtest.so");
    void* h1 = mod_load(&p1, 1, "./modtest.so");

    assert (h0 != h1);

    p0.init(1234);
    p1.init(4321);

    p0.trig(NULL, 1111);
    p1.trig(NULL, 2222);
    p0.trig(NULL, 1112);

    mod_close(h0);
    mod_close(h1);
}
#endif
