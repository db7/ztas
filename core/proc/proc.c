/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <unistd.h> // _exit
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <ztas/shrp.h>
//x#define DEBUG
#include <debug.h>

#include "proc.h"
#include "timers.h"
#include "utils.h"

#include "ztas_core.h"
#include "ztas_lowif.h"
#include "mod.h"

#define _ZTAS_CORE
#include <ztas/flags.h>
#include <ztas/ztime.h>
#include <ztas/net/connection.h>
//#include "../ztimex.h"

#define UDS_MAX_SIZE 60000

/* proc has to set of connections: connections initiated by proc and
 * connections accepted by proc. The former are called endpoints, they
 * are used to send data. The latter are called listener connections,
 * they are used to receive data.
 */
__thread proc_t* __self;

typedef struct {
    uint32_t pad;
    uint32_t size;
} header_t;

typedef struct {
    uint32_t size;
    void* data;
} proc_message_t;

static void
async_recv_cb(struct ev_loop* loop, struct ev_async* watcher, int revents)
{
    if(EV_ERROR & revents) {
        perror("got invalid event in async recv");
        return;
    }
    proc_t* proc = (proc_t*) watcher->data;

    // first check whether we should stop the process or not
    if (proc->threaded && proc->stop_now) {
        assert (proc->stop_now == 1);
        proc->stop_now = 2;
        proc_stop(proc);
        return;
    }

    proc_message_t* m = NULL;

    m = cqueue_deq(proc->lqueue);
    if (m) {
        assert (m->data);

        DLOG("DEQ: pos = %d addr = %p\n", proc->lqueue->head-1, m);

        assert (__self == NULL);
        __self = proc;
        proc->uif.recv(proc->state, m->data, m->size);
        __self = NULL;

        shrp_free(m->data);
        shrp_free(m);

        // If more requests are available, signal myself.  Looping while
        // lcqueue is not empty wouldn't be fair to other events.
        if (!cqueue_empty(proc->lqueue))
            ev_async_send(proc->loop, &proc->lw);
    }
}

static void
stats_timer_cb(struct ev_loop* loop, struct ev_timer* w, int revents)
{
    if (EV_CLEANUP & revents) {
        printf("CLEANUP event!!!\n");
    }


    if (EV_ERROR & revents) {
        perror("got invalid event in async recv");
        return;
    }

    proc_t* proc = (proc_t*) w->data;
    if (proc->cpu_stats)
        cpu_stats_measure(proc->cpu_stats);

    if (proc->stats) {
        // collect from connections
        // iterate over all connections
        tree_it_t it;
        tree_it_init(proc->endpoints, &it);
        connection_t* c;
        while ((c = tree_it_next(&it)) != NULL) {
            char topic[128];
            sprintf(topic, "connection.%d", tree_it_key(&it));
            stats_report(proc->stats, topic, connection_stats(c));
        }
    }
}

/* Heart beat protocol */
typedef struct {
    int32_t size;
    char data[];
} hbeat_msg_t;

static void
hbeat_recv(uds_t* uds, const void* data, size_t size,
         struct sockaddr_in* addr, void* args)
{
    // if it is a hearbeat
    // check which connections we haven't seen
    // create them.
}

static void
hbeat_timer_cb(struct ev_loop* loop, struct ev_timer* w, int revents)
{
    if (EV_ERROR & revents) {
        assert (0 && "should never happen");
    }
    //proc_t* proc = (proc_t*) w->data;
    // send to all known guys the table of addresses
    // that should be taken from the connections.
    // uds_send(proc->uds, &hbeat, sizeof(uds_msg_t) + hbeat.header.size);
}

/* uds recv */
static void
uds_recv(uds_t* uds, const void* data, size_t size,
         struct sockaddr_in* addr, void* args)
{
    assert (args);
    proc_t* proc = (proc_t*) args;

    assert (__self == NULL);
    __self = proc;
    proc->uif.recv(proc->state, data, size);
    __self = NULL;
}


/* listener callbacks */
static void
proc_listener_done(connection_t* connection, void* data, void* args)
{
    /* Done is called is called if a message is sent on the
     * connection. Proc uses the listener connections only for
     * receiving, so this should never be called. */

    assert (0 && "should never be called");
}

static uint64_t
proc_listener_recv(connection_t* connection, void** data, ssize_t* read,
                   void* args)
{
    /* Recv is called once there is some byte waiting to be read. data
     * contains the buffer previously used (if any). We make a "guess"
     * regarding the size of the message and then we read
     * sizeof(header_t) bytes. The method is called again after header
     * is read. We then know the real size of the message and can
     * check if the buffer is large enough. */

    assert (data && read && args);
    proc_t* proc = (proc_t*) args;

    if (*data == NULL) { // no buffer yet
        void* header = connection_get_data(connection);
        if (header == NULL) {
            header = malloc(sizeof(header_t));
            assert (header);
            connection_set_data(connection, header);
        }

        *read = 0;
        *data = header;
        // read size of ztas header
        return sizeof(header_t);
    }

    if (*data && *read == 0) {
        // client disconnected
        // free payload (if any)
        if (*data != connection_get_data(connection))
            shrp_free(*data);

        // free header
        if (connection_get_data(connection) != NULL)
            free(connection_get_data(connection));

        return 0;
    }

    assert (*read > 0);

    if (*data == connection_get_data(connection)
        && *read < sizeof(header_t)) {
        return sizeof(header_t) - *read;
    }

    header_t* header = (header_t*) connection_get_data(connection);

    if (*data == connection_get_data(connection)) {
        *read = 0;
        DLOG("receiving message with %d bytes\n", header->size);
        *data = shrp_malloc(header->size);
        assert(*data && "Allocation failed");
        return header->size;
    }

    if (*read < header->size) {
        return header->size - *read;
    }

    assert (*read == header->size);

    // Message is complete, perform upcall.
    DLOG("read complete message (size: %d). Calling recv with ptr %p\n", header->size, *data);
    assert (__self == NULL);
    __self = proc;
    proc->uif.recv(__self->state, *data, header->size);
    __self = NULL;

    shrp_free(*data);

    // wait for next message
    *data = NULL;
    *read = 0;

    return 0;
}

static void
proc_listener_close(connection_t* connection, void* data, void* args)
{
    if (data == connection_get_data(connection)) {
        // header == data
        if (data) free(data);
    } else {
        // header != data
        if (data) shrp_free(data);
        if (connection_get_data(connection))
            free(connection_get_data(connection));
    }
}

/* endpoint callbacks */
static void
proc_endpoint_done(connection_t* connection, void* data, void* args)
{
    shrp_free(data);
}

void*
run_loop(void* args)
{
    ev_loop((struct ev_loop*) args, 0);
    pthread_exit(0); // clear pthread
}

proc_t*
proc_init(struct ev_loop* loop, int pid, const char* cfg_fname) {
    cfg_t* cfg = cfg_parse(cfg_fname);
    proc_t* proc = proc_init_cfg(loop, pid, cfg);
    proc->close_cfg = 1;
    return proc;
}

proc_t*
proc_init_cfg(struct ev_loop* loop, int pid, const cfg_t* cfg)
{
    return proc_init_cfg_uif(loop, pid, cfg, NULL);
}

proc_t*
proc_init_cfg_uif(struct ev_loop* loop, int pid, const cfg_t* cfg, ztas_uif_t* uif)
{
    proc_t* proc = (proc_t*) malloc(sizeof(proc_t));
    assert (proc);

    proc->pid       = pid;

    // save configuration
    proc->cfg       = cfg;
    proc->close_cfg = 0;


    if (loop == NULL) {
        proc->threaded = 1;
        proc->loop = ev_loop_new(0);
        assert (proc->loop);
    } else {
        // save loop
        proc->loop   = loop;
        proc->threaded = 0;
    }
    // used to stop proc if proc is threaded
    proc->stop_now = 0;

    // create server
    DLOG("creating process %d\n", pid);

    // should we save OS' pid in a file (savepid should be a directory)
    const char* savepid = cfg_get(proc->cfg, "main:savepid");
    if (savepid) save_pid(pid, savepid);

    // create uds
    int p = cfg_get_port(proc->cfg, pid);
    proc->uds = uds_init(proc->loop, p, uds_recv, (void*) proc);
    //proc->hbeat = uds_init(proc->loop, p + 20000, hbeat_recv, (void*) proc);

    // create listener
    connection_cb_t listner_cbs;
    listner_cbs.recv  = proc_listener_recv;
    listner_cbs.done  = proc_listener_done;
    listner_cbs.close = proc_listener_close;

    proc->listener = listener_init(proc->loop, cfg_get_port(proc->cfg, pid),
                                   &listner_cbs, (void*) proc);

    // create endpoints
    proc->idset     = cfg_get_idset(proc->cfg);
    proc->endpoints = tree_init();

    connection_cb_t endpoint_cbs;
    endpoint_cbs.recv  = NULL;
    endpoint_cbs.done  = proc_endpoint_done;
    endpoint_cbs.close = NULL;

    idset_it_t it;
    idset_it_init(proc->idset, &it);
    int id;
    while ((id = idset_it_next(&it)) >= 0) {
        const char* host = cfg_get_host(proc->cfg, id);
        int port = cfg_get_port(proc->cfg, id);
        printf("Process %d\n", id);
        printf("\thost = %s\n", host);
        printf("\tport = %d\n", port);
        connection_t* ep = connection_init(proc->loop, host, port,
                                           &endpoint_cbs, (void*) proc);
        tree_put(proc->endpoints, id, (void*) ep);
    }

    // initialize timer infrastructure
    proc->tmgr = timers_init(proc->loop);

    // create queue for local messages
    proc->lqueue = cqueue_create(10000);

    // local send async watcher
    ev_async_init(&proc->lw, async_recv_cb);
    proc->lw.data = (void*) proc;
    // local send has lower priority
    ev_set_priority(&proc->lw, -2);
    ev_async_start(proc->loop, &proc->lw);

    // statistics
    proc->stats = stats_init(proc->loop, pid, proc->cfg);

    const char* cpu_stats = cfg_get(proc->cfg, "main:cpu_stats");
    if (cpu_stats) {
        WLOG("Saving cpu stats in folder: %d\n", cpu_stats);
        proc->cpu_stats = cpu_stats_init(pid, cpu_stats);
    } else {
        proc->cpu_stats = NULL;
    }
    ev_timer_init(&proc->stats_timer, stats_timer_cb, 1.0, 1.0);
    ev_timer_start(proc->loop, &proc->stats_timer);
    proc->stats_timer.data = (void*) proc;

    // load library or get interface
    if (uif) {
        proc->handle = 0;
        memcpy(&proc->uif, uif, sizeof(ztas_uif_t));
    } else {
        const char* module = cfg_get_mod(proc->cfg, pid);
        proc->handle = mod_load(&proc->uif, NULL, pid, module);
    }

    // call ztas_init() and store state
    assert (__self == NULL);
    __self = proc;
    proc->state = proc->uif.init(pid);
    __self = NULL;

    if (proc->threaded) {
        pthread_create(&proc->thread, NULL, run_loop, proc->loop);
    }
    return proc;
}


/** this method shouldn't be called more than once by another thread!!
 * If proc is threaded, the calling thread signalizes the proc thread
 * that it should stop, and the proc thread then calls proc_stop
 * again!
 */
void
proc_stop(proc_t* proc)
{
    if (proc->threaded) {
        if (proc->stop_now == 0) {
            // tell proc thread it should stop
            proc->stop_now = 1;
            ev_async_send(proc->loop, &proc->lw);
            return;
        } else {
            // it must be the proc's thread which is calling proc_stop
            assert (proc->stop_now == 2 && "proc_stop called twice?");
            proc->stop_now = 3;
        }
    }

    // stop all endpoint
    idset_it_t it;
    idset_it_init(proc->idset, &it);
    int dst;
    while ((dst = idset_it_next(&it)) >= 0) {
        connection_t* ep = (connection_t*) tree_get(proc->endpoints, dst);
        assert (ep);
        connection_stop(ep);
    }

    // stop timers
    timers_stop(proc->tmgr);

    // stop listener watchers
    listener_stop(proc->listener);

    // stop uds object
    uds_stop(proc->uds);

    // stop stats collector
    if (proc->stats)
        stats_stop(proc->stats);

    // stop local async watcher
    ev_async_stop(proc->loop, &proc->lw);
    ev_timer_stop(proc->loop, &proc->stats_timer);
}

void
proc_fini(proc_t* proc)
{
    if (proc->threaded)
        pthread_join(proc->thread, NULL);

    assert (__self == NULL);
    proc->uif.fini(proc->state);
    mod_close(proc->handle);

    // free local queue
    while (!cqueue_empty(proc->lqueue)) {
        proc_message_t* m = (proc_message_t*) cqueue_deq(proc->lqueue);
        shrp_free(m->data);
        shrp_free(m);
    }
    cqueue_free(proc->lqueue);

    // destroy all endpoints
    idset_it_t it;
    idset_it_init(proc->idset, &it);
    int dst;
    while ((dst = idset_it_next(&it)) >= 0) {
        connection_t* ep = (connection_t*) tree_pop(proc->endpoints, dst);
        assert (ep);
        connection_fini(ep);
    }
    tree_fini(proc->endpoints);

    // clear timers
    timers_fini(proc->tmgr);

    // clear server
    listener_fini(proc->listener);

    // clear uds
    uds_fini(proc->uds);

    // close configuration file?
    if (proc->close_cfg)
        cfg_close((cfg_t*) proc->cfg);

    // clear other proc data structures
    free(proc->idset);

    // free stats
    if (proc->cpu_stats) cpu_stats_fini(proc->cpu_stats);
    if (proc->stats) stats_fini(proc->stats);

    // free loop if proc created its own loop
    if (proc->threaded)
        ev_loop_destroy(proc->loop);

    free(proc);
}

/*
 * this method will be used to invoke methods from the vertical
 * interface. the methods will set the self variable and then call f
 * with the right parameters. this might have to set the self inside
 * the ztax2 as well in case ztax2 is being used.
 *
 * the motivation to have this method is to embed a ztas process in a
 * normal program. the user has simply to keep a proc_t* pointer and
 * whenever it needs to comunicate with the process it can use a
 * vertical interface.
 */
/*
   #include <stdarg.h>
typedef void* (proc_f)(...);

void*
proc_invoke(proc_t* proc, proc_f* foo, ...)
{
    assert (self == NULL);
    self = proc;
    va_list ap;
    va_start (ap, format);
    void* ret = foo(ap);
    va_end (ap);
    self = NULL;
    return ret;
}
*/

int
proc_lsend(proc_t* proc, void* data, size_t size)
{
    assert (data);
    assert (size > 0);

    shrp_holdcpy(&data, size);
    proc_message_t* m = (proc_message_t*) shrp_malloc(sizeof(proc_message_t));
    m->data = data;
    m->size = size;
    int rc = cqueue_enq(proc->lqueue, (void*) m);
    DLOG("ENQ: pos = %d addr = %p\n", proc->lqueue->tail-1, m);
    if (rc == QUEUE_FULL) {
        shrp_free(m);
        shrp_free(data);
        return ZTAS_UCAST_FULL;
    }

    // wake async
    ev_async_send(proc->loop, &proc->lw);

    return ZTAS_UCAST_OK;
}

int
proc_alarm(proc_t* proc, int32_t aid, uint32_t ms)
{
    timers_set(proc->tmgr, aid, ms, proc);
    return 0;
}

int
proc_sched(proc_t* proc, int32_t aid, const ztime_t* at)
{
    uint64_t now = __ztas_now64();
    /* printf("now: %lu\n", now); */
    /* printf("at : %lu\n", ZTIME_TS(*at)); */
    uint64_t then = ZTIME_TS(*at);
    uint64_t ms64;
    if (then > now) {
        ms64 = ZTIME_TS(*at) - now;
        //printf("number is: %lu\n", ms64);
        ms64 /= 1000000; // in ms
        //printf("number is: %lu\n", ms64);
    } else {
        ms64 = 0;
    }
    int32_t ms = ms64;

    assert (ms64 == (uint64_t)(ms));

    timers_set(proc->tmgr, aid, ms, proc);
    return 0;
}

int
proc_ucast(proc_t* proc, int32_t dst, const void* data, size_t size, int flags)
{
    connection_t* ep = (connection_t*) tree_get(proc->endpoints, dst);
    if (!ep) return ZTAS_UCAST_DSTERR;

    assert (ep && "should not crash here!");

    if (flags == 0) {
        void* ptr = (void*) data;
        shrp_holdcpy(&ptr, size);
        header_t* header = (header_t*) shrp_malloc(sizeof(header_t));
        header->pad  = 0;
        header->size = size;

        void*    dptr[2] = {(void*) header, ptr};
        uint64_t sptr[2] = {sizeof(header_t), size};

        int r = connection_sendv(ep, dptr, sptr, 2);
        if (r == CONNECTION_SEND_FULL) {
            shrp_free(header);
            shrp_free(ptr);
            return ZTAS_UCAST_FULL;
        }
        assert (r == CONNECTION_SEND_OK);
        return ZTAS_UCAST_OK;
    }

    if (flags == ZTAS_CAST_UMSG) {
        if (size > UDS_MAX_SIZE) return ZTAS_UCAST_ESIZE;

        struct sockaddr_in addr;
        memcpy(&addr, connection_addr(ep), sizeof(addr));
        uds_send(proc->uds, data, size, &addr);
        return ZTAS_UCAST_OK;
    }

    return ZTAS_UCAST_EFLAG;
}

/** note that if ZTAS_SEND_OK is returned, the data pointers will be
    free by the connection.  We don't call shrp_holcpy for each data,
    so we assume the user is giving us the ownership of the data
    pointers if OK is returned! (Otherwise the user is responsible for
    freeing it)
 */
int
proc_sendv(proc_t* proc, int32_t dst, void** data, uint64_t* sizes, int len)
{
    connection_t* ep = (connection_t*) tree_get(proc->endpoints, dst);
    if (!ep) return ZTAS_UCAST_DSTERR;

    assert (ep && "should not crash here!");

    // check whether we will be able to copy data
    int r = connection_sendv_check(ep, len + 1);
    if (r == CONNECTION_SEND_FULL) return ZTAS_UCAST_FULL;
    assert (r == CONNECTION_SEND_OK);

    if (data == NULL || sizes == NULL)
        // query whether there is enough space (len) or not.
        return ZTAS_UCAST_OK;

    int32_t size = 0;
    int i;
    for (i = 0; i < len; ++i) size += sizes[i];
    header_t* header = (header_t*) shrp_malloc(sizeof(header_t));
    header->pad  = 0;
    header->size = size;
    r = connection_send(ep, header, sizeof(header));
    assert (r == CONNECTION_SEND_OK);
    // we assume all pointers in data are shrp'd and we take the ownership
    r = connection_sendv(ep, data, sizes, len);
    assert (r == CONNECTION_SEND_OK);

    return ZTAS_UCAST_OK;
}

int
proc_bcast(proc_t* proc, const void* data, size_t size, int flags)
{
    int r = ZTAS_BCAST_OK;

    if (flags == 0) {
        // controller sends msg (data) which contains header and payload
        // why size+sizeof(header_t)
        void* stuff = shrp_malloc(size+sizeof(header_t));
        memcpy((char*)stuff + sizeof(header_t), data, size);
        header_t* header = (header_t*) stuff;
        header->pad  = 0;
        header->size = size;

        idset_it_t it;
        idset_it_init(proc->idset, &it);
        int dst;
        while ((dst = idset_it_next(&it)) >= 0) {
            connection_t* ep = (connection_t*) tree_get(proc->endpoints, dst);
            assert (ep);
            shrp_hold(stuff); // +1 for each endpoint
            int rc = connection_send(ep, stuff, size + sizeof(header_t));
            if (rc != CONNECTION_SEND_OK) {
                r = ZTAS_BCAST_NOK;
                shrp_free(stuff);
            }
        }
        shrp_free(stuff); // -1 for shrp_malloc above

        return r;
    }


    if (flags == ZTAS_CAST_UMSG) {
        if (size > UDS_MAX_SIZE) return ZTAS_BCAST_ESIZE;

        idset_it_t it;
        idset_it_init(proc->idset, &it);
        int dst;
        while ((dst = idset_it_next(&it)) >= 0) {
            connection_t* ep = (connection_t*) tree_get(proc->endpoints, dst);
            assert (ep);
            struct sockaddr_in addr;
            memcpy(&addr, connection_addr(ep), sizeof(addr));
            uds_send(proc->uds, data, size, &addr);
        }
        return ZTAS_UCAST_OK;
    }

    return ZTAS_BCAST_EFLAG;
}


/************* CORE INTERFACE IMPLEMANTATION *******************************/

int
__ztas_alarm(int32_t aid, uint32_t ms)
{
    return proc_alarm(__self, aid, ms);
}

int
__ztas_sched(int32_t aid, const ztime_t* at)
{
    return proc_sched(__self, aid, at);
}

int
__ztas_ucast(int32_t dst, const void* data, size_t size, int flags)
{
    return proc_ucast(__self, dst, data, size, flags);
}

int
__ztas_sendv(int32_t dst, void** data, uint64_t* sizes, int len)
{
    return proc_sendv(__self, dst, data, sizes, len);
}


int
__ztas_bcast(const void* data, size_t size, int flags)
{
    return proc_bcast(__self, data, size, flags);
}


void
__ztas_term()
{
    ev_unloop(__self->loop, EVUNLOOP_ALL);
    //__ztas_fini(__self);
    fprintf(stderr, "ZTAS TERM CALLED!!!\n");
    fflush(stdout);
    fflush(stderr);
    _exit(0);
}

const char*
__ztas_cread(char* key)
{
    return cfg_get(__self->cfg, key);
}
