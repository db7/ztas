/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <stdio.h>
#include <assert.h>
#include <sys/types.h> // pid_t is an int
#include <unistd.h>

void
save_pid(int pid, const char* dir)
{
    char fname[128];
    if (!dir) dir = ".";
    sprintf(fname, "%s/ztas-%d.pid", dir, pid);
    printf("save pid = %s\n", fname);
    FILE* fp = fopen(fname, "w+");
    assert (fp);
    fprintf(fp, "%d\n", getpid());
    fclose(fp);
}
