/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <stdio.h>
#include <assert.h>

#include <iniparser.h>
#include <dictionary.h>

#include <ztas/proc/cfg.h>


cfg_t*
cfg_parse(const char* fname)
{
    if (fname) {
        cfg_t* cfg = iniparser_load(fname);
        assert (cfg);
        return cfg;
    } else {
        cfg_t* dict = dictionary_new(0);
        return dict;
    }
}

void
cfg_close(cfg_t* cfg)
{
    iniparser_freedict(cfg);
}

int
cfg_get_port(const cfg_t* ini, int pid)
{
    char key[128];
    sprintf(key, "Pid%d:port", pid);
    const char* cport = iniparser_getstring((cfg_t*)ini, key, NULL);

    if (!cport) {
        sprintf(key, "p%d:port", pid);
        cport = iniparser_getstring((cfg_t*)ini, key, NULL);
    }

    int port = cport ? atoi(cport) : 3000 + pid;
    return port;
}

const char*
cfg_get_host(const cfg_t* ini, int pid)
{
    char key[128];
    sprintf(key, "Pid%d:host", pid);
    const char* host = iniparser_getstring((cfg_t*)ini, key, NULL);

    if (!host) {
        sprintf(key, "p%d:host", pid);
        host = iniparser_getstring((cfg_t*)ini, key, "localhost");
    }
    return host;
}

const char*
cfg_get_mod(const cfg_t* ini, int pid)
{
    char key[128];
    sprintf(key, "Pid%d:mod", pid);
    const char* mod = iniparser_getstring((cfg_t*)ini, key, NULL);

    if (!mod) {
        sprintf(key, "p%d:mod", pid);
        mod = iniparser_getstring((cfg_t*)ini, key, NULL);
    }
    return mod;
}

idset_t*
cfg_get_idset(const cfg_t* ini)
{
    // changed to 1002 for controller
    idset_t* idset = idset_init(0, 1002);
    char pids[]  = "ProcessList:pid";

    const char* pid_value = iniparser_getstring((cfg_t*)ini, pids, NULL);
    if (!pid_value) {
        char pids[]  = "main:processes";
        pid_value = iniparser_getstring((cfg_t*)ini, pids, NULL);
    }
    assert (pid_value);

    char* b;
    char* e;
    e = b = (char*) pid_value;

    for (e = b = (char*) pid_value;; b = ++e) {
        while (*e != ',' && *e != '\n' && *e != '\0' && *e != ';') ++e;
        while (*b == ' ') ++b;
        if (b == e) break;

        int pid;
        char cpid[64];
        strncpy(cpid, b, e-b);
        cpid[e-b] = '\0';
        pid = atoi(cpid);
        idset_add(idset, pid);

        if (*e == '\0' || *e == '\n' || *e == ';') break;
    }

    return idset;
}


int
cfg_set(cfg_t* cfg, const char* entry, const char* val)
{
    return iniparser_set(cfg, (char*) entry, (char*) val);
}

const char*
cfg_get(const cfg_t* cfg, const char* key)
{
    return iniparser_getstring((cfg_t*) cfg, (char*) key, NULL);
}
