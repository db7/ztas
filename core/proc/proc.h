/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _PROC_H_
#define _PROC_H_

#include <ev.h>
#include <pthread.h>

#include <ztas/ds/tree.h>
#include <ztas/ds/queue.h>
#include <ztas/ds/cqueue.h>
#include <ztas/net/listener.h>
#include <ztas/net/connection.h>
#include <ztas/ds/idset.h>
#include <ztas/proc/cfg.h>
#include <ztas/proc/proc.h>
#include <ztas/stats/cpu.h>
#include <ztas/net/uds.h>
#include <ztas/stats/stats.h>

#include "mod.h"
#include "timers.h"

struct proc {
    int32_t pid;
    void* state;

    // threading and event loop
    pthread_t thread;
    int threaded;
    int stop_now;
    struct ev_loop* loop;

    listener_t* listener;
    tree_t* endpoints;
    timer_mgr_t* tmgr;
    idset_t* idset;

    cqueue_t* lqueue;
    struct ev_async lw;

    // statistics
    struct ev_timer stats_timer;
    cpu_stats_t* cpu_stats;
    stats_t* stats;

    // configuration
    int close_cfg;
    const cfg_t* cfg;

    // module
    void* handle;
    ztas_uif_t uif;

    // datagram communication
    uds_t* uds;

};

proc_t* proc_init_cfg(struct ev_loop* loop, int pid, const cfg_t* cfg);
proc_t* proc_init(struct ev_loop* loop, int pid, const char* fname);
void    proc_fini(proc_t*);
void    proc_stop(proc_t*);

#endif
