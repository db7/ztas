/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

/* a module should define either one macro before including this file
 */
#ifndef _ZTAS_LOWIF_
#define _ZTAS_LOWIF_

#include <ztas/proc/cfg.h>

// methods implemented in the low interface
void*  __ztas_init(int32_t pid); //, cfg_t* cfg 
void   __ztas_fini(void* state);
void   __ztas_recv(void* state, const void* data, size_t size);
void   __ztas_trig(void* state, int32_t aid);

#endif
