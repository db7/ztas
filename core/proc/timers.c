/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 * a timer manager
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdint.h>
#include "timers.h"
#include "proc.h"
#include "ztas_lowif.h"

typedef struct {
    ev_timer timeout_w;
    int32_t aid;
    timer_mgr_t* tm;
    proc_t* proc;
} alarm_t;


extern __thread proc_t* __self;

static void
timeout_cb (struct ev_loop* loop, struct ev_timer *w, int revents)
{
    alarm_t* t = (alarm_t*) w->data;
    if (EV_ERROR & revents) {
        printf("terminating timer %d\n", t->aid);
        return;
    }
    ev_timer_stop(loop, w);

    assert (__self == NULL);
    __self = t->proc;
    __self->uif.trig(__self->state, t->aid);
    __self = NULL;
}

void
timers_set(timer_mgr_t* tm, int32_t aid, uint32_t ms, proc_t* proc)
{
    /* if (aid != 5673) assert (ms != 0); */
    /* printf("AID = %d MS = %d\n", aid, ms); */
  alarm_t* t = (alarm_t*) tree_get(tm->timers, aid);
  if (t == NULL) {
    t = (alarm_t*) malloc(sizeof(alarm_t));
    assert (t);
    t->aid = aid;
    t->timeout_w.data = (void*) t;
    t->tm = tm;
    t->proc = proc;

    void* r = tree_put(tm->timers, aid, t);
    assert (r == NULL);

    idset_add(tm->idset, aid);
    ev_timer_init(&t->timeout_w, timeout_cb, ms/1000.0, 0.);
    //ev_set_priority(&t->timeout_w, -2);
    ev_timer_start(tm->loop, &t->timeout_w);
  } else {
    ev_timer_stop(tm->loop, &t->timeout_w);
    ev_timer_set(&t->timeout_w, ms/1000.0, 0.);
    //ev_set_priority(&t->timeout_w, -2);
    ev_timer_start(tm->loop, &t->timeout_w);
  }
}

timer_mgr_t*
timers_init(struct ev_loop* loop)
{
  timer_mgr_t* mgr = (timer_mgr_t*) malloc(sizeof(timer_mgr_t));
  mgr->loop = loop;
  mgr->timers = tree_init();
  mgr->idset  = idset_init(0, 9999);
  return mgr;
}

void
timers_fini(timer_mgr_t* mgr)
{
  idset_it_t it;
  idset_it_init(mgr->idset, &it);
  int aid;
  while ((aid = idset_it_next(&it)) >= 0) {
      tree_del(mgr->timers, aid);
  }
  free(mgr->timers);
  free(mgr->idset);
  free(mgr);
}

void
timers_stop(timer_mgr_t* mgr)
{
  idset_it_t it;
  idset_it_init(mgr->idset, &it);
  int aid;
  while ((aid = idset_it_next(&it)) >= 0) {
      alarm_t* t = tree_get(mgr->timers, aid);
      ev_timer_stop(mgr->loop, &t->timeout_w);
  }
}

