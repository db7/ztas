/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/** 
 * A funnel Listener
 * --------------
 * 
 * Receives requests from multiple clients and funnels the requests to
 * a given server acting as a single client.
 * 
 * Assumption: server works in a request-response way and works a
 * single client requests in FIFO order.
 *
 * The requests from clients have to be split and individually sent to
 * the server. Similarly the responses from the server have to be
 * split and redirected back to the correct client.
 *
 * TODO: the retry mechanism offered might be unnecessary. We can
 * factor it out.
 *
 * TODO: check memory management with valgrind
 *
 * TODO: document header file
 *
 * TODO: create better log messages and use DEBUG and DEBUG_LOG
 *
 * TODO: use pthread_lock to allow more than 2 threads
 *
 * TODO: define consistent error codes for functions read_cb, done_cb
 * and *_send
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <strings.h>
#include <string.h>

/* TCP, libev, pthread and others */
#include <ev.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <sys/fcntl.h> // fcntl
#include <unistd.h> // close
#include <netdb.h>
#include <pthread.h>

/* Custom data structures */
#include <ztas/ds/queue.h>
#include <ztas/ds/tree.h>
#include <ztas/net/listener.h>
#include <ztas/net/connection.h>

/* -----------------------------------------------------------------------------
 * Definitions
 * -------------------------------------------------------------------------- */

#define PORT 3033
#define BUFFER_SIZE 1024
#define MAX_INFLIGHT 100
#define MIN_BACKOFF 100000 / 1000000.0 // microsec
#define MAX_BACKOFF 5.
#define DEBUG_LOG(...) fprintf(stderr,...);
#define ASYNC_SEND

/* -----------------------------------------------------------------------------
 * Data Structures
 * -------------------------------------------------------------------------- */

/**
 * Listener data structure
 */
struct listener {

    int pid; // a listener id TODO: remove

    void* args;

    // event loop
    struct ev_loop* loop;

    // client socket, watchers and callbacks
    char hostname[128];
    struct sockaddr_in addr;
    int addr_len;
    int port;
    int sock;

    struct ev_io accept_w;
    struct ev_async async_w;

    connection_cb_t extern_cbs;
    connection_cb_t intern_cbs;

    int count;           // next client id
    int total_nb;        // current number of clients
    tree_t* connections; // connections

    pthread_spinlock_t lock;
};


/* -----------------------------------------------------------------------------
 * Prototypes
 * -------------------------------------------------------------------------- */

/* callbacks */
static void listener_accept_cb(struct ev_loop* loop, struct ev_io* watcher,
                               int revents);

/* -----------------------------------------------------------------------------
 * Client connections
 * -------------------------------------------------------------------------- */

/* Accept client requests */
void
listener_accept_cb(struct ev_loop* loop, struct ev_io* watcher, int revents)
{
    struct sockaddr_in client_addr;
    socklen_t client_len = sizeof(client_addr);
    int client_sd;
    listener_t* listener = (listener_t*) watcher->data;
    assert (listener);

    if(EV_ERROR & revents) {
        perror("got invalid event");
        return;
    }

    // Accept client request
    client_sd = accept(watcher->fd,
                       (struct sockaddr *)&client_addr, &client_len);
    if (client_sd < 0) {
        perror("accept error");
        return;
    }
    int flag = 1;
    if (-1 == setsockopt(client_sd, IPPROTO_TCP, TCP_NODELAY,
                         (char *)&flag, sizeof(flag))) {
        printf("Couldn't setsockopt(TCP_NODELAY)\n");
        perror("setsock");
        exit( EXIT_FAILURE );
    }

    // create a connection object
    connection_t* c = connection_init(listener->loop,
                                      NULL, 0,
                                      &listener->intern_cbs,
                                      (void*) listener);
    connection_set(c, client_sd);
    tree_put(listener->connections, (uint64_t) c, c);

    ++listener->count; // Increment client_count
    ++listener->total_nb; // Increment total_clients count
    printf("Successfully connected with client.\n");
    printf("%d client(s) connected.\n", listener->total_nb);
}



void
client_async_cb(struct ev_loop* loop, struct ev_async* watcher, int revents)
{
    if(EV_ERROR & revents) {
        perror("got invalid event in async event");
        return;
    }
    // do nothing
}


/* -----------------------------------------------------------------------------
 * Default callbacks
 * -------------------------------------------------------------------------- */

static void default_done (connection_t* connection, void* data, void* args)
{ free(data); }

static uint64_t
listener_recv_cb(connection_t* connection, void** data, ssize_t* read,
                 void* args)
{
    assert (args);
    listener_t* listener = (listener_t*) args;
    assert (listener->extern_cbs.recv);
    return listener->extern_cbs.recv(connection, data, read, listener->args);
}

static void
listener_done_cb(connection_t* connection, void* data, void* args)
{
    assert (args);
    listener_t* listener = (listener_t*) args;
    if (listener->extern_cbs.done)
        listener->extern_cbs.done(connection, data, listener->args);
    else free(data);
}

static void
listener_close_cb(connection_t* connection, void* data, void* args)
{
    assert (args);
    listener_t* listener = (listener_t*) args;
    if (listener->extern_cbs.close)
        listener->extern_cbs.close(connection, data, listener->args);

    // remove connection from tree and free it
    // TODO
    /* connection_stop(connection); */
    /* tree_pop(listener->connections, (uint64_t) connection); */
    /* connection_fini(connection); */
}


/* -----------------------------------------------------------------------------
 * Listener main functions
 * -------------------------------------------------------------------------- */


listener_t*
listener_init(struct ev_loop* loop, int port,
              const connection_cb_t* cbs,
              void* args)
{
    assert (loop);
    assert (cbs);

    // We expect write failures to occur but we want to handle them where
    // the error occurs rather than in a SIGPIPE handler.
    signal(SIGPIPE, SIG_IGN);

    listener_t* listener = (listener_t*) malloc(sizeof(listener_t));
    assert (listener);

    // save argument pointer
    listener->args = args;

    // event loop
    listener->loop = loop;

    // client data structures
    listener->total_nb = 0;
    listener->count = 0;
    listener->connections = tree_init();
    listener->extern_cbs.recv  = cbs->recv;
    listener->extern_cbs.done  = cbs->done  ? cbs->done  : default_done;
    listener->extern_cbs.close = cbs->close;
    listener->intern_cbs.recv  = listener_recv_cb;
    listener->intern_cbs.done  = listener_done_cb;
    listener->intern_cbs.close = listener_close_cb;
    //TODO: pthread_spin_init(&listener->lock, 0);
    listener->addr_len = sizeof(listener->addr);
    bzero(&listener->addr, sizeof(listener->addr));
    listener->addr.sin_family = AF_INET;
    listener->addr.sin_port = htons(port);
    listener->addr.sin_addr.s_addr = INADDR_ANY;


    // Create listener socket
    if( (listener->sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0 ) {
        perror("socket error");
        free(listener);
        return NULL;
    }

    // this is important, so that if a process restarts, it can quickly
    // reuse the same port.
    int on = 1;
    if (-1 == setsockopt(listener->sock, SOL_SOCKET, SO_REUSEADDR, &on,
                         sizeof(on))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    // Bind socket to address
    if (bind(listener->sock, (struct sockaddr*) &listener->addr,
             sizeof(listener->addr)) == -1) {
        perror("bind error");
    }

    // Start listing on the socket
    if (listen(listener->sock, 2) < 0) {
        perror("listen error");
        free(listener);
        return NULL;
    }

    // Initialize and start a watcher to accepts client requests
    ev_io_init(&listener->accept_w, listener_accept_cb,
               listener->sock, EV_READ);
    listener->accept_w.data = (void*) listener;
    ev_io_start(listener->loop, &listener->accept_w);

    // client async watcher
    ev_async_init(&listener->async_w, client_async_cb);
    ev_async_start(listener->loop, &listener->async_w);

    return listener;
}


void
listener_stop(listener_t* listener)
{
    // stop listener watchers
    ev_async_stop(listener->loop, &listener->async_w);
    ev_io_stop(listener->loop, &listener->accept_w);

    // Iterate on tree and call connection_stop for all
    // connection. TODO: We save that in a temporary tree since we
    // have no iterator for the tree.
    tree_t* tmpconn = tree_init();
    connection_t* c = NULL;
    while ((c = tree_popdf(listener->connections)) != NULL) {
        connection_stop(c);
         tree_put(tmpconn, (uint64_t) c, c);
    }
    tree_fini(listener->connections);
    listener->connections = tmpconn;
}

void
listener_fini(listener_t* listener)
{

    // free connections
    connection_t* c = NULL;
    while ((c = tree_popdf(listener->connections)) != NULL) {
        connection_fini(c);
    }
    tree_fini(listener->connections);
    free(listener);
}
