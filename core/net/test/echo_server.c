/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h> // memcmp
#include <strings.h> // bzero
#include <ev.h>
#include <ztas/net/listener.h>
#include <ztas/net/connection.h>
#include <ztas/shrp.h>

typedef struct {
    struct ev_loop* loop;
    listener_t* listener;

    void*  message;
    size_t message_size;
    int send_count;
    int recv_count;

} self_t;

#define MAX_BUFFER 10

void
server_done(connection_t* connection, void* data, void* args)
{
    free(data);
}

uint64_t
server_recv(connection_t* connection, void** data, ssize_t* read, void* args)
{
    self_t* self = (self_t*) args;
    if (*data == NULL) {
        *data = malloc(MAX_BUFFER);
        assert (*data);
    } else {
        assert (*read != 0);

        connection_send(connection, *data, *read);

        ++self->recv_count;
        printf("received %d messages\n", self->recv_count);
        if (self->send_count == self->recv_count) {
            connection_stop(connection);
            listener_stop(self->listener);
            ev_unloop(self->loop, EVUNLOOP_ALL);
        }

        *data = NULL;
    }

    *read = 0;
    return MAX_BUFFER;
}

int
main(const int argc, const char* argv[])
{
    self_t* self = (self_t*) malloc(sizeof(self_t));
    assert (self);
    self->loop = ev_loop_new(0);
    self->message = malloc(1024);
    self->message_size = 1024;

    // to make valgrind stop complaining
    bzero(self->message, self->message_size);

    self->send_count = 10;
    self->recv_count = 0;

    // create listener
    connection_cb_t cbs;
    cbs.recv  = server_recv;
    cbs.done  = server_done;
    cbs.close = NULL;
    self->listener = listener_init(self->loop, 3000, &cbs, (void*) self);

    ev_loop(self->loop, 0);

    listener_fini(self->listener);

    free(self->message);
    ev_loop_destroy(self->loop);
    free(self);

    return 0;
}
