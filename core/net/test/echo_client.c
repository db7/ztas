/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> // memcmp
#include <strings.h> // bzero
#include <ev.h>
#include <ztas/net/listener.h>
#include <ztas/net/connection.h>
#include <ztas/shrp.h>

#define MAX_BUFFER 1024

void
client_done(connection_t* connection, void* data, void* args)
{}

uint64_t
client_recv(connection_t* connection, void** data, ssize_t* read, void* args)
{
    if (*data == NULL) {
        *data = shrp_malloc(MAX_BUFFER);
        assert (*data);
    } else {
        assert (*read != 0);
        printf("Replied %s \n", (char*) *data);
    }

    *read = 0;
    return MAX_BUFFER;
}


int
main(const int argc, const char* argv[])
{
    struct ev_loop* loop = ev_loop_new(0);

    connection_cb_t cbs;
    cbs.recv  = client_recv;
    cbs.done  = client_done;
    cbs.close = NULL;
    connection_t* c = connection_init(loop, "localhost", 3000, &cbs, NULL);

    int i;
    for (i = 0; i < 10; ++i) {
        char* msg = "hello world";
        connection_send(c, msg, strlen(msg));
    }
    ev_loop(loop, 0);
    return 0;
}
