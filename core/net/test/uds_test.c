/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <stdio.h>
#include <string.h>
#include <ev.h>
#include <assert.h>
#include <ztas/net/uds.h>


typedef struct uds_msg {
    int32_t pid;
    char text[128];
} uds_msg_t;

struct sockaddr_in addr;

void
my_recv(uds_t* uds, const void* data, size_t size,
        struct sockaddr_in* sockaddr, void* args)
{
    //assert (size == sizeof(uds_msg_t));
    //uds_msg_t* msg = (uds_msg_t*) data;
    //printf("uds client said: %s", msg->text);
    printf("uds client said: %s\n", (char*) data);

    uds_send(uds, data, size, sockaddr);
}

void
timer_cb(struct ev_loop* loop, struct ev_timer* w, int revents)
{
    uds_t* uds = (uds_t*) w->data;

    printf("sending message\n");
    const char* msg = "hello world";
    uds_send(uds, msg, strlen(msg) + 1, &addr);
}

int
main(int argc, char* argv[])
{
    struct ev_loop* loop = ev_loop_new(0);

    // initialize uds
    uds_t* uds = uds_init(loop, 3333, my_recv, NULL);

    // initialize global addr
    int r = uds_addr_set(&addr, "localhost", 3333);
    assert (r == 1);

    // initilize timer to create message
    struct ev_timer t;
    t.data = (void*) uds;
    ev_timer_init(&t, timer_cb, .2, 0.);
    // you can use:
    //   echo "hi" | nc -uq1 localhost 3333
    // to test this echo server, or enable following line
    //ev_timer_start(loop, &t);

    ev_loop(loop, 0);

    uds_fini(uds);
    return 0;
}
