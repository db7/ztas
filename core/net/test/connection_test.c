/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h> // memcmp
#include <strings.h> // bzero
#include <ev.h>
#include <ztas/net/listener.h>
#include <ztas/net/connection.h>

typedef struct {
    struct ev_loop* loop;
    listener_t* listener;

    void*  message;
    size_t message_size;
    int send_count;
    int recv_count;

} self_t;

void
listener_done(connection_t* connection, void* data, void* args)
{
    assert (0 && "should never be called"); 
}

uint64_t
listener_recv(connection_t* connection, void** data, ssize_t* read, void* args)
{
    self_t* self = (self_t*) args;
    if (*data == NULL) {
        *data = malloc(self->message_size);
        assert (*data);
        *read = 0;
        return self->message_size;
    } else {
        assert (memcmp(*data, self->message, self->message_size) == 0);
        free (*data);
        ++self->recv_count;
        printf("received %d messages\n", self->recv_count);
        if (self->send_count == self->recv_count) {
            connection_stop(connection);
            listener_stop(self->listener);
            ev_unloop(self->loop, EVUNLOOP_ALL);
        }
        *data = 0;
        *read = 0;
        return 0;
    }
}

void
sender_done(connection_t* connection, void* data, void* args)
{}

uint64_t
sender_recv(connection_t* connection, void** data, ssize_t* read, void* args)
{
    assert (0 && "should never be called"); 
    return 0;
}


void
test_many()
{
    self_t* self = (self_t*) malloc(sizeof(self_t));
    assert (self);
    self->loop = ev_loop_new(0);
    self->message = malloc(1024);
    self->message_size = 1024;

    // to make valgrind stop complaining
    bzero(self->message, self->message_size); 

    self->send_count = 10;
    self->recv_count = 0;

    // create listener
    connection_cb_t cbs;
    cbs.recv  = listener_recv;
    cbs.done  = listener_done;
    cbs.close = NULL;
    self->listener = listener_init(self->loop, 3000, &cbs, (void*) self); 

    cbs.recv  = sender_recv;
    cbs.done  = sender_done;
    cbs.close = NULL;
    connection_t* c = connection_init(self->loop, "localhost", 3000, &cbs, (void*) self);

    int i;
    for (i = 0; i < self->send_count; ++i)
        connection_send(c, self->message, self->message_size);

    ev_loop(self->loop, 0);

    connection_fini(c);
    listener_fini(self->listener);

    free(self->message);
    ev_loop_destroy(self->loop);
    free(self);
}

int
main(const int argc, const char* argv[])
{

    test_many();
    return 0;
}
