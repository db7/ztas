/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/* unreliable datagram service (using UDP)
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <sys/socket.h>
#include <arpa/inet.h> // inet_ntoa
#include <netinet/in.h>
#include <netdb.h> // gethostbyname
//#include <resolv.h>
#include <ev.h>
#include <ztas/net/uds.h>

#define BUF_SIZE 4096

struct uds {
    int sock;
    struct sockaddr_in addr;
    socklen_t addr_len;
    char buffer[BUF_SIZE];
    uds_recv_cb* recv;
    void* args;

    struct ev_loop* loop;
    struct ev_io recv_w;
};

static void
recv_cb(struct ev_loop* loop, struct ev_io* w, int revents)
{
    uds_t* uds = (uds_t*) w->data;
    assert (uds);

    struct sockaddr_in addr;
    socklen_t addr_len = sizeof(addr);

    int bytes = recvfrom(uds->sock, &uds->buffer, BUF_SIZE - 1, 0,
                         (struct sockaddr*) &addr, &addr_len);

    assert (addr_len == sizeof(addr));

    //uds->buffer[bytes] = '\0';
    uds->recv(uds, &uds->buffer, bytes, &addr, uds->args);
    // if (datagram complete) uds->recv(buffer->cb, bytes, args);
    //sendto(sd, buffer, bytes, 0, (struct sockaddr*) &addr, sizeof(addr));
}

uds_t*
uds_init(struct ev_loop* loop, int port, uds_recv_cb* cb, void* args)
{
    uds_t* uds = (uds_t*) malloc(sizeof(uds_t));
    assert (uds);
    uds->recv = cb;
    uds->args = args;
    uds->loop = loop;

    struct sockaddr_in addr;

    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;

    uds->sock = socket(PF_INET, SOCK_DGRAM, 0);
    if (bind(uds->sock, (struct sockaddr*) &addr, sizeof(addr)) != 0) {
        perror("bind");
        exit (EXIT_FAILURE);
    }

    // make socket non-blocking? I don't think we need to do that:
    // http://lists.schmorp.de/pipermail/libev/2010q1/001011.html

    uds->recv_w.data = (void*) uds;
    ev_io_init(&uds->recv_w, recv_cb, uds->sock, EV_READ);
    ev_io_start(uds->loop, &uds->recv_w);

    return uds;
}


void
uds_stop(uds_t* uds)
{
    ev_io_stop(uds->loop, &uds->recv_w);
}

void
uds_fini(uds_t* uds)
{
    assert (uds);
    close(uds->sock);
    free(uds);
}

void
uds_send(uds_t* uds, const void* data, size_t size,
         const struct sockaddr_in* addr)
{
    assert (uds->sock != -1);
    int bytes = sendto(uds->sock, data, size, 0,
                       (struct sockaddr*) addr, sizeof(struct sockaddr_in));
    if (bytes < 0) {
        perror("UDS error on send:");
    }
}

void
uds_sendv(uds_t* uds, const void** data, size_t* size, int len,
          const struct sockaddr_in* addr)
{
    assert (0 && "not implemented");
    // use sendmsg
}

int
uds_addr_set(struct sockaddr_in* addr, const char* host, int port)
{
    bzero(addr, sizeof(struct sockaddr_in));
    addr->sin_family = AF_INET;
    addr->sin_port   = htons(port);
    struct hostent* he;

    // resolve localhost to an IP (should be 127.0.0.1)
    if ((he = gethostbyname(host)) == NULL) {
        perror("resolving hostname");
        return 0;
    }

    // copy the network address part of the structure to the
    // sockaddr_in
    memcpy(&addr->sin_addr, he->h_addr_list[0], he->h_length);

    printf("Set address to %s:%d\n", inet_ntoa(addr->sin_addr),
           ntohs(addr->sin_port));

    return 1;
}
