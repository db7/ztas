/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <strings.h>
#include <string.h>

/* TCP, libev, pthread and others */
#include <ev.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <sys/fcntl.h> // fcntl
#include <unistd.h> // close
#include <arpa/inet.h> // inet_ntoa
#include <netdb.h>
#include <pthread.h>
#include <errno.h>

/* Custom data structures */
#include <ztas/ds/queue.h>
#include <ztas/ds/tree.h>
#include "connection_impl.h"
//#define DEBUG
#include <debug.h>

/* -----------------------------------------------------------------------------
 * Definitions
 * -------------------------------------------------------------------------- */

#define BUFFER_SIZE 1024
#define MAX_INFLIGHT 10000
#define MIN_BACKOFF 100 / 1000000.0 // microsec
#define MAX_BACKOFF 5.
#define DEBUG_LOG(...) fprintf(stderr,...);
#define ASYNC_SEND

/* -----------------------------------------------------------------------------
 * Data Structures
 * -------------------------------------------------------------------------- */

/**
 * a message buffer used internally.
 */
typedef struct {
    void*    data;
    uint64_t size;
} message_t;


/* -----------------------------------------------------------------------------
 * Prototypes
 * -------------------------------------------------------------------------- */

/* callbacks */
static void connection_connect_cb(struct ev_loop *loop, struct ev_io *w,
                                  int revents);
static void connection_send_cb(struct ev_loop *loop, struct ev_io *w,
                               int revents);
static void connection_read_cb(struct ev_loop *loop, struct ev_io *w,
                               int revents);
static void connection_wait_cb(struct ev_loop *loop, struct ev_timer *w,
                               int revents);
static void connection_async_cb(struct ev_loop* loop, struct ev_async* watcher,
                                int revents);


static void connection_socket(connection_t* connection);
static int  connection_connect(connection_t* connection);
static void connection_start(connection_t* connection);
static void connection_close(connection_t* connection);
//static void connection_teardown(connection_t* connection);

/* -----------------------------------------------------------------------------
 * Helper functions
 * -------------------------------------------------------------------------- */

// Simply adds O_NONBLOCK to the file descriptor of choice
int
setnonblock(int fd)
{
    int flags;

    flags = fcntl(fd, F_GETFL);
    flags |= O_NONBLOCK;
    return fcntl(fd, F_SETFL, flags);
}

/* -----------------------------------------------------------------------------
 * Connecting and disconnecting
 * -------------------------------------------------------------------------- */

void
connection_socket(connection_t* connection)
{
    assert (connection->sock == -1);

    if ((connection->sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        perror("socket error");
        return;
    }

    int flag = 1;
    if (-1 == setsockopt(connection->sock, IPPROTO_TCP, TCP_NODELAY,
                         (void*) &flag, sizeof(flag))) {
        perror("Couldn't setsockopt(TCP_NODELAY)\n");
        exit( EXIT_FAILURE );
    }

    // Set it non-blocking
    if (-1 == setnonblock(connection->sock)) {
        perror("configuring connection socket nonblock failed");
        exit(EXIT_FAILURE);
    }
}

void
connection_set(connection_t* connection, int sock)
{
    assert (connection->sock == -1);

    connection->sock = sock;

    int flag = 1;
    if (-1 == setsockopt(connection->sock, IPPROTO_TCP, TCP_NODELAY,
                         (char *)&flag, sizeof(flag))) {
        perror("Couldn't setsockopt(TCP_NODELAY)\n");
        exit( EXIT_FAILURE );
    }

    // Set it non-blocking
    if (-1 == setnonblock(connection->sock)) {
        perror("configuring connection socket nonblock failed");
        exit(EXIT_FAILURE);
    }

    // prepare watchers
    connection->connect_w.data = (void*) connection;
    connection->read_w.data = (void*) connection;
    connection->send_w.data = (void*) connection;
    connection->wait_w.data = (void*) connection;

    // initialize watchers
    ev_io_init (&connection->read_w, connection_read_cb, connection->sock,
                EV_READ);
    ev_set_priority(&connection->read_w, 1);
    ev_io_init (&connection->send_w, connection_send_cb, connection->sock,
                EV_WRITE);
    ev_set_priority(&connection->send_w, 1);
    ev_timer_init(&connection->wait_w, connection_wait_cb, 0.1, 0.);

    // stop other watchers
    ev_io_stop (connection->loop, &connection->connect_w);

    // sockt is already open
    connection->state = OPEN;
    connection_start(connection);
}

int
connection_config(connection_t* connection, const char* hostname, int port)
{
    if (hostname) {
        connection->initiator = 1;

        // if tcp_sock already there. close it.
        connection_close(connection);

        // reset addr
        bzero(&connection->addr, sizeof(struct sockaddr_in));
        connection->addr.sin_family = AF_INET;
        connection->addr.sin_port   = htons(port);

        struct hostent* he;

        /* resolve localhost to an IP (should be 127.0.0.1) */
        if ((he = gethostbyname(hostname)) == NULL) {
            perror("resolving hostname");
            exit(EXIT_FAILURE);
        }

        /* copy the network address part of the structure to the
         * sockaddr_in structure which is passed to connect()
         */
        memcpy(&connection->addr.sin_addr, he->h_addr_list[0], he->h_length);
        connection->addr_len = sizeof(connection->addr);

        connection->state = WAITING;
        ev_timer_start(connection->loop, &connection->wait_w);
    }
    return 0;
}

int
connection_connect(connection_t* connection)
{
    connection->state = CONNECTING;
    ev_io_set (&connection->connect_w, connection->sock, EV_WRITE);
    ev_io_start(connection->loop, &connection->connect_w);
    return 0;
}

void
connection_start(connection_t* connection)
{
    DLOG("STARTING connection with %s:%d\n",
         inet_ntoa(connection->addr.sin_addr),
         ntohs(connection->addr.sin_port));

    ev_io_set (&connection->read_w, connection->sock, EV_READ);
    ev_io_set (&connection->send_w, connection->sock, EV_WRITE);
    ev_io_start(connection->loop, &connection->read_w);

    if (!queue_empty(connection->requests)) {
        ev_io_start(connection->loop, &connection->send_w);
    }
}

/*
void
connection_teardown(connection_t* connection)
{
    assert(0);
    DLOG("Teardown connection with %s:%d\n",
         inet_ntoa(connection->addr.sin_addr),
         ntohs(connection->addr.sin_port));

    ev_io_stop(connection->loop, &connection->connect_w);
    ev_io_stop(connection->loop, &connection->read_w);
    ev_io_stop(connection->loop, &connection->send_w);
}
*/

/* -----------------------------------------------------------------------------
 * Connection callbacks
 * -------------------------------------------------------------------------- */

void
connection_connect_cb(struct ev_loop* loop, struct ev_io* w, int revents)
{
    if (EV_CLEANUP & revents) {
        DLOG("CLEANUP event!!!\n");
    }

    if (EV_ERROR & revents) {
        assert (0 && "should never happen");
        // from manual: You best act on it by reporting the problem
        // and somehow coping with the watcher being stopped. Note
        // that well-written programs should not receive an error
        // ever, so when your watcher receives it, this usually
        // indicates a bug in your program.
        return;
    }
    connection_t* connection = (connection_t*) w->data;

    int error = 0;
    int r = 0;

    if (connection->state == INPROGRESS) {
        // the file descriptor became writable/readble again!
        error = 0;
        socklen_t len;
        len = sizeof(error);

        // get socket pending error
        int sr = getsockopt(connection->sock, SOL_SOCKET, SO_ERROR, &error,
                            &len);
        if (sr < 0) {
            // error with getsockopt!
            perror("getsockopt");
            exit(EXIT_FAILURE);
        }
        if (error) errno = error;
    } else {
        r = connect(connection->sock, (struct sockaddr *) &connection->addr,
                    connection->addr_len);
        DLOG("Connect cbto %s:%d\n",
             inet_ntoa(connection->addr.sin_addr),
             ntohs(connection->addr.sin_port)
            );
    }

    // note that errno is thread-safe!
    if (error || (r < 0 && errno != EINPROGRESS)) {
        // there was a connection problem
        //perror("Connect");
        ev_io_stop(loop, w);
        ++connection->retry_count;

        WLOG("Error connecting to %s:%d backing off %f (errno:%d)\n",
             inet_ntoa(connection->addr.sin_addr),
             ntohs(connection->addr.sin_port),
             connection->backoff, errno
            );

        //perror("Connect error");
        connection_close(connection);

        //if (!queue_empty(connection->requests)) {
        connection->state = WAITING;
        ev_timer_set(&connection->wait_w, connection->backoff, 0.);
        ev_timer_start(connection->loop, &connection->wait_w);
        //} else {
        // else there is nothing to do anyway
        //}
        return;
    }

    if (r < 0) {
        assert (errno == EINPROGRESS && "we checked above");
        // just return
        connection->state = INPROGRESS;
        return;
    }

    ev_io_stop(loop, w);
    connection->state = OPEN;
    connection_start(connection);
}

void
connection_wait_cb(struct ev_loop* loop, struct ev_timer* w, int revents)
{
    if (EV_ERROR & revents) {
        assert (0 && "should never happen");
    }

    // try again
    connection_t* connection = (connection_t*) w->data;
    assert (connection->state == WAITING);
    ev_timer_stop(loop, w);

    connection_socket(connection);
    connection_connect(connection);

    connection->backoff *= 2;
    if (connection->backoff > MAX_BACKOFF) connection->backoff = MAX_BACKOFF;
}

/* -----------------------------------------------------------------------------
 * Send and receive callbacks
 * -------------------------------------------------------------------------- */

void
connection_send_cb(struct ev_loop* loop, struct ev_io* w, int revents)
{
    if (EV_ERROR & revents) {
        assert (0 && "should never happen");
    }

    connection_t* connection = (connection_t*) w->data;

  more_to_send:
    if (connection->send_data == NULL) {
        if (queue_empty(connection->requests)) {
            DLOG("nothing to write to %s:%d: state = %d, initiator = %d\n",
                 inet_ntoa(connection->addr.sin_addr),
                 ntohs(connection->addr.sin_port),
                 connection->state, connection->initiator);
            ev_io_stop(loop, w);
            return;
        }

        message_t* m = queue_deq(connection->requests);
        assert (m != NULL);
        assert (m->data != NULL);
        connection->send_data = m->data;
        connection->send_size = m->size;
        assert (connection->send_size > 0 && "messages shouldnt have 0 size");
        connection->send_offset = 0;
        int r = queue_enq(connection->free_req, (void*) m);
        assert (r == QUEUE_OK && "more messages than MAX_INFLIGHT");
    }

    char* ptr = ((char*) connection->send_data) + connection->send_offset;
    assert (connection->send_size > connection->send_offset);
    ssize_t sent = write(connection->sock, (const void*) ptr,
                         connection->send_size - connection->send_offset);

    DLOG("Sent %ld bytes to %s:%d\n", sent,
                 inet_ntoa(connection->addr.sin_addr),
                 ntohs(connection->addr.sin_port)
        );
    /* printf("sent : %u %u\n", ((uint32_t*) m->to_write[1].iov_base)[0], */
    /*        ((uint32_t*) m->to_write[1].iov_base)[0]); */

    if (sent < 0) {
        WLOG("Error while seding message to %s:%d\n",
               inet_ntoa(connection->addr.sin_addr),
               ntohs(connection->addr.sin_port));
        if (errno == EWOULDBLOCK) return;

        //if (errno == EFAULT) assert (0);

        perror("Send error");

        connection_close(connection);
        // retry
        if (connection->initiator) {
            connection->state = WAITING;
            ev_timer_set(&connection->wait_w, connection->backoff, 0.);
            ev_timer_start(connection->loop, &connection->wait_w);
        }
        return;
    }

    connection->send_offset += sent;

    // statistics
    connection->stats.sent += sent;

    // should continue?
    if (connection->send_offset < connection->send_size) {
        WLOG("Too much to send (%d != %d) to %s:%d\n",
             connection->send_offset, connection->send_size,
             inet_ntoa(connection->addr.sin_addr),
             ntohs(connection->addr.sin_port));
        // try again
    } else {
        assert (connection->send_offset == connection->send_size);
        // reduce backoff
        connection->backoff /= 2;
        if (connection->backoff < MIN_BACKOFF)
            connection->backoff = MIN_BACKOFF;

        // statistics
        ++connection->stats.send_count;

        // clean up data
        connection->cbs.done(connection, connection->send_data,
                             connection->args);
        connection->send_data   = NULL;
        connection->send_offset = 0;
        connection->send_size   = 0;
        if (!queue_empty(connection->requests))
            goto more_to_send;
    }
}

void
connection_read_cb(struct ev_loop* loop, struct ev_io* w, int revents)
{
    if (EV_ERROR & revents) {
        assert (0 && "should never happen");
    }

    connection_t* connection = (connection_t*) w->data;
    assert (connection);

    if (!connection->cbs.recv) {
        // no reading function, simply monitor connection and reopen
        // connection in case it gets closed ?
        char buf[1024];
        ssize_t read = recv(w->fd, &buf, 1024, 0);

        if (read <= 0)  {
            WLOG("stopping read to %s:%d: state = %d, initiator = %d\n",
                 inet_ntoa(connection->addr.sin_addr),
                 ntohs(connection->addr.sin_port),
                 connection->state, connection->initiator);

            // Stop and free watchet if client socket is closing
            connection_close(connection);
            // retry
            if (connection->initiator) {
                connection->state = WAITING;
                ev_timer_set(&connection->wait_w, connection->backoff, 0.);
                ev_timer_start(connection->loop, &connection->wait_w);
            }
        }
        return;
    }

    // create an initial buffer if no message
    if (connection->buffer == NULL ) {
        assert (connection->offset == 0);
        connection->toread = connection->cbs.recv(connection,
                                                  &connection->buffer,
                                                  &connection->offset,
                                                  connection->args);
        assert (connection->buffer);
        assert (connection->offset >= 0);
        assert (connection->toread > 0);
    }

    ssize_t read = recv(w->fd, ((char*)connection->buffer) + connection->offset,
                        connection->toread, 0);

    if (read <= 0) {
        DLOG("connection to %s:%d terminated: initiator = %d\n",
             inet_ntoa(connection->addr.sin_addr),
             ntohs(connection->addr.sin_port),
             connection->initiator);

        // Stop and free watchet if client socket is closing

        // TODO: if not initiator pass special parameter to close so
        // that listner can delete connection
        connection_close(connection);

        if (connection->initiator) {
            connection->state = WAITING;
            ev_timer_set(&connection->wait_w, connection->backoff, 0.);
            ev_timer_start(connection->loop, &connection->wait_w);
        }
    } else  {
        // statistics
        connection->stats.recvd += read;


        DLOG("read %d from %s:%d\n",
             connection->offset,
             inet_ntoa(connection->addr.sin_addr),
             ntohs(connection->addr.sin_port));

        connection->offset += read;
        connection->toread -= read; // TODO: ???

        assert (connection->toread >= 0);

        // should read more?
        connection->toread = connection->cbs.recv(connection,
                                                  &connection->buffer,
                                                  &connection->offset,
                                                  connection->args);
        if (connection->toread == 0) {
            // cleanup for next request
            connection->toread = 0;
            connection->offset = 0;
            connection->buffer = NULL;
        }
    }
}

void
connection_async_cb(struct ev_loop* loop, struct ev_async* watcher, int revents)
{
    if (EV_ERROR & revents) {
        assert (0 && "should never happen");
    }

    connection_t* connection = (connection_t*) watcher->data;

    if (connection->stop) {
        ev_async_stop(connection->loop, &connection->async_w);
        if (connection->state != CLOSED)
            connection_close(connection);
        return;
    }

    if (!queue_empty(connection->requests)) {
        if (connection->state == OPEN) // start send watcher
            ev_io_start(connection->loop, &connection->send_w);
        else if (connection->state == CLOSED) {
            connection->state = WAITING;
            ev_timer_set(&connection->wait_w, 0., 0.);
            ev_timer_start(connection->loop, &connection->wait_w);
        }
    }
}


/* -----------------------------------------------------------------------------
 * Default callbacks
 * -------------------------------------------------------------------------- */

static void
default_done(connection_t* connection, void* data, void* args)
{ free(data); }


/* -----------------------------------------------------------------------------
 * Connection main functions
 * -------------------------------------------------------------------------- */


connection_t*
connection_init(struct ev_loop* loop,
                const char* connection_host,
                int connection_port,
                const connection_cb_t* cbs,
                void* args)
{
    assert (loop);

    // We expect write failures to occur but we want to handle them
    // where the error occurs rather than in a SIGPIPE handler.
    signal(SIGPIPE, SIG_IGN);

    connection_t* connection = (connection_t*) malloc(sizeof(connection_t));
    assert (connection);

    // save argument pointer
    connection->args = args;

    // event loop
    connection->loop = loop;

    // endpoit data structures
    connection->cbs.recv  = cbs->recv;
    connection->cbs.done  = cbs->done ? cbs->done : default_done;
    connection->cbs.close = cbs->close;

    connection->state = CLOSED;
    connection->stop = 0;
    connection->backoff = MIN_BACKOFF;
    connection->sock = -1;
    connection->initiator = 0;
    connection->retry_count = 0;

    connection->requests  = queue_create(MAX_INFLIGHT);
    connection->free_req  = queue_create(MAX_INFLIGHT);

    connection->send_data = NULL;
    connection->send_size = 0;
    connection->send_offset = 0;

    connection->read  = 0;
    connection->toread = 0;
    connection->offset = 0;
    connection->buffer = NULL;
    connection->data   = NULL;

    // statistics
    connection->stats.recvd = 0;
    connection->stats.sent  = 0;

    // create free requests
    int i = 0;
    for (i = 0; i < MAX_INFLIGHT; ++i) {
        message_t* r = (message_t*) malloc(sizeof(message_t));
        r->data = NULL;
        r->size = 0;
        queue_enq(connection->free_req, (void*) r);
    }

    // prepare watchers
    connection->connect_w.data = (void*) connection;
    connection->read_w.data = (void*) connection;
    connection->send_w.data = (void*) connection;
    connection->wait_w.data = (void*) connection;
    connection->async_w.data = (void*) connection;

    // initialize watchers
    ev_io_init (&connection->connect_w, connection_connect_cb, connection->sock,
                EV_WRITE);
    ev_set_priority(&connection->connect_w, 1);
    ev_io_init (&connection->read_w, connection_read_cb, connection->sock,
                EV_READ);
    ev_set_priority(&connection->read_w, 1);
    ev_io_init (&connection->send_w, connection_send_cb, connection->sock,
                EV_WRITE);
    ev_set_priority(&connection->send_w, 1);
    ev_timer_init(&connection->wait_w, connection_wait_cb, 0.1, 0.);

    // connection async watcher
    ev_async_init(&connection->async_w, connection_async_cb);
    ev_async_start(connection->loop, &connection->async_w);

    connection_config(connection, connection_host, connection_port);

    return connection;
}

int
connection_send(connection_t* connection, void* data, uint64_t size)
{
#ifdef ASYNC_SEND
    DLOG("Sending message %lu bytes to %s:%d\n", size,
         inet_ntoa(connection->addr.sin_addr),
         ntohs(connection->addr.sin_port));

    message_t* m = queue_deq(connection->free_req);
    if (!m) return CONNECTION_SEND_FULL;

    assert (data);
    m->data = data;
    m->size = size;

    int r = queue_enq(connection->requests, m);
    assert (r == QUEUE_OK && "should never happen");

    // wake up thread
    ev_async_send(connection->loop, &connection->async_w);

    return CONNECTION_SEND_OK;
#else
    ssize_t val = send(connection->sock, data, size, 0);
    connection->done_fn(data, connection->args);
    return val;
#endif
}

int
connection_sendv_check(connection_t* connection, int len)
{
    if (queue_fsize(connection->requests) < len)
        return CONNECTION_SEND_FULL;
    else return CONNECTION_SEND_OK;
}


int
connection_sendv(connection_t* connection, void** dataptr, uint64_t* sizeptr,
                 int len)
{
    assert (dataptr && sizeptr && len > 0);
    if (queue_fsize(connection->requests) < len)
        return CONNECTION_SEND_FULL;

    int i;
    for (i = 0; i < len; ++i) {
        void*    data = dataptr[i];
        uint64_t size = sizeptr[i];
        DLOG("Sending message %lu bytes to %s:%d\n", size,
             inet_ntoa(connection->addr.sin_addr),
             ntohs(connection->addr.sin_port));

        message_t* m = queue_deq(connection->free_req);
        if (0 && !m /* lossy channel */) {
            // drop oldest message
            m = queue_deq(connection->requests);
            assert (m);
            connection->cbs.done(connection, m->data, connection->args);
        }
        assert (m && "Cannot send message. Something straaange.");
        assert (data);
        m->data = data;
        m->size = size;

        int r = queue_enq(connection->requests, m);
        assert (r == QUEUE_OK && "should never happen");
    }
    // wake up thread
    ev_async_send(connection->loop, &connection->async_w);

    return CONNECTION_SEND_OK;
}

void
connection_close(connection_t* connection)
{
    //connection_teardown(connection);
    ev_io_stop(connection->loop, &connection->connect_w);
    ev_io_stop(connection->loop, &connection->read_w);
    ev_io_stop(connection->loop, &connection->send_w);

    // stop async watchers
    ev_timer_stop(connection->loop, &connection->wait_w);

    // tell user to close connection and delete buffer or any other related data
    if (connection->cbs.close) connection->cbs.close(connection,
                                                     connection->buffer,
                                                     connection->args);

    if (connection->send_data) connection->cbs.done(connection,
                                                    connection->send_data,
                                                    connection->args);

    connection->send_data = NULL;
    connection->send_offset = 0;
    connection->send_size = 0;

    // free all elements in queue
    while (!queue_empty(connection->requests)) {
        message_t* m = queue_deq(connection->requests);
        connection->cbs.done(connection, m->data, connection->args);
        int r = queue_enq(connection->free_req, (void*) m);
        assert (r == QUEUE_OK && "more messages than MAX_INFLIGHT");
    }

    if (connection->sock != -1)
        close(connection->sock);
    connection->state = CLOSED;
    connection->sock = -1;
}

void
connection_stop(connection_t* connection)
{
    connection->stop = 1;
    ev_async_send(connection->loop, &connection->async_w);
}


void
connection_fini(connection_t* connection)
{
    while (!queue_empty(connection->requests)) {
        message_t* m = queue_deq(connection->requests);
        connection->cbs.done(connection, m->data, connection->args);
        free(m);
    }
    queue_free(connection->requests);
    while (!queue_empty(connection->free_req)) {
        message_t* m = queue_deq(connection->free_req);
        free(m);
    }
    queue_free(connection->free_req);

    free(connection);
}

void*
connection_get_data(connection_t* connection)
{
    return connection->data;
}


void
connection_set_data(connection_t* connection, void* data)
{
    connection->data = data;
}


const struct sockaddr_in*
connection_addr(connection_t* connection)
{
    return &connection->addr;
}

const char*
connection_stats(connection_t* connection)
{
    sprintf(connection->stats.msg,
            "%lu %lu %lu %lu",
            connection->stats.sent,
            connection->stats.send_count,
            connection->stats.recvd,
            connection->stats.recv_count
        );
    connection->stats.sent = 0;
    connection->stats.recvd = 0;
    connection->stats.send_count = 0;
    connection->stats.recv_count = 0;

    return connection->stats.msg;
}
