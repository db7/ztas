/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _CONNECTION_IMPL_H_
#define _CONNECTION_IMPL_H_

#include <ztas/net/connection.h>

struct connection {

    //int pid; // a proxy id TODO: remove

    void* args;

    // event loop
    struct ev_loop* loop;

    // connection socket and callbacks
    int initiator;
    struct sockaddr_in addr;
    int addr_len;
    int sock;

    struct ev_io connect_w;
    struct ev_io send_w;
    struct ev_io read_w;
    struct ev_timer wait_w;
    struct ev_async async_w;

    double backoff;
    enum {OPEN, CLOSED, CONNECTING, WAITING, INPROGRESS} state;
    int stop;
    int retry_count;

    queue_t* requests;
    queue_t* free_req;

    // next message to be received
    void* buffer;
    ssize_t offset;
    uint64_t read;
    uint32_t toread;

    // next message to be sent
    void* send_data;
    size_t send_size;
    size_t send_offset;

    connection_cb_t cbs;

    pthread_spinlock_t lock;

    // special data field for receive callbacks
    void* data;

    // statistics
    struct {
        uint64_t send_count;
        uint64_t sent;

        uint64_t recv_count;
        uint64_t recvd;

        char msg[128];
    } stats;
};
#endif /* _CONNECTION_IMPL_H_ */
