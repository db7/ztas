/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/msg.h>
#include <assert.h>

#define CANARY 0xDEADBAC0
typedef struct {
    int32_t canary;
    int32_t dst;    //< destination id (-1 = bcast)
    int32_t src;    //< sender id
    uint32_t chksum;//< checksum value (mode defined in initialization)
    int32_t size;   //< size of message (sanity check)
    int32_t type;   //< upper level algorithm (port)
} ztas_msg_impl_t;

void
ztas_msg_init(void* msg, int32_t src, int32_t type)
{
    ztas_msg_impl_t* m = (ztas_msg_impl_t*) msg;
    m->canary = CANARY;
    m->dst    = -1;
    m->src    = src;
    m->type   = type;
    m->size   = -1;
    m->chksum = 0;
}

int32_t
ztas_msg_type(const void* ptr)
{
    ztas_msg_impl_t* msg = (ztas_msg_impl_t*) ptr;
    assert (msg->canary == CANARY && "Uninitialized or corrupted message");
    return msg->type;
}

int32_t
ztas_msg_src(const void* ptr)
{
    ztas_msg_impl_t* msg = (ztas_msg_impl_t*) ptr;
    assert (msg->canary == CANARY && "Uninitialized or corrupted message");
    return msg->src;
}
