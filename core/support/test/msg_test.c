/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

/* author: Haosheng He
 * Name : msg_test.c
 * Date : 21.04.2013
 * Desc : Unit testing of message.
*/

#include <ztas/msg.h>
#include <assert.h>
#include <stdint.h> 

typedef struct {
    int32_t canary;
    int32_t dst;    //< destination id (-1 = bcast)
    int32_t src;    //< sender id
    uint32_t chksum;//< checksum value (mode defined in initialization)
    int32_t size;   //< size of message (sanity check)
    int32_t type;   //< upper level algorithm (port)
} ztas_msg_impl_test_t;
#define CANARY_TEST 0xDEADBAC0

void test_init();
void test_msg_type();
void test_msg_src();

int main(int argc, char* argv[])
{
    test_init();
    test_msg_type();
    test_msg_src();

    return 0;
}

test_init()
{
    //log
    printf("message initialization test.");
    ztas_msg_t msg;
    int32_t msg_src = 9;
    int32_t msg_type = 3;
    ztas_msg_init(&msg, msg_src, msg_type);
    ztas_msg_impl_test_t* p_msg = (ztas_msg_impl_test_t*)&msg;
    assert(p_msg->canary == CANARY_TEST);
    assert(p_msg->dst    == -1);
    assert(p_msg->src    == msg_src);
    assert(p_msg->type   == msg_type);
    assert(p_msg->size   == -1);
    assert(p_msg->chksum == 0);
    //log
    printf("		Done.\n");
}

test_msg_type()
{
    //log
    printf("message type test.");
    ztas_msg_t msg;
    int32_t msg_src = 9;
    int32_t msg_type = 3;
    ztas_msg_init(&msg, msg_src, msg_type);
    assert(msg_type == ztas_msg_type(&msg));
    //log
    printf("		Done.\n");
}

test_msg_src()
{
    //log
    printf("message source test.");
    ztas_msg_t msg;
    int32_t msg_src = 9;
    int32_t msg_type = 3;
    ztas_msg_init(&msg, msg_src, msg_type);
    assert(msg_src == ztas_msg_src(&msg));
    //log
    printf("		Done.\n");
}

