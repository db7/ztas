/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/utils/hash.h>

/** hash function by Daniel J. Bernstein */
uint32_t
hash(const char *str, uint32_t size)
{
    uint32_t hash = 5381;
    int c;
    unsigned i = 0;
    while ((size > i++)) {
        c = *str++;
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    }
    return hash;
}
