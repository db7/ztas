/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 * \file
 * Definitions for check sum methods
 * \author diogo
 * \date Mon Feb  6 15:38:20 CET 2012
 * \brief chksum functions
 *
 */

#ifndef _CHKSUM_H_
#define _CHKSUM_H_

#include <stdint.h>

uint32_t crc (const void *data, uint32_t len, uint32_t hash);
uint32_t sum (const void *data, uint32_t len, uint32_t hash);
uint32_t sum8(const void *data, uint32_t len, uint32_t hash);


#endif /* _CHKSUM_H_ */

