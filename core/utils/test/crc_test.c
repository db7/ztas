#include <stdio.h>
#include <string.h>
#include <ztas/utils/crc.h>

#define CRC(S) printf("crc(%s) = %u\n", S, crc(S, strlen(S)));

int
main(int argc, char* argv[])
{
    CRC("hello world");
    return 0;
}
