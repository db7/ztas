/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/** 
 * \file crc.c
 * \author diogo
 * \date Mon Feb  6 17:11:00 CET 2012
 * \brief chksum functions
 *
 */

#include "chksum.h"

uint32_t sum(const void *key, uint32_t len, uint32_t hash)
{
    uint32_t i;
    const uint32_t* k = key;
    for (hash=len, i=0; i<len/sizeof(uint32_t); ++i)
        hash += k[i]; 
    return hash;
}


uint32_t sum8(const void *key, uint32_t len, uint32_t hash)
{
    uint32_t i;
    const uint8_t* k = key;
    for (hash=len, i=0; i<len; ++i)
        hash += k[i]; 
    return hash;
}
