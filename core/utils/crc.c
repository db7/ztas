/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

#define CRC_OPENSSH
#ifdef CRC_OPENSSH
#include <stdint.h>
#include <sys/types.h> // off_t, size_t

typedef uint32_t u_int32_t;
typedef uint8_t  u_int8_t;
#include "crc-openssh.c"

uint32_t
crc(const void* key, uint32_t len)
{
    CKSUM_CTX ctx;
    CKSUM_Init(&ctx);
    CKSUM_Update(&ctx, (uint8_t*) key, len);
    CKSUM_Final(&ctx);
    return ctx.crc;
}
#else
#include "crc-jenkins.c"
uint32_t
crc(const void* key, uint32_t len)
{
    return crc_jenkins(key, len, 0);
}
#endif
