/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/log.h>
#include <ztas.h>
#include <ztas/ztimex.h>
#include <assert.h>
#include <ztas/ztdur.h>
#include <ztas/ztime.h>

#define SET_MSB(value, type) { (value) = ~( (type) - 1 >> 1); }

void testZtime2US32() {
    ztime_t t;
    uint64_t i;
    for (i = 1; i < 8000; i += 100) {
        uint32_t r;
        ZTIME_ZERO(t);
        ZTIME_ADD_MS(t, i, t);
        LOG2("%lu = %lu\n", i, ZTIME_TS(t));
        ZTIME_2US_32(t, r);
        assert (i*1000 == r);
    }
}

void testZTimeEq(){

    ztime_t t1;
    ztime_t t2;

    ZTIME_ZERO(t1);
    ZTIME_ZERO(t2);

    assert(ZTIME_EQ(t1, t1));
    assert(ZTIME_EQ(t1, t2));
    assert(ZTIME_EQ(t2, t1));
    assert(ZTIME_EQ(t2, t2));

    t1.low = 100;
    t2.high = 100;

    assert(!ZTIME_EQ(t1, t2));
    t1.high = 100;
    assert(!ZTIME_EQ(t1,t2));
    t2.low = 100;
    assert(ZTIME_EQ(t1,t2));
}
/**
 * test division of time by 2
 */
void testZtimeDiv2(){
    ztime_t t1;
    uint64_t compValue = 0;
    ZTIME_ZERO(t1);

    // set msbs on both
    SET_MSB(t1.high, uint32_t);
    SET_MSB(compValue, uint64_t);
    assert(ZTIME_TS(t1) == compValue);

    while(compValue > 1){
        // divide both by 2
        ZTIME_DIV2(t1);
        compValue /= 2;

        // compare again!
        assert(ZTIME_TS(t1) == compValue);
    }

}

/**
 * test calulating the duration
 */
void testZtDurDur(){

    ztime_t t1;
    ztime_t t2;
    ztdur_t dur;

    ZTIME_ZERO(t1);
    ZTIME_ZERO(t2);
    ZTDUR_ZERO(dur);

    ZTDUR_DUR(t1,t2,dur);
    assert(ZTIME_TS(dur) == 0x0);

    t1.low = 100;
    t2.low = 200;
    ZTDUR_DUR(t1,t2,dur);
    assert(dur.low == 100);

    ZTDUR_DUR(t2,t1,dur);
    assert(dur.low == -100);
}

void testZtDurDurReal(){
    ztime_t t1;
    ztime_t t2;
    ztdur_t zero;
    ztime_t zero2;
    ZTIME_ZERO(zero);
    ZTIME_ZERO(zero2);

    ztime_t time_dur;
    ztdur_t neg_dur, pos_dur;
    ztdur_t tmp;
    ztime_t result;


    ztas_clock(&t1);
    ZTIME_ADD_US(t1, 1, t2);

    // t2 is one ns greater than t2
    assert(!ZTIME_EQ(t1,t2));

    // t2 still greater than t1
    assert(ZTIME_GT(t2, t1));
    ZTIME_DUR(t1,t2,time_dur);

    // t2 - t1 should be 1 (greater 0)
    assert(ZTIME_GT(time_dur, zero2));

    // t1 + (t2-t1) should be t2 again
    ZTIME_PLUS(t1, time_dur, tmp);
    ZTIME_SET(result, tmp);
    assert(ZTIME_EQ(t2, result));

    // t1 - t2 should be negative
    ZTDUR_DUR(t2,t1,neg_dur);
    assert(ZTDUR_GT(zero, neg_dur));

    // 0 - (t1 - t2) should be (t2-t1) and positive
    ZTDUR_DUR(neg_dur, zero, pos_dur);
    ZTIME_SET(result, pos_dur);
    assert(ZTIME_EQ(result, time_dur));

    // add the two negative durations
    // one negative - the two negatives
    // should be positive again
    ztdur_t tmp2, tmp3;
    ZTIME_PLUS(neg_dur, neg_dur, tmp2);
    ZTDUR_DUR(tmp2, neg_dur, tmp3);
    assert(ZTIME_EQ(tmp3, pos_dur));
}

void testZtDurDurFromNegatives()
{
    ztdur_t neg;
    ztdur_t zero;
    ztdur_t result;
    ztdur_t expected;
    ZTDUR_SET64(neg, -123456789);
    ZTDUR_SET64(expected, 123456789);
    ZTDUR_ZERO(zero);

    ZTDUR_DUR(neg, zero, result);
    assert(ZTIME_EQ(expected, result));

    neg.low=-200;
    neg.high=-1;
    expected.low=200;
    expected.high=0;

    ZTDUR_DUR(neg, zero, result);
    assert(ZTIME_EQ(expected, result));
}

void testZtdurGtz(){
    ztdur_t t1;

    ZTDUR_SET64(t1, (int64_t) 3647456148u);

    assert(ZTDUR_GTZ(t1));
}



int main(void){

    testZTimeEq();
    testZtimeDiv2();
    testZtDurDur();
    testZtDurDurReal();
    testZtDurDurFromNegatives();
    testZtdurGtz();
    testZtime2US32();

    return 0;
}
