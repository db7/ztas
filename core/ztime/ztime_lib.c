/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas.h>
#include <ztas/ztime.h>
#include <ztas/ztdur.h>

extern void __ztas_clock(ztime_t* ts);

void
ztas_clock(ztime_t* ts)
{
    __ztas_clock(ts);
}
