/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _ZTIMEX_H_
#define _ZTIMEX_H_

#ifdef __cplusplus
extern "C" {
#endif 

#include <stdint.h>

uint64_t __ztas_now64();

#define ZTIME_TS(a) (((uint64_t) (a).high) << 32 | (a).low)
#define ZTDUR_TS(a) (((int64_t) (a).high) << 32 | (a).low)

#define ZTIME_SET32(a, b) {                               \
        (a).high = (uint32_t)((b) >> 32);                 \
        (a).low  = (uint32_t)((b) & 0x00000000FFFFFFFFL); \
    }
    
#define ZTDUR_SET64(a, b) {                               \
        (a).high = (int32_t)((b) >> 32);                 \
        (a).low  = (int32_t)((b) & 0x00000000FFFFFFFFL); \
    }


#ifdef __cplusplus
}
#endif

#endif /* _ZTIMEX_H_ */
