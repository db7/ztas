/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#define _ZTAS_CORE
#include <ztas.h>
#include <ztas/ztime.h>
#include "ztimex.h"
#include <assert.h>
#include <sys/time.h>


uint64_t 
__ztas_now64()
{
   
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return ((uint64_t) tv.tv_sec)*1000000000 + (uint64_t)(tv.tv_usec)*1000;
}


void
__ztas_clock(ztime_t* ts) 
{
    uint64_t t = __ztas_now64();
    assert (ts != NULL);

    ts->high = t >> 32;
    ts->low  = (t & 0x00000000FFFFFFFFL);
}
