/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <sys/personality.h>
#include <malloc.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <ctype.h>
#include <libgen.h> // basename
#include <sys/resource.h> // rlimit

#define MEMLIMIT (1024*1024*1024) // 1 GB

#include <ev.h>
//#include <ztas/proc/cfg.h>
#include <ztas/proc/proc.h>
#include <ztas/ds/vector.h>
/* struct proc; */
/* extern  struct proc* proc_init(struct ev_loop* loop, int pid, const char* fname); */
/* extern  void         proc_fini(struct proc*); */

static void usage(char* argv[])
{
char* prog = basename(argv[0]);
    printf("\nUsage of ztas: \n");
    printf("%s <id> [id] -f <cfg_filename>\n\n", prog);
    printf("id specifies the local's process ID (or a list of process ids).\n");
    printf("The config file must define the topology!\n\n");
}

#ifdef ENCODING
extern    int SEP_init();
#endif

typedef struct {
    struct ev_timer halt_timer;
    struct ev_loop* loop;
    vector_t* pids;
    cfg_t* cfg;
    proc_t** procs;
} runner_t;

static void
halt_timer_cb(struct ev_loop* loop, struct ev_timer* watcher, int revents)
{
    runner_t* runner = (runner_t*) watcher->data;
    assert (runner);
    printf("stopping processes\n");
    int i;
    for (i = 0; i < vector_size(runner->pids); ++i) {
        proc_stop(runner->procs[i]);
    }
}

int
main(int argc, char* argv[])
{
#ifdef ENCODING
    if (-1 == personality(PER_LINUX_32BIT)) {
        perror("Personality");
        exit(-2);
    }

    int malloptRes = mallopt (M_MMAP_MAX, 0);
    assert (1 == malloptRes);

    printf("ZTAS: SEP init\n");
    SEP_init();
#endif

    char *fname = NULL;
    int index;
    int c;

    opterr = 0;

    while ((c = getopt (argc, argv, "f:")) != -1)
        switch (c) {
        case 'f':
            fname = optarg;
            break;
        case '?':
            usage(argv);
            /*
            if (optopt == 'c')
                fprintf (stderr, "Option -%c requires an argument.\n", optopt);
            else if (isprint (optopt))
                fprintf (stderr, "Unknown option `-%c'.\n", optopt);
            else
                fprintf (stderr,
                         "Unknown option character `\\x%x'.\n",
                         optopt);
            */
            return 1;
        default:
            abort ();
        }

    // we can start at most 10 processes
    runner_t runner;
    runner.pids = vector_init(10);
    for (index = optind; index < argc; index++) {
        printf ("proc %s\n", argv[index]);
        vector_push(runner.pids, atoi(argv[index]));
    }

    if (vector_size(runner.pids) == 0) {
        usage(argv);
        return 1;
    }


    /* set resource limits */
    struct rlimit rlim;
    rlim.rlim_cur = MEMLIMIT;
    rlim.rlim_max = MEMLIMIT;
    if (0 != setrlimit (RLIMIT_AS, &rlim)) {
        perror("setrlimit failed");
        exit(EXIT_FAILURE);
    }


    // start processes
    runner.loop = ev_default_loop(0);
    runner.cfg = cfg_parse(fname);

    // should the process terminate after some time?
    const char* halt_time = cfg_get(runner.cfg, "main:halt_after");
    if (halt_time) {
        double halt_time_s = atof(halt_time); // in seconds
        ev_timer_init(&runner.halt_timer, halt_timer_cb, halt_time_s, 0.);
        runner.halt_timer.data = (void*) &runner;
        ev_timer_start(runner.loop, &runner.halt_timer);
    } else {
        ev_timer_init(&runner.halt_timer, halt_timer_cb, 0., 0.);
        // will never be called since we do not call start
    }

    // initilize processes
    const int32_t* array = vector_array(runner.pids);
    runner.procs = (proc_t**) malloc(sizeof(proc_t*) * vector_size(runner.pids));
    int i;
    for (i = 0; i < vector_size(runner.pids); ++i) {
        runner.procs[i] = proc_init(runner.loop, array[i], fname);
    }
    ev_loop(runner.loop, 0);

    for (i = 0; i < vector_size(runner.pids); ++i) {
        proc_fini(runner.procs[i]);
    }
    vector_fini(runner.pids);
    free(runner.procs);
    cfg_close(runner.cfg);

    ev_loop_destroy(runner.loop);
    return 0;
}
