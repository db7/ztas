/* ----------------------------------------------------------------------
 * Copyright (c) 2012,2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

#include <ztas.h>
#include <ztas/chain.h>
#include <ztas/log.h>
#include <ztas/mem.h>
#include <assert.h>
#include <ztas/algorithms/fd.h>

#define CHECK_ALARM 1
#define CHECK_PERIOD 1000

typedef struct {
    fd_t* fd;
    int32_t pid;
} self_t;

void*
ztas_init(int32_t pid)
{
    int32_t delta = 1000;

    const char* str = ztas_cread("fd:delta");
    if (str) delta = atoi(str);

    self_t* self = (self_t*) malloc(sizeof(self_t));
    assert (self);
    self->fd  = fd_init(pid, delta);
    self->pid = pid;

    ztas_alarm(CHECK_ALARM, CHECK_PERIOD);

    return self;
}

int
check_other(self_t* self, int32_t aid)
{
    if (aid != CHECK_ALARM) return ZTAS_CHAIN_NEXT;

    LOG2("p%d alive = %s\n", 1 - self->pid,
         fd_isalive(self->fd, 1 - self->pid) == FD_UP ? "up" : "down");
    ztas_alarm(CHECK_ALARM, CHECK_PERIOD);

    return ZTAS_CHAIN_DONE;
}

void
ztas_trig(void* state, int32_t aid)
{
    self_t* self = (self_t*) state;
    CHAIN(
        || CHAIN_HANDLER(fd_trig(self->fd, aid))
        || CHAIN_HANDLER(check_other(self, aid))
        );
}

void
ztas_recv(void* state, const void* data, size_t size)
{
    self_t* self = (self_t*) state;
    const ztas_msg_t* msg = (const ztas_msg_t*) data;
    assert (size >= sizeof(ztas_msg_t));

    CHAIN(
        || CHAIN_HANDLER(fd_recv(self->fd, msg))
        );
}

void
ztas_fini(void* state)
{
    self_t* self = (self_t*) state;
    fd_fini(self->fd);
    free(self);
}
