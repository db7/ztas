/* ----------------------------------------------------------------------
 * Copyright (c) 2012,2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

/**
 * \file fd.c
 * \brief a failure detector
 */

#include <ztas/algorithms/fd.h>
#include <ztas.h>
#include <ztas/chain.h>
#include <ztas/ds/tree.h>
#include <ztas/mem.h>
#include <ztas/log.h>

#define PUSH_PERIOD 500 //5-second
#define DELTA 1000 //100 ms
#define DEBUG 0

struct fd {
    int32_t pid;
    ztime_t delta;
    ztime_t t0;
    tree_t* processes;
    int save_rtt;
};

typedef struct {
    int32_t pid;
    ztime_t rtt;
    ztime_t last_ts;
} fd_proc_t;

typedef struct {
    ZTAS_MSG
    ztime_t t0;
} message_t;


fd_t*
fd_init(int32_t pid, int32_t delta_ms)
{
    // allocate process state
    fd_t* fd = (fd_t*) malloc(sizeof(fd_t));
    assert (fd && "error allocating process state");

    // set fields in process state
    fd->pid = pid;

    // read current time and save it in my state
    ZTIME_ZERO(fd->delta);
    ZTIME_ADD_MS(fd->delta, delta_ms, fd->delta);
    ztas_clock(&fd->t0);

    // create tree for processes
    fd->processes = tree_init();
    assert (fd->processes);

    // print some message
    LOG1("FD(%d): init\n", pid);

    ztas_alarm(FD_PUSH_ALARM, PUSH_PERIOD);

    // return state to framework
    return fd;
}



void
fd_fini(fd_t* fd)
{
    // free state allocated in fd_init()
    free(fd);
}


int
fd_recv(fd_t* fd, const ztas_msg_t* header)
{
    // cast received message to our message_t type
    message_t* msg = (message_t*) header;

    // read current time
    ztime_t now;
    ztas_clock(&now);

    switch (ztas_msg_type(header)) {
    case FD_PUSH_MSG: {
        // was this processes alive?
        fd_proc_t* fd_proc = tree_get(fd->processes, ztas_msg_src(msg));
        if (!fd_proc) {
            // if not, create an entry for processe
            fd_proc = malloc(sizeof(fd_proc_t));
            assert (fd_proc);
            void* r = tree_put(fd->processes, ztas_msg_src(msg), fd_proc);
            assert (!r);

            fd_proc->pid = ztas_msg_src(msg);
        }

        ZTIME_SET(fd_proc->last_ts, now);
        if (DEBUG) {
            LOG1("FD(%d): ",                fd->pid);
            LOG1("(%d)'s DELTA SAVED.\n",   ztas_msg_src(msg));
            LOG1("FD(%d): ",                fd->pid);
            LOG1("Received PUSH from %d\n", ztas_msg_src(msg));
            printf("===================\n");
            LOG1("FD(%d): ",                fd->pid);
            LOG1("DELTA to %d ",            ztas_msg_src(msg));
            /* LOG1("\tdelta.hgh = %u",        fd->last_ts.high); */
            /* LOG1("\tdelta.low = %u\n",      fd->last_ts.low); */
            printf("===================\n");
        }

#ifdef FD_WITH_RTT
        if (FD_WITH_RTT) {
            // create a new message FD_REPLY
            message_t reply;
            ztas_msg_init(&reply, fd->pid, FD_REPLY);
            // copy the time stamp to FROM PUSH TO PUSH_REPLY
            //    this can be done with: ZTIME_SET(dest, src);
            ZTIME_SET(reply.t0, msg->t0);
            // send ucast to PING's sender
            //LOG1("Sending PUSH_REPLY to %d\n", msg->pid);
            ztas_ucast(msg->pid, (void*) &reply, sizeof(message_t));
        }
#endif
        break;
    }

    case FD_REPLY_MSG: {
#ifdef FD_WITH_RTT
        ztime_t t1;//, dur
        ztas_clock(&t1);              // read current time
        ZTIME_DUR(msg->t0, t1, fd->rtt_array[msg->pid]);  // calculate duration from t0 to t1 and put it in rtt array
        //ZTIME_SET();
        if(DEBUG) {
            LOG1("Received PUSH_REPLY from %d\n", msg->pid);
            LOG1("FD(%d): ", fd->pid);
            LOG1("(%d)'s RTT SAVED.\n",msg->pid);
            // print round trip time
            printf("===================\n");
            LOG1("FD(%d): ",     fd->pid);
            LOG1("RTT to %d ",        msg->pid);
            LOG1("\trtt.hgh = %u",   fd->rtt_array[msg->pid].high);
            LOG1("\trtt.low = %u\n", fd->rtt_array[msg->pid].low);
            printf("===================\n");
        }
#endif
        /////////////////////////////////////////////////////////////////////////////////////////
        break;
    }
    default:
        return ZTAS_CHAIN_NEXT;
    }
    return ZTAS_CHAIN_DONE;
}


int
fd_trig(fd_t* fd, int32_t aid)
{
    if (aid != FD_PUSH_ALARM) return ZTAS_CHAIN_NEXT;

    // create a PUSH message to broadcast
    message_t msg;
    ztas_msg_init(&msg, fd->pid, FD_PUSH_MSG);
    ztas_clock(&msg.t0); // read clock
    ztas_bcast((void*) &msg, sizeof(message_t), 0);

    // schedule next PUSH
    ztas_alarm(FD_PUSH_ALARM, PUSH_PERIOD);

    return ZTAS_CHAIN_DONE;
}

//asks for the rtt of a specific id
void
fd_rtt(fd_t* fd, int32_t pid, ztime_t* rtt)
{
    // cast state to fd_t type
    //fd_t* fd = (fd_t*) state;
    //ZTIME_SET(*rtt, fd->rtt_array[pid]);
}

//asks for the delta of a specific id
int
fd_isalive(fd_t* fd, int32_t pid)
{
    ztime_t now, dur;
    ztas_clock(&now);

    fd_proc_t* p = tree_get(fd->processes, pid);
    if (!p) return FD_DOWN;

    ZTIME_DUR(p->last_ts, now, dur);
    if(DEBUG) {
        LOG1("\tdelta.high = %u",   p->last_ts.high);
        LOG1("\tdelta.low = %u\n", p->last_ts.low);
        LOG1("\tdur.high = %u",   dur.high);
        LOG1("\tdur.low = %u\n", dur.low);
    }

    if (ZTIME_GE(dur, fd->delta)) {
        fd_proc_t* p = tree_pop(fd->processes, pid);
        free(p);
        return FD_DOWN;
    } else return FD_UP;
}
