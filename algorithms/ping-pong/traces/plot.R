## ---------------------------------------------------------------------
## Copyright (c) 2013 Technische Universitaet Dresden
## Distributed under the MIT license. See accompanying file LICENSE.
## ---------------------------------------------------------------------

library(ggplot2)
library(plyr)

read.table('1/output/injection', header=T) -> df
df$n <- 1
aggregate(df$n, df[, c('failure', 'type', 'fault')], sum) -> df.1

read.table('2/output/injection', header=T) -> df
df$n <- 1
aggregate(df$n, df[, c('failure', 'type', 'fault')], sum) -> df.2
#sum(df.2[df.2$type == 'native', 'x'])

read.table('3/output/injection', header=T) -> df
df$n <- 1
aggregate(df$n, df[, c('failure', 'type', 'fault')], sum) -> df.3

read.table('4/output/injection', header=T) -> df
df$n <- 1
aggregate(df$n, df[, c('failure', 'type', 'fault')], sum) -> df.4

read.table('5/output/injection', header=T) -> df
df$n <- 1
aggregate(df$n, df[, c('failure', 'type', 'fault')], sum) -> df.5


rbind(df.1, df.2, df.3, df.4, df.5) -> dfx

dfx$type <- factor(dfx$type, levels = c("native", "an",  "anb"))

## attach(dfx)
## x <- dfx[type == 'anb' & fault == 'faultyOperations',]
## detach(dfx)

ddply(dfx, c('type', 'fault'), function(x) {
  total <- sum(x$x)
  print (paste(x$fault[1], x$type[1], total))

  unknown <- sum(x$x[x$failure == 'WrongPath' |
                     x$failure == 'OpenEnd' ])

  benign <- sum(x$x[x$failure == 'Crashed' |
                    x$failure == 'WrongDestination' |
                    x$failure == 'WrongSize' |
                    x$failure == 'InvalidMessage'
                    ])
  arbi   <- x$x[x$failure == 'WrongMessage']

  if (length(arbi) == 0) {
    arbi <- 0
  }
  
  arbi   <- cbind(x[1,c('type','fault')], list(failure='arbitrary', x=arbi))
  benign <- cbind(x[1,c('type','fault')], list(failure='benign', x=benign))
  unknown <- cbind(x[1,c('type','fault')], list(failure='unknown', x=unknown))

  
  if ((arbi$x + benign$x + unknown$x) != total) {
    print (paste("Error:", x[1,c('type','fault')])) }

  arbi$x   <- arbi$x * 1.0 / total
  benign$x <- benign$x * 1.0 / total
  unknown$x <- unknown$x * 1.0 / total
  
  return(rbind(arbi,benign,unknown))
  }) -> dfy


dfy$sig <- round(dfy$x*100,digits=2)
##dfy

#dfx[dfx$fault=='exchangeOperators',]
#dfy <- na.omit(dfy)

#levels(dfy$type) <- c("nat", "an", "anb")
#levels(dfy$fault) <- c("Mo", "XOPT", "XOPA", "FOP", "LSTR")
##dfy$fault <- factor(dfy$fault, levels = c("FOP", "MOPA", "XOPT", "XOPA", "LSTR"))

levels(dfy$fault) <- c("modified operand", "exchanged operator",
"exchanged operand", "faulty operation", "lost store")
dfy$fault <- factor(dfy$fault, levels = c("faulty operation",
                                 "modified operand",
                                 "exchanged operator",
                                 "exchanged operand",
                                 "lost store"))

dfy$type <- factor(dfy$type, levels = c("anb", "an", "native"))

dfy$pos <- 0
dfy$pos[dfy$type == 'an'] <- 2
dfy$pos[dfy$type == 'anb'] <- dfy$x[dfy$type == 'anb']*120
dfy$pos[dfy$type == 'native'] <- dfy$x[dfy$type == 'native']*150

p <- ggplot(dfy[dfy$failure=='arbitrary',], aes(fault,x*100,fill=type)) +
  geom_bar(stat="identity", position="dodge") +
  geom_text(aes(x=fault,y=x*70,label=sig),
            position=position_dodge(width=1),
            size = 2) +
  coord_flip() +
  scale_fill_grey() +
  scale_y_continuous(limit=c(0,30)) +
  scale_y_log10() +
  ylab("arbitrary failures (%)") +
  xlab("") +
  theme_bw(base_size=9) +
  ## scale_x_discrete(hjust=-1) +
  opts (legend.position = c(0.6,0.97), legend.direction="horizontal") +
  opts (legend.text=theme_text(size=6), legend.key.size = unit(2, "mm")) + 
  opts (legend.title=theme_blank(), legend.key=theme_blank()) +
  opts (axis.text.y = theme_text(size=6, hjust=1))+
  opts (axis.ticks.margin = unit(1, "mm"))+
  opts (axis.title.y = theme_blank()) +
  opts (axis.title.x = theme_text(size=8, vjust=-0.2, hjust=0.75)) 
p
ggsave("fi.pdf", width=3, height=2)


p <- ggplot(dfy[dfy$failure=='arbitrary',], aes(fault,x*100,fill=type)) +
  geom_bar(stat="identity", position="dodge") +
  geom_text(aes(x=fault,y=x*100,label=sig),
            position=position_dodge(width=1),
            size = 2, vjust=-1) +
  scale_fill_grey() +
  scale_y_continuous(limit=c(0, 20)) +
  ylab("arbitrary failures (%)") +
  xlab("") +
  theme_bw(base_size=9) +
  ## scale_x_discrete(rotate=90) +
  opts (legend.position = c(0.3,0.8), legend.direction="horizontal") +
  opts (legend.text=theme_text(size=6), legend.key.size = unit(2, "mm")) + 
  opts (legend.title=theme_blank(), legend.key=theme_blank()) +
  opts (axis.text.y = theme_text(size=6, hjust=1))+
  opts (plot.margin = unit(c(0, 0, 0, 0), "cm"))+
  ## opts (axis.ticks.margin = unit(1, "mm"))+
  opts (axis.text.x = theme_text(angle=20, hjust=1,vjust=1, size = 7))+
  ## opts (axis.title.y = theme_blank()) +
  opts (axis.title.x = theme_text(size=8, vjust=-0.2, hjust=0.75)) 
p
ggsave("fi2.pdf", width=3.5, height=2.5)




ddply(dfy, c('type', 'fault'), function(x) {
  total <- sum(x$x)
  unknown <- x[x$failure == 'unknown', 'x']
  benign  <- x[x$failure == 'benign', 'x']
  arbi    <- x[x$failure == 'arbitrary', 'x']

  arbi    <- paste(arbi*2000,"/",((arbi+benign)*2000), "(", round(arbi,digits=2), "%)", SEP="")
  arbi   <- cbind(x[1,c('type','fault')], list(failure='arbitrary', x=arbi))
  return (arbi)
  }) #-> dfz

#dfz



ddply(dfx, c('type', 'fault'), function(x) {
  total <- sum(x$x)
  print (paste(x$fault[1], x$type[1], total))

  unknown <- sum(x$x[x$failure == 'WrongPath' |
                     x$failure == 'OpenEnd' ])

  benign <- sum(x$x[x$failure == 'Crashed' |
                    x$failure == 'WrongDestination' |
                    x$failure == 'WrongSize' |
                    x$failure == 'InvalidMessage'
                    ])
  arbi   <- x$x[x$failure == 'WrongMessage']

  if (length(arbi) == 0) {
    arbi <- 0
  }
  
  ## arbi   <- cbind(x[1,c('type','fault')], list(failure='arbitrary', x=arbi))
  ## benign <- cbind(x[1,c('type','fault')], list(failure='benign', x=benign))
  ## unknown <- cbind(x[1,c('type','fault')], list(failure='unknown', x=unknown))

  
  ## if ((arbi$x + benign$x + unknown$x) != total) {
  ##   print (paste("Error:", x[1,c('type','fault')])) }

  ## arbi$x   <- arbi$x * 1.0 / total
  ## benign$x <- benign$x * 1.0 / total
  ## unknown$x <- unknown$x * 1.0 / total
  total <- arbi + benign
  arbi    <- paste(round(arbi*100.0/total,digits=2), "% (", arbi,"/",(arbi+benign), ")", sep="")
  #print(arbi)
  arbi   <- cbind(x[1,c('type','fault')], list(failure='arbitrary', x=arbi))

  return(rbind(arbi))
  })
