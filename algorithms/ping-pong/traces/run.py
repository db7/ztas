# -*- python -*-

# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
import muddi
import os
wd = os.getcwd()
print wd

cmds = [('stream4%d' %i, 'cd %s/%d && dude run' % (wd, i)) for i in range(1,7)]

muddi.sh(cmds)
