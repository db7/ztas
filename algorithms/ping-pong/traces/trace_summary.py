# -*- python -*-

# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
import glob,os,re,sys
import dude.summaries

def get_failure(fname):
    fd = open(fname)
    lines = fd.readlines()
    failure = []
    for l in lines:
        lx = ''.join(l[:-1].split(' '))
        if True \
                and not re.match('^[0-9]+--[0-9]+', lx) \
                and not re.match('^\thost', lx) \
                and not re.match('^\tport', lx) \
                and not re.match('^pid=', lx) \
                and not re.match('^Process', lx) \
                and not re.match('^creatingprocess', lx) \
                and not re.match('^PROCESS', lx) \
                and not re.match('^POOL', lx) \
                and not re.match('^Replayerrefile', lx) \
                and not re.match('^encodedValue:.*', lx):
                #and not re.match('^Error:messagesize.*', lx):
            failure.append(lx)
        if re.match('.*glibcdetected.*', lx) \
                or re.match('.*given=.*', lx): break
    return failure

class Injection(dude.summaries.SummaryBase):
    def __init__(self, name, groupby = [], quiet = False):
        dude.summaries.SummaryBase.__init__(self, name, groupby, "trigger failure")
        self.ignore = ['PROC:starting100', 
                       '[ZTAX]process100init', 
                       '[ZTAX]settingchecksummode=0', 
                       'LEADER(100):initializing', 
                       'acceptors:012', 
                       'replica:30',
                       'pid=100',                       
                       'Errorwhilereadingreplayfile:0/44',
                       'terminatingreplay'
                       ]

    def visit(self, optpt, stdout, group_out):
        f = open('status.out')
        crashed = f.readline()
        f.close()
        assert int(crashed) in [0,1]
        crashed = True if crashed == 1 else False

        fname = 'injection.log'
        if os.path.exists(fname):
            f = open(fname, 'r')
            lines =  f.readlines()
            trigger = lines[0].split('=')[1].split(';')[0]

            failure = []
            files = glob.glob('log/execution-*')
            assert len(files) >= 2
            for fn in files:
                fd = open(fn)
                lines = fd.readlines()
                if len(lines) == 0: continue
                failure += get_failure(fn)
                continue
                reason = []
                for l in lines:
                    lx = ''.join(l[:-1].split(' '))
                    if lx not in self.ignore \
                            and not re.match('^POOL', lx) \
                            and not re.match('^Replayerrefile', lx) \
                            and not re.match('^encodedValue:.*', lx) \
                            and not re.match('^Error:messagesize.*', lx):
                        reason.append(lx)
                    if re.match('.*glibcdetected.*', lx) \
                            or re.match('.*given=.*', lx): break
                if reason == []: continue
                if len(reason) > 1:
                    #print reason
                    #assert False
                    reason = reason[0]
                failure = ''.join(reason)
                if not crashed: 
                    failure = "OK"
                    continue
                if crashed and not failure:
                    failure = "Crashed"
                elif re.match('.*check_AN.*', failure):
                    failure = 'InvalidMessage'
                elif re.match('.*REPLAYER.*', failure):
                    if re.match('.*REPLAYER.*ERROR.*', failure):
                        if re.match('.*wrongdestination*', failure):
                            failure = 'WrongDestination'
                        elif re.match('.*wrong.*content.*', failure):
                            failure = 'WrongMessage'
                        elif re.match('.*wrong.*size.*', failure):
                            failure = 'WrongSize'
                        elif re.match('.*wrongsched.*', failure):
                            failure = 'WrongSched' ## benign at least
                        elif re.match('.*wrongalarm.*', failure):
                            failure = 'WrongAlarm' ## benign at least
                        else: pass
                    else:
                        if re.match('.*handlin*', failure):
                            failure = "WrongPath"
                        elif re.match('.*ztime_t*', failure):
                            failure = "WrongPath"
                        elif re.match('.*event_sched_t*', failure):
                            failure = "WrongPath"
                        elif re.match('.*ztas_term*', failure):
                            failure = "Crashed"
                        elif re.match('.*wrong.*size.*', failure):
                            failure = 'WrongPath'
                        else:
                            print failure
                            assert False
                # elif re.match('.*wrongdestination*', failure):
                #     failure = 'Failure'
                # elif re.match('.*Loadaddresswasnotaligned.*', failure):
                #     failure = 'Detected'
                # elif re.match('.*MismatchingSignatures.*', failure):
                #     detected = 'Detected'
                # elif re.match('.*checkIncomingSignatures.*', detected):
                #     detected = 'Detected'
                # elif re.match('.*tmp01.*', detected):
                #     detected = 'Detected'
                # elif re.match('.*tmp01.*', detected):
                #     detected = 'Detected'
                # elif re.match('.*invalidztasmessagetype.*', detected):
                #     detected = 'Detected'
                else:
                    if crashed:
                        failure = 'Crashed'
                    else:
                        failure = 'OK'
                print failure

            if failure == []: 
                failure = "OK" if not crashed else "Crashed"

            s = self.format(optpt, trigger, failure)
            print >>group_out, s
            f.close()


def redo_filter(optpt, outf):
    f = open('status.out')
    crashed = f.readline()
    f.close()

    files = glob.glob('log/execution-*')
    for fn in files:
        fd = open(fn)
        lines = fd.readlines()
        if len(lines) == 0: continue
        for l in lines:
            if re.match('.*REPLAYER.*ERROR.*size.*', l) or \
                    re.match('.*REPLAYER.*ERROR.*content.*', l):
                return True
    return False


def throw_filter(optpt, outf):
    files = glob.glob('log/execution-*')
    for fn in files:
        fd = open(fn)
        lines = fd.readlines()
        if len(lines) == 0: continue
        for l in lines:
            if re.match('.*throw.*', l):
                #print l
                return True
            
    return False


def replayer_filter(optpt, outf):
    if optpt['type'] != 'native': return False
    if not os.path.exists('injection.log'): return False
    f = open('injection.log')
    ls = f.readlines()
    ty  = ls[1].split(' ')[7].split(';')[0]
    idx = ls[1].split(' ')[5].split(';')[0]
    f.close()
    if idx == '4': 
        #print ls
        return True
    else:
        return False
    # files = glob.glob('log/execution-*')
    # for fn in files:
    #     fd = open(fn)
    #     lines = fd.readlines()
    #     if len(lines) == 0: continue
    #     for l in lines:

    # return False


def glibc_filter(optpt, outf):
    files = glob.glob('log/execution-*')
    for fn in files:
        fd = open(fn)
        lines = fd.readlines()
        if len(lines) == 0: continue
        for l in lines:
            if re.match('.*glibc.*', l):
                #print optpt, l
                return True
    return False


def trivial_filter(optpt, outf):
    if not os.path.exists('injection.log'): return False
    f = open('injection.log')
    ls = f.readlines()
    f.close()

    if len(ls) == 3:
        #print ls
        old = ls[1].split('old: ')[1].split(';')[0].split('0x')[1].rstrip()
        new = ls[1].split('new: ')[1].split(';')[0].split('0x')[1].rstrip()
        return old == new
    else:
        return False

if __name__ == "__main__":
    print sys.argv[1]
    print get_failure(sys.argv[1])
