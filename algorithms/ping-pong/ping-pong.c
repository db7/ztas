/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 * @addtogroup algorithms
 * @section alg_pingpong Ping Pong algorithm
 * Simplest ping pong algorithm that sends messages `ping` and `pong` between two nodes.
 * \see algorithms/ping-pong/ping-pong.c
 */

#include <ztas/log.h>
#include <ztas/mem.h>
#include <ztas.h>
#include <assert.h>

typedef struct {
    int32_t pid;
    ztime_t t0;
} self_t;

typedef struct {
    int32_t pid;
    enum {PING = 1, PONG = 2} type;
    ztime_t t0;
} message_t;

#define PING_ALARM  1
#define PING_PERIOD 1000

void*
ztas_init(int32_t pid)
{
    // allocate process state
    self_t* self = (self_t*) malloc(sizeof(self_t));
    assert (self && "error allocating process state");

    // set fields in process state
    self->pid = pid;

    // print some message
    LOG1("PROCESS(%d): init\n", pid);

    // schedule ping alarm id PING_ALARM, and delay PING_PERIOD
    ztas_alarm(PING_ALARM, PING_PERIOD);

    // return state to framework
    return (void*) self;
}



void
ztas_fini(void* state)
{
    // free state allocated in ztas_init()
    free(state);
}


void
ztas_recv(void* state, const void* data, size_t size)
{
    // initial assertions
    assert (state != NULL && "received empty state");
    assert (data != NULL && "received empty message");

    // cast state to self_t type
    self_t* self = (self_t*) state;

    // there is only one size of message being received, so assert that
    assert (size == sizeof(message_t) && "invalid message received");

    // cast received message to our message_t type
    message_t* msg = (message_t*) data;

    if (msg->type == PING) {
        LOG1("PROCESS(%d): ", self->pid);
        LOG1("Received PING from %d\n", msg->pid);

        message_t m;
        m.pid = self->pid;
        m.type = PONG;
        ZTIME_SET(m.t0, msg->t0);
        ztas_ucast(msg->pid, (void*) &m, sizeof(message_t), 0);
    } else {
        LOG1("PROCESS(%d): ", self->pid);
        LOG1("Received PONG from %d\n", msg->pid);

        ztime_t t1, dur;
        ztas_clock(&t1);              // read current time
        ZTIME_DUR(msg->t0, t1, dur);  // calculate duration from t0 to t1

        // print round trip time
        LOG1("PROCESS(%d): ",     self->pid);
        LOG1("RTT to %d ",        msg->pid);
        LOG1("\trtt.hgh = %u",   dur.high);
        LOG1("\trtt.low = %u\n", dur.low);
    }
}


void
ztas_trig(void* state, int32_t aid)
{
    // state should not be empty
    assert (state && "received empty state (trig)");

    // cast state
    self_t* self = (self_t*) state;
    LOG1("PROCESS(%d): ", self->pid);
    LOG1("alarm %d trigged\n", aid);

    // create a PING message to broadcast
    message_t msg;
    msg.pid = self->pid; // add my pid to message
    msg.type = PING;     // set type of message
    ztas_clock(&msg.t0); // read clock

    // broadcast message
    ztas_bcast((void*) &msg, sizeof(message_t), 0);

    // set alarm for next period
    ztas_alarm(PING_ALARM, PING_PERIOD);
}
