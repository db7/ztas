/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

extern int ztas_main(const int argc, const char* argv[]);

#include <ztas/mem.h>
#include <ztas/log.h>
#include <ztas/ztas.h>


extern void* zte_init(int32_t pid, const void* config, size_t size);
extern void  zte_fini(void* state);
extern void  zte_recv(void* state, const void* data, size_t size);
extern void  zte_trig(void* state, int32_t aid);

extern int ztas_ucast(int32_t pid, const void* data, size_t size);
extern int ztas_bcast(const void* data, size_t size);
int
zte_ucast(int32_t pid, const void* data, size_t size)
{
    return ztas_ucast(pid, data, size);
}

int
zte_bcast(const void* data, size_t size)
{
    return ztas_bcast(data, size);
}


typedef struct {
    // fads configuration
    int32_t N;
    // rotel4n configuration
    int32_t N2;
    int32_t delta;
} config_t;

void*
ztas_init(int32_t pid)
{
    config_t cfg;
    cfg.N  = 1;
    cfg.N2 = 1;
    cfg.delta = 1000;
    return zte_init(pid, (const void*) &cfg, sizeof(config_t));
}

void
ztas_trig(void* state, int32_t aid)
{
    zte_trig(state, aid);
}
void
ztas_fini(void* state)
{
    zte_fini(state);
}

void
ztas_recv(void* state, const void* data, size_t size)
{
    zte_recv(state, data, size);
}
    
int
main(int argc, char* argv[])   
{
    const char* args[4] = {
        "-",
        "0",
        "2000",
        "localhost:2000"
    };
    return ztas_main(4, args);
}
