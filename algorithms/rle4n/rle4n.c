/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

/* Implementation of the (n+4)-stable leader election with message
 * loss from Aguilera's Stable Leader Election Paper (2000)
 *
 * Instead of implementing expiring links with their approach, we use
 * FADS.
 */

#include <ztas.h>
#include <ztas/log.h>
#include <ztas/mem.h>
#include <ztas/string.h>
#include <ztas/msg.h>
#include <ztas/chain.h>
#include <ztas/algorithms/fads.h>

#include <assert.h>

/* state data structure */
typedef struct {
    int32_t pid;
    int32_t N;

    int32_t round;                 /* current round */
    int32_t leader;                /* round's leader */
    int32_t ok_received;           /* with > 2 ok's, leader is valid*/

    ztime_t t0;                   /* timestamp */
    ztime_t delta;
    int32_t leader_beat;          /* heart beat period */
    int32_t leader_dead;          /* dead leader detection time */

    fads_t* fads;
} rle4n_t;

typedef struct {
    int32_t N;
    int32_t delta;
} rle4n_config_t;

/* election message */
typedef struct {
    int32_t pid;            // my pid
    enum {OK, START} type;  // type of message
    int32_t round;          // election round
    int32_t subround;       // messages in the round
    ztime_t ts;
} msg_t;

#define SIZEOF sizeof(msg_t)

/* alarms */
#define LEADER_DEAD 1000
#define LEADER_BEAT 1001

/* helper functions' prototypes */
void send_ok       (rle4n_t* rle);
void send_start_all(rle4n_t* rle);
void send_start_to (rle4n_t* rle, int32_t dst);
void start_round   (rle4n_t* rle, int32_t round);


/* ZTAS INTERFACE */

rle4n_t*
rle4n_init(int32_t pid, int32_t N, int32_t delta, fads_t* fads)
{
    rle4n_t* rle = (rle4n_t*) malloc(sizeof(rle4n_t));
    assert (rle);
    rle->pid = pid;
    rle->fads = fads;

    // open configuration
    rle->N = N;

    // convert it to ztime_t
    ztime_t ts;
    ZTIME_ZERO(ts);
    ZTIME_ADD_MS(ts, delta, rle->delta);

    // set period for beat and dead
    rle->leader_beat = delta;
    rle->leader_dead = delta*2;

    LOG1("ALGO: rotel%d (stable)\n", 4);
    LOG1("ALGO: pid    = %d \n", rle->pid);
    LOG1("ALGO: N      = %d \n", rle->N);
    LOG1("ALGO: LEADER BEAT = %d ms\n", rle->leader_beat);
    LOG1("ALGO: LEADER DEAD = %d ms\n", rle->leader_dead);

    rle->round   = 0;
    rle->leader  = -1;

    start_round(rle, 1);

    return rle;
}



void
rle4n_fini(rle4n_t* rle)
{
    assert (rle);
    free(rle);
}



void
start_round(rle4n_t* rle, int32_t round)
{
    assert (rle);

    ztime_t now;
    ztas_clock(&now);

    LOG1("ALGO: starting new round %d\n", round);
    LOG1("ALGO:           time.h = %u\n", now.high);
    LOG1("ALGO:           time.l = %u\n", now.low);
    rle->round       = round;
    rle->leader      = round % rle->N;

    if (rle->round == 1 || rle->ok_received >= 2) {
        /* if there was a leader, start measuring election time */
        ztas_clock(&rle->t0);
    }

    /* reset oks */
    rle->ok_received = 0;

    send_start_all(rle);
    if (rle->leader == rle->pid) send_ok(rle);

    /* wait for leader's death */
    ztas_alarm(LEADER_DEAD, rle->leader_dead);
}


void
fads_deliver(rle4n_t* rle, int32_t pid, void* data, size_t size,
             ztime_t* delay, ztime_t* now)
{
    assert (rle);

    if (size != SIZEOF) return;

    // is message timely?
    if (ZTIME_GT(*delay, rle->delta)) {
        LOG1("ALGO: ignoring slow message from pid = %d\n", pid);
        return;
    }

    msg_t* msg = (msg_t*) data;
    assert (msg);

    switch(msg->type) {
    case OK:
        if (msg->round == rle->round) {
            ++rle->ok_received;
            //LOG1("ALGO: oks count: %d\n", rle->ok_received);

            if (rle->ok_received == 2) {
                ztime_t dur;
                ZTIME_DUR(rle->t0, *now, dur);
                LOG1("ALGO: leader %d ", rle->leader);
                LOG1("%u ", dur.high);
                LOG1("%u ", dur.low);
                LOG1("%u ", now->high);
                LOG1("%u\n", now->low);
            }
            ztas_alarm(LEADER_DEAD, rle->leader_dead);
            break;
        }
        // no break here
    case START: // or OK
        if (msg->round > rle->round) {
            //LOG1("ALGO: got a greater round: %d\n", msg->round);
            start_round(rle, msg->round);
        }
        if (msg->round < rle->round) {
            send_start_to(rle, pid);
        }
        break;
    default:
        assert (0 && "invalid type");
    }
}



int
rle4n_trig(rle4n_t* rle, int32_t aid)
{
    assert (rle);

    switch(aid) {
    case LEADER_BEAT:
        if (rle->leader == rle->pid) send_ok(rle);
        break;
    case LEADER_DEAD:
        LOG1("leader dead in round: %d\n", rle->round);
        start_round(rle, rle->round + 1);
        break;
    default:
        return ZTAS_CHAIN_NEXT;
    }
    return ZTAS_CHAIN_DONE;
}


/* HELPER FUNCTIONS */

void
send_ok(rle4n_t* rle)
{
    assert (rle);

    assert(rle->leader == rle->pid);
    //LOG1("ALGO: sending ok round = %d\n", rle->round);

    msg_t msg;
    msg.type  = OK;
    msg.pid   = rle->pid;
    msg.round = rle->round;
    fads_bcast(rle->fads, (void*) &msg, SIZEOF);
    ztas_alarm(LEADER_BEAT, rle->leader_beat);
    ztas_alarm(LEADER_DEAD, rle->leader_dead); // message to myrle takes long, so let me reset timer here.
}

void
send_start_all(rle4n_t* rle)
{
    assert (rle);

    LOG1("ALGO: sending start to all. round = %d\n", rle->round);

    msg_t msg;
    msg.type  = START;
    msg.pid   = rle->pid;
    msg.round = rle->round;
    fads_bcast(rle->fads, (void*) &msg, SIZEOF);
}


void
send_start_to(rle4n_t* rle, int32_t dst)
{
    assert (rle);

    /* LOG1("ALGO: sending start to %d\n", dst); */
    /* LOG1("ALGO:         round = %d\n", rle->round); */

    msg_t msg;
    msg.type  = START;
    msg.pid   = rle->pid;
    msg.round = rle->round;
    fads_ucast(rle->fads, dst, (void*) &msg, SIZEOF);
}








/////////// GLUE CODE ////////////////////

typedef struct {
    uint32_t timeout;
    rle4n_t* rle4n;
    fads_t* fads;
} state_t;

/** glue_init initializes all the */
void*
ztas_init(int32_t pid)
{
    state_t* state = (state_t*) malloc(sizeof(state_t));
    assert(state);
    bzero(state, sizeof(state_t));

    //state->timeout = ztas_cread("main:timeout");
    //ztas_alarm(TERM_ALARM, ->timeout);
    int32_t N = atoi(ztas_cread("rle4n:N"));
    int32_t delta = atoi(ztas_cread("rle4n:delta"));
    state->fads = fads_init(pid, N);
    state->rle4n = rle4n_init(pid, N, delta, state->fads);
    return (void*) state;
}


void
ztas_fini(void* state)
{
    assert (state);
    state_t* self = (state_t*) state;

    fads_fini(self->fads);
    rle4n_fini(self->rle4n);
    free(self);
}

void
ztas_recv(void* state, const void* data, size_t size)
{
    assert (state);
    state_t* self = (state_t*) state;

    CHAIN(
        || CHAIN_HANDLER(fads_recv(self->fads, data, size))
        );
}


void
ztas_trig(void* state, int32_t aid)
{
    assert (state != NULL);
    state_t* self = (state_t*) state;
    //if (aid == TERM_ALARM) ztas_term();

    CHAIN(
        || CHAIN_HANDLER(fads_trig(self->fads, aid))
        || CHAIN_HANDLER(rle4n_trig(self->rle4n, aid))
        );
}


