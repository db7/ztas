/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/log.h>
#include <ztas.h>

void*
ztas_init(int32_t pid)
{
    ztas_alarm(1, 1000);
    return NULL;
}

void
ztas_trig(void* state, int32_t aid)
{
    LOG1("hello world: %d\n", aid);
    ztas_alarm(1, 1000);
}


void ztas_fini(void* state){}
void ztas_recv(void* state, const void* data, size_t size){}
