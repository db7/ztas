/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

#include <ztas/log.h>
#include <ztas/mem.h>
#include <ztas.h>
#include <assert.h>
#include "sarr.h"
#include <ztas/string.h>


enum operation {PONG = 1, ADD = 2, SUB = 3, MULT = 4,
                DIV = 5, MOD = 6, NONE = 7, LOOP = 8,
                AND = 9, IFAND = 10, SARR = 11, MMF = 12};

typedef struct {
    int32_t pid;
    enum operation type;
    ztime_t t0;
} message_t;

typedef struct {
    int32_t pid;
    ztime_t t0;
    enum operation type;
    int32_t msg_size;
    int32_t period;
    int32_t operation_loop;
    sarr_t* sarr;
    int32_t sarr_counter;
    message_t* msg;
} self_t;


#define PING_ALARM  1


// to test different modulo implementations
//
//uint32_t mod3( uint32_t a ) {
//    a = (a >> 16) + (a & 0xFFFF); /* sum base 2**16 digits
//                                        a <= 0x1FFFE */
//    a = (a >>  8) + (a & 0xFF);   /* sum base 2**8 digits
//                                        a <= 0x2FD */
//    a = (a >>  4) + (a & 0xF);    /* sum base 2**4 digits
//                                        a <= 0x3C; worst case 0x3B */
//    a = (a >>  2) + (a & 0x3);    /* sum base 2**2 digits
//                                        a <= 0x1D; worst case 0x1B */
//    a = (a >>  2) + (a & 0x3);    /* sum base 2**2 digits
//                                        a <= 0x9; worst case 0x7 */
//    a = (a >>  2) + (a & 0x3);    /* sum base 2**2 digits
//                                        a <= 0x4 */
//    if (a > 2) a = a - 3;
//    return a;
//}


void*
ztas_init(int32_t pid)
{
    // allocate process state
    self_t* self = (self_t*) malloc(sizeof(self_t));
    assert (self && "error allocating process state");

    self->pid = pid;

    // read period (ping interval) from config
    const char* str = ztas_cread("encoded_ops:period");
    self->period = str ? atoi(str) : 100;

    // read msg size from config
    str = ztas_cread("encoded_ops:msg_size");
    self->msg_size = str ? atoi(str) : 64;

    // read operation to be performed from config
    str = ztas_cread("encoded_ops:operation");
    self->type = str ? atoi(str) : ADD;

    // read how often an operation should be performed from config
    str = ztas_cread("encoded_ops:operation_loop");
    self->operation_loop = str ? atoi(str) : 10000;

    self->sarr = sarr_init(0, 1000);
    self->sarr_counter = 0;

    if (pid == 0) {
        // schedule ping alarm
        ztas_alarm(PING_ALARM, self->period);
        // allocate and initialize msg
        self->msg = (message_t*) malloc(sizeof(message_t) + self->msg_size);
        self->msg->pid = self->pid;
        self->msg->type = self->type;
    } else {
        self->msg = (message_t*) malloc(sizeof(message_t));
        self->msg->pid = self->pid;
        self->msg->type = PONG;
    }

    // return state to framework
    return (void*) self;
}


void
ztas_fini(void* state)
{
    // free state allocated in ztas_init()
    self_t* self = (self_t*) state;
    free(self->msg);
    sarr_fini(self->sarr);
    free(state);
}


void
ztas_recv(void* state, const void* data, uint32_t size)
{
    self_t* self = (self_t*) state;
    message_t* msg = (message_t*) data;

    switch(msg->type) {
        case PONG: {
            ztime_t t1, dur;
            // read current time
            ztas_clock(&t1);
            // calculate duration from t0 to t1
            ZTIME_DUR(msg->t0, t1, dur);

            // print round trip time
            LOG1("PROCESS(%d): ", self->pid);
            LOG1("response time to %d ", msg->pid);
            LOG1("\trt.hgh = %u", dur.high);
            LOG1("\trt.low = %u", dur.low);
            LOG1("\ttime.hgh = %u", t1.high);
            LOG1("\ttime.low = %u\n", t1.low);
            return;
        }

        case ADD: {
            int32_t i = 1;
            for (; i <= self->operation_loop; i++) {
                int32_t a = 0;
                a = a + i;
                a = a + i;
                a = a + i;
                a = a + i;
                a = a + i;
                a = a + i;
                a = a + i;
                a = a + i;
                a = a + i;
                a = a + i;
            }
            break;
        }

        case SUB: {
            int32_t i = 1;
            for (; i <= self->operation_loop; i++) {
                int32_t a = 0;
                a = a - i;
                a = a - i;
                a = a - i;
                a = a - i;
                a = a - i;
                a = a - i;
                a = a - i;
                a = a - i;
                a = a - i;
                a = a - i;
            }
            break;
        }

        case MULT: {
            int32_t i = 1;
            for (; i <= self->operation_loop; i++) {
                int32_t a = 0;
                a = a * i;
                a = a * i;
                a = a * i;
                a = a * i;
                a = a * i;
                a = a * i;
                a = a * i;
                a = a * i;
                a = a * i;
                a = a * i;
            }
            break;
        }

        case DIV: {
            int32_t i = 1;
            for (; i <= self->operation_loop; i++) {
                int32_t a = 15000;
                a = a / i;
                a = a / i;
                a = a / i;
                a = a / i;
                a = a / i;
                a = a / i;
                a = a / i;
                a = a / i;
                a = a / i;
                a = a / i;
            }
            break;
        }

        case MOD: {
            int32_t i = 1;
            for (; i <= self->operation_loop; i++) {
                int32_t a = 5000;
                a = a % i;
                a = a % i;
                a = a % i;
                a = a % i;
                a = a % i;
                a = a % i;
                a = a % i;
                a = a % i;
                a = a % i;
                a = a % i;
            }
            break;
        }

        case NONE: {
            break;
        }

        case LOOP: {
            int32_t i = 1;
            for (; i <= self->operation_loop; i++) {
                int32_t a = 0;
            }

            break;
        }

        case AND: {
            int32_t i = 1;
            for (; i <= self->operation_loop; i++) {
                if (self->operation_loop == 10000 && i == self->period) ;
                if (self->operation_loop == 10000 && i == self->period) ;
                if (self->operation_loop == 10000 && i == self->period) ;
                if (self->operation_loop == 10000 && i == self->period) ;
                if (self->operation_loop == 10000 && i == self->period) ;
                if (self->operation_loop == 10000 && i == self->period) ;
                if (self->operation_loop == 10000 && i == self->period) ;
                if (self->operation_loop == 10000 && i == self->period) ;
                if (self->operation_loop == 10000 && i == self->period) ;
                if (self->operation_loop == 10000 && i == self->period) ;
            }
            break;
        }

        case IFAND: {
            int32_t i = 1;
            for (; i <= self->operation_loop; i++) {
                if (self->operation_loop == 10000)
                    if(self->period == i) ;
                if (self->operation_loop == 10000)
                    if(self->period == i) ;
                if (self->operation_loop == 10000)
                    if(self->period == i) ;
                if (self->operation_loop == 10000)
                    if(self->period == i) ;
                if (self->operation_loop == 10000)
                    if(self->period == i) ;
                if (self->operation_loop == 10000)
                    if(self->period == i) ;
                if (self->operation_loop == 10000)
                    if(self->period == i) ;
                if (self->operation_loop == 10000)
                    if(self->period == i) ;
                if (self->operation_loop == 10000)
                    if(self->period == i) ;
                if (self->operation_loop == 10000)
                    if(self->period == i) ;
            }
            break;
        }

        case SARR: {
            int32_t i = 1;
            for (; i <= self->operation_loop; i++) {
                if (sarr_put(self->sarr, self->sarr_counter,
                             (void*) self) == SARR_TOO_HIGH)
                    sarr_next(self->sarr);
                self->sarr_counter++;
            }
            break;
        }

        case MMF: {
            int32_t i = 1;
            void* tmp = malloc(sizeof(message_t) + self->msg_size);
            for (; i <= self->operation_loop; i++) {
                memcpy(tmp, data, sizeof(message_t) + self->msg_size);
            }
            free(tmp);
            break;
        }
    }

    // send PONG msg after execution of operation-loop
    ZTIME_SET(self->msg->t0, msg->t0);
    ztas_ucast(msg->pid, (void*) self->msg, sizeof(message_t), 0);
}


void
ztas_trig(void* state, int32_t aid)
{
    self_t* self = (self_t*) state;

    // set time of msg before sending
    ztas_clock(&(self->msg->t0));

    // broadcast message
    //ztas_bcast((void*) self->msg, sizeof(message_t) + self->msg_size, 0);
    ztas_ucast(1, (void*) self->msg, sizeof(message_t) + self->msg_size, 0);
    // set alarm for next period
    ztas_alarm(PING_ALARM, self->period);
}
