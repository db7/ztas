/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 * Circular slot array
 */
#ifndef _SARR_H_
#define _SARR_H_
#include <stdint.h>

typedef struct sarr sarr_t;

sarr_t* sarr_init(int32_t slot_min, int32_t max_slots);
void    sarr_fini(sarr_t* sarr);
int     sarr_put(sarr_t* sarr, int32_t slot, void* value);
void*   sarr_get(sarr_t* sarr, int32_t slot);
/** same as sarr_get with slot_min */
void*   sarr_first(sarr_t* sarr);
/** return the highest slot seen. If last == min, returns NULL.*/
void*   sarr_last(sarr_t* sarr);
/** same as sarr_first but increments slot_min and slot_max */
void*   sarr_next(sarr_t* sarr);
int32_t sarr_min(sarr_t* sarr);
int32_t sarr_max(sarr_t* sarr);
int32_t sarr_full(sarr_t* sarr);


#define SARR_OK       1
#define SARR_TOO_HIGH 2
#define SARR_TOO_LOW  4
#define SARR_OOBOUND  6
#define SARR_ERROR    8

#endif
