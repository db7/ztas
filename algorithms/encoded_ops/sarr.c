/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/mem.h> // malloc
#include <ztas/string.h>
#include <assert.h>
#include "sarr.h"

struct sarr {
    void** array;
    int32_t max_slots; // maximum number of slots
    int32_t slot_min;  // id of lowest slot
    int32_t slot_max;  // id of highest slot
    int32_t slot_last; // id of highest used slot
    int32_t entries;   // occupied entries
};


sarr_t*
sarr_init(int32_t slot_min, int32_t max_slots)
{
    sarr_t* sarr = (sarr_t*) malloc(sizeof(sarr_t));
    assert (sarr);

    sarr->max_slots = max_slots;
    sarr->slot_min  = slot_min;
    sarr->slot_max  = slot_min + max_slots-1;
    sarr->slot_last = slot_min;

    sarr->array = malloc(sizeof(void*) * max_slots);
    assert (sarr->array);
    bzero(sarr->array, sizeof(void*) * max_slots);

    return sarr;
}

void
sarr_fini(sarr_t* sarr)
{
    assert (sarr);
    free(sarr->array);
    free(sarr);
}

int
sarr_put(sarr_t* sarr, int32_t slot, void* value)
{
    assert (sarr);
    if (slot > sarr->slot_max) return SARR_TOO_HIGH;
    if (slot < sarr->slot_min) return SARR_TOO_LOW;
    if (sarr->array[slot % sarr->max_slots] != NULL)
        return SARR_ERROR;

    sarr->array[slot % sarr->max_slots] = value;

    if (slot > sarr->slot_last) sarr->slot_last = slot;
    ++sarr->entries;
    return SARR_OK;
}

void*
sarr_get(sarr_t* sarr, int32_t slot)
{
    assert (sarr);
    if (slot > sarr->slot_max) return NULL;
    if (slot < sarr->slot_min) return NULL;
    return sarr->array[slot % sarr->max_slots];
}

void*
sarr_next(sarr_t* sarr)
{
    assert (sarr);
    ++sarr->slot_max;
    void* v = sarr->array[sarr->slot_min % sarr->max_slots];
    sarr->array[sarr->slot_min++ % sarr->max_slots] = NULL;
    --sarr->entries;
    return v;
}

void*
sarr_first(sarr_t* sarr)
{
    assert (sarr);
    return sarr->array[sarr->slot_min % sarr->max_slots];
}

void*
sarr_last(sarr_t* sarr)
{
    assert (sarr);
    return sarr->array[sarr->slot_last % sarr->max_slots];
}

int32_t
sarr_min(sarr_t* sarr)
{
    assert (sarr);
    return sarr->slot_min;
}

int32_t
sarr_max(sarr_t* sarr)
{
    assert (sarr);
    return sarr->slot_max;
}

int
sarr_full(sarr_t* sarr)
{
    return sarr->entries == sarr->max_slots;
}
