/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

/*
 * Implementation of FADS
 */

#include <ztas.h>
#include <ztas/msg.h>
#include <ztas/chain.h>
#include <ztas/log.h>
#include <ztas/mem.h>
#include <ztas/string.h>
#include <ztas/algorithms/fads.h>

#include <assert.h>

struct fads {
    void* args;

    int32_t N;
    int32_t pid;

    ztime_t* st;
    ztime_t* rt;

    // size in bytes
    size_t vec_size;
    size_t tot_size;

    // self indexes in bytes
    size_t st_idx;
    size_t rt_idx;

    ztime_t update;
};


#define FADS_SELF 0
#define FADS_HELPER_PERIOD 12345 // 5000

#define __FADS_BCAST_INFO_SIZE(N) (2*N*sizeof(ztime_t))
#define __FADS_BCAST_ST_IDX(i,N) (i*sizeof(ztime_t))
#define __FADS_BCAST_RT_IDX(i,N) ((i+N)*sizeof(ztime_t))
#define __FADS_BCAST_SIZE(N, size) (sizeof(fads_bcast_t) + size + 2*N*sizeof(ztime_t))

extern void fads_deliver(void* args, int32_t pid,
                         void* data, size_t size,
                         ztime_t* delay, ztime_t* now);


fads_t*
fads_init(int32_t pid, int32_t N)
{
    fads_t* fads = (fads_t*) malloc(sizeof(fads_t));
    fads->N      = N;
    fads->pid    = pid;

    fads->args   = NULL;

    size_t vec_size_bytes = sizeof(ztime_t)*fads->N;

    fads->st   = (ztime_t*) malloc(vec_size_bytes);
    fads->rt   = (ztime_t*) malloc(vec_size_bytes);
    fads->vec_size = vec_size_bytes;
    fads->tot_size = vec_size_bytes + vec_size_bytes;
    fads->st_idx   = __FADS_BCAST_ST_IDX(fads->pid, fads->N);
    fads->rt_idx   = __FADS_BCAST_RT_IDX(fads->pid, fads->N);

    int32_t i;
    for (i = 0; i < fads->N; ++i) {
        fads->rt[i].high = 0;
        fads->rt[i].low  = 0;
        fads->st[i].high = 0;
        fads->st[i].low  = 0;
    }

    ztas_alarm(FADS_HELPER_ALARM, FADS_HELPER_PERIOD);

    return fads;
}

void
fads_set_args(fads_t* fads, void* args)
{
    fads->args = args;
}

void
fads_fini(fads_t* fads)
{
    assert (fads);
    free(fads);
}


static void
fads_update(fads_t* fads, uint32_t pid, ztime_t* c, ztime_t* d)
{
    //ztime_t old_c, old_d; //, diff_c, diff_d;
    //ZTIME_SET(old_c, fads->st[pid]);
    //ZTIME_SET(old_d, fads->rt[pid]);

    if (pid == fads->pid) {
        // if the update is to myfads, ignore
        // TODO: is this ok?
    } else {
        // diff = (d-old_d) - (c - old_c)
        /* ZTIME_DUR(old_c, *c, diff_c); */
        /* ZTIME_DUR(old_d, *d, diff_d); */

        // if (diff < 0)
        if (ZTIME_GT(*c, fads->st[pid])
            /* || ZTIME_GT(diff_c, diff_d) */
            ) {
            ZTIME_SET(fads->st[pid], *c);
            ZTIME_SET(fads->rt[pid], *d);
        }
    }
}

static void
fads_delay(fads_t* fads, int32_t pid,
           ztime_t* a, ztime_t* b, ztime_t* c, ztime_t* d,
           ztime_t* result)
{
    if (pid == fads->pid) { // my id
        // result := (d-c)
        ZTIME_DUR(*c, *d, *result);
    } else {
        // result := (d-a) - (c-b)

        ztime_t diff_cb, diff_da;

        // if b == 0, then we simply consider it is -inf
        // that is, result == +inf
        // this happens when the first message is sent
        if (b->high == 0 && b->low == 0) {
            result->high = -1;
            result->low = -1;
            return;
        }

        ZTIME_DUR(*b, *c, diff_cb);
        ZTIME_DUR(*a, *d, diff_da);
        ZTIME_DUR(diff_cb, diff_da, *result);
    }
}

int
fads_recv(fads_t* fads, const void* data, size_t size)
{
    switch(ztas_msg_type(data)) {
    case FADS_UCAST_MSG:
    {
        fads_ucast_t* m = (fads_ucast_t*) data;
        /* LOG1("FADS: got unicast message from %d\n", dg->pid);        */
        ztime_t d, delay;

        ztas_clock(&d);
        fads_delay (fads,  ztas_msg_src(data), &m->a, &m->b, &m->c, &d, &delay);
        fads_update(fads,  ztas_msg_src(data), &m->c, &d);

        fads_deliver(fads->args, ztas_msg_src(data), m->data, m->size, &delay, &d);
        break;
    }
    case FADS_BCAST_MSG:
    {
        fads_bcast_t* m = (fads_bcast_t*) data;
        //LOG1("FADS: got broadcast message size %d\n", size);
        ztime_t d, delay;
        ztas_clock(&d);

        // fix data pointer
        //m->data = m->__data + fads->tot_size;

        size_t st_idx = __FADS_BCAST_ST_IDX(ztas_msg_src(data), fads->N);


        fads_delay(fads, ztas_msg_src(data),
                   (ztime_t*)(m->__data + fads->st_idx),
                   (ztime_t*)(m->__data + fads->rt_idx),
                   (ztime_t*)(m->__data + st_idx),
                   &d, &delay);

        fads_update (fads, ztas_msg_src(data), (ztime_t*)(m->__data + st_idx), &d);
        fads_deliver(fads->args, ztas_msg_src(data), m->__data + fads->tot_size, m->size, &delay, &d);

        break;
    }
    case FADS_HELPER_MSG:
    {
        fads_helper_t* m = (fads_helper_t*) data;
        //LOG1("FADS: got helper message from %d\n", dg->pid);
        ztime_t d;
        ztas_clock(&d);
        fads_update(fads, ztas_msg_src(data), &m->time, &d);
        break;
    }
    default:
        return ZTAS_CHAIN_NEXT;
    }
    return ZTAS_CHAIN_DONE;
}



int
fads_trig(fads_t* fads, int32_t aid)
{
    switch(aid) {
    case FADS_HELPER_ALARM:
    {
        ztime_t now;
        ztas_clock(&now);
        LOG1("FADS: HELPER ALARM %d\n", aid);
        ZTIME_SET(fads->st[fads->pid], now);

        fads_helper_t helper;
        ztas_msg_init((void*)&helper, fads->pid, FADS_HELPER_MSG);
        ZTIME_SET(helper.time, now);

        ztas_bcast(&helper, sizeof(fads_helper_t), 0);

        /* schedule new heartbeat */
        ztas_alarm(FADS_HELPER_ALARM, FADS_HELPER_PERIOD);
        break;
    }
    default:
        return ZTAS_CHAIN_NEXT;
    }
    return ZTAS_CHAIN_DONE;
}

#define __FADS_UCAST_SIZE(size) (sizeof(fads_ucast_t) + size)


/* interface to the user */
void
fads_ucast_dgram(fads_t* fads, int32_t dst, fads_ucast_t* m)
{
    //LOG1("FADS: sending unicast message to %d\n", dst);
    ztas_clock(&fads->st[fads->pid]);

    ZTIME_SET(m->a, fads->st[dst]);
    ZTIME_SET(m->b, fads->rt[dst]);
    ZTIME_SET(m->c, fads->st[fads->pid]);

    ztas_ucast(dst, (void*) m, __FADS_UCAST_SIZE(m->size), 0);
}

fads_ucast_t*
fads_ucast_open(fads_t* fads, size_t size)
{
    fads_ucast_t* m = (fads_ucast_t*) malloc(__FADS_UCAST_SIZE(size));

    ztas_msg_init((void*)m, fads->pid, FADS_UCAST_MSG);
    m->size = size;

    return m;
}

void
fads_ucast_close(fads_t* fads, fads_ucast_t* m)
{
    free(m);
}


void
fads_ucast(fads_t* fads, int32_t dst, void* data, size_t size)
{
    fads_ucast_t* m = fads_ucast_open(fads, size);
    memcpy(m->data, data, size);
    fads_ucast_dgram(fads, dst, m);
    fads_ucast_close(fads, m);
}


void
fads_bcast_dgram(fads_t* fads, fads_bcast_t* m, ztime_t* send_time)
{
    if (send_time == NULL)
        ztas_clock(&fads->st[fads->pid]);
    else
        ZTIME_SET(fads->st[fads->pid], *send_time);

    //LOG1("FADS: sending broadcast at %lu\n", ZTIME_TS(fads->fads.st[fads->pid]));

    // update info
    memcpy(m->__data, fads->st, fads->vec_size);
    memcpy(m->__data + fads->vec_size, fads->rt, fads->vec_size);

    size_t size = sizeof(fads_bcast_t) + m->size + fads->tot_size;

    // clear data pointer (it has to be reset on receipt)
    //m->data = NULL;

    ztas_bcast(m, size, 0);

    /* reset alarm */
    ztas_alarm(FADS_HELPER_ALARM, FADS_HELPER_PERIOD);
}


fads_bcast_t*
fads_bcast_open(fads_t* fads, size_t size)
{
    size_t info_size = fads->tot_size;
    fads_bcast_t* m = (fads_bcast_t*) malloc(sizeof(fads_bcast_t) +
                                             size + info_size);
    assert (m && "allocation of fads_bcast_t failed");

    ztas_msg_init((void*)m, fads->pid, FADS_BCAST_MSG);
    m->size        = size;
    //m->data        = m->__data + info_size;

    return m;
}

void
fads_bcast_close(fads_t* fads, fads_bcast_t* m)
{
    free(m);
}

void
fads_bcast(fads_t* fads, void* data, size_t size)
{
    fads_bcast_t* m = fads_bcast_open(fads, size);
    memcpy(m->__data + fads->tot_size, data, size);

    fads_bcast_dgram(fads, m, NULL);
    fads_bcast_close(fads, m);
}
