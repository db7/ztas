/* ----------------------------------------------------------------------
 * Copyright (c) 2012,2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 * @addtogroup algorithms
 * @section alg_lead_election Rotating Leader Election
 * Rotating Leader election implementation.
 * \see algorithms/rle/rle.c
 *
 * The algorithm follows the rotating leader election algorithm
 * suggested in "Stable leader election" by Aguilera et al. This
 * algorithm is the simplest version of the rotating leader election,
 * ie, it is not stable!
 *
 */
#include <ztas.h>
#include <ztas/log.h>
#include <ztas/mem.h>
#include <ztas/ds/vector.h>
#include <assert.h>
#include <ztas/algorithms/rle.h>

extern void rle_alarm(int32_t aid, int32_t ms);
extern void rle_send(int32_t pid, rle_msg_t* msg);

/* Constants */
#define LOOP_ALARM  RLE_LOOP_ALARM
#define LOOP_PERIOD 500
#define TIMEOUT_ALARM  RLE_TIMEOUT_ALARM
#define TIMEOUT_PERIOD 1000

/* Algorithm state */
struct rle {
    int32_t p;
    int32_t r;
    int32_t leader;
    int32_t N;
    vector_t* map;
};

static void rle_msg_init(rle_msg_t *msg) {
    int i = 0;
    for (; i < sizeof(msg->ph) / sizeof(msg->ph[0]); ++i) {
        msg->ph[i] = 0;
    }
}

/** ----------------------------------------------------------------------------
 * Client interface
 * -------------------------------------------------------------------------- */

int32_t
rle_leader(const rle_t* rle)
{
    //! @todo this can't actually happen because it means init wasnt called.
    //! it should be checked instead of le->leader has a
    //! reasonable value.

    //! This is actually allowed if you call the leader le in
    //! the init of another algorithm.

    if (rle == NULL) {
        return NO_LEADER;
    }

    return vector_array(rle->map)[0];
}

static void
start_round(int32_t s, rle_t* rle)
{
    int32_t N = rle->N;
    int32_t p = rle->p;
    const int32_t* map = vector_array(rle->map);
    assert(N == vector_size(rle->map));

    // r <- s
    rle->r = s;

    // leader <- s mod n
    rle->leader = map[(s % N)];

    // if p != s mod N then send (START,s) to s mod n
    if (p != rle->leader) {
        rle_msg_t start;
        rle_msg_init(&start);
        start.pid = p;
        start.type = START;
        start.number = s;
        rle_send(rle->leader, &start);
    }

    LOG1("leader is process (%d) \n", rle->leader);

    // restart timer
    rle_alarm(LOOP_ALARM, LOOP_PERIOD);
    rle_alarm(TIMEOUT_ALARM, TIMEOUT_PERIOD);
}

rle_t*
rle_init(int32_t pid, const idset_t* processes)
{
    // allocate process state
    rle_t* rle = (rle_t*) malloc(sizeof(rle_t));
    assert(rle && "error allocating process state");

    // initialize rle
    rle->p = pid;
    rle->N = idset_size(processes);

    // map round to pid
    rle->map = vector_init(rle->N);
    assert(rle->map);

    // Add pids from processes idset into vector. We assume all
    // processes receive the same idset.
    idset_it_t it;
    idset_it_init((idset_t*) processes, &it);
    int32_t p;
    while ((p = idset_it_next(&it)) >= 0) {
        vector_push(rle->map, p);
    }

    start_round(0, rle);

    return rle;
}

void
rle_fini(rle_t* rle)
{
    vector_fini(rle->map);
    free(rle);
}

void
rle_recv(rle_t* rle, rle_msg_t* msg)
{
    // initial assertions
    assert(msg != NULL && "received empty message");

    // upon receive (OK,k) with k=r do restart timer
    if ((msg->type == OK) && (msg->number == rle->r))
        rle_alarm(TIMEOUT_ALARM, TIMEOUT_PERIOD);

    // upon receive (OK,k) or (START,k) with k>r do StartRound(k)
    if (msg->number > rle->r)
        start_round(msg->number, rle);

}

void
rle_trig(rle_t* rle, int32_t aid)
{
    switch (aid) {
    case LOOP_ALARM:
        if (rle->p == rle->leader) {
            const int32_t* map = vector_array(rle->map);

            // create (OK,r) message
            rle_msg_t ok;
            rle_msg_init(&ok);
            ok.pid = rle->p;
            ok.type = OK;
            ok.number = rle->r;

            // send this message to all processes
            int i;
            for (i = 0; i < rle->N; ++i) {
                rle_send(map[i], &ok);
            }
        }
        rle_alarm(LOOP_ALARM, LOOP_PERIOD);
        break;
    case TIMEOUT_ALARM:
        start_round(rle->r + 1, rle);
        break;
    }
}
