/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas.h>
#include <ztas/ds/idset.h>
#include <assert.h>
#include <ztas/algorithms/rle.h>

void*
ztas_init(int32_t pid)
{
    idset_t* processes = idset_init_str(ztas_cread("rle:processes"));
    rle_t* rle = rle_init(pid, processes);
    return (void*) rle;
}

void
ztas_fini(void* state)
{
    rle_t* rle = (rle_t*) state;
    rle_fini(rle);
}


void
ztas_recv(void* state, const void* data, size_t size)
{
    assert (state && data);
    assert (size == sizeof(rle_msg_t));
    rle_msg_t* msg = (rle_msg_t*) data;
    rle_t* rle = (rle_t*) state;
    rle_recv(rle, msg);
}


void
ztas_trig(void* state, int32_t aid)
{
    assert (state);
    rle_t* rle = (rle_t*) state;
    rle_trig(rle, aid);
}


void
rle_send(int32_t pid, rle_msg_t* msg)
{
    ztas_ucast(pid, (void*) msg, sizeof(rle_msg_t), ZTAS_CAST_UMSG);
}

void
rle_alarm(int32_t aid, int32_t ms)
{
    ztas_alarm(aid, ms);
}
