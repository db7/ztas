/* ----------------------------------------------------------------------
 * Copyright (c) 2012,2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

/**
 * \file rle.h
 * \author Yue
 * \brief A simple leader election.
 *
 * \section Description 
 *
 * This leader election implementation rotates the leader once it is
 * detected as crashed. `le` implements a Omega failure dectector if
 * no messages are lost and processes are fully connected.
 *
 *
 * \addtogroup algorithms
 * \section alg_lead_election Rotating Leader Election
 * A leader election implementation.
 * \see algorithms/leader-election/leader-election.h
 */
#ifndef _RLE_H_
#define _RLE_H_

#include <ztas/types.h>
#include <ztas/ds/idset.h>

#define NO_LEADER -1

struct rle;
typedef struct rle rle_t;

typedef struct rle_msg {
    int32_t ph[6]; // place holder for header
    int32_t pid;
    enum {START = 1, OK = 2} type;
    int32_t number;
} rle_msg_t;

rle_t* rle_init(int32_t pid, const idset_t* processes);
void   rle_fini(rle_t* rle);
void   rle_recv(rle_t* rle, rle_msg_t* msg);
void   rle_trig(rle_t* rle, int32_t aid);

/**
 * returns the process ID of the current leader
 * may return NO_LEADER. This method is safe to be called in any
 * init() method.
 */
int32_t rle_leader(const rle_t* rle);

/* expected methods */
void rle_alarm(int32_t aid, int32_t ms);
void rle_send(int32_t pid, rle_msg_t* msg);

/* alarms */

#define RLE_ID             5000
#define RLE_LOOP_ALARM     RLE_ID + 1
#define RLE_TIMEOUT_ALARM  RLE_ID + 2

#endif /* _RLE_H_ */
