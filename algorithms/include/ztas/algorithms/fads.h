/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

/**
 * \file fads.h
 * \author Diogo
 * \brief Timed-channel service.
 * 
 * Implementation of the "Fail-aware Datagram Service" by Fetzer and
 * Christian.
 *
 * FADS provides a layer above the ztas messaging service which
 * calculates an upper-bound in the transmission delay of each
 * message. The message delay upperbound can be used to discard late
 * messages.
 */

#ifndef _FADS_H_
#define _FADS_H_

#include <ztas/msg.h>
#include <ztas/mem.h>


/********************************************************************************
 * message types
 *******************************************************************************/

#define FADS_UCAST_MSG    3
#define FADS_BCAST_MSG    4
#define FADS_HELPER_MSG   5
#define FADS_HELPER_ALARM 2


typedef struct {
    ZTAS_MSG
    ztime_t a;
    ztime_t b;
    ztime_t c;
    size_t  size;
    char    data[];
} fads_ucast_t;

typedef struct fads_broadcast fads_bcast_t;
struct fads_broadcast {
    ZTAS_MSG
    size_t  size;      // user data size
    char    __data[];  // 2*N for timestamps + user data
};

typedef struct fads_helper fads_helper_t;
struct fads_helper {
    ZTAS_MSG
    ztime_t time;
};


typedef struct fads fads_t;

/********************************************************************************
 * methods
 *******************************************************************************/

fads_t* fads_init(int32_t pid, int32_t N);
void    fads_fini(fads_t* fads);
int     fads_recv(fads_t* fads, const void*, size_t);
int     fads_trig(fads_t* fads, int32_t);

void  fads_ucast(fads_t* fads, int32_t, void*, size_t);
void  fads_bcast(fads_t* fads, void*, size_t);
void  fads_set_args(fads_t* fads, void* args);

fads_bcast_t* fads_bcast_open(fads_t* fads, size_t size);
fads_ucast_t* fads_ucast_open(fads_t* fads, size_t size);

#define CAST_DATA(m, type)  ((type*)m->data)
#define DEREF_DATA(m, type) (*CAST_DATA(m,type))

#endif /* _FADS_H_ */
