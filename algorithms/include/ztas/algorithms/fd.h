/* ----------------------------------------------------------------------
 * Copyright (c) 2012,2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

/**
 * \file fd.h
 * \author Kewaan
 * \brief A push unreliable failure detector.
 *
 * \section Description
 * fd is a push failure detector which uses ztas_bcast() to send
 * heart-beat messagess.  fd is an unreliable failure detector,
 * meaning that a process might appear as crashed and eventually
 * appear to be alive again.
 *
 * \note fd additionally provides round-trip-time (RTT)
 * measurements. RTT is measured by answering any heart-beat message
 * received with ztas_ucast(). The RTT measurements are disabled by
 * default. Set FD_WITH_RTT in fd.c to enable fd_rtt() method.
 *
 * \note Debugging can be enabled by defining DEBUG in fd.c. N
 * indicates the maximum # of processes.
 *
 * \addtogroup algorithms
 * \section alg_fd Failure Detector
 * A push unreliable failure detector.
 * \see algorithms/fd/fd.h
 */

#ifndef _FD_H_
#define _FD_H_

#include <ztas/msg.h>
#include <ztas/ds/idset.h>
#include <ztas/types.h>
typedef struct fd fd_t;

/**
 * \brief constructor
 */
fd_t* fd_init(int32_t pid, int32_t delta_ms);

/**
 * \brief destructor
 */
void fd_fini(fd_t* fd);

/**
 * \brief
 */
int fd_recv(fd_t* fd, const ztas_msg_t* msg);

/**
 * \brief
 */
int fd_trig(fd_t* fd, int32_t aid);

/**
 * \brief returns 1 if a specified 'pid' is running
 * or 0 if the specified 'pid' is thought to be crashed.
 */
int fd_isalive(fd_t* fd, int32_t pid);
#define FD_UP   1
#define FD_DOWN 0

/**
 * \brief sets the rtt value in the supplied ztime_t struct for the
 * specified pid
 */
void fd_rtt(fd_t* fd, int32_t pid, ztime_t* rtt);


#define FD_PUSH_MSG    101
#define FD_REPLY_MSG   102
#define FD_PUSH_ALARM  104


#endif /* _FD_H_ */
