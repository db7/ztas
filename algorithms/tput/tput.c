/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

/**
 * \file tput.c
 * \brief Measures maximum bandwidth between processes
 */
#include <ztas.h>
#include <ztas/mem.h>
#include <ztas/log.h>
#include <assert.h>
#include <ztas/shrp.h>

#ifdef IS_MODULE
#include <ztas/module.h>
#endif

#define TPUT_ALARM 1
#define TERM_DELTA 1000
#define TERM_ALARM 2

typedef struct {
  int32_t pid;
  ztime_t t0;
  char data[0]; // payload
} message_t;


typedef struct {
    int32_t pid;
    ztime_t t0;
    size_t read;
    message_t* msg;
    struct {
        uint32_t message_size;
        int32_t  count;
        uint32_t period;
        uint32_t timeout; // in seconds
    } config;
} self_t;


void*
ztas_init(int32_t pid)
{
    self_t* self = (self_t*) malloc(sizeof(self_t));
    assert (self);
    self->pid = pid;
    ztas_clock(&self->t0);
    self->config.message_size = atoi(ztas_cread("tput:message_size"));
    self->config.count = atoi(ztas_cread("tput:count"));
    self->config.period = atoi(ztas_cread("tput:period"));
    self->config.timeout = atoi(ztas_cread("tput:timeout"));

    if (self->pid == 0) {
        ztas_alarm(TPUT_ALARM, 1 + self->config.period);
        self->msg = (message_t*) shrp_malloc(self->config.message_size + sizeof(message_t));
        self->msg->pid = self->pid;
    } else {
        assert (self->pid == 1);
        self->msg = NULL;
    }

    ztas_alarm(TERM_ALARM, TERM_DELTA*self->config.timeout);
    return self;
}



void
ztas_fini(void* state)
{
    self_t* self = (self_t*) state;
    shrp_free(self->msg);
    free(state);
}


void
ztas_recv(void* state, const void* data, size_t size)
{
    self_t* self = (self_t*) state;

    ztime_t now;
    ztas_clock(&now);
    assert (self->pid == 1);

    message_t* msg = (message_t*) data;
    assert (msg != NULL);

    self->read += size;

    ztime_t max_ts;
    ZTIME_ADD_MS(self->t0, 1000, max_ts);
    if (ZTIME_GE(now, max_ts)) {
        LOG1("%u",     self->t0.high);
        LOG1(" %u",    self->t0.low);
        LOG1(" %u",    now.high);
        LOG1(" %u",    now.low);
        LOG1(" %u\n",  self->read);

        LOG1("tput: %f MB/s\n", self->read*1.0*S/(ZTIME_TS(now) - ZTIME_TS(self->t0))/1024/1024);
        self->read = 0;
        ZTIME_SET(self->t0, now);
    }
}



void
ztas_trig(void* state, int32_t aid)
{
    if (aid == TERM_ALARM) ztas_term();

    self_t* self = (self_t*) state;
    assert (self->pid == 0);

    // ztas_clock(&msg.t0);
    int c = self->config.count;
    while (c--> 0) {
        ztas_ucast(1, (void*) self->msg,
                   self->config.message_size + sizeof(message_t), 0);
        // we ignore the return value (we don't care if it was sent or not)
    }
    ztas_alarm(TPUT_ALARM, self->config.period);
}
