/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/log.h>
#include <ztas.h>

typedef struct {
    int32_t pad;
} message_t;

void*
ztas_init(int32_t pid)
{
    ztas_alarm(1, 0);

    message_t m;
    ztas_bcast(&m, sizeof(message_t), 0);

    return NULL;
}

void
ztas_trig(void* state, int32_t aid)
{
    ztas_alarm(1, 0);
}


void ztas_fini(void* state)
{}

void ztas_recv(void* state, const void* data, size_t size)
{
    message_t m;
    ztas_bcast(&m, sizeof(message_t), 0);
}
