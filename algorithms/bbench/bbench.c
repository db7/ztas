/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 * \file bbench.c
 * \brief Measures maximum bandwidth between processes
 *
 * Bandwidth benchmark.
 * Arguments:
 *  - request size
 *  - response size
 *  - response count
 *  - source process (-1=all)
 *
 * [bbench]
 * req_period = request period
 * req_size   = request size in bytes
 * res_size   = request size in bytes
 * N          = number of participants
 * K          = quorum size
 *
 */
#include <ztas.h>
#include <ztas/ds/tree.h>
#include <ztas/ds/queue.h>
#include <ztas/log.h>
#include <ztas/mem.h>
#include <ztas/string.h>
#include <assert.h>

#define BBENCH_ALARM 1

/* message type */
typedef struct {
    int32_t pid;
    enum { REQUEST, RESPONSE } type;
    uint32_t i;   // request id
    char data[0]; // payload
    ztime_t t0;   // time of generation
} message_t;

typedef struct {
    uint32_t i;       // request number
    uint32_t count;   // number of reponses
    uint32_t self;    // self already answered
    ztime_t t0;       // request time
    ztime_t t_self;   // reponse from self
    ztime_t t_first;  // first response (except self)
    ztime_t t_quorum; // response up to count
    ztime_t t_last;   // last response
} req_t;

/* state type */
typedef struct {
    int32_t pid;
    ztime_t t0;
    size_t read;
    message_t* request;
    message_t* response;
    uint32_t counter;
    tree_t*  sent_reqs;
    queue_t* free_reqs;

    /* configuration of benchmark */
    struct {
        uint32_t req_period; // period to produce message
        uint32_t req_size;   // size of requests
        uint32_t res_size;   // size of responses
        uint32_t K;          // number of responses to wait for
        uint32_t N;          // total number of responses
    } config;
} self_t;


void*
ztas_init(int32_t pid)
{
    // create self state
    self_t* self = (self_t*) malloc(sizeof(self_t));
    assert (self && "failed to initialize self");

    // test configuration (65488 msg_size max!)
    self->config.req_period = atoi(ztas_cread("bbench:req_period"));;
    self->config.req_size   = atoi(ztas_cread("bbench:req_size"));
    self->config.res_size   = atoi(ztas_cread("bbench:res_size"));
    self->config.K          = atoi(ztas_cread("bbench:K"));
    self->config.N          = atoi(ztas_cread("bbench:N"));

    assert (self->config.N >= self->config.K);

    LOG0("\n~~~ BBENCH configuration ~~~\n");
    LOG1("           pid: %u\n", pid);
    LOG1("             K: %u\n", self->config.K);
    LOG1("             N: %u\n", self->config.N);
    LOG1("request period: %u\n", self->config.req_period);
    LOG1("  request size: %u\n", self->config.req_size);
    LOG1(" response size: %u\n", self->config.res_size);
    LOG0("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");

    // initialize state and copy configuration
    self->pid = pid;
    ztas_clock(&self->t0);
    self->sent_reqs = tree_init();
    self->free_reqs = queue_create(10000);

    // initialize request counter
    self->counter = 0;

    // prepare template request and response
    self->request = (message_t*) malloc(self->config.req_size
                                        + sizeof(message_t));
    self->request->pid = self->pid;
    self->request->type = REQUEST;

    self->response = (message_t*) malloc(self->config.res_size
                                         + sizeof(message_t));
    self->response->pid = self->pid;
    self->response->type = RESPONSE;

    if (self->pid == 0) {
        // if process is a source process then
        // schedule request production
        ztas_alarm(BBENCH_ALARM, self->config.req_period);
    } else {
        assert (self->pid != 0);
    }

    return self;
}


void
ztas_fini(void* state)
{
    // free all allocated locations
    self_t* self = (self_t*) state;
    free(self->request);
    free(self->response);

    // free stuff from sent_reques void*
    req_t* r = NULL;
    while((r = tree_popdf(self->sent_reqs)) != NULL) {
        free(r);
    }
    tree_fini(self->sent_reqs);

    // free elements in free list
    while(!queue_empty(self->free_reqs)) {
        req_t* r = queue_deq(self->free_reqs);
        free(r);
    }
    queue_free(self->free_reqs);

    free(state);
}


void
ztas_recv(void* state, const void* data, size_t size)
{
    // read current time
    ztime_t now;
    ztas_clock(&now);

    // load self state
    self_t* self = (self_t*) state;

    // receive message
    message_t* msg = (message_t*) data;
    assert (msg != NULL && "msg: NULL");

    // calculate received broadcast
    self->read += size;

    // handle message
    switch (msg->type) {
    case REQUEST: { // create the response
        self->response->i = msg->i;
        ZTIME_SET(self->response->t0, msg->t0);
        memcpy((void*)&self->response->data[0], (void*)&msg->data[0],
               self->config.req_size);
        ztas_ucast(msg->pid, (void*) self->response, self->config.res_size
                   + sizeof(message_t), 0);
        break;
    }
    case RESPONSE: {
        req_t* r = (req_t*) tree_get(self->sent_reqs, msg->i);
        assert(r != NULL && "received response to request never sent");

        if (msg->pid == self->pid) {
            // ignore self for the time and just count up
            //r->self = 1;
            ZTIME_SET(r->t_self, now);
        }

        r->count++;

        if (r->count == 1) ZTIME_SET(r->t_first, now);
        if (r->count == self->config.K) ZTIME_SET(r->t_quorum, now);

        if (r->count == self->config.N) {
            // received all responses, printing summary
            // in the following format:
            // Summary: req# timestamp first count all tput

            ZTIME_SET(r->t_last, now);
            // line beginning and request number
            LOG1("Summary: %u", r->i);
            // current timestamp
            //LOG1(" %llu", now);

            ztime_t dur;

            // time to first answer in ms
            ZTIME_DUR(r->t0, r->t_first, dur);
            LOG1(" %f", ZTIME_TS(dur)/1000.0/1000.0);

            // time to count answers in ms
            ZTIME_DUR(r->t0, r->t_quorum, dur);
            LOG1(" %f", ZTIME_TS(dur)/1000.0/1000.0);

            // time to all answers in ms
            ZTIME_DUR(r->t0, r->t_last, dur);
            LOG1(" %f", ZTIME_TS(dur)/1000.0/1000.0);

            // throughput in MB/s
            ZTIME_DUR(self->t0, now, dur);
            LOG1(" %f\n", ((self->read)/1024.0/1024.0)
                 / (ZTIME_TS(dur)/1000.0/1000.0/1000.0));
            // reset self->time and self->read for next measurement
            self->read = 0;
            ZTIME_SET(self->t0, now);

            // request answered by all, queue to reuse it later
            int rc = queue_enq(self->free_reqs, (void*) r);
            if (rc == QUEUE_FULL) free(r);     // free if queue full
            tree_pop(self->sent_reqs, msg->i); // remove req from tree
        }
        break;
    }
    default:
        assert (0 && "should never happen");
    }
}



void
ztas_trig(void* state, int32_t aid)
{
    // load state and assert source process
    self_t* self = (self_t*) state;
    assert (self->pid == 0);
    assert (aid == BBENCH_ALARM);

    // read
    ztime_t t0;
    ztas_clock(&t0);

    // use an old req or allocate a new one
    req_t* r = (req_t*) queue_deq(self->free_reqs);
    if (r == NULL) r = (req_t*) malloc(sizeof(req_t));
    assert (r && "request allocation failed");

    // initialize request
    r->i     = ++self->counter;
    r->count = 0;
    r->self  = 0;
    ZTIME_SET(r->t0, t0);
    ZTIME_ZERO(r->t_self);
    ZTIME_ZERO(r->t_first);
    ZTIME_ZERO(r->t_quorum);
    ZTIME_ZERO(r->t_last);

    // save request information
    void* rc = tree_put(self->sent_reqs, r->i, (void*) r);
    assert (rc == NULL);

    // send request
    ZTIME_SET(self->request->t0, t0);
    self->request->i = r->i;
    ztas_bcast((void*) self->request, self->config.req_size
               + sizeof(message_t), 0);

    // schedule new alarm for message generation
    ztas_alarm(BBENCH_ALARM, self->config.req_period);
}
