/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include "hale_set.h"
#include <assert.h>
#include <ztas/string.h>

hale_set_t*
hale_set_create(pid_t N)
{
    hale_set_t* set = (hale_set_t*) malloc(sizeof(hale_set_t) + hale_set_sizeof(N));
    assert (set);
    set->N = N;
    hale_set_init(set);
    return set;
}

void
hale_set_destroy(hale_set_t* set)
{
    assert (set && set->N > 0);
    free(set);
}


size_t
hale_set_sizeof(pid_t N)
{
    return sizeof(hale_set_entry_t)*N;
}

void
hale_set_init(hale_set_t* set)
{
    assert (set && set->N > 0);
    //pid_t i;
    bzero(set->entries, sizeof(hale_set_entry_t) * set->N);
}

pid_t
hale_set_min(hale_set_t* set)
{
    assert (set && set->N > 0);
    pid_t i;
    for (i = 0; i < set->N; ++i)
        if (set->entries[i].valid)
            return i;
    return set->N;
}

size_t
hale_set_size(hale_set_t* set)
{
    assert (set && set->N > 0);
    size_t size = 0;
    pid_t i;
    for (i = 0; i < set->N; ++i)
        if (set->entries[i].valid)
            ++size;
    return size;
}

bool_t
hale_set_in(hale_set_t* set, pid_t i)
{
    assert (set && set->N > 0);
    assert (i >= 0 && i < set->N);
    return set->entries[i].valid;
}

void hale_set_insert(hale_set_t* set, pid_t i)
{
    assert (set && set->N > 0);
    assert (i >= 0 && i < set->N);
    set->entries[i].valid = 1;
}

void hale_set_remove(hale_set_t* set, pid_t i)
{
    assert (set && set->N > 0);
    assert (i >= 0 && i < set->N);
    set->entries[i].valid = 0;
}


pid_t hale_set_cmp(hale_set_t* set1, hale_set_t* set2)
{
    assert (set1 && set1->N > 0);
    assert (set2 && set2->N > 0);
    assert (set1->N == set2->N);
    pid_t i;

    for (i = 0; i < set1->N; ++i) {
        if (set1->entries[i].valid != set2->entries[i].valid) return i;
    }
    assert (i == set1->N);
    return i;
}

void hale_set_cpy(hale_set_t* src, hale_set_t* dst)
{
    assert (src && src->N > 0);
    assert (dst && dst->N > 0);
    assert (src->N == src->N);
    memcpy(dst->entries, src->entries, src->N*sizeof(hale_set_entry_t));
}
