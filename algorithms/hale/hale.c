/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas.h>
#include <ztas/string.h>
#include <ztas/log.h>
#include <ztas/mem.h>
#include <ztas/msg.h>
#include <ztas/chain.h>
#include <ztas/algorithms/fads.h>
#include "hale.h"

#include <assert.h>



typedef struct {
    int32_t N;
    int32_t EP;
} hale_config_t;

struct hale {
    int32_t     pid;
    int32_t     N;
    bool_t      im_leader;
    ztime_t     expiration_time;
    ztime_t     locked_until;
    pid_t       locked_to_pid;
    ztime_t     locked_to_req;
    hale_set_t* support_set;
    hale_set_t* alive_set;
    hale_set_t* reply_set;
    hale_set_t* target_set;
    ztime_t*    last_msg;
    ztime_t     last_request;
    bool_t      release_ac;

    ztime_t     delta;

    pid_t       leader;
    ztime_t     last_leader;

    // configuration
    uint32_t     LOCK_TIME;
    uint32_t     EXPIRES;
    uint32_t     EP;

    fads_t*      fads;
};

#define MIN_SUPPORTERS 2
//#define SIGMA 30 # paper
#define SIGMA 30

#define RHO 0
#define DELTA_MIN 0
#define DELTA 15  //15 s (paper)
//#define DELTA 1
//#define EP        100 // paper: 50
//#define LOCK_TIME (1-RHO)*((EP-SIGMA)*(1-RHO) - DELTA + DELTA_MIN)
//#define LOCK_TIME (1-RHO)*(EP*(1-RHO) - DELTA + DELTA_MIN)
//#define EXPIRES   EP*4 // min == EP (paper: 230)

#ifndef ENCODED
//# include <hcext.h>
# define PRINT(...) {printf("[pid: %d] ", hale->pid); printf(__VA_ARGS__);}
#else
# define ZTIME_TS(A) 0
# define PRINT(...)
#endif

#define SIZEOF_ELECTION(N) sizeof(hale_election_t) + hale_set_sizeof(N)


/* prototypes */
void hale_deliver_release (hale_t* hale, int32_t src, void* data, size_t size);
void hale_deliver_reply   (hale_t* hale, int32_t src, void* data, size_t size, ztime_t* delay, ztime_t* rec_time);
void hale_deliver_election(hale_t* hale, int32_t src, void* data, size_t size, ztime_t* delay, ztime_t* rec_time);
void hale_wakeup          (hale_t* hale, int32_t aid);

void hale_purge_alive_set (hale_t* hale, ztime_t* now, ztime_t* result);
void update_alive_set     (hale_t* hale, int32_t src, bool_t fast, ztime_t* rec_time);


hale_t*
hale_init(int32_t pid, int32_t N, int32_t EP, fads_t* fads)
{
    int32_t i;
    hale_t* hale = (hale_t*) malloc(sizeof(hale_t));
    assert(hale);

    hale->pid  = pid;
    hale->fads = fads;

    // configure hale
    hale->N         = N;
    hale->EP        = EP; // in ms
    hale->LOCK_TIME = (1-RHO)*((hale->EP-SIGMA)*(1-RHO) - DELTA + DELTA_MIN);
    hale->EXPIRES   = hale->EP*4 + 30;


    /* print some info */
    LOG1("ALGO: hale %c\n", ' ');
    LOG1("ALGO: pid    = %d \n", hale->pid);
    LOG1("ALGO: N      = %d \n", hale->N);
    LOG1("ALGO: SIGMA   = %d ms\n", SIGMA);
    LOG1("ALGO: DELTA   = %d ms\n", DELTA);
    LOG1("ALGO: EP      = %d ms\n", hale->EP);
    LOG1("ALGO: EXPIRES = %d ms\n", hale->EXPIRES);
    LOG1("ALGO: LOCKTIM = %d ms\n", hale->LOCK_TIME);

    hale->im_leader       = False;
    ZTIME_ZERO(hale->expiration_time);
    ZTIME_ZERO(hale->locked_until);
    ZTIME_ZERO(hale->locked_to_req);
    hale->locked_to_pid   = hale->N;

    hale->support_set     = hale_set_create(hale->N);
    hale->alive_set       = hale_set_create(hale->N);
    hale->reply_set       = hale_set_create(hale->N);
    hale->target_set      = hale_set_create(hale->N);
    hale->last_msg        = (ztime_t*) malloc(sizeof(ztime_t) * hale->N);
    for (i = 0; i < hale->N; ++i)
        ZTIME_ZERO(hale->last_msg[i]);
    ZTIME_ZERO(hale->last_request);
    hale->release_ac      = False;

    /* initialize stats support */
    hale->leader          = hale->N;
    ztas_clock(&hale->last_leader); // mark current time as last

    /* initialize some constants */
    ztime_t delta;
    ZTIME_ZERO(delta);
    ZTIME_ADD_MS(delta, DELTA, hale->delta);

    ztas_alarm(HALE_ALIVE_AC, 0);

    return hale;
}

void
hale_fini(hale_t* hale)
{
    if (hale) {
        free(hale->support_set);
        free(hale->alive_set);
        free(hale->reply_set);
        free(hale->target_set);
        free(hale->last_msg);
        free(hale);
    }
}


void
fads_deliver(void* args, int32_t pid, void* data, size_t size,
             ztime_t* delay, ztime_t* now)
{

    hale_t* hale = (hale_t*) args;

    assert(size >= sizeof(hale_type_t));
    enum hale_type type = *(hale_type_t*) data;

    switch(type) {
    case ELECTION:
        hale_deliver_election(hale, pid, data, size, delay, now);
        break;
    case REPLY:
        hale_deliver_reply   (hale, pid, data, size, delay, now);
        break;
    case RELEASE:
        hale_deliver_release (hale, pid, data, size);
        break;
    default:
        assert(0);
    }
}



static void*
hale_factory(hale_t* hale, hale_type_t type)
{
    switch(type) {
    case ELECTION:
    {
        hale_election_t* msg = (hale_election_t*) malloc(SIZEOF_ELECTION(hale->N));
        msg->type = type;
        ZTIME_ZERO(msg->request);
        msg->alive.N = hale->N;
        msg->is_leader = hale->im_leader;
        return msg;
    }
    case REPLY:
    {
        hale_reply_t* msg = (hale_reply_t*) malloc(sizeof(hale_reply_t));
        msg->type = type;
        msg->support = 0;
        return msg;
    }
    case RELEASE:
    {
        hale_release_t* msg = (hale_release_t*) malloc(sizeof(hale_release_t));
        msg->type = type;
        ZTIME_ZERO(msg->request);
        return msg;
    }
    default:
        assert (0);
    }
    assert (0);
    return NULL;
}


void
hale_purge_alive_set(hale_t* hale, ztime_t* now, ztime_t* result)
{
    ztime_t no_before;
    ZTIME_SET(no_before, *now);
    int32_t i;
    for (i = 0; i < hale->N; ++i)
        if (hale_set_in(hale->alive_set, i)) {
            ztime_t plus_expires;
            ZTIME_ADD_MS(hale->last_msg[i], hale->EXPIRES, plus_expires);
            if (ZTIME_GE(*now, plus_expires)) {
                hale_set_remove(hale->alive_set, i);
            } else {
                if (hale->pid > i  && ZTIME_GT(plus_expires, no_before)) {
                    ZTIME_SET(no_before, plus_expires);
                }
            }
        }

    if (result != NULL)
        ZTIME_SET(*result, no_before);
}

void
update_alive_set(hale_t* hale, int32_t src, bool_t fast, ztime_t* rec_time)
{
    if (fast) {
        hale_set_insert(hale->alive_set, src);
        ZTIME_SET(hale->last_msg[src], *rec_time);
    }
    hale_purge_alive_set(hale, rec_time, NULL);
}


void
check_if_leader(hale_t* hale)
{
    /* PRINT("IN = %d\n", hale_set_in(hale->target_set, hale->pid)); */
    /* PRINT("CMP = %d\n", hale_set_cmp(hale->reply_set, hale->alive_set) == hale->N); */
    /* PRINT("SIZE = %d\n", hale_set_size(hale->reply_set)); */
    /* PRINT("MIN = %d\n", hale_set_min(hale->reply_set)); */

    if (hale_set_in(hale->target_set, hale->pid)
        && hale_set_cmp(hale->reply_set, hale->alive_set) == hale->N
        && hale_set_size(hale->reply_set) >= MIN_SUPPORTERS
        && hale->pid == hale_set_min(hale->reply_set)
        ) {
        hale_set_cpy(hale->reply_set, hale->support_set);

        if (!hale->im_leader) {
            ztime_t now;
            ztas_clock(&now);

            ztime_t dur;
            ZTIME_DUR(hale->last_leader, now, dur);
            ZTIME_SET(hale->last_leader, now);

            //hale->leader = hale->pid;
            LOG1("ALGO: im_leader %d ", hale->pid);
            LOG1("%u ", dur.high);
            LOG1("%u ", dur.low);
            LOG1("%u ", now.high);
            LOG1("%u\n", now.low);
        }

        hale->im_leader = True;
        ZTIME_ADD_MS(hale->last_request, hale->LOCK_TIME*(1 - 2*RHO), hale->expiration_time);

        ztime_t at;
        ZTIME_SUB_MS(hale->expiration_time, 2*DELTA*(1 + RHO) + SIGMA, at);
        ztas_sched(HALE_ALIVE_AC, &at);

        /* if(1) */
        /*     { */
        /*         int32_t i; */
        /*         LOG1("LEADER! support = [%d", hale->support_set->entries[0]); */
        /*         for (i = 1; i < hale->N; ++i) printf(", %d", hale->support_set->entries[i]); */
        /*         printf("]\n"); */
        /*     } */
    } else {
        if(hale->im_leader)
            LOG1("ALGO: not leader anymore %d\n", hale->pid);
        hale->im_leader = False;

        ztime_t at;
        ZTIME_ADD_MS(hale->last_request, hale->EP - SIGMA, at);
        ztas_sched(HALE_ALIVE_AC, &at);

        if (hale_set_min(hale->reply_set) != hale->N) { /* not empty? */
            hale_release_t* msg = (hale_release_t*) hale_factory(hale, RELEASE);
            ZTIME_SET(msg->request, hale->last_request);
            fads_bcast(hale->fads, msg, sizeof(hale_release_t));
            free(msg);
        }
    }
    //hale->last_request = 0;
}



int
hale_trig(hale_t* hale, int32_t aid)
{
    assert (hale);

    switch (aid) {
    case HALE_ALIVE_AC: {
        ztas_clock(&hale->last_request);
        ztime_t next_timeout;
        hale_purge_alive_set(hale, &hale->last_request, &next_timeout);

        hale_set_init(hale->reply_set);
        hale_set_cpy(hale->alive_set, hale->target_set);

        int32_t min = hale_set_min(hale->alive_set);
        if (min == hale->N || hale->pid <= min) {
            hale_election_t* msg = (hale_election_t*) hale_factory(hale, ELECTION);
            ZTIME_SET(msg->request, hale->last_request);

            hale_set_cpy(hale->alive_set, &msg->alive);
            fads_bcast(hale->fads, msg, SIZEOF_ELECTION(hale->N));
            free(msg);

            hale->release_ac = True; // enable

            ztime_t at;
            ZTIME_ADD_MS(hale->last_request, 2*DELTA*(1+RHO), at);
            ztas_sched(HALE_RELEASE_AC, &at);
        } else {
            ztas_sched(HALE_ALIVE_AC, &next_timeout);
        }
        break;
    }
    case HALE_RELEASE_AC: {
        if (hale->release_ac) check_if_leader(hale);
        break;
    }
    default:
        return 0;
    }
    return 1;
}


void
hale_deliver_election(hale_t* hale, int32_t src, void* data, size_t size,
                      ztime_t* delay, ztime_t* rec_time)
{
    assert (hale);

//    assert(size == SIZEOF_ELECTION(hale->N));

    bool_t fast = !ZTIME_GT(*delay, hale->delta);
    update_alive_set(hale, src, fast, rec_time);

    hale_election_t* msg = (hale_election_t*) data;

    ztime_t now;
    ztas_clock(&now);

    bool_t support =
        (ZTIME_GE(now, hale->locked_until) || src == hale->locked_to_pid)
        && src == hale_set_min(hale->alive_set)
        && src <= hale->pid
        && fast;

    if (support) {
        hale->locked_to_pid = src;
        ZTIME_SET(hale->locked_to_req, msg->request);
        ZTIME_ADD_MS(*rec_time, hale->LOCK_TIME, hale->locked_until);

        if (msg->is_leader) {
            if (src != hale->leader) {
                ztime_t dur;
                ZTIME_DUR(hale->last_leader, now, dur);
                ZTIME_SET(hale->last_leader, now);

                hale->leader = src;
                LOG1("ALGO: leader %d ", src);
                LOG1("%u ", dur.high);
                LOG1("%u ", dur.low);
                LOG1("%u ", now.high);
                LOG1("%u\n", now.low);
            }
        } else {
            hale->leader = hale->N;
        }
        //PRINT("supporting %d  until %lu \n", src, ZTIME_TS(hale->locked_until));
    } else {
        //LOG1("NO SUPPORT for %d\n", src);
    }

    if (fast) {
        if (src != hale->pid || hale_set_size(&msg->alive) <= 1) {
            hale_reply_t* reply = (hale_reply_t*) hale_factory(hale, REPLY);
            ZTIME_SET(reply->request, msg->request);
            reply->support = support;
            fads_ucast(hale->fads, src, reply, sizeof(hale_reply_t));
            free(reply);
        } else if (support && ZTIME_EQ(msg->request, hale->last_request)) {
            hale_set_insert(hale->reply_set, hale->pid);
        }
    }
}

void
hale_deliver_reply(hale_t* hale, int32_t src, void* data, size_t size,
                   ztime_t* delay, ztime_t* rec_time)
{
    assert (hale);

    assert(size == sizeof(hale_reply_t));
    hale_reply_t* msg = (hale_reply_t*) data;

    bool_t fast = !ZTIME_GT(*delay, hale->delta);
    update_alive_set(hale, src, fast, rec_time);

    if (fast && ZTIME_EQ(msg->request, hale->last_request) && msg->support) {
        hale_set_insert(hale->reply_set, src);
        ztime_t now;
        ztas_clock(&now);
        if (hale_set_cmp(hale->target_set, hale->reply_set) == hale->N /* are the same ?*/
            && (ZTIME_GT(hale->expiration_time, now) && hale->im_leader) /* leader? */
            ) {
            check_if_leader(hale);
            hale->release_ac = False; // cancel
        }
    }
}

void
hale_deliver_release(hale_t* hale, int32_t src, void* data, size_t size)
{
    assert (hale);

    assert(size == sizeof(hale_release_t));
    hale_release_t* msg = (hale_release_t*) data;

    if (hale->locked_to_pid == src
        && ZTIME_EQ(hale->locked_to_req,msg->request)) {
        ZTIME_ZERO(hale->locked_until);
    }
}


/////////// GLUE CODE ////////////////////

#define TERM_ALARM 5000

typedef struct {
    uint32_t timeout;
    hale_t* hale;
    fads_t* fads;
} state_t;

/** glue_init initializes all the */
void*
ztas_init(int32_t pid)
{
    state_t* state = (state_t*) malloc(sizeof(state_t));
    assert(state);
    bzero(state, sizeof(state_t));

    int32_t N = atoi(ztas_cread("hale:N"));
    int32_t EP = atoi(ztas_cread("hale:EP"));
    state->fads = fads_init(pid, N);
    state->hale = hale_init(pid, N, EP, state->fads);
    fads_set_args(state->fads, state->hale);
    return (void*) state;
}


void
ztas_fini(void* state)
{
    assert (state);
    state_t* self = (state_t*) state;

    fads_fini(self->fads);
    hale_fini(self->hale);
    free(self);
}

void
ztas_recv(void* state, const void* data, size_t size)
{
    assert (state);
    state_t* self = (state_t*) state;

    CHAIN(
        || CHAIN_HANDLER(fads_recv(self->fads, data, size))
        );
}


void
ztas_trig(void* state, int32_t aid)
{
    assert (state != NULL);
    state_t* self = (state_t*) state;
    if (aid == TERM_ALARM) ztas_term();

    CHAIN(
        || CHAIN_HANDLER(fads_trig(self->fads, aid))
        || CHAIN_HANDLER(hale_trig(self->hale, aid))
        );
}


