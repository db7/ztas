/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _HALE_H_
#define _HALE_H_
#include <ztas/types.h>
#include "hale_set.h"

#define HALE_SELF 1

#define HALE_RELEASE_AC 1002
#define HALE_ALIVE_AC   1003
#define HALE_LEAD_AC    1004

enum hale_type { ELECTION = 0 , REPLY = 1, RELEASE = 2};
typedef enum hale_type hale_type_t;


typedef struct hale_election hale_election_t;
struct hale_election {
    hale_type_t type;
    ztime_t request;
    bool_t is_leader;
    hale_set_t alive; // variable size, should be last var
};

typedef struct hale_reply hale_reply_t;
struct hale_reply {
    hale_type_t type;
    ztime_t request;
    bool_t support;
};

typedef struct hale_release hale_release_t;
struct hale_release {
    hale_type_t type;
    ztime_t request;
};

typedef struct hale hale_t;

#endif /* _HALE_H_ */
