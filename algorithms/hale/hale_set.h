/* ----------------------------------------------------------------------
 * Copyright (c) 2011-2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _HALE_SET_H_
#define _HALE_SET_H_

#include <ztas/types.h>
#include <ztas/mem.h>
typedef int32_t pid_t;

typedef struct hale_set_entry hale_set_entry_t;
struct hale_set_entry {
    bool_t valid;
};

typedef struct hale_set hale_set_t;
struct hale_set {
    pid_t N;
    hale_set_entry_t entries[0];
};


hale_set_t* hale_set_create (pid_t N);
void        hale_set_destroy(hale_set_t*);
void        hale_set_init   (hale_set_t*);
size_t      hale_set_sizeof (pid_t);
size_t      hale_set_size   (hale_set_t*);
bool_t      hale_set_in     (hale_set_t*, pid_t);
pid_t       hale_set_min    (hale_set_t*);
pid_t       hale_set_cmp    (hale_set_t*, hale_set_t*);
void        hale_set_cpy    (hale_set_t*, hale_set_t*);
void        hale_set_insert (hale_set_t*, pid_t);
void        hale_set_remove (hale_set_t*, pid_t);


#endif /* _HALE_SET_H_ */
